package com.gennext.sports.gplus;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;

import com.gennext.sports.ProfileActivity;
import com.gennext.sports.SignUpGoogle;
import com.gennext.sports.util.AppTokens;
import com.gennext.sports.util.Buddy;
import com.gennext.sports.util.L;
import com.gennext.sports.util.Utility;
import com.google.android.gms.auth.GoogleAuthUtil;

import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

public abstract class AbstractGetNameNameTask extends AsyncTask<Void, Void, Void>{

	protected SignUpGoogle mActivity;
	public static String GOOGLE_USER_DATE="no data";
	protected String mScope;
	protected String mEmail;
	protected int mRequest;
	
	public AbstractGetNameNameTask(SignUpGoogle mActivity, String mEmail, String mScope) {
		super();
		this.mActivity = mActivity;
		this.mScope = mScope;
		this.mEmail = mEmail;
	}




	@Override
	protected Void doInBackground(Void... arg0) {
		// TODO Auto-generated method stub
		try{
			fetchNameFromeProfileServer();
			
		}catch(IOException ex){
			onError("Following error occored"+ ex.getMessage(),ex);
		}catch (JSONException ex) {
			onError("Bad response"+ ex.getMessage(),ex);
		}
		return null;
	}

	protected void onError(String msg,Exception e) {
		if(e!=null){
			Log.e("", "Exception: ",e);
		}
		
	}
	protected abstract String fetchToken() throws IOException;
	
	private void fetchNameFromeProfileServer() throws IOException,JSONException{
		String token =fetchToken();
		
		URL url =new URL("https://www.googleapis.com/oauth2" +
				"/v1/userinfo?access_token="+token);
		
		HttpURLConnection con =(HttpURLConnection)url.openConnection();
		int sc=con.getResponseCode();
		if(sc==200){
			InputStream is=con.getInputStream();
			GOOGLE_USER_DATE=readResponse(is);
			is.close();
			L.m(mEmail);
			
			Utility.SavePref(mActivity,Buddy.gmainId, mEmail);
			Utility.SavePref(mActivity,AppTokens.SessionSignup, "success");
			Utility.SavePref(mActivity,AppTokens.LoginStatus, "g");
			Intent intent=new Intent(mActivity,ProfileActivity.class);
			intent.putExtra("from", "google");
			mActivity.startActivity(intent);
			mActivity.finish();
			return;
		}else if(sc==401){
			GoogleAuthUtil.invalidateToken(mActivity, token);
			onError("Server auth error",null);
		}else{
			onError("Return by server"+ sc,null);
		}
		return;
	}

	private static String readResponse(InputStream is) throws IOException{
		ByteArrayOutputStream bos=new ByteArrayOutputStream();
		byte[] data=new byte[2048];
		int len=0;
		while((len=is.read(data,0,data.length))>=0){
			bos.write(data,0,len);
		}
		return new String(bos.toByteArray(),"UTF-8");
	}





	
}
