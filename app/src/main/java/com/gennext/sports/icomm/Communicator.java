package com.gennext.sports.icomm;

public interface Communicator {
	public void respond(int position);
}
