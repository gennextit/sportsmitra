package com.gennext.sports;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.sports.model.ChatSbuddyAdapter;
import com.gennext.sports.model.ChatSbuddyModel;
import com.gennext.sports.util.AppSettings;
import com.gennext.sports.util.AppTokens;
import com.gennext.sports.util.AsyncRequest;
import com.gennext.sports.util.Buddy;
import com.gennext.sports.util.HttpReq;
import com.gennext.sports.util.L;
import com.gennext.sports.util.Utility;
import com.gennext.sports.util.Validation;
import com.gennext.sports.util.WakeLocker;
import com.gennext.sports.util.internet.BasicNameValuePair;
import com.google.android.gcm.GCMRegistrar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ChatActivity extends BaseActivity implements  AsyncRequest.OnTaskComplete {
	FragmentManager mannager;
	//public static int position = 1;
	ListView lvMain;
	EditText etMessage;
	LinearLayout llSend;
	ImageView ivProfile;
	// LoadMessageList loadMessageList;
	SendMessage sendMessage;
	ArrayList<ChatSbuddyModel> myChatList;
	ChatSbuddyAdapter adapter;
	Activity act;
	View footerView, footerView1, footerView2, footerView3, footerView4;
	String ErrorMessage = "not available", playerId;
	public static String sltBuddyId="", sltBuddyName, sltBuddyImage;
	String visitFrom;
	Boolean addFooterViewStatus = false;
	String[] container = new String[1000];
	int countContainerLastPos = 0;
	boolean footerStatus = true;
	AsyncRequest asyncRequest;
	ProgressBar progressBar;
	public static Boolean isSoftKeyboardDisplayedStatus = false;
	BlockUnblockBuddyTask blockUnblockBuddyTask;
	ImageView ivChat;
	boolean textChangeStatus = false;

	public static final int BLOCK=1,UNBLOCK=2;
	private int SWITCH=0;
	TextView tvBlockUnblock;
	String blockStatus="";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chat);
		initUi();
		registerReceiver(mHandleMessageReceiver, new IntentFilter(AppTokens.DISPLAY_MESSAGE_ACTION));
		// Getting status
		mannager = getSupportFragmentManager();
		hideActionBar();

//		setScreenDynamically(position);
		/*if (position == 2) {*/
			LoadChatData();
		/*}*/
		playerId = LoadPref(Buddy.PlayerId);

		// if (!GCMIntentService.notificationMessage.equals("")) {
		// addNewReviewInList(sltBuddyId, GCMIntentService.notificationMessage,
		// getDate(), getTime24(), "r");
		GCMIntentService.notificationMessage = "";
		// }

	}

	private void LoadChatData() {
		L.m("playerId :: " + playerId);
		L.m("sBuddyId :: " + sltBuddyId);

		List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
		params.add(new BasicNameValuePair("receiverId", LoadPref(Buddy.PlayerId)));
		params.add(new BasicNameValuePair("senderId", sltBuddyId));

		asyncRequest = new AsyncRequest(ChatActivity.this, progressBar, null, AsyncRequest.GET, params);
		asyncRequest.execute(AppSettings.getChatMessage);
	}

	private void initUi() {
		//GCMIntentService.notificationStatus = false;

		setChatActionBarOption();

		lvMain = (ListView) findViewById(R.id.lv_chat_buddy_text_chat);
		etMessage = (EditText) findViewById(R.id.et_chat_buddy_text_chat_message);
		ivChat = (ImageView) findViewById(R.id.imageView1);
		llSend = (LinearLayout) findViewById(R.id.ll_chat_buddy_text_chat_send);
		progressBar = (ProgressBar) findViewById(R.id.progressBar1);

		Utility.setTypsFace(this, etMessage);
		etMessage.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub
				isSoftKeyboardDisplayedStatus = true;
				return false;
			}
		});
		etMessage.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (s.length() != 0) {
					ivChat.setImageResource(R.drawable.chat_send);
					textChangeStatus = true;
				} else {
					ivChat.setImageResource(R.drawable.chat_send_inactive);
					textChangeStatus = false;
				}
			}
		});
		llSend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (textChangeStatus) {
					if (checkValidation()) {
						setMessageInContainer(etMessage.getText().toString());
						assignTask(etMessage.getText().toString());
						etMessage.setText("");
						textChangeStatus = false;
						ivChat.setImageResource(R.drawable.chat_send_inactive);
					}
				}
			}
		});

		myChatList = new ArrayList<ChatSbuddyModel>();
		adapter = new ChatSbuddyAdapter(this, R.layout.custom_slot_find_coach_message_left, myChatList);
		lvMain.setAdapter(adapter);

	}

	private boolean checkValidation() {
		boolean ret = true;

		if (!Validation.isEmpty(etMessage, true)) {
			ret = false;
		}
		return ret;
	}

	public void addNewReviewInList(String name, String message, String dateOfMsg, String timeOfMsg, String status) {
		ChatSbuddyModel model = new ChatSbuddyModel();
		model.setName(name);
		model.setMessage(message);
		model.setDateOfMsg(dateOfMsg);
		model.setTimeOfMsg(timeOfMsg);
		model.setStatus(status);
		model.setImage(sltBuddyImage);
		myChatList.add(model);
		if (adapter != null) {
			adapter.notifyDataSetChanged();
		}
	}

	private void setMessageInContainer(String message) {
		for (int i = 0; i < container.length; i++) {
			if (container[i] == null) {
				L.m("container[" + i + "] = " + message);
				container[i] = message;
				break;
			}
		}
	}

	private String getAllMessageInContainer(int position) {
		String res = null;
		for (int i = position; i < container.length; i++) {
			if (container[i] != null) {
				countContainerLastPos++;
				if (res != null) {
					res += "\n" + container[i];
				} else {
					res = container[i];
				}
			} else {
				break;
			}
		}

		return res;
	}

	private void assignTask(String message) {
		addFooterView(message);
		String allMessage = getAllMessageInContainer(countContainerLastPos);
		// if(allMessage!=null){
		// if(sendMessage!=null){
		sendMessage = new SendMessage(ChatActivity.this, countContainerLastPos, allMessage);
		sendMessage.execute(AppSettings.setChatMessage);
		// }
		// }

	}

	public void addFooterView(String text) {
		TextView tvMessage, tvName, tvDate, tvTime;
		if (footerView != null) {
			if (footerView1 == null) {
				footerView1 = footerView;
				L.m("assignFooterView1");
			}
			if (footerView2 == null) {
				footerView2 = footerView;
				L.m("assignFooterView2");
			}
			if (footerView3 == null) {
				footerView3 = footerView;
				L.m("assignFooterView3");
			}
			if (footerView4 == null) {
				footerView4 = footerView;
				L.m("assignFooterView4");
			}

		}
		LayoutInflater inflater = getLayoutInflater();
		footerView = inflater.inflate(R.layout.chat_custom_slot_footer, null, false);

		tvMessage = (TextView) footerView.findViewById(R.id.tv_chat_footer_text);
		ivProfile = (ImageView) footerView.findViewById(R.id.iv_chat_footer_profile);
		tvDate = (TextView) footerView.findViewById(R.id.tv_chat_footer_date);
		tvTime = (TextView) footerView.findViewById(R.id.tv_chat_footer_time);
		tvMessage.setText(text);
		// tvName.setText(LoadPref(Buddy.Name));
		tvDate.setText(getDate());
		tvTime.setText(viewTime());
		Buddy.setBuddyProfileImage(ChatActivity.this, ivProfile);

		lvMain.addFooterView(footerView);
		lvMain.setAdapter(adapter);
		footerStatus = false;

	}

	public void removeFooterView() {
		if (footerView4 != null) {
			lvMain.removeFooterView(footerView4);
			footerView4 = null;
			L.m("removeFooterView4");
		}
		if (footerView3 != null) {
			lvMain.removeFooterView(footerView3);
			footerView3 = null;
			L.m("removeFooterView3");
		}
		if (footerView2 != null) {
			lvMain.removeFooterView(footerView2);
			footerView2 = null;
			L.m("removeFooterView2");
		}
		if (footerView1 != null) {
			lvMain.removeFooterView(footerView1);
			footerView1 = null;
			L.m("removeFooterView1");
		}
		if (footerView != null) {
			lvMain.removeFooterView(footerView);
			footerView = null;
			L.m("removeFooterView");
		}

	}

	public void setChatActionBarOption() {
		LinearLayout ActionBack;
		ActionBack = (LinearLayout) findViewById(R.id.ll_actionbar_back);
		LinearLayout ActionBlock = (LinearLayout) findViewById(R.id.ll_actionbar_block);
		tvBlockUnblock = (TextView) findViewById(R.id.tv_actionbar_blockUnblock);
		TextView actionbarTitle = (TextView) findViewById(R.id.actionbar_title);
		actionbarTitle.setText(Utility.setBoldFont(this, R.color.white, Typeface.BOLD, "CHAT"));
		ActionBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// mannager.popBackStack("chatBuddyzTextChat",
				// FragmentManager.POP_BACK_STACK_INCLUSIVE);

				if (isSoftKeyboardDisplayedStatus) {
					hideKeybord(ChatActivity.this);
					finish();
				} else {
					finish();
				}

			}
		});
		ActionBlock.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// Toast.makeText(ChatActivity.this, "update later",
				// Toast.LENGTH_SHORT).show();
				switch (SWITCH){
					case BLOCK:
						showMessageAlert(ChatActivity.this);
						break;
					case UNBLOCK:
						blockUnblockBuddyTask = new BlockUnblockBuddyTask(ChatActivity.this, null);
						blockUnblockBuddyTask.execute(AppSettings.unblockPlayer);
						break;
				}
			}
		});
	}

	private class SendMessage extends AsyncTask<String, Void, String> {

		private Activity activity;
		String message;
		int lastCounter;

		public SendMessage(Activity activity, int countContainerLastPos, String message) {
			this.message = message;
			this.lastCounter = countContainerLastPos;
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// showPDialog(getActivity(), "Processing please wait");

		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error";
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				// L.m("Player Id : "+LoadPref(AppTokens.PlayerId));
				params.add(new BasicNameValuePair("senderId", LoadPref(Buddy.PlayerId)));
				params.add(new BasicNameValuePair("receiverId", sltBuddyId));
				params.add(new BasicNameValuePair("message", message));

			}

			HttpReq ob = new HttpReq();
			response = ob.makeConnection(urls[0], 3, params);
			String output = null;
			L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray json = new JSONArray(response);
					for (int i = 0; i < json.length(); i++) {
						JSONObject obj = json.getJSONObject(i);
						if (obj.optString("status").equals("success")) {
							output = "success";

						} else if (obj.optString("status").equals("failure")) {
							output = obj.optString("message");
						}
					}

				} catch (JSONException e) {
					L.m("Json Error :" + e.toString());
					ErrorMessage = "Json Error :" + e.toString() + response;
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				ErrorMessage = response;
				return null;
			}

			return output;
			// return "success";

		}

		@Override
		protected void onPostExecute(String result) {

			if (activity != null) {
				// dismissPDialog();
				removeFooterView();
				if (result != null) {
					L.m(result);
					if (result.equalsIgnoreCase("success")) {
						// Toast.makeText(ChatActivity.this, "Message Sent
						// successfull", Toast.LENGTH_SHORT).show();
						addNewReviewInList(LoadPref(Buddy.Name), message, getDate(), getTime24(), "s");

						String allMessage = getAllMessageInContainer(countContainerLastPos);
						if (allMessage != null) {
							// if(sendMessage!=null){
							sendMessage = new SendMessage(ChatActivity.this, countContainerLastPos, allMessage);
							sendMessage.execute(AppSettings.setChatMessage);
							// }
						} else {
							removeFooterView();
						}
					} else {
						Toast.makeText(ChatActivity.this, result, Toast.LENGTH_SHORT).show();
					}
				} else {
					showServerErrorAlertBox(ErrorMessage, etMessage.getText().toString());
				}
			}
		}
	}

	public void showServerErrorAlertBox(String errorDetail, String message) {
		showAlertBox(getSt(R.string.server_time_out_tag), message, getSt(R.string.server_time_out_msg), 2, errorDetail);
	}

	public void showInternetAlertBox(String messgae) {
		showAlertBox(getSt(R.string.internet_error_tag), messgae, getSt(R.string.internet_error_msg), 2, null);
	}

	public void showAlertBox(String title, final String message, String Description, int noOfButtons,
			final String errorMessage) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ChatActivity.this);
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = getLayoutInflater();

		View v = inflater.inflate(R.layout.alert_dialog, null);
		dialogBuilder.setView(v);
		ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
		Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
		Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
		final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
		LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
		LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);
		ivAbout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (errorMessage != null) {
					tvDescription.setText(errorMessage);
				}
			}
		});

		tvTitle.setText(title);
		tvDescription.setText(Description);
		if (noOfButtons == 1) {
			button2.setVisibility(View.GONE);
			llBtn2.setVisibility(View.GONE);
		}
		button1.setText("Retry");
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				if (isOnline()) {
					// sendMessage = new SendMessage(ChatActivity.this,
					// message);
					// sendMessage.execute(AppSettings.setChatMessage);
				} else {
					// showInternetAlertBox(ChatActivity.this);
				}
			}
		});
		button2.setText("Cancel");
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}

//	private void setScreenDynamically(int position) {
//		setScreenDynamically(position, null, null, null);
//	}

//	private void setScreenDynamically(int position, String sltBuddyId, String sltBuddyName, String sltBuddyImage) {
//		FragmentTransaction transaction;
//		switch (position) {
//
//		case 1:
//			ChatBuddyzList chatBuddyzList = new ChatBuddyzList();
//			chatBuddyzList.setCommunicator(ChatActivity.this);
//			transaction = mannager.beginTransaction();
//			transaction.replace(android.R.id.content, chatBuddyzList, "chatBuddyzList");
//			transaction.addToBackStack("chatBuddyzList");
//			transaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//			transaction.commit();
//			break;
//		case 2:
//			// ChatBuddyzTextChat chatBuddyzTextChat = new ChatBuddyzTextChat();
//			// //chatBuddyzTextChat.setCommunicator(ChatActivity.this);
//			// chatBuddyzTextChat.setData(sltBuddyId,sltBuddyName,sltBuddyImage);
//			// transaction = mannager.beginTransaction();
//			// transaction.replace(R.id.group_chat, chatBuddyzTextChat,
//			// "chatBuddyzTextChat");
//			// transaction.addToBackStack("chatBuddyzTextChat");
//			// transaction.setTransitionStyle(FragmentTransaction.TRANSIT_ENTER_MASK);
//			// transaction.commit();
//			break;
//		case 3:
//			MainFindSbuddyDetail mainFindSbuddyDetail = (MainFindSbuddyDetail) mannager
//					.findFragmentByTag("mainFindSbuddyDetail");
//			if (mainFindSbuddyDetail != null) {
//				transaction = mannager.beginTransaction();
//				transaction.attach(mainFindSbuddyDetail);
//				transaction.commit();
//				break;
//			}
//		}
//	}

	/**
	 * Receiving push messages
	 */
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			int result = intent.getExtras().getInt(AppTokens.EXTRA_RESULT);
			if (result == AppTokens.CHAT) {
				String newMessage = intent.getExtras().getString(AppTokens.EXTRA_MESSAGE);
				String senderId = intent.getExtras().getString(AppTokens.EXTRA_MESSAGE_SID);
				String senderName = intent.getExtras().getString(AppTokens.EXTRA_MESSAGE_SNAME);
				// Waking up mobile if it is sleeping
				WakeLocker.acquire(getApplicationContext());

				/**
				 * Take appropriate action on this message depending upon your
				 * app requirement For now i am just displaying it on the screen
				 */

				// Showing received message
				// Toast.makeText(getApplicationContext(), "New Message: " +
				// newMessage, Toast.LENGTH_LONG).show();
				if(senderId.equalsIgnoreCase(sltBuddyId)){
					addNewReviewInList(sltBuddyId, newMessage, getDate(), getTime24(), "r");
				}else{
					String message= senderName + " sent a message : " + newMessage;
					GCMIntentService.generateNotification(ChatActivity.this, message, GCMIntentService.CHAT_NOTIFICATION);
				}
				// Releasing wake lock
			}

			WakeLocker.release();
		}
	};

//	@Override
//	public void onChatBuddyzListListener(String sltBuddyId, String sltBuddyName, String sltBuddyImage) {
//		setScreenDynamically(2, sltBuddyId, sltBuddyName, sltBuddyImage);
//		this.sltBuddyId = sltBuddyId;
//		this.sltBuddyName = sltBuddyName;
//		this.sltBuddyImage = sltBuddyImage;
//
//		MainFindSbuddyDetail mainFindSbuddyDetail = (MainFindSbuddyDetail) mannager
//				.findFragmentByTag("mainFindSbuddyDetail");
//		if (mainFindSbuddyDetail != null) {
//			FragmentTransaction transaction = mannager.beginTransaction();
//			transaction.detach(mainFindSbuddyDetail);
//			transaction.commit();
//		}
//		ChatBuddyzList chatBuddyzList = (ChatBuddyzList) mannager.findFragmentByTag("chatBuddyzList");
//
//		if (chatBuddyzList != null) {
//			FragmentTransaction transaction = mannager.beginTransaction();
//			transaction.detach(chatBuddyzList);
//			transaction.commit();
//		}
//
//		LoadChatData();
//	}

	@Override
	public void onDestroy() {
		if (asyncRequest != null) {
			asyncRequest.onDetach();

		}
		if (sendMessage != null) {
			sendMessage.onDetach();
		}
		if (blockUnblockBuddyTask != null) {
			blockUnblockBuddyTask.onDetach();
		}
		try {
			unregisterReceiver(mHandleMessageReceiver);
			GCMRegistrar.onDestroy(this);
			GCMIntentService.notificationStatus = true;
		} catch (Exception e) {
			L.m("UnRegister Receiver Error"+ "> " + e.getMessage());
		}
		ChatActivity.sltBuddyId="";

		super.onDestroy();
	}

	@Override
	public void asyncResponseServer(String result) {
		// L.m(result);
		// new LoadMessageList().execute(result);
		if (LoadMessageList(result)) {
			if (adapter != null) {
				if(blockStatus.equalsIgnoreCase("NO")){
					SWITCH=BLOCK;
					tvBlockUnblock.setText("Block");
				}else if(blockStatus.equalsIgnoreCase("YES")){
					SWITCH=UNBLOCK;
					tvBlockUnblock.setText("UnBlock");
				}
				adapter.notifyDataSetChanged();
			}
		} else {
			showServerErrorAlertBox(ErrorMessage, "alert");
		}
	}

	private Boolean LoadMessageList(String result) {
		String response = result;
		// showServerErrorAlertBox(response,"alert");
		if (response.contains("[")) {
			try {
				JSONArray resArray = new JSONArray(response);
				JSONObject status = resArray.getJSONObject(0);
				if (status.optString("status").equals("success")) {
					JSONObject message = status.getJSONObject("message");
					sltBuddyImage = message.optString("imageUrl");
					blockStatus=message.optString("blocked");
					JSONArray json = message.getJSONArray("chat");
					for (int i = 0; i < json.length(); i++) {
						JSONObject obj = json.getJSONObject(i);
						if (obj.optString("senderId").equals(playerId)
								&& obj.optString("receiverId").equals(sltBuddyId)) {
							ChatSbuddyModel model = new ChatSbuddyModel();
							model.setName(playerId);
							model.setMessage(obj.optString("message"));
							model.setDateOfMsg(obj.optString("dateOfMsg"));
							model.setTimeOfMsg(obj.optString("timeOfMsg"));
							model.setStatus("s");
							myChatList.add(model);
						} else if (obj.optString("senderId").equals(sltBuddyId)
								&& obj.optString("receiverId").equals(playerId)) {
							ChatSbuddyModel model = new ChatSbuddyModel();
							model.setName(sltBuddyId);
							model.setMessage(obj.optString("message"));
							model.setDateOfMsg(obj.optString("dateOfMsg"));
							model.setTimeOfMsg(obj.optString("timeOfMsg"));
							model.setStatus("r");
							model.setImage(sltBuddyImage);
							myChatList.add(model);
						}

					}
				} else if (status.optString("status").equals("failure")) {
					ErrorMessage = status.optString("message");
					return false;
				}

			} catch (JSONException e) {
				L.m("Json Error :" + e.toString());
				ErrorMessage = "Json Error :" + e.toString() + response;
				return false;
			}
		} else {
			L.m("Invalid JSON found : " + response);
			ErrorMessage = response;
			return false;
		}
		return true;

	}

	// public class LoadMessageList extends AsyncTask<String, Void,
	// ArrayList<ChatSbuddyModel>>{
	//
	// @Override
	// protected ArrayList<ChatSbuddyModel> doInBackground(String... arg0) {
	//
	// String response=arg0[0];
	// if (response.contains("[")) {
	// try {
	// JSONArray json = new JSONArray(response);
	// for (int i = 0; i < json.length(); i++) {
	// JSONObject obj = json.getJSONObject(i);
	// if (obj.optString("senderId").equals(playerId)&&
	// obj.optString("receiverId").equals(sltBuddyId)) {
	// ChatSbuddyModel model = new ChatSbuddyModel();
	// model.setName(playerId);
	// model.setMessage(obj.optString("message"));
	// model.setDateOfMsg(obj.optString("dateOfMsg"));
	// model.setTimeOfMsg(obj.optString("timeOfMsg"));
	// model.setStatus("s");
	// myChatList.add(model);
	// }else if(obj.optString("senderId").equals(sltBuddyId)&&
	// obj.optString("receiverId").equals(playerId)) {
	// ChatSbuddyModel model = new ChatSbuddyModel();
	// model.setName(sltBuddyId);
	// model.setMessage(obj.optString("message"));
	// model.setDateOfMsg(obj.optString("dateOfMsg"));
	// model.setTimeOfMsg(obj.optString("timeOfMsg"));
	// model.setStatus("r");
	// myChatList.add(model);
	// }
	//
	//
	// }
	//
	// } catch (JSONException e) {
	// L.m("Json Error :" + e.toString());
	// ErrorMessage = "Json Error :" + e.toString() + response;
	// return null;
	// }
	// } else {
	// L.m("Invalid JSON found : " + response);
	// ErrorMessage = response;
	// return null;
	// }
	// return myChatList;
	// }
	// @Override
	// protected void onPostExecute(ArrayList<ChatSbuddyModel> result) {
	// // TODO Auto-generated method stub
	// super.onPostExecute(result);
	// if (adapter != null) {
	// adapter.notifyDataSetChanged();
	// }
	// }
	// }

	public void showMessageAlert(Activity Act) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Act);

		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = Act.getLayoutInflater();

		View v = inflater.inflate(R.layout.custom_chat_block, null);

		dialogBuilder.setView(v);
		LinearLayout confirm = (LinearLayout) v.findViewById(R.id.ll_chat_blocking_confirm);
		LinearLayout cancel = (LinearLayout) v.findViewById(R.id.ll_chat_blocking_cancel);
		final EditText etReason = (EditText) v.findViewById(R.id.et_chat_blocking_reason);

		confirm.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				blockUnblockBuddyTask = new BlockUnblockBuddyTask(ChatActivity.this, etReason.getText().toString());
				blockUnblockBuddyTask.execute(AppSettings.blockPlayer);
			}
		});
		cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
			}
		});

		dialog = dialogBuilder.create();
		// dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_alert_dialog);
		dialog.show();

	}

	private class BlockUnblockBuddyTask extends AsyncTask<String, Void, String> {

		private Activity activity;
		String reason;

		public BlockUnblockBuddyTask(Activity activity, String reason) {
			this.reason = reason;
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showPDialog(ChatActivity.this, "Processing please wait");
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error";
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				// L.m("Player Id : "+LoadPref(AppTokens.PlayerId));
				params.add(new BasicNameValuePair("senderId", LoadPref(Buddy.PlayerId)));
				params.add(new BasicNameValuePair("receiverId", sltBuddyId));

				if(SWITCH==BLOCK){
					params.add(new BasicNameValuePair("reason", reason));
				}
			}

			HttpReq ob = new HttpReq();
			return response = ob.makeConnection(urls[0], HttpReq.POST, params, HttpReq.EXECUTE_TASK);

		}

		@Override
		protected void onPostExecute(String result) {

			if (activity != null) {
				dismissPDialog();
				if (result != null) {
					L.m(result);
					if (result.equalsIgnoreCase("success")) {
						switch (SWITCH){
							case BLOCK:
								tvBlockUnblock.setText("UnBlock");
								SWITCH=UNBLOCK;
								Toast.makeText(ChatActivity.this, "User Blocked successfully", Toast.LENGTH_SHORT).show();
								break;
							case UNBLOCK:
								tvBlockUnblock.setText("Block");
								SWITCH=BLOCK;
								Toast.makeText(ChatActivity.this, "User UnBlocked successfully", Toast.LENGTH_SHORT).show();
								break;
						}
					} else {
						Toast.makeText(ChatActivity.this, result, Toast.LENGTH_SHORT).show();
					}
				} else {
					Toast.makeText(ChatActivity.this, getResources().getString(R.string.internet_error_msg),
							Toast.LENGTH_SHORT).show();
				}
			}
		}
	}

//	@Override
//	public void onBackPressed() {
//		// TODO Auto-generated method stub
//		if (mannager.getBackStackEntryCount() > 1) {
//			// L.m("MainActivity" + "popping backstack:
//			// "+mannager.getBackStackEntryCount());
//			int count = mannager.getBackStackEntryCount();
//			// mannager.popBackStack("mainRSSFeed", 0);
//			L.m(String.valueOf(count));
//			mannager.popBackStack();
//			if (count == 2) {
//				setScreenDynamically(3);
//			}
//		} else {
//			L.m("ChatActivity" + "nothing on backstack, calling super");
//			super.onBackPressed();
//			finish();
//		}
//	}

}
