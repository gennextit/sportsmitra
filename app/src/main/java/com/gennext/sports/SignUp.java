package com.gennext.sports;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.gennext.sports.coach.CoachRegistration;
import com.gennext.sports.fragments.AppWebView;
import com.gennext.sports.fragments.LoginCoachVenue;
import com.gennext.sports.fragments.LoginCoachVenue.OnLoginCoachVenueListener;
import com.gennext.sports.fragments.LoginOpions;
import com.gennext.sports.fragments.SignUpMobile;
import com.gennext.sports.fragments.SignUpMobile.OnVerifyMobile;
import com.gennext.sports.fragments.SignUpMobileVerify;
import com.gennext.sports.fragments.SignUpMobileVerify.OnSuccessVerifyedMobile;
import com.gennext.sports.global.PopupAlert;
import com.gennext.sports.util.AppPermission;
import com.gennext.sports.util.AppSettings;
import com.gennext.sports.util.AppTokens;
import com.gennext.sports.util.AppUser;
import com.gennext.sports.util.Buddy;
import com.gennext.sports.util.Coach;
import com.gennext.sports.util.L;
import com.gennext.sports.util.Utility;
import com.gennext.sports.util.Venue;
import com.gennext.sports.venue.VenueRegistration;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SignUp extends BaseActivity
		implements View.OnClickListener, OnVerifyMobile, OnSuccessVerifyedMobile, OnLoginCoachVenueListener {

	private static final int RC_SIGN_IN = 9001;
	private static final String TAG = "Signup";
	private RelativeLayout llMain;
	private ProgressDialog pdialog;
	private LinearLayout llAlreadyMember;
	private boolean conn = false;
	public AlertDialog dialog = null;
	private FragmentManager mannager;
	private TextView tvHeading;
	private Button btnSignUpCoach, btnSignUpVenue;
	int TAB_COACH = 1, TAB_VENUE = 2;
	private GoogleSignInClient mGoogleSignInClient;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
//		FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_signup);
		// hideActionBar();
		// setActionBarOption();
		mannager = getSupportFragmentManager();
		initUi(savedInstanceState);
		setActionBar();
		AppTokens.EDIT_PROFILE_FROM="signup";
		addFragmentWithoutBackstack(LoginOpions.newInstance(),R.id.container_signup, "loginOpions");

		GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
				.requestEmail()
				.build();
		// [END configure_signin]

		// [START build_client]
		// Build a GoogleSignInClient with the options specified by gso.
		mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
		generateHashkey();
	}

	public void generateHashkey(){
		String PACKAGE = "com.gennext.sports";
		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					PACKAGE,
					PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());

				String haskey= Base64.encodeToString(md.digest(), Base64.NO_WRAP);
				Log.d(TAG,haskey);
				showToast(haskey);
			}
		} catch (PackageManager.NameNotFoundException e) {
			Log.d(TAG, e.getMessage(), e);
		} catch (NoSuchAlgorithmException e) {
			Log.d(TAG, e.getMessage(), e);
		}
	}

	private void initUi(Bundle savedInstanceState) {
		llMain = (RelativeLayout) findViewById(R.id.ll_signup1);
		llAlreadyMember = (LinearLayout) findViewById(R.id.ll_signup_alreadyMember);
		tvHeading = (TextView) findViewById(R.id.actionbar_title);
		btnSignUpCoach = (Button) findViewById(R.id.btn_signup_coach);
		btnSignUpVenue = (Button) findViewById(R.id.btn_signup_venue);
		btnSignUpCoach.setText(Utility.setBoldFont(this, R.color.white, Typeface.NORMAL,
				getResources().getString(R.string.sign_up_btn_coach)));
		btnSignUpVenue.setText(Utility.setBoldFont(this, R.color.white, Typeface.NORMAL,
				getResources().getString(R.string.sign_up_btn_venue)));
		tvHeading.setText(Utility.setBoldFont(this, R.color.white, Typeface.BOLD, "SIGN UP"));

		llAlreadyMember.setOnClickListener(this);
		btnSignUpCoach.setOnClickListener(this);
		btnSignUpVenue.setOnClickListener(this);

	}



	@Override
	public void onVerifyMobile(String sportId) {
		// TODO Auto-generated method stub
		llMain.setVisibility(View.GONE);

		SignUpMobile signUpMobile = (SignUpMobile) mannager.findFragmentByTag("signUpMobile");
		FragmentTransaction ft = mannager.beginTransaction();
		if (signUpMobile != null) {
			ft.remove(signUpMobile);
			ft.commit();
		}

		SignUpMobileVerify signUpMobileVerify = new SignUpMobileVerify();
		signUpMobileVerify.setCommunicator(SignUp.this);
		FragmentTransaction transaction = mannager.beginTransaction();
		transaction.add(R.id.group_signup, signUpMobileVerify, "signUpMobileVerify");
		transaction.commit();
	}

	@Override
	public void onSuccessVerifyedMobile(Boolean status) {
		if (status) {
			Utility.SavePref(getApplicationContext(), AppTokens.SessionSignup, "success");
			Utility.SavePref(getApplicationContext(), AppTokens.LoginStatus, "m");
			Intent intent = new Intent(SignUp.this, ProfileActivity.class);
			intent.putExtra("from", "mobile");
			startActivity(intent);
			finish();
		}
	}

	public String getSt(int id) {

		return getResources().getString(id);
	}



	@Override
	public void onLoginCoachVenueListener(String CoachJsonData, int CoachVenueStatus) {
		if (CoachJsonData != null) {
			if (CoachVenueStatus == TAB_COACH) {
				Utility.SavePref(getApplicationContext(), AppTokens.SessionSignup, "success");
				Utility.SavePref(getApplicationContext(), AppTokens.APP_USER, "coach");
				Utility.SavePref(getApplicationContext(), Coach.CoachJsonData, CoachJsonData);
				Intent i = new Intent(SignUp.this, CoachMainActivity.class);
				startActivity(i);
				finish();
			} else {
				Utility.SavePref(getApplicationContext(), AppTokens.SessionSignup, "success");
				Utility.SavePref(getApplicationContext(), AppTokens.APP_USER, "venue");
				Utility.SavePref(getApplicationContext(), Venue.VenueJsonData, CoachJsonData);
				Intent i = new Intent(SignUp.this, VenueMainActivity.class);
				startActivity(i);
				finish();
			}
		}
	}

	@Override
	public void onClick(View v) {
		FragmentTransaction transaction;
		switch (v.getId()) {
			case R.id.ll_signup_alreadyMember:
				if(AppPermission.checkStoragePermission(SignUp.this)){LoginCoachVenue loginCoachVenue = new LoginCoachVenue();
					loginCoachVenue.setCommunicator(SignUp.this);
					transaction = mannager.beginTransaction();
					transaction.add(android.R.id.content, loginCoachVenue, "loginCoachVenue");
					transaction.addToBackStack("loginCoachVenue");
					transaction.commit();
				}
				break;
			case R.id.btn_signup_coach:
				if(AppPermission.checkStoragePermission(SignUp.this)){
					CoachRegistration coachRegistration = new CoachRegistration();
					transaction = mannager.beginTransaction();
					transaction.add(android.R.id.content, coachRegistration, "coachRegistration");
					transaction.addToBackStack("coachRegistration");
					transaction.commit();
				}
				break;
			case R.id.btn_signup_venue:
				if(AppPermission.checkStoragePermission(SignUp.this)){
					VenueRegistration venueRegistration = new VenueRegistration();
					transaction = mannager.beginTransaction();
					transaction.add(android.R.id.content, venueRegistration, "venueRegistration");
					transaction.addToBackStack("venueRegistration");
					transaction.commit();
				}
				break;
		}

	}



	@Override
	public void onBackPressed() {
		if (mannager.getBackStackEntryCount() >0) {
			mannager.popBackStack();
		} else {
			super.onBackPressed();
		}
	}

	public void OnClickSignInGoogle() {
		signIn();
	}

	public void OnClickTermsOfUse() {
		if(AppPermission.checkStoragePermission(SignUp.this)){
			AppWebView appWebView=new AppWebView();
			appWebView.setUrl(getResources().getString(R.string.t_and_c),AppSettings.USER_GUIDE_TC, AppWebView.URL);
			addFragment(appWebView,"appWebView");
			mannager=getSupportFragmentManager();
		}
	}

	public void OnClickSignInMobile() {
		if(AppPermission.checkStoragePermission(SignUp.this)){
			SignUpMobile signUpMobile = new SignUpMobile();
			signUpMobile.setCommunicator(SignUp.this);
			addFragment(signUpMobile,"signUpMobile");
		}
	}

	// google login
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		// Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
		if (requestCode == RC_SIGN_IN) {
			// The Task returned from this call is always completed, no need to attach
			// a listener.
			Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
			handleSignInResult(task);
		}
	}

	private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
		try {
			GoogleSignInAccount account = completedTask.getResult(ApiException.class);

			// Signed in successfully, show authenticated UI.
			updateUI(account);
		} catch (ApiException e) {
			// The ApiException status code indicates the detailed failure reason.
			// Please refer to the GoogleSignInStatusCodes class reference for more information.
			Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
			Log.w(TAG, "signInResult:failed code=" + e.getMessage());
			showPopupAlert("Status Code: "+e.getStatusCode()+"\n\nMessage: "+e.getMessage());
		}
	}
	// [END handleSignInResult]

	// [START signIn]
	public void signIn() {
		Intent signInIntent = mGoogleSignInClient.getSignInIntent();
		startActivityForResult(signInIntent, RC_SIGN_IN);
	}
	// [END signIn]

	// [START signOut]
	private void signOut() {
		mGoogleSignInClient.signOut()
				.addOnCompleteListener(this, new OnCompleteListener<Void>() {
					@Override
					public void onComplete(@NonNull Task<Void> task) {
						// [START_EXCLUDE]
						updateUI(null);
						// [END_EXCLUDE]
					}
				});
	}
	// [END signOut]

	// [START revokeAccess]
	private void revokeAccess() {
		mGoogleSignInClient.revokeAccess()
				.addOnCompleteListener(this, new OnCompleteListener<Void>() {
					@Override
					public void onComplete(@NonNull Task<Void> task) {
						// [START_EXCLUDE]
						updateUI(null);
						// [END_EXCLUDE]
					}
				});
	}
	// [END revokeAccess]

	private void updateUI(@Nullable GoogleSignInAccount account) {
		Context context = SignUp.this;
		if (account != null) {
			AppUser.setBuddyName(context,account.getDisplayName());
			AppUser.setBuddyImage(context,account.getPhotoUrl()!=null ? account.getPhotoUrl().toString():"");
			AppUser.setBuddyEmail(context,account.getEmail());
			Utility.SavePref(context, Buddy.socialId, account.getId());
			Utility.SavePref(context, AppTokens.LoginStatus, "g");
			Utility.SavePref(context, AppTokens.SessionSignup, "success");
			Intent intent = new Intent(context, ProfileActivity.class);
			intent.putExtra("from", "google");
			startActivity(intent);
			finish();
		}
	}


	private void showPopupAlert(String message) {
		PopupAlert.newInstance("Alert!",message, PopupAlert.POPUP_DIALOG)
				.show(getSupportFragmentManager(), "popupAlert");
	}

}
