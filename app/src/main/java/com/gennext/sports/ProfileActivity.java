package com.gennext.sports;

import java.util.ArrayList;

import com.gennext.sports.fragments.VerifyMobile;
import com.gennext.sports.fragments.profile.ProfileMyProfile;
import com.gennext.sports.fragments.profile.ProfileMySkills;
import com.gennext.sports.fragments.profile.ProfileMySkills.OnInitMainListener;
import com.gennext.sports.fragments.profile.ProfileMySports;
import com.gennext.sports.fragments.profile.ProfileMySports.OnInitSkillListener;
import com.gennext.sports.icomm.OnInitProfileListener;
import com.gennext.sports.model.MySportsModel;
import com.gennext.sports.util.AppTokens;
import com.gennext.sports.util.Buddy;
import com.gennext.sports.util.L;
import com.gennext.sports.util.Utility;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

public class ProfileActivity extends BaseActivity implements OnInitProfileListener, OnInitSkillListener, OnInitMainListener ,VerifyMobile.OnVerifyMobile{
	FragmentManager mannager;
	int position = 1;
	public static boolean EDIT_PROFILE=true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		L.m("Profile Activity :: on create");
		setContentView(R.layout.activity_profile);
		mannager = getSupportFragmentManager();
		hideActionBar();
		SavePref(AppTokens.APP_USER, "sMitra");
		setScreenDynamically(position);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		L.m("Profile Activity :: on onSaveInstanceState");
		
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		L.m("Profile Activity :: on onRestoreInstanceState");
		
	}
	
	public void onLogOutClick(View v) {

		Utility.SavePref(ProfileActivity.this,AppTokens.LoginStatus, "Log out");
		Intent intent = new Intent(ProfileActivity.this, SignUp.class);
		startActivity(intent);
		finish();
	}

	

	@Override
	public void onInitProfileListener(int position, String imageUrl, String name, String age, String gender,
			String city, String location, String clubOrApartment, String about) {
		setScreenDynamically(position, imageUrl, name, age, gender, city, location, clubOrApartment, about,null);
		Utility.SavePref(ProfileActivity.this,Buddy.City, city);
	}

	public void setScreenDynamically(int position) {
		setScreenDynamically(position, null, null, null, null, null, null, null, null,null);
	}

	public void setScreenDynamically(int position,ArrayList<MySportsModel> skillList) {
		setScreenDynamically(position, null, null, null, null, null, null, null, null,skillList);
	}

	//

	@Override
	protected void onResume() {
		super.onResume();
		startActivityToSetImage();

	}

	private void startActivityToSetImage() {


		ProfileMyProfile profileMyFrofile = (ProfileMyProfile) getSupportFragmentManager().findFragmentByTag("profileMyFrofile");
		if (profileMyFrofile != null) {
			profileMyFrofile.setCropedImageAndUri(null,ProfileMyProfile.mTempCropImageUri);

		}


	}

	private void setScreenDynamically(int position, String imageUrl, String name, String age, String gender,
									  String city, String location, String clubOrApartment, String about, ArrayList<MySportsModel> skillList) {

		switch (position) {

		case 1: 
			ProfileMyProfile profileMyFrofile = (ProfileMyProfile) mannager.findFragmentByTag("profileMyFrofile");
			if (profileMyFrofile == null) {
				profileMyFrofile = new ProfileMyProfile();
				profileMyFrofile.setCommunicator(ProfileActivity.this);
				profileMyFrofile.setData(EDIT_PROFILE);
				FragmentTransaction transaction = mannager.beginTransaction();
				transaction.add(R.id.group_profile, profileMyFrofile, "profileMyFrofile");
				transaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
				transaction.commit();
			}else{
				profileMyFrofile.setCommunicator(ProfileActivity.this);
			}
			
			break;
		case 2:
			ProfileMySports profileMySports = new ProfileMySports();
			profileMySports.setCommunicator(ProfileActivity.this);
			//profileMySports.setData(EDIT_PROFILE);
			FragmentTransaction transaction2 = mannager.beginTransaction();
			transaction2.add(R.id.group_profile, profileMySports, "profileMySports");
			transaction2.addToBackStack("replaceSports");
			transaction2.commit();
			break;
		case 3:
			ProfileMySkills profileMySkills = new ProfileMySkills();
			profileMySkills.setCommunicator(ProfileActivity.this);
			//profileMySkills.setData(EDIT_PROFILE);
			FragmentTransaction transaction3 = mannager.beginTransaction();
			transaction3.add(R.id.group_profile, profileMySkills, "profileMySkills");
			transaction3.addToBackStack("replaceSkills");
			transaction3.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			transaction3.commit();
			break;
		case 4:
			if(AppTokens.EDIT_PROFILE_FROM!=null){
				if(AppTokens.EDIT_PROFILE_FROM.equals("main")){
					finish();
				}else{
					Utility.SavePref(ProfileActivity.this,AppTokens.SessionProfile, "success");
					Intent ob = new Intent(ProfileActivity.this, MainActivity.class);
					//LoadArraytoserver 
					startActivity(ob);
					finish();
				}
			}else{
				finish();
			}
			
			
			break;
		}
	}

	@Override
	public void onInitSkillListener(int screenPosition,ArrayList<MySportsModel> skillList) {
		setScreenDynamically(screenPosition,skillList);
	}

	@Override
	public void onInitMainListener(int screenPosition, ArrayList<MySportsModel> skillList) {
		setScreenDynamically(screenPosition,skillList);
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (mannager.getBackStackEntryCount() > 1) {
	        L.m("ProfileActivity"+ "popping backstack");
	        mannager.popBackStack();
	    } else {
	        L.m("ProfileActivity"+ "nothing on backstack, calling super");
	        super.onBackPressed();  
	        //finish();
	    }

	}

	@Override
	public void onSuccessVerifyedMobile(Boolean status) {

	ProfileMyProfile profile=(ProfileMyProfile)getSupportFragmentManager().findFragmentByTag("profileMyFrofile");
		if(profile!=null){
			profile.onSaveButtonClick();
		}

	}
}
