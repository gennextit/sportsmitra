package com.gennext.sports;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.gennext.sports.fragments.MainMessage.OnMessageCounterChangeListener;
import com.gennext.sports.fragments.MainNotification;
import com.gennext.sports.fragments.MainNotification.OnNotificationCounterChangeListener;
import com.gennext.sports.fragments.MainRSSFeed;
import com.gennext.sports.fragments.coaches.MainFindCoaches;
import com.gennext.sports.fragments.coaches.MainFindCoaches.OnFindCoachesListener;
import com.gennext.sports.fragments.coaches.MainFindCoachesDetail;
import com.gennext.sports.fragments.coaches.MainFindCoachesDetailReviews.OnCoachReviewCount;
import com.gennext.sports.fragments.coaches.MainFindCoachesView;
import com.gennext.sports.fragments.sbuddy.MainFindSbuddyz;
import com.gennext.sports.fragments.sbuddy.MainFindSbuddyz.OnFindSbuddyzListener;
import com.gennext.sports.fragments.sbuddy.MainFindSbuddyzView;
import com.gennext.sports.fragments.venu.MainFindVenuDetail;
import com.gennext.sports.fragments.venu.MainFindVenuDetailReviews.OnVenueReviewCount;
import com.gennext.sports.fragments.venu.MainFindVenue;
import com.gennext.sports.fragments.venu.MainFindVenue.OnFindVenueListener;
import com.gennext.sports.fragments.venu.MainFindVenueView;
import com.gennext.sports.service.GetNotificationServices;
import com.gennext.sports.util.AppSettings;
import com.gennext.sports.util.AppTokens;
import com.gennext.sports.util.Buddy;
import com.gennext.sports.util.L;
import com.gennext.sports.util.WakeLocker;
import com.google.android.gcm.GCMRegistrar;

public class MainActivity extends BaseActivity implements OnFindSbuddyzListener, OnFindCoachesListener, OnFindVenueListener, OnCoachReviewCount, OnVenueReviewCount, OnNotificationCounterChangeListener, OnMessageCounterChangeListener {
    LinearLayout llMenu, llShadow, llFsbuddyz, llFCoaches, llFVenues, llCEvent;
    LinearLayout llFsbuddyzLine, llFCoachesLine, llFVenuesLine, llCEventLine;
    ImageView ivFsbuddyz, ivFCoaches, ivFVenues, ivCEvent;
    FragmentManager mannager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        hideActionBar();
        //setActionBarOption();
        mannager = getSupportFragmentManager();
        GCMIntentService.notificationStatus = true;
        registerReceiver(mHandleMessageReceiver, new IntentFilter(AppTokens.DISPLAY_MESSAGE_ACTION));


        initUi();
    }

    private void initUi() {
        llMenu = (LinearLayout) findViewById(R.id.ll_main_menu_bottom);
        llShadow = (LinearLayout) findViewById(R.id.ll_main_shadow);
        llFsbuddyz = (LinearLayout) findViewById(R.id.ll_main_fsbuddyz);
        llFsbuddyzLine = (LinearLayout) findViewById(R.id.ll_main_fsbuddyz_line);
        llFCoaches = (LinearLayout) findViewById(R.id.ll_main_fCoaches);
        llFCoachesLine = (LinearLayout) findViewById(R.id.ll_main_fCoaches_line);
        llFVenues = (LinearLayout) findViewById(R.id.ll_main_fVenues);
        llFVenuesLine = (LinearLayout) findViewById(R.id.ll_main_fVenues_line);
        llCEvent = (LinearLayout) findViewById(R.id.ll_main_cEvents);
        llCEventLine = (LinearLayout) findViewById(R.id.ll_main_cEvents_line);

        ivFsbuddyz = (ImageView) findViewById(R.id.iv_main_findSbuddy);
        ivFCoaches = (ImageView) findViewById(R.id.iv_main_searchCoach);
        ivFVenues = (ImageView) findViewById(R.id.iv_main_searchVenue);
        ivCEvent = (ImageView) findViewById(R.id.iv_main_createEvent);

        llFsbuddyz.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                mannager.popBackStack("mainRSSFeed", 0);
                switchTab(1);
                setScreenDynamically(1);
            }
        });
        llFCoaches.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                mannager.popBackStack("mainRSSFeed", 0);
                switchTab(2);
                setScreenDynamically(3);
            }
        });
        llFVenues.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                mannager.popBackStack("mainRSSFeed", 0);
                switchTab(3);
                setScreenDynamically(5);
            }
        });
        llCEvent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                //mannager.popBackStack("mainRSSFeed", 0);
                mannager.popBackStack("mainRSSFeed", 0);
                switchTab(4);
                setScreenDynamically(7);
            }
        });
        setScreenDynamically(0);

//		if(GCMIntentService.notifyMsgOrNotification.equals("msg")){
//			
//		}else if(GCMIntentService.notifyMsgOrNotification.equals("noti")){
//			setScreenDynamically(9);
//		}
    }


    public void SwitchTab() {
        switchTab(0);
    }


    private void switchTab(int key) {

        switch (key) {
            case 0:
                llFsbuddyzLine.setVisibility(View.INVISIBLE);
                llFCoachesLine.setVisibility(View.INVISIBLE);
                llFVenuesLine.setVisibility(View.INVISIBLE);
                llCEventLine.setVisibility(View.INVISIBLE);
                ivFsbuddyz.setImageDrawable(getResources().getDrawable(R.drawable.search_sbuddy_inact));
                ivFCoaches.setImageDrawable(getResources().getDrawable(R.drawable.search_coach_inact));
                ivFVenues.setImageDrawable(getResources().getDrawable(R.drawable.service_venue_inactivate));
                ivCEvent.setImageDrawable(getResources().getDrawable(R.drawable.create_venue_inactive));
                break;
            case 1:
                llFsbuddyzLine.setVisibility(View.VISIBLE);
                llFCoachesLine.setVisibility(View.INVISIBLE);
                llFVenuesLine.setVisibility(View.INVISIBLE);
                llCEventLine.setVisibility(View.INVISIBLE);
                ivFsbuddyz.setImageDrawable(getResources().getDrawable(R.drawable.search_sbuddy_act));
                ivFCoaches.setImageDrawable(getResources().getDrawable(R.drawable.search_coach_inact));
                ivFVenues.setImageDrawable(getResources().getDrawable(R.drawable.service_venue_inactivate));
                ivCEvent.setImageDrawable(getResources().getDrawable(R.drawable.create_venue_inactive));

                break;
            case 2:
                llFsbuddyzLine.setVisibility(View.INVISIBLE);
                llFCoachesLine.setVisibility(View.VISIBLE);
                llFVenuesLine.setVisibility(View.INVISIBLE);
                llCEventLine.setVisibility(View.INVISIBLE);
                ivFsbuddyz.setImageDrawable(getResources().getDrawable(R.drawable.search_sbuddy_inact));
                ivFCoaches.setImageDrawable(getResources().getDrawable(R.drawable.search_coach_active));
                ivFVenues.setImageDrawable(getResources().getDrawable(R.drawable.service_venue_inactivate));
                ivCEvent.setImageDrawable(getResources().getDrawable(R.drawable.create_venue_inactive));
                break;
            case 3:
                llFsbuddyzLine.setVisibility(View.INVISIBLE);
                llFCoachesLine.setVisibility(View.INVISIBLE);
                llFVenuesLine.setVisibility(View.VISIBLE);
                llCEventLine.setVisibility(View.INVISIBLE);
                ivFsbuddyz.setImageDrawable(getResources().getDrawable(R.drawable.search_sbuddy_inact));
                ivFCoaches.setImageDrawable(getResources().getDrawable(R.drawable.search_coach_inact));
                ivFVenues.setImageDrawable(getResources().getDrawable(R.drawable.service_venue_activate));
                ivCEvent.setImageDrawable(getResources().getDrawable(R.drawable.create_venue_inactive));
                break;
            case 4:
                llFsbuddyzLine.setVisibility(View.INVISIBLE);
                llFCoachesLine.setVisibility(View.INVISIBLE);
                llFVenuesLine.setVisibility(View.INVISIBLE);
                llCEventLine.setVisibility(View.INVISIBLE);
                ivFsbuddyz.setImageDrawable(getResources().getDrawable(R.drawable.search_sbuddy_inact));
                ivFCoaches.setImageDrawable(getResources().getDrawable(R.drawable.search_coach_inact));
                ivFVenues.setImageDrawable(getResources().getDrawable(R.drawable.service_venue_inactivate));
                ivCEvent.setImageDrawable(getResources().getDrawable(R.drawable.create_venue_inactive));
                break;

        }

    }

    private void setScreenDynamically(int position) {
        setScreenDynamically(position, null, null, null, null);
    }

    private void setScreenDynamically(int position, String sltSportId) {
        setScreenDynamically(position, sltSportId, null, null, null);
    }

    private void setScreenDynamically(int position, String sltSportId, String searchData) {
        setScreenDynamically(position, sltSportId, null, null, searchData);
    }

    private void setScreenDynamically(int position, String sltSportId, String sltSportName, String sltSportBaseImage, String searchData) {
        FragmentTransaction transaction;
        switch (position) {
            case 0:
                transaction = mannager.beginTransaction();
                MainRSSFeed mainRSSFeed = new MainRSSFeed();
                transaction.add(R.id.group_main, mainRSSFeed, "mainRSSFeed");
                transaction.commit();
                break;
            case 1:
                clearBackStack();
                transaction = mannager.beginTransaction();
                MainFindSbuddyz mainFindSbuddyz = new MainFindSbuddyz();
                mainFindSbuddyz.setCommunicator(MainActivity.this);
                transaction.add(R.id.group_main, mainFindSbuddyz, "mainFindSbuddyz");
                transaction.addToBackStack("mainFindSbuddyz");
                transaction.commit();
                break;
            case 2:

                MainFindSbuddyzView mainFindSbuddyzView = new MainFindSbuddyzView();
                mainFindSbuddyzView.setSelectSport(sltSportId, sltSportName);
                mainFindSbuddyzView.setSearchBuddyData(searchData);
                mainFindSbuddyzView.setSportBaseImage(sltSportBaseImage);
                transaction = mannager.beginTransaction();
                transaction.add(R.id.group_main, mainFindSbuddyzView, "mainSearchSbuddyzView");
                transaction.addToBackStack("mainFindSbuddyzView");
                transaction.commit();
                break;
            case 3:
                clearBackStack();
                transaction = mannager.beginTransaction();
                MainFindCoaches mainFindCoaches = new MainFindCoaches();
                mainFindCoaches.setCommunicator(MainActivity.this);
                transaction.add(R.id.group_main, mainFindCoaches, "mainFindCoaches");
                transaction.addToBackStack("mainFindCoaches");
                transaction.commit();

                break;

            case 4:
                MainFindCoachesView mainFindCoachesView = new MainFindCoachesView();
                mainFindCoachesView.setSelectSport(sltSportId, sltSportName);
                mainFindCoachesView.setSearchCoachData(searchData);
                mainFindCoachesView.setSportBaseImage(sltSportBaseImage);
                transaction = mannager.beginTransaction();
                transaction.add(R.id.group_main, mainFindCoachesView, "mainFindCoachesView");
                transaction.addToBackStack("mainFindSbuddyzView");
                transaction.commit();
                break;
            case 5:
                clearBackStack();
                transaction = mannager.beginTransaction();
                MainFindVenue mainFindVenue = new MainFindVenue();
                mainFindVenue.setCommunicator(MainActivity.this);
                transaction.add(R.id.group_main, mainFindVenue, "mainFindVenue");
                transaction.addToBackStack("mainFindVenue");
                transaction.commit();
                break;

            case 6:
                MainFindVenueView mainFindVenueView = new MainFindVenueView();
                mainFindVenueView.setSelectSport(sltSportId, sltSportName);
                mainFindVenueView.setSearchVenueData(searchData);
                mainFindVenueView.setSportBaseImage(sltSportBaseImage);
                transaction = mannager.beginTransaction();
                transaction.add(R.id.group_main, mainFindVenueView, "mainFindVenueView");
                transaction.addToBackStack("mainFindVenueView");
                transaction.commit();
                break;
            case 7:
                clearBackStack();
                Intent ob = new Intent(MainActivity.this, MainEvent.class);
                startActivity(ob);
                break;

            case 9:
                MainNotification mainNotification = new MainNotification();
                transaction = mannager.beginTransaction();
                transaction.add(R.id.group_main, mainNotification, "mainNotification");
                transaction.commit();

                break;
        }
    }

    private void clearBackStack() {
        FragmentManager fm = getSupportFragmentManager(); // or 'getSupportFragmentManager();'
        int count = fm.getBackStackEntryCount();
        for (int i = 0; i < count; ++i) {
            fm.popBackStack();
        }
    }

    private Boolean isFragmentVisible(String FragmentTag) {
        if (getFragmentManager().findFragmentByTag(FragmentTag) != null
                && getFragmentManager().findFragmentByTag(FragmentTag).isVisible()) {
            return true;
        }
        return false;
    }

    @Override
    public void onFindSbuddyzListener(Boolean status, String sltSportId, String sltSportName, String sltSportBaseImage, String searchBuddyData) {
        setScreenDynamically(2, sltSportId, sltSportName, sltSportBaseImage, searchBuddyData);
    }

    @Override
    public void onFindCoachesListener(Boolean status, String sltSportId, String sltSportName, String sltSportBaseImage, String searchCoachData) {
        setScreenDynamically(4, sltSportId, sltSportName, sltSportBaseImage, searchCoachData);
    }

    @Override
    public void onFindVenueListener(Boolean status, String sltSportId, String sltSportName, String sltSportBaseImage, String searchVenueData) {
        setScreenDynamically(6, sltSportId, sltSportName, sltSportBaseImage, searchVenueData);
    }


    @Override
    public void onBackPressed() {
        if (mannager.getBackStackEntryCount() > 0) {
            //L.m("MainActivity" + "popping backstack: "+mannager.getBackStackEntryCount());
            int count = mannager.getBackStackEntryCount();
            //mannager.popBackStack("mainRSSFeed", 0);
            mannager.popBackStack();
            if (count == 1) {
                //ActionBarHeading.setText("HOME");
                llMenu.setVisibility(View.VISIBLE);
                llShadow.setVisibility(View.VISIBLE);
                switchTab(0);
            }
//			for(int i=count-1;i>=0;i--){
//				FragmentManager.BackStackEntry entry=mannager.getBackStackEntryAt(i);
//				L.m("BackButton pressed "+entry.getName());
//			}
        } else {
            L.m("MainActivity" + "nothing on backstack, calling super");
            super.onBackPressed();
//            finish();
        }
    }

    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String newMessage = intent.getExtras().getString(AppTokens.EXTRA_MESSAGE);
            // Waking up mobile if it is sleeping
            WakeLocker.acquire(getApplicationContext());

            /**
             * Take appropriate action on this message
             * depending upon your app requirement
             * For now i am just displaying it on the screen
             * */

            // Showing received message
            //Toast.makeText(getApplicationContext(), "New Message: " + newMessage, Toast.LENGTH_LONG).show();
            // Releasing wake lock
            WakeLocker.release();
        }
    };

    public void getConnectRequest() {
        Intent grapprIntent = new Intent(MainActivity.this, GetNotificationServices.class);
        grapprIntent.putExtra(GetNotificationServices.URL, AppSettings.getConnectRequest);
        grapprIntent.putExtra(GetNotificationServices.PLAYER_ID, LoadPref(Buddy.PlayerId));
        startService(grapprIntent);

    }

    @Override
    public void onResume() {
        super.onResume();
        L.m("The onResume() event");
        GetNotificationServices.shouldContinue = true;

        // Register for the particular broadcast based on ACTION string
        IntentFilter filter = new IntentFilter(GetNotificationServices.ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(testReceiver, filter);

        getConnectRequest();


    }

    @Override
    public void onPause() {
        super.onPause();
        L.m("The onPause() event");
        GetNotificationServices.shouldContinue = false;

        // Unregister the listener when the application is paused
        LocalBroadcastManager.getInstance(this).unregisterReceiver(testReceiver);
        // or `unregisterReceiver(testReceiver)` for a normal broadcast
    }

    // Define the callback for what to do when data is received
    private BroadcastReceiver testReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int resultCode = intent.getIntExtra(GetNotificationServices.NotiResultCode, RESULT_CANCELED);
            if (resultCode == RESULT_OK) {
                int NotiCounter = intent.getIntExtra(GetNotificationServices.NotiCounter, 0);
                setNotificationCounter();
            }
        }
    };

    public void setNotificationCounter() {
        MainRSSFeed mainRSSFeed = (MainRSSFeed) mannager.findFragmentByTag("mainRSSFeed");
        if (mainRSSFeed != null) {

            mainRSSFeed.setNotificationCounter(AppTokens.newNotificationPush.size() + AppTokens.newNotificationPull.size() + AppTokens.newChat.size());

        }
    }

    @Override
    public void onDestroy() {

        try {
            unregisterReceiver(mHandleMessageReceiver);
            GCMRegistrar.onDestroy(this);
            GCMIntentService.notificationStatus = true;
        } catch (Exception e) {
            L.m("UnRegister Receiver Error" + "> " + e.getMessage());
        }
        super.onDestroy();
    }

    @Override
    public void onCoachReviewCount(String review, String rating) {
        MainFindCoachesDetail mainFindCoachesDetail = (MainFindCoachesDetail) mannager.findFragmentByTag("mainFindCoachesDetail");
        if (mainFindCoachesDetail != null) {
            mainFindCoachesDetail.setRatingAndReview(rating, review);
        }
    }

    @Override
    public void onVenueReviewCount(String review, String rating) {
        MainFindVenuDetail mainFindVenuDetail = (MainFindVenuDetail) mannager.findFragmentByTag("mainFindVenuDetail");
        if (mainFindVenuDetail != null) {
            mainFindVenuDetail.setRatingAndReview(rating, review);
        }
    }

    @Override
    public void onNotificationCounterChangeListener(int NotiCounter) {
        MainRSSFeed mainRSSFeed = (MainRSSFeed) mannager.findFragmentByTag("mainRSSFeed");
        if (mainRSSFeed != null) {
            mainRSSFeed.setNotificationCounter(AppTokens.newNotificationPush.size() + AppTokens.newNotificationPull.size() + AppTokens.newChat.size());
        }
    }

    @Override
    public void onMessageCounterChangeListener(int MsgCounter) {
        MainRSSFeed mainRSSFeed = (MainRSSFeed) mannager.findFragmentByTag("mainRSSFeed");
        if (mainRSSFeed != null) {
            mainRSSFeed.setMessageCounter(MsgCounter);
        }
    }

//	@Override
//	public void onRSSFeedListener(Boolean status) {
//		llMenu.setVisibility(View.VISIBLE);
//		llShadow.setVisibility(View.VISIBLE);
//	}


}
