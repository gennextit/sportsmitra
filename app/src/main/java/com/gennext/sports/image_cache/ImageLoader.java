package com.gennext.sports.image_cache;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.ImageView;

import com.gennext.sports.R;
import com.bumptech.glide.Glide;

import java.io.IOException;

public class ImageLoader {
    public static final String DEFAULT_SQUIRE = "base_squire";
    public static final String DEFAULT_MEDIUM = "base_medium";
    public static final String DEFAULT_SMALL = "base_small";

    private final Context mContext;

    public ImageLoader(Context context) {
        this.mContext = context;
    }

    public void DisplayImage(String imgSrc, ImageView ivProfile, String defaultImage) {
        DisplayImage(imgSrc,ivProfile,defaultImage,true);
    }
    public void DisplayImage(String imgSrc, ImageView ivProfile, String defaultImage, Boolean decodeImage) {
        int stub_id;
        if(defaultImage.equals("profile")){
            // default image show in list (Before online image download)
            stub_id=R.drawable.profile;
        }else if(defaultImage.equals(DEFAULT_SMALL)){
            stub_id= R.drawable.sport_bg_small;
        }else if(defaultImage.equals(DEFAULT_MEDIUM)){
            stub_id=R.drawable.sport_bg_medium;
        }else if(defaultImage.equals(DEFAULT_SQUIRE)){
            stub_id=R.drawable.sport_bg_medium;
        }else{
            // default image show in list (Before online image download)
            stub_id=R.drawable.sports_bg_squire;
        }

        Glide.with(mContext)
                .load(imgSrc)
                .placeholder(stub_id)
                .error(stub_id)
                .into(ivProfile);
    }

    public Bitmap getImageBitmap(Uri uri) {
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), uri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }
}
