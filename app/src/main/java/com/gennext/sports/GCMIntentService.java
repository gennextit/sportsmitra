package com.gennext.sports;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import java.util.ArrayList;

import com.gennext.sports.MainActivity;
import com.gennext.sports.R;
import com.gennext.sports.model.ChatSbuddyModel;
import com.gennext.sports.model.FindCoachModel;
import com.gennext.sports.model.MessageModel;
import com.gennext.sports.model.NotificationModel;
import com.gennext.sports.util.AppPermission;
import com.gennext.sports.util.AppTokens;
import com.gennext.sports.util.Buddy;
import com.gennext.sports.util.Coach;
import com.gennext.sports.util.L;
import com.gennext.sports.util.Utility;
import com.gennext.sports.util.Venue;
import com.google.android.gcm.GCMBaseIntentService;

public class GCMIntentService extends GCMBaseIntentService {

    private static final String TAG = "GCMIntentService";
    public static int CHAT_NOTIFICATION = 1, MESSAGE_NOTIFICATION = 2, ALL_NOTIFICATION = 3;
    public static boolean notificationStatus = true;
    public static String notificationMessage = "";
    public static String notifyMsgOrNotification = "";
    public static int notificationCounter = 0;
    public static int messageCounter = 0;


    public GCMIntentService() {
        super(AppTokens.SENDER_ID);
    }

    /**
     * Method called on device registered
     **/
    @Override
    protected void onRegistered(Context context, String registrationId) {
        Log.i(TAG, "Device registered: regId = " + registrationId);
        AppTokens.displayMessage(context, "Your device is registered with GCM");
        L.m("NAME"+ "com.gennext.sports");
        L.m(registrationId);
        // ServerUtilities.register(context, MainActivity.name,
        // MainActivity.email, registrationId);
        Utility.SavePref(context, AppTokens.GCM_ID, registrationId);
    }

    /**
     * Method called on device un registred
     */
    @Override
    protected void onUnregistered(Context context, String registrationId) {
        Log.i(TAG, "Device unregistered");
        AppTokens.displayMessage(context, getString(R.string.gcm_unregistered));
        L.m("Device unregistered" + registrationId);
        // ServerUtilities.unregister(context, registrationId);
    }

    /**
     * Method called on Receiving a new message
     */
    @Override
    protected void onMessage(Context context, Intent intent) {
        Log.i(TAG, "Received message");
        String message = null;
        String sltBuddy;
        message = intent.getExtras().getString("chat");
        //Get chat message, if message received, then set notification type to trueand format notification
        //if ntoifcation is set to true and user notification already exist then remove previous notification and
        //add current notification-
        if (message != null) {
            L.m("chat : " + message);
            sltBuddy=ChatActivity.sltBuddyId;
            notifyMsgOrNotification = "noti";
            String[] chatData = AppTokens.executeQuery(message);
            if (chatData != null) {
                message = chatData[1] + " sent a message : " + chatData[2];
                if(!sltBuddy.equalsIgnoreCase(chatData[0])){
                    notificationCounter++;
                    removeChatCounter(chatData[0]);
                    ChatSbuddyModel ob = new ChatSbuddyModel();
                    ob.setSenderId(chatData[0]);
                    ob.setName(chatData[1]);
                    ob.setMessage(chatData[2]);
                    AppTokens.newChat.add(ob);
                }
//                if (notificationStatus) {
//                    notificationCounter++;
//
//                }
                AppTokens.displayChatMessage(context,chatData[0],chatData[1], chatData[2]);
            }
            if (notificationStatus) {
                // notifies user
                if(!sltBuddy.equalsIgnoreCase(chatData[0])) {
                    generateNotification(context, message, CHAT_NOTIFICATION);
                }
            }
        }
        message = intent.getExtras().getString("message");
        if (message != null) {
            L.m("message : " + message);
            notifyMsgOrNotification = "msg";
            messageCounter++;
            String[] chatData = AppTokens.executeMsgQuery(message);
            if (chatData != null) {
                message = chatData[1] + " sent a message : " + chatData[2];
                //if message  received from buddy to coach or venue
                if (!chatData[3].equals("buddy")) {
                    removeMessageCounter(chatData[0]);
                    MessageModel ob = new MessageModel();
                    ob.setRequestId(chatData[0]);
                    ob.setRequestedName(chatData[1]);
                    ob.setMessage(chatData[2]);
                    ob.setStatusRecived(chatData[3]);
                    AppTokens.newMessage.add(ob);
                    AppTokens.displayMsgMessage(context, chatData[2]);
                } else {  //if message  received from coach or venue to buddy
                    AppTokens.displayMsgMessage(context, chatData[2]);
                }

            }
            generateNotification(context, message, MESSAGE_NOTIFICATION);
        }

        //Connect, reject, approve notification
        message = intent.getExtras().getString("notification");
        if (message != null) {
            L.m("Notification : " + message);
            notifyMsgOrNotification = "noti";
            notificationCounter++;
            String[] chatData = AppTokens.executeNotiQuery(message);
            if (chatData != null) {
                message = chatData[2];
                NotificationModel ob = new NotificationModel();
                ob.setRequestId(chatData[0]);
                ob.setRequestedName(chatData[1]);
                ob.setNotification(chatData[2]);
                ob.setRequestedStatue(chatData[3]);
                if(!chatData[3].equals("request")){
//                    int flagCount=0;
//                    for (int j = 0; j <  AppTokens.newNotificationPull.size(); j++) {
//                        if (AppTokens.newNotificationPull.get(j).getRequestId().equalsIgnoreCase(chatData[0])) {
//                            if(AppTokens.newNotificationPull.get(j).getRequestedStatue().equals("request")){
//                                flagCount=1;
//                            }
//                        }
//                    }
//                    if(flagCount==0){
                       AppTokens.newNotificationPush.add(ob);
                        //AppTokens.newNotificationPull.add(ob);
                        GCMIntentService.notificationCounter++;
//                    }
//                }else{
//                    AppTokens.newNotificationPush.add(ob);
                }
                AppTokens.displayNotificationMessage(context, message);

            }

            // notifies user
            generateNotification(context, message, ALL_NOTIFICATION);

        }

    }

    /**
     * Method called on receiving a deleted message
     */
    @Override
    protected void onDeletedMessages(Context context, int total) {
        Log.i(TAG, "Received deleted messages notification");
        String message = getString(R.string.gcm_deleted, total);
        AppTokens.displayMessage(context, message);
        // notifies user
        generateNotification(context, message, ALL_NOTIFICATION);
    }

    /**
     * Method called on Error
     */
    @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);
        AppTokens.displayMessage(context, getString(R.string.gcm_error, errorId));
    }

    public void removeChatCounter(String playerId) {
        String player = playerId;
//        ArrayList<ChatSbuddyModel> tempList = new ArrayList<ChatSbuddyModel>();
//        ArrayList<ChatSbuddyModel> finalList = new ArrayList<ChatSbuddyModel>();
//        tempList =  AppTokens.newChat;
//        finalList = tempList;
        for (int i = 0; i < AppTokens.newChat.size(); i++) {
            if (AppTokens.newChat.get(i).getSenderId().equalsIgnoreCase(player)) {
                AppTokens.newChat.remove(i);
                notificationCounter--;
            }
        }
//        AppTokens.newChat.clear();
//        AppTokens.newChat = finalList;
    }

    public void removeMessageCounter(String playerId) {
        String player = playerId;
        ArrayList<MessageModel> tempList = new ArrayList<MessageModel>();
        ArrayList<MessageModel> finalList = new ArrayList<MessageModel>();
        tempList = AppTokens.newMessage;
        finalList = tempList;
        for (int i = 0; i < tempList.size(); i++) {
            if (tempList.get(i).getRequestId().equalsIgnoreCase(player)) {
                finalList.remove(i);
                messageCounter--;
            }
        }
        AppTokens.newMessage.clear();
        AppTokens.newMessage = finalList;
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        // log message
        Log.i(TAG, "Received recoverable error: " + errorId);
        AppTokens.displayMessage(context, getString(R.string.gcm_recoverable_error, errorId));
        return super.onRecoverableError(context, errorId);
    }

    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static void generateNotification(Context context, String message, int from) {

        if (AppPermission.checkNotificationPermission(context)) {


            int icon = R.drawable.ic_launcher;
            long when = System.currentTimeMillis();
            NotificationManager manager;
            Notification myNotication;
            String title = context.getString(R.string.app_name);
            Intent notificationIntent;
            if (!Utility.LoadPref(context, Buddy.PlayerId).equals("")) {
                notificationIntent = new Intent(context, MainActivity.class);
            } else if (!Utility.LoadPref(context, Coach.CoachId).equals("")) {
                notificationIntent = new Intent(context, CoachMainActivity.class);
            } else if (!Utility.LoadPref(context, Venue.VenueId).equals("")) {
                notificationIntent = new Intent(context, VenueMainActivity.class);
            }else{
                notificationIntent = new Intent(context, SplashScreen.class);
            }

            manager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
            // set intent so it does not start a new activity
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
            //notification.setLatestEventInfo(context, title, message, intent);
            Notification.Builder builder = new Notification.Builder(context);

            builder.setAutoCancel(true);
            //builder.setTicker("this is ticker text");
            builder.setContentTitle(title);
            builder.setContentText(message);
            builder.setSmallIcon(icon);
            builder.setContentIntent(pendingIntent);
            builder.setOngoing(true);
            //builder.setSubText("This is subtext...");   //API level 16
            builder.setNumber(100);
            builder.build();

            myNotication = builder.getNotification();
            myNotication.defaults = Notification.DEFAULT_ALL;
            manager.notify(0, myNotication);

        }
    }

}
