package com.gennext.sports.gps;

import android.location.Location;

public interface GPSCallback
{
   public abstract void onGPSUpdate(Location location);
}

