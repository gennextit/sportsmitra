package com.gennext.sports;

import com.gennext.sports.gplus.AbstractGetNameNameTask;
import com.gennext.sports.gplus.GetNameInForeGround;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.SignInButton;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;



public class SignUpGoogle extends BaseActivity{

	Context mContext =SignUpGoogle.this;
	AccountManager mAM;
	String Token;
	int serverCode;
	private SignInButton googleBtn;
	private static final String SCOPE="oauth2:https://www.googleapis.com/auth/userinfo.profile";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_google);
		hideActionBar();
		setActionBarOption();
		syncGoogleAccount();

	}
	private String[] getAccountNames(){
		mAM=AccountManager.get(SignUpGoogle.this);
		Account[] accounts=mAM.getAccountsByType(GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE);
		String[] names=new String[accounts.length];
		for(int i=0;i< names.length;i++){
			names[i]=accounts[i].name;
		}
		return names;
	}

	private AbstractGetNameNameTask getTask(SignUpGoogle activity,String email,String scope) {
		return new GetNameInForeGround(activity, email, scope);
	}

	private void syncGoogleAccount() {
		if(isNetAvailable()){
			String[] accArr=getAccountNames();
			if(accArr.length>0){
				getTask(SignUpGoogle.this, accArr[0], SCOPE).execute();
			}else{
				Toast.makeText(mContext, "no g Acc Sync", Toast.LENGTH_SHORT);
			}
		}else{
			Toast.makeText(mContext, "no connection Available", Toast.LENGTH_SHORT);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		//syncGoogleAccount();
	}


	private boolean isNetAvailable() {
		// TODO Auto-generated method stub
		return true;
	}

}
