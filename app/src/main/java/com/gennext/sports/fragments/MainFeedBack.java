package com.gennext.sports.fragments;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.gennext.sports.R;
import com.gennext.sports.util.AppSettings;
import com.gennext.sports.util.Buddy;
import com.gennext.sports.util.HttpReq;
import com.gennext.sports.util.L;
import com.gennext.sports.util.Validation;
import com.gennext.sports.util.internet.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class MainFeedBack extends CompactFragment implements View.OnTouchListener {
	private static final String LOG_TAG = "sMitra";
	EditText etFeedback;
	Button btnSubmit;
	FeedbackRequest feedbackRequest;
	ProgressBar progressBar;
	FragmentManager mannager;
	Boolean isSoftKeyboardDisplayed = false;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (feedbackRequest != null) {
			feedbackRequest.onAttach(activity);
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (feedbackRequest != null) {
			feedbackRequest.onDetach();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.main_feedback, container, false);
		mannager = getFragmentManager();
		setActionBarOption(v);
		isSoftKeyboardDisplayed = false;

		etFeedback = (EditText) v.findViewById(R.id.et_main_feedback);
		btnSubmit = (Button) v.findViewById(R.id.btn_main_feedback);
		progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
		setTypsFace(etFeedback);
		setTypsFace(btnSubmit);
		
		etFeedback.setOnTouchListener(this);
		
		btnSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (checkValidation()) {
					if (isSoftKeyboardDisplayed) {
						hideKeybord();
					}

					feedbackRequest = new FeedbackRequest(getActivity());
					feedbackRequest.execute(AppSettings.feedback);
				}
			}
		});

		

		return v;
	}

	public void setActionBarOption(View view) {
		LinearLayout ActionBack, ActionHome;
		ActionBack = (LinearLayout) view.findViewById(R.id.ll_actionbar_back);
		ActionHome = (LinearLayout) view.findViewById(R.id.ll_actionbar_home);
//		TextView actionbarTitle=(TextView)view.findViewById(R.id.actionbar_title);
//		actionbarTitle.setText(setActionBarTitle("FEEDBACK"));
		ActionBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (isSoftKeyboardDisplayed) {
					hideKeybord();
				}

				mannager.popBackStack();
			}
		});
		ActionHome.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (isSoftKeyboardDisplayed) {
					hideKeybord();
				}

				mannager.popBackStack();
			}
		});
	}

	private boolean checkValidation() {
		boolean ret = true;

		if (!Validation.isEmpty(etFeedback, true)) {
			ret = false;
		} 
		return ret;
	}

	

	private class FeedbackRequest extends AsyncTask<String, Void, String> {

		private Activity activity;

		public FeedbackRequest(Activity activity) {
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressBar.setVisibility(View.VISIBLE);
			btnSubmit.setVisibility(View.GONE);
		}

		@Override
		protected String doInBackground(String... urls) {
			String response;
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				// sportId,choosePlan, chooseDate, chooseSlot, playerId, venueId
				params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
				params.add(new BasicNameValuePair("feedback", etFeedback.getText().toString()));
				
			}

			HttpReq ob = new HttpReq();

			return response = ob.makeConnection(urls[0], HttpReq.POST, params, HttpReq.EXECUTE_TASK);

		}

		@Override
		protected void onPostExecute(String result) {

			if (activity != null) {
				progressBar.setVisibility(View.GONE);
				btnSubmit.setVisibility(View.VISIBLE);
				if (result != null) {
					L.m(result);
					if (result.equalsIgnoreCase("success")) {
						showReviewAlertSuccess(getActivity(), "feedBack", 6);
					} else {
						Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
					}
				} else {
					Toast.makeText(getActivity(), ErrorMessage, Toast.LENGTH_SHORT).show();
				}
			}
		}
	}

	

	@Override
	public boolean onTouch(View arg0, MotionEvent arg1) {
		// TODO Auto-generated method stub
		isSoftKeyboardDisplayed = true;
		return false;
	}

}
