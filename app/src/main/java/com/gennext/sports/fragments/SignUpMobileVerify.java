package com.gennext.sports.fragments;

import android.app.Activity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.gennext.sports.R;
import com.gennext.sports.service.SmsVerifyService;
import com.gennext.sports.util.AppSettings;
import com.gennext.sports.util.Buddy;
import com.gennext.sports.util.L;

public class SignUpMobileVerify extends CompactFragment {

	private EditText etOTP;
	private Button btnVerify;
	public static final String FRAGMENT_TAG = "signUpMobileVerify";
	private FragmentManager mannager;
	private ProgressBar progressBar;
	private OnSuccessVerifyedMobile comm;

	Boolean isSoftKeyboardDisplayed = false;

	public SignUpMobileVerify() {
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);

	}

	public void setCommunicator(OnSuccessVerifyedMobile onSuccessVerifyedMobile) {
		this.comm = onSuccessVerifyedMobile;
	}

	public interface OnSuccessVerifyedMobile {
		public void onSuccessVerifyedMobile(Boolean status);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.fragment_mobile_verify, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		etOTP = (EditText) view.findViewById(R.id.et_mobile_verify_otp);
		btnVerify = (Button) view.findViewById(R.id.btn_mobile_verify);
		progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
		LinearLayout tearmsAndUse = (LinearLayout) view.findViewById(R.id.tearmsOfUse);
		tearmsAndUse.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				AppWebView appWebView = new AppWebView();
				appWebView.setUrl(getResources().getString(R.string.t_and_c), AppSettings.USER_GUIDE_TC,
						AppWebView.URL);
				mannager = getFragmentManager();
				FragmentTransaction transaction = mannager.beginTransaction();
				transaction.add(android.R.id.content, appWebView, "appWebView");
				transaction.addToBackStack("appWebView");
				transaction.commit();
			}
		});
		etOTP.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub
				isSoftKeyboardDisplayed = true;
				return false;
			}
		});
		btnVerify.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (isSoftKeyboardDisplayed) {
					hideKeybord();
				}
				String otp = etOTP.getText().toString();
				verifyOtp(otp);

			}
		});
	}

	public void hideProgress() {
		progressBar.setVisibility(View.GONE);
		btnVerify.setVisibility(View.VISIBLE);

	}

	public void showProgress() {
		progressBar.setVisibility(View.VISIBLE);
		btnVerify.setVisibility(View.GONE);

	}

	//
	/**
	 * sending the OTP to server and activating the user
	 */
	private void verifyOtp(String otp) {

		if (!otp.isEmpty()) {
			Intent grapprIntent = new Intent(getActivity(), SmsVerifyService.class);
			grapprIntent.putExtra("otp", otp);
			grapprIntent.putExtra("mobile", LoadPref(Buddy.CompleteMobile));
			grapprIntent.putExtra("url", AppSettings.verifyOtp);
			grapprIntent.putExtra("playerId", "");
			getActivity().startService(grapprIntent);
			showProgress();
		} else {
			Toast.makeText(getActivity(), "Please enter the OTP", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		L.m("The onResume() event");
		SmsVerifyService.shouldContinue = true;

		// Register for the particular broadcast based on ACTION string
		IntentFilter filter = new IntentFilter(SmsVerifyService.ACTION);
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(testReceiver, filter);
	}

	@Override
	public void onPause() {
		super.onPause();
		L.m("The onPause() event");
		SmsVerifyService.shouldContinue = false;

		// Unregister the listener when the application is paused
		LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(testReceiver);
		// or `unregisterReceiver(testReceiver)` for a normal broadcast
	}

	//
	// Define the callback for what to do when data is received
	private BroadcastReceiver testReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			int resultCode = intent.getIntExtra("CSResultCode", getActivity().RESULT_CANCELED);
			if (resultCode == getActivity().RESULT_OK) {
				String resultOtp = intent.getStringExtra("otp");
				String status = intent.getStringExtra("status");
				if (status.equalsIgnoreCase("success")) {
					comm.onSuccessVerifyedMobile(true);
				} else if (status.equalsIgnoreCase("otp")) {
					etOTP.setText(resultOtp);
					showProgress();
				} else {
					hideProgress();
					Toast.makeText(getActivity(), status, Toast.LENGTH_SHORT).show();
				}
			}
		}
	};

	// private class HttpTask extends AsyncTask<String, Void, String> {
	// private Button btn;
	// private ProgressBar pBar;
	// private String otp;
	// public HttpTask(Button btn,ProgressBar pBar,String otp) {
	// this.btn=btn;
	// this.pBar=pBar;
	// this.otp=otp;
	// }
	// @Override
	// protected void onPreExecute() {
	// // TODO Auto-generated method stub
	// super.onPreExecute();
	// pBar.setVisibility(View.VISIBLE);
	// btn.setVisibility(View.GONE);
	// }
	//
	// @Override
	// protected String doInBackground(String... urls) {
	// String response = "error";
	// int flagSelect = 0;
	//
	// List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
	// params.add(new BasicNameValuePair("mobileNumber",
	// LoadPref(AppTokens.Mobile)));
	// params.add(new BasicNameValuePair("otp", otp));
	//
	// HttpReq ob = new HttpReq();
	// response = ob.makeConnection(urls[0], 3, params);
	// String errorMsg="Not Available";
	//
	// if (response.contains("[")) {
	// try {
	// JSONArray json=new JSONArray(response);
	// for(int i=0;i<json.length();i++){
	// JSONObject obj=json.getJSONObject(i);
	// if(!obj.optString("status").equals("")){
	// errorMsg=obj.optString("message");
	// }
	// }
	//
	// } catch (JSONException e) {
	// L.m("Json Error :"+e.toString());
	// }
	// } else {
	// L.m("Invalid JSON found : " + response);
	// return null;
	// }
	//
	// return errorMsg;
	// // return "success";
	//
	// }
	//
	// @Override
	// protected void onPostExecute(String result) {
	//
	// pBar.setVisibility(View.GONE);
	// btn.setVisibility(View.VISIBLE);
	// if (result != null) {
	// L.m(result);
	// Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
	// }
	// }
	// }

	// public void VerifyNo() {
	//
	// Uri uri=Uri.parse("content://sms/inbox");
	// Cursor cursor=getActivity().getContentResolver().query(uri, new
	// String[]{"id","address","date","body"}, null, null, null);
	//
	// }

}
