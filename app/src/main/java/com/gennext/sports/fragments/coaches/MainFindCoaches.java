package com.gennext.sports.fragments.coaches;

import android.app.Activity;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.sports.MainActivity;
import com.gennext.sports.R;
import com.gennext.sports.fragments.CompactFragment;
import com.gennext.sports.model.MySportsModel;
import com.gennext.sports.model.SpinnerSportsAdapter;
import com.gennext.sports.util.AppSettings;
import com.gennext.sports.util.AppTokens;
import com.gennext.sports.util.Buddy;
import com.gennext.sports.util.HttpReq;
import com.gennext.sports.util.L;
import com.gennext.sports.util.Utility;

import com.gennext.sports.util.internet.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class MainFindCoaches extends CompactFragment {
	Spinner spSports;
	TextView tvRadious;
	AutoCompleteTextView acLocality;
	Button btnFindCoaches;
	SeekBar sbRadious;
	private ProgressBar progressBar;
	private ArrayList<MySportsModel> sportArrayList;
	private String sltSportId, sltSportName, sltSportBaseImage;
	private static String searchCoachData;
	Boolean isSoftKeyboardDisplayed = false;

	private OnFindCoachesListener comm;
	LinearLayout mainLayout;
	FragmentManager mannager;

	HttpTask httpTask;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (httpTask != null) {
			httpTask.onAttach(getActivity());
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (httpTask != null) {
			httpTask.onDetach();
		}
	}

	public void setCommunicator(OnFindCoachesListener onFindCoachesListener) {
		this.comm = onFindCoachesListener;
	}

	public interface OnFindCoachesListener {
		public void onFindCoachesListener(Boolean status, String sltSportId, String sltSportName,
				String sltSportBaseImage, String searchCoachData);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.main_find_coaches, container, false);
		mannager = getFragmentManager();
		setActionBarOption(view);
		tvRadious = (TextView) view.findViewById(R.id.tv_main_find_coaches_searchRadius);
		spSports = (Spinner) view.findViewById(R.id.sp_main_find_coaches_sports);
		acLocality = (AutoCompleteTextView) view.findViewById(R.id.ac_main_find_coaches_locality);
		btnFindCoaches = (Button) view.findViewById(R.id.btn_main_find_coaches_searchCoaches);
		progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
		sbRadious = (SeekBar) view.findViewById(R.id.sb_main_find_coaches_sRadious);
		mainLayout = (LinearLayout) view.findViewById(R.id.ll_main_find_coaches_mainLayout);

		setTypsFace(acLocality);
		setTypsFace(btnFindCoaches);
		spSports.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub
				spSports.setSelection(0);
				return false;
			}
		});  
		spSports.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
				// TODO Auto-generated method stub
				if (sportArrayList != null && spSports.getSelectedItem().toString() != "Select Sport") {
					sltSportId = sportArrayList.get(position).getSportId();
					sltSportName = sportArrayList.get(position).getSportName();
					sltSportBaseImage = sportArrayList.get(position).getBaseImage();

				} else {
					sltSportName = spSports.getSelectedItem().toString();
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		acLocality.setAdapter(new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.autocomplete_list_item));
		acLocality.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
				// TODO Auto-generated method stub
				String str = (String) adapterView.getItemAtPosition(position);
			}
		});

		acLocality.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub
				isSoftKeyboardDisplayed = true;
				return false;
			}
		});

		sbRadious.setMax(60);
		sbRadious.setProgress(5);
		sbRadious.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (progress < 5) {
					sbRadious.setProgress(5);
				} else {
					tvRadious.setText(String.valueOf(progress));
				}
			}
		});

		btnFindCoaches.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (isOnline()) {
					if (!sltSportName.equalsIgnoreCase("Select Sport")) {
						httpTask = new HttpTask(getActivity(), btnFindCoaches, progressBar);
						httpTask.execute(AppSettings.findCoach);
					} else {
						Toast.makeText(getActivity(), "Please Select Sports", Toast.LENGTH_SHORT).show();
					}

				} else {
					showInternetAlertBox(getActivity());
				}

				if (isSoftKeyboardDisplayed) {
					hideKeybord();
					isSoftKeyboardDisplayed = false;
				}
			}
		});
		mainLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (isSoftKeyboardDisplayed) {
					hideKeybord();
					isSoftKeyboardDisplayed = false;
				}
			}
		});
		LoadSportsDate();
		return view;
	}

	public void setActionBarOption(View view) {
		LinearLayout ActionBack,ActionHome;
		ActionBack=(LinearLayout)view.findViewById(R.id.ll_actionbar_back);
		ActionHome=(LinearLayout)view.findViewById(R.id.ll_actionbar_home);
		ActionBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				((MainActivity)getActivity()).SwitchTab();
				mannager.popBackStack("mainFindCoaches", FragmentManager.POP_BACK_STACK_INCLUSIVE);
			}
		});
		ActionHome.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				((MainActivity)getActivity()).SwitchTab();
				mannager.popBackStack("mainFindCoaches", FragmentManager.POP_BACK_STACK_INCLUSIVE);
			}
		});
	}

	public void LoadSportsDate() {
		ArrayList<String> result = LoadSports();
		if (result != null) {
			SpinnerSportsAdapter adapter = new SpinnerSportsAdapter(getActivity(), R.layout.spinner_slot);
			adapter.addAll(result);
			adapter.add("Select Sport");
			spSports.setAdapter(adapter);
			spSports.setSelection(adapter.getCount());
		}
	}

	private ArrayList<String> LoadSports() {

		ArrayList<String> sList = new ArrayList<String>();
		sportArrayList = new ArrayList<MySportsModel>();
		String response = Utility.LoadPref(getActivity(), AppTokens.SportList);

		if (response.contains("[")) {
			try {
				JSONArray sportList = new JSONArray(response);

				for (int i = 0; i < sportList.length(); i++) {

					JSONObject obj = sportList.getJSONObject(i);
					if (!obj.optString("status").equals("") && obj.optString("status").equalsIgnoreCase("success")) {
						JSONArray message = obj.getJSONArray("message");
						for (int j = 0; j < message.length(); j++) {
							JSONObject messageData = message.getJSONObject(j);
							MySportsModel model = new MySportsModel();
							model.setSportId(messageData.optString("sportId"));
							model.setSportName(messageData.optString("sportName"));
							model.setLogo(messageData.optString("logo"));
							model.setBaseImage(messageData.optString("baseImage"));
							sList.add(messageData.optString("sportName"));
							sportArrayList.add(model);

						}

					}
				}
			} catch (JSONException e) {
				L.m(e.toString());
				return null;
			}
		} else {
			L.m("Invalid JSON found : " + response);
			return null;
		}

		return sList;
	}

	private class HttpTask extends AsyncTask<String, Void, String> {
		Button btn;
		ProgressBar pBar;
		Activity activity;

		public HttpTask(Activity activity, Button btn, ProgressBar pBar) {
			this.btn = btn;
			this.pBar = pBar;
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		private void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pBar.setVisibility(View.VISIBLE);
			btn.setVisibility(View.GONE);
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error";
			int flagSelect = 0;

			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if(activity!=null){
				params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
			}
			params.add(new BasicNameValuePair("sportId", sltSportId));
			params.add(new BasicNameValuePair("location", acLocality.getText().toString()));
			params.add(new BasicNameValuePair("radius", tvRadious.getText().toString()));

			if (activity != null) {
				if (!LoadPref(AppTokens.GpsLatitude).equals("")) {
					params.add(new BasicNameValuePair("latitude", LoadPref(AppTokens.GpsLatitude)));
				} else {
					params.add(new BasicNameValuePair("latitude", AppTokens.lat));
				}
				if (!LoadPref(AppTokens.GpsLongitude).equals("")) {
					params.add(new BasicNameValuePair("longitude", LoadPref(AppTokens.GpsLongitude)));
				} else {
					params.add(new BasicNameValuePair("longitude", AppTokens.lng));
				}
			}

			HttpReq ob = new HttpReq();
			response = ob.makeConnection(urls[0], 3, params);
			String output = "failure";
			L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray messageArray = new JSONArray(response);
					JSONObject messageObj = messageArray.getJSONObject(0);
					if (messageObj.optString("status").equals("success")) {
						JSONArray message = messageObj.getJSONArray("message");
						for (int i = 0; i < message.length(); i++) {
							JSONObject obj = message.getJSONObject(i);
							if (!obj.optString("coachId").equals("")) {
								JSONArray locArray = obj.getJSONArray("location");
								for (int j = 0; j < locArray.length(); j++) {
									JSONObject locObj = locArray.getJSONObject(j);
									if (locObj.optString("radius").equalsIgnoreCase("y")
											|| locObj.optString("localityAvailable").equalsIgnoreCase("y")) {
										output = "success";
										searchCoachData = message.toString();
									}
								}
							}
						} 
					}else if (messageObj.optString("status").equals("failure")) {
						output = messageObj.optString("message");
					}	
					

				} catch (JSONException e) {
					L.m("Json Error :" + e.toString());
					ErrorMessage = e.toString();
					return "server";
				}
			} else {
				L.m("Server Error : " + response);
				ErrorMessage = response;
				return "server";
			}

			return output;

		}

		@Override
		protected void onPostExecute(String result) {

			if (activity != null) {
				pBar.setVisibility(View.GONE);
				btn.setVisibility(View.VISIBLE);
				if (result != null) {
					L.m(result); 
					switch (result) {
					case "success":
						if (LoadPref(AppTokens.GpsUpdateStatue).equals("")) {
							Toast.makeText(getActivity(), getSt(R.string.search_coach_home_location_msg),
									Toast.LENGTH_SHORT).show();
						}
						if (acLocality.getText().toString().length() != 0
								&& tvRadious.getText().toString().equalsIgnoreCase("5")) {
							MainFindCoachesView.SWITCH = 2;
						} else {
							MainFindCoachesView.SWITCH = 1;
						}
						comm.onFindCoachesListener(true, sltSportId, sltSportName, sltSportBaseImage, searchCoachData);

						break;
					case "failure":
						Toast.makeText(getActivity(), "Information not available", Toast.LENGTH_SHORT).show();
						break;
					case "server":
						Button retry=showBaseServerErrorAlertBox(ErrorMessage);
						retry.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								// Close dialog
								if(dialog!=null)
								dialog.dismiss();
								if (isOnline()) {
									httpTask = new HttpTask(getActivity(), btnFindCoaches, progressBar);
									httpTask.execute(AppSettings.findCoach);
								} else {
									showInternetAlertBox(getActivity());
								}
							}
						});
						break;
					default:
						L.m(result);
						Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
						break;
					}
				}
			}
		}
	}

	public static ArrayList<String> autocomplete(String input) {
		ArrayList<String> resultList = null;

		HttpURLConnection conn = null;
		StringBuilder jsonResults = new StringBuilder();
		try {
			StringBuilder sb = new StringBuilder(
					AppTokens.PLACES_API_BASE + AppTokens.TYPE_AUTOCOMPLETE + AppTokens.OUT_JSON);
			sb.append("?key=" + AppTokens.API_KEY);
			sb.append("&components=country:in");
			sb.append("&components=city:dl");
			sb.append("&input=" + URLEncoder.encode(input, "utf8"));

			URL url = new URL(sb.toString());

			System.out.println("URL: " + url);
			conn = (HttpURLConnection) url.openConnection();
			InputStreamReader in = new InputStreamReader(conn.getInputStream());

			// Load the results into a StringBuilder
			int read;
			char[] buff = new char[1024];
			while ((read = in.read(buff)) != -1) {
				jsonResults.append(buff, 0, read);
			}
		} catch (MalformedURLException e) {
			Log.e("Find Buddyz", "Error processing Places API URL", e);
			return resultList;
		} catch (IOException e) {
			Log.e("Find Buddyz", "Error connecting to Places API", e);
			return resultList;
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}

		try {

			L.m(jsonResults.toString());
			// Create a JSON object hierarchy from the results
			JSONObject jsonObj = new JSONObject(jsonResults.toString());
			JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

			// Extract the Place descriptions from the results
			resultList = new ArrayList<String>(predsJsonArray.length());
			for (int i = 0; i < predsJsonArray.length(); i++) {
				String res = predsJsonArray.getJSONObject(i).getString("description");
				res = res.substring(0, res.length() - 7);
				System.out.println(res);
				System.out.println("============================================================");
				resultList.add(res);

			}
		} catch (JSONException e) {
			Log.e("Find Buddyz", "Cannot process JSON results", e);
		}

		return resultList;
	}

	public class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {
		private ArrayList<String> resultList;

		public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
			super(context, textViewResourceId);
		}

		@Override
		public int getCount() {
			return resultList.size();
		}

		@Override
		public String getItem(int index) {
			return resultList.get(index);
		}

		@Override
		public Filter getFilter() {
			Filter filter = new Filter() {
				@Override
				protected FilterResults performFiltering(CharSequence constraint) {
					FilterResults filterResults = new FilterResults();
					if (constraint != null) {
						// Retrieve the autocomplete results.
						resultList = autocomplete(constraint.toString());

						// Assign the data to the FilterResults
						filterResults.values = resultList;
						filterResults.count = resultList.size();
					}
					return filterResults;
				}

				@Override
				protected void publishResults(CharSequence constraint, FilterResults results) {
					if (results != null && results.count > 0) {
						notifyDataSetChanged();
					} else {
						notifyDataSetInvalidated();
					}
				}
			};
			return filter;
		}
	}

	
}
