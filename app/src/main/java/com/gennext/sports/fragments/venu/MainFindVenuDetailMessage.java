package com.gennext.sports.fragments.venu;

import java.util.ArrayList;
import java.util.List;

import com.gennext.sports.util.internet.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gennext.sports.R;
import com.gennext.sports.fragments.CompactFragment;
import com.gennext.sports.model.FindCoachMessageAdapter;
import com.gennext.sports.model.FindCoachModel;
import com.gennext.sports.util.AppSettings;
import com.gennext.sports.util.Buddy;
import com.gennext.sports.util.HttpReq;
import com.gennext.sports.util.L;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainFindVenuDetailMessage extends CompactFragment {

	ListView lvMain;
	String jsonData, venueId, sportId;
	ArrayList<FindCoachModel> myMsgList;
	LoadMessageList loadMessageList;
	Button btnSendReview;
//	private AlertDialog dialog = null;
	ProgressBar progressBar;
	SendMessage sendMessage;
	String TAG = "VenueDetailMessage : ";
	FindCoachMessageAdapter ExpAdapter;
	String tempMessgae="";

	public void setData(String JsonData, String venueId, String sportId) {
		this.jsonData = JsonData;
		this.venueId = venueId;
		this.sportId = sportId;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		L.m(TAG + "onAttach");
		if (loadMessageList != null) {
			loadMessageList.onAttach(activity);
		}
		if (sendMessage != null) {
			sendMessage.onAttach(activity);
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		L.m(TAG + "onDetach");
		if (loadMessageList != null) {
			loadMessageList.onDetach();
		}
		if (sendMessage != null) {
			sendMessage.onDetach();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		L.m(TAG + "onCreateView");
		View v = inflater.inflate(R.layout.main_find_venu_detail_message, container, false);

		lvMain = (ListView) v.findViewById(R.id.lv_main_find_venue_detail_messages);
		btnSendReview = (Button) v.findViewById(R.id.btn_main_find_venue_detail_messages);
		progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
		setTypsFace(btnSendReview);
		btnSendReview.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// sendReview = new SendReview(getActivity(), "5", "review");
				// sendReview.execute(AppSettings.setCoachReview);
				showMessageAlert(getActivity());
			}
		});
		// L.m(AppSettings.getVenueMessage);
		loadMessageList = new LoadMessageList(getActivity());
		loadMessageList.execute(AppSettings.receivePlayerVenueMessages);
		return v;
	}

	private class LoadMessageList extends AsyncTask<String, Void, String> {

		private Activity activity;

		public LoadMessageList(Activity activity) {
			this.activity = activity;
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error";

			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				// L.m("Player Id : "+LoadPref(AppTokens.PlayerId));
				params.add(new BasicNameValuePair("venueId", venueId));
				params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
			}

			HttpReq ob = new HttpReq();
			response = ob.makeConnection(urls[0], 1, params);

			myMsgList = new ArrayList<FindCoachModel>();
			L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray message = new JSONArray(response);
					JSONObject venueData = message.getJSONObject(0);
					if (venueData.optString("status").equalsIgnoreCase("success")) {
						JSONArray messageArr = venueData.getJSONArray("message");
						JSONObject messageObj = messageArr.getJSONObject(0);
						String venueName = messageObj.optString("venueName");

						JSONArray mainArr = messageObj.getJSONArray("message");
						for (int k = 0; k < mainArr.length(); k++) {
							JSONObject sPData = mainArr.getJSONObject(k);
							response = "success";
							if (sPData.optString("status").equalsIgnoreCase("s")) {
								FindCoachModel model = new FindCoachModel();
								model.setFullName("You");
								model.setMessage(sPData.optString("message"));
								model.setDate(sPData.optString("dateOfMsg"));
								model.setTime(sPData.optString("timeOfMsg"));
								model.setStatus(sPData.optString("status"));
								model.setClosedDay(viewTime());
								myMsgList.add(model);

							} else if (sPData.optString("status").equalsIgnoreCase("r")) {
								FindCoachModel model = new FindCoachModel();
								model.setFullName(venueName);
								model.setMessage(sPData.optString("message"));
								model.setDate(sPData.optString("dateOfMsg"));
								model.setTime(sPData.optString("timeOfMsg"));
								model.setStatus(sPData.optString("status"));

								myMsgList.add(model);
							}
						}
					} else if (venueData.optString("status").equalsIgnoreCase("failure")) {
						response = "failure";
						ErrorMessage=venueData.optString("message");
					}

				} catch (JSONException e) {
					L.m(e.toString());
					ErrorMessage = e.toString() + response;
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				ErrorMessage = response;
				return null;
			}

			return response;
			// return "success";

		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			if (activity != null) {

				progressBar.setVisibility(View.GONE);
				if (result != null) {
					if (result.equals("success")) {
						ExpAdapter = new FindCoachMessageAdapter(activity, R.layout.custom_slot_find_coach_message_left,
								myMsgList);
						lvMain.setAdapter(ExpAdapter);
					} else if (result.equals("failure")) {
						Toast.makeText(getActivity(), ErrorMessage, Toast.LENGTH_SHORT).show();
					}else{
						Toast.makeText(getActivity(), "No message available", Toast.LENGTH_SHORT).show();

					}

				} else {
					showServerErrorAlertBox(ErrorMessage);
				}
			}
		}
	}

	private class SendMessage extends AsyncTask<String, Void, String> {

		private Activity activity;
		String message;

		public SendMessage(Activity activity, String message) {
			this.message = message;
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showPDialog(getActivity(), "Processing please wait");
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error";
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				// L.m("Player Id : "+LoadPref(AppTokens.PlayerId));
				params.add(new BasicNameValuePair("venueId", venueId));
				params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
				params.add(new BasicNameValuePair("message", message));

			}

			HttpReq ob = new HttpReq();
			response = ob.makeConnection(urls[0], 3, params);
			String output = null;
			L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray json = new JSONArray(response);
					for (int i = 0; i < json.length(); i++) {
						JSONObject obj = json.getJSONObject(i);
						if (obj.optString("status").equals("success")) {
							output = "success";

						} else if (obj.optString("status").equals("failure")) {
							output = obj.optString("message");
						}
					}

				} catch (JSONException e) {
					L.m("Json Error :" + e.toString());
					ErrorMessage=e.toString()+response;
					return null;
					// return e.toString();
				}
			} else {
				L.m("Invalid JSON found : " + response);
				ErrorMessage=response;
				// return response;
				return null;
			}

			return output;
			// return "success";

		}

		@Override
		protected void onPostExecute(String result) {

			if (activity != null) {
				dismissPDialog();
				if (result != null) {
					L.m(result);
					if (result.equalsIgnoreCase("success")) {
						Toast.makeText(getActivity(), "Message Sent successfull", Toast.LENGTH_SHORT).show();
						addNewReviewInList(message, getDate(), viewTime(), "s");
						// loadMessageList=new LoadMessageList(getActivity());
						// loadMessageList.execute(AppSettings.getVenueMessage);
					} else {
						addNewReviewInList(message, getDate(), viewTime(), "s");
						Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
					}
				} else {
//					Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.internet_error_msg),
//							Toast.LENGTH_SHORT).show();
					Button retry=showBaseServerErrorAlertBox(ErrorMessage);
					retry.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View view) {
							if(dialog!=null)dialog.dismiss();
							sendMessage = new SendMessage(getActivity(),tempMessgae);
							sendMessage.execute(AppSettings.sendP2VMessage);
						}
					});
				}
			}
		}
	}

	public void showMessageAlert(Activity Act) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Act);

		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = Act.getLayoutInflater();

		View v = inflater.inflate(R.layout.custorm_find_coach_sendmessage, null);
		dialogBuilder.setView(v);
		Button sendButton = (Button) v.findViewById(R.id.btn_custorm_find_coach_sendmessage_send);
		final EditText etMessage = (EditText) v.findViewById(R.id.et_custorm_find_coach_sendmessage);

		sendButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				hideKeyboard(getActivity());
				tempMessgae=etMessage.getText().toString();
				sendMessage = new SendMessage(getActivity(),tempMessgae);
				sendMessage.execute(AppSettings.sendP2VMessage);
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}

	public void addNewReviewInList(String message, String dateOfMsg, String timeOfMsg, String status) {
		FindCoachModel model = new FindCoachModel();
		model.setFullName("You");
		model.setMessage(message);
		model.setDate(dateOfMsg);
		model.setTime(timeOfMsg);
		model.setStatus(status);
		model.setClosedDay(viewTime());
		myMsgList.add(model);
		if (ExpAdapter != null) {
			ExpAdapter.notifyDataSetChanged();

		}
	}

	public void showServerErrorAlertBox(String errorDetail) {
		showAlertBox(getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), 2, errorDetail);
	}

	public void showInternetAlertBox() {
		showAlertBox(getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), 2, null);
	}

	public void showAlertBox(String title, String Description, int noOfButtons, final String errorMessage) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = getActivity().getLayoutInflater();

		View v = inflater.inflate(R.layout.alert_dialog, null);
		dialogBuilder.setView(v);
		ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
		Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
		Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
		final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
		LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
		LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);
		ivAbout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (errorMessage != null) {
					tvDescription.setText(errorMessage);
				}
			}
		});

		tvTitle.setText(title);
		tvDescription.setText(Description);
		if (noOfButtons == 1) {
			button2.setVisibility(View.GONE);
			llBtn2.setVisibility(View.GONE);
		}
		button1.setText("Retry");
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				if (isOnline()) {
					loadMessageList = new LoadMessageList(getActivity());
					loadMessageList.execute(AppSettings.receivePlayerVenueMessages);

				} else {
					showInternetAlertBox(getActivity());
				}
			}
		});
		button2.setText("Cancel");
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}
}
