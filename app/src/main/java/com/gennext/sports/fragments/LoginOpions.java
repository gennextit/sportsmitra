package com.gennext.sports.fragments;

/**
 * Created by Admin on 12/18/2017.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.gennext.sports.MainActivity;
import com.gennext.sports.ProfileActivity;
import com.gennext.sports.R;
import com.gennext.sports.SignUp;
import com.gennext.sports.global.PopupAlert;
import com.gennext.sports.util.AppAnimation;
import com.gennext.sports.util.AppTokens;
import com.gennext.sports.util.AppUser;
import com.gennext.sports.util.Buddy;
import com.gennext.sports.util.Utility;

import org.json.JSONException;
import org.json.JSONObject;

//login without button
//        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));


public class LoginOpions extends CompactFragment {

    private CallbackManager callbackManager;
    private LoginButton loginButton;

    public static LoginOpions newInstance() {
        LoginOpions fragment = new LoginOpions();
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.screen_signup, container, false);
        initUi(v);
        return v;
    }


    private void initUi(View v) {
        callbackManager = CallbackManager.Factory.create();

        loginButton = (LoginButton) v.findViewById(R.id.fb_login_button);
        LinearLayout mobileSignUp = (LinearLayout) v.findViewById(R.id.ll_signup_with_mobile);
        LinearLayout facebookSignUp = (LinearLayout) v.findViewById(R.id.ll_signup_with_facebook);
        LinearLayout googleSignUp = (LinearLayout) v.findViewById(R.id.ll_signup_with_googlePlus);
        LinearLayout termsOfUser = (LinearLayout) v.findViewById(R.id.tearmsOfUse);
        loginButton.setReadPermissions("public_profile email");
        // If using in a fragment
        loginButton.setFragment(LoginOpions.this);

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
//                showToast(getContext(),loginResult.getAccessToken().toString());
                RequestData();
                // App code
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                showToast(getContext(),exception.toString());
                // App code
            }
        });

        facebookSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginButton.performClick();
            }
        });

        mobileSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((SignUp)getActivity()).OnClickSignInMobile();
            }
        });

        googleSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((SignUp)getActivity()).OnClickSignInGoogle();
            }
        });

        termsOfUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((SignUp)getActivity()).OnClickTermsOfUse();
            }
        });
    }




    public void RequestData(){
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                JSONObject json = response.getJSONObject();
                try {
                    if(json != null){
                        JSONObject picture = json.getJSONObject("picture");
                        JSONObject data = picture.getJSONObject("data");
                        String imageUrl = data.optString("url");
                        updateUi(json.getString("id"),json.getString("name"),json.getString("email"), imageUrl);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    PopupAlert.newInstance("Error","Response: "+ json.toString()+"\n\nError: "+e.toString(), PopupAlert.FRAGMENT)
                            .show(getFragmentManager(),"popupAlert");
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void updateUi(String id, String name, String email, String link) {
        AppUser.setBuddyName(getContext(),name);
        AppUser.setBuddyImage(getContext(),link);
        AppUser.setBuddyEmail(getContext(),email);
        Utility.SavePref(getContext(), Buddy.fbId, id);
        Utility.SavePref(getContext(), AppTokens.LoginStatus, "f");
        Utility.SavePref(getContext(), AppTokens.SessionSignup, "success");
        Intent intent = new Intent(getContext(), ProfileActivity.class);
        intent.putExtra("from", "fb");
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


}

