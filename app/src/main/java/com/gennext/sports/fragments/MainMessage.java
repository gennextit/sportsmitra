package com.gennext.sports.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.sports.GCMIntentService;
import com.gennext.sports.MainActivity;
import com.gennext.sports.R;
import com.gennext.sports.fragments.coaches.MainFindCoachesDetail;
import com.gennext.sports.fragments.venu.MainFindVenuDetail;
import com.gennext.sports.model.FindCoachModel;
import com.gennext.sports.model.FindVenueModel;
import com.gennext.sports.model.MessageAdapter;
import com.gennext.sports.model.MessageModel;
import com.gennext.sports.util.AppSettings;
import com.gennext.sports.util.AppTokens;
import com.gennext.sports.util.Buddy;
import com.gennext.sports.util.HttpReq;
import com.gennext.sports.util.L;
import com.gennext.sports.util.internet.BasicNameValuePair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainMessage extends CompactFragment {
	Button test;
	EditText et;
	ListView lvMain;
	ProgressBar progressBar;
	FragmentManager mannager;
	ArrayList<MessageModel> myMessageList;
	MessageAdapter adapter;
	HttpTask httpTask;
	GetCoachVenueDetailRequest getCoachVenueDetailRequest;
	int NOTIFICATION = 1, APPROVED = 2, REJECTED = 3;
	String searchCoachData,searchVenueData;
	ArrayList<FindCoachModel> myCoachList;
	ArrayList<FindVenueModel> myVenueList;
	OnMessageCounterChangeListener comm;
	
	public void setComm(Activity activity) {
		this.comm=(MainActivity) activity;
	}
	
	public interface OnMessageCounterChangeListener{
		public void onMessageCounterChangeListener(int MsgCounter);
	}
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (httpTask != null) {
			httpTask.onAttach(activity);
		}
		if (getCoachVenueDetailRequest != null) {
			getCoachVenueDetailRequest.onAttach(activity);
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (httpTask != null) {
			httpTask.onDetach();
		}
		if (getCoachVenueDetailRequest != null) {
			getCoachVenueDetailRequest.onDetach();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.main_message, container, false);
		mannager = getFragmentManager();
		initUi(v);


//		httpTask = new HttpTask(getActivity(), progressBar);
//		httpTask.execute();
		LoadMessageList();
		return v;
	}

	private void initUi(View v) {
		setActionBarOption(v);

		lvMain = (ListView) v.findViewById(R.id.lv_message_list);
		progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);

		lvMain.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {

				if (myMessageList != null) {
					if (myMessageList.get(position).getRequestId() != null) {
						removeCounter(myMessageList.get(position).getRequestId());
						getCoachVenueDetailRequest = new GetCoachVenueDetailRequest(getActivity(),
								myMessageList.get(position).getRequestId(),
								myMessageList.get(position).getStatusRecived());
						getCoachVenueDetailRequest.execute(); 
						myMessageList.remove(position);
						comm.onMessageCounterChangeListener(GCMIntentService.messageCounter=0);
					}
				}

			}
		});

		myMessageList = new ArrayList<MessageModel>();
		adapter = new MessageAdapter(getActivity(), R.layout.custom_notification_slot, myMessageList);
		lvMain.setAdapter(adapter);

	}
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		adapter.notifyDataSetChanged();
	}
	
	public void setActionBarOption(View view) {
		LinearLayout ActionBack, ActionHome;
		ActionBack = (LinearLayout) view.findViewById(R.id.ll_actionbar_back);
		ActionHome = (LinearLayout) view.findViewById(R.id.ll_actionbar_home);
//		TextView actionbarTitle=(TextView)view.findViewById(R.id.actionbar_title);
//		actionbarTitle.setText(setActionBarTitle("MESSAGE"));
		ActionBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				mannager.popBackStack("mainMessage", FragmentManager.POP_BACK_STACK_INCLUSIVE);

			}
		});
		ActionHome.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				mannager.popBackStack("mainRSSFeed", 0);
			}
		});
	}
	 
	
	public void removeCounter(String playerId) {
		String player = playerId;
		ArrayList<MessageModel> tempList = new ArrayList<MessageModel>();
		ArrayList<MessageModel> finalList = new ArrayList<MessageModel>();
		tempList = AppTokens.newMessage;
		finalList = tempList;
		for (int i = 0; i < tempList.size(); i++) {
			L.m("slt playerId"+playerId+" :: list "+tempList.get(i).getRequestId());
			if (tempList.get(i).getRequestId().equalsIgnoreCase(player)) {
				GCMIntentService.messageCounter--;
				finalList.remove(i);
			}
		}
		AppTokens.newMessage.clear();
		AppTokens.newMessage = finalList;
	}

	private class HttpTask extends AsyncTask<Void, Void, String> {
		ProgressBar pBar;
		Activity activity;

		public HttpTask(Activity activity, ProgressBar pBar) {
			this.pBar = pBar;
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		private void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(Void... urls) {
			String response = "failure";

			// List<BasicNameValuePair> params = new
			// ArrayList<BasicNameValuePair>();
			// if(activity!=null){
			// params.add(new BasicNameValuePair("playerId",
			// LoadPref(Buddy.PlayerId)));
			// }
			// HttpReq ob = new HttpReq();
			// response = ob.makeConnection(urls[0], HttpReq.GET, params);
			// String output = "not available";
			// L.m(response);
			// if (response.contains("[")) {
			// try {
			// JSONArray json = new JSONArray(response);
			// for (int i = 0; i < json.length(); i++) {
			// JSONObject obj = json.getJSONObject(i);
			// if(!obj.optString("senderId").equals("")){
			// output="success";
			// NotificationModel model = new NotificationModel();
			// model.setNotification("A buddy request has been sent to you by
			// ");
			// model.setRequestId(obj.optString("senderId"));
			// model.setRequestedName(obj.optString("senderName"));
			// myMessageList.add(model);
			// }else if(!obj.optString("status").equals("")){
			// output="failure";
			// ErrorMessage=obj.optString("message");
			// }
			// }
			//
			//
			// } catch (JSONException e) {
			// L.m("Json Error :" + e.toString());
			// ErrorMessage = e.toString()+"\n"+response;
			// return null;
			// }
			// } else {
			// L.m("Server Error : " + response);
			// ErrorMessage = response;
			// return null;
			// }

			ArrayList<MessageModel> tempList = new ArrayList<MessageModel>();
			tempList = AppTokens.newMessage;
			if (tempList != null) {
				for (MessageModel ob : tempList) {
					response="success";
					MessageModel data = new MessageModel();
					data.setRequestId(ob.getRequestId());
					data.setMessage(ob.getMessage());
					if(ob.getStatusRecived().equalsIgnoreCase("coach")){
						data.setRequestedName("Coach - "+ob.getRequestedName());
					}else{
						data.setRequestedName("Venue - "+ob.getRequestedName());
					}
					data.setStatusRecived(ob.getStatusRecived());
					myMessageList.add(data);
				}
			}
			return response;
			// return "success";

		}

		@Override
		protected void onPostExecute(String result) {
			if (activity != null) {
				pBar.setVisibility(View.GONE);
				if (result != null) {

					switch (result) {
					case "success":
						if (adapter != null) {
							adapter.notifyDataSetChanged(); 
						}
						break;
					case "failure":
							Toast.makeText(getActivity(), "There is No pending Message for you", Toast.LENGTH_SHORT)
									.show();
						 
						break;
					case "not available":

						break;
					}
				} 
			}

		}
	}
	
	private void LoadMessageList() {
		String response = "failure";
		progressBar.setVisibility(View.VISIBLE);
		ArrayList<MessageModel> tempList = new ArrayList<MessageModel>();
		tempList = AppTokens.newMessage;
		if (tempList != null) {
			for (MessageModel ob : tempList) {
				response="success";
				MessageModel data = new MessageModel();
				data.setRequestId(ob.getRequestId());
				data.setMessage(ob.getMessage());
				if(ob.getStatusRecived().equalsIgnoreCase("coach")){
					data.setRequestedName("Coach - "+ob.getRequestedName());
				}else{
					data.setRequestedName("Venue - "+ob.getRequestedName());
				}
				data.setStatusRecived(ob.getStatusRecived());
				myMessageList.add(data);
			}
		}
		
		progressBar.setVisibility(View.GONE);
		if (response != null) {

			switch (response) {
			case "success":
				if (adapter != null) {
					adapter.notifyDataSetChanged(); 
				}
				break;
			case "failure":
					Toast.makeText(getActivity(), "There is No pending Message for you", Toast.LENGTH_SHORT)
							.show();
				 
				break;
			case "not available":

				break;
			}
		} 
	}

	public void showServerErrorAlertBox1(String errorDetail, int option) {
		showAlertBox1(option, getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), 2, errorDetail);
	}

	public void showInternetAlertBox1() {
		showAlertBox1(0, getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), 2, null);
	}

	public void showAlertBox1(final int option, String title, String Description, int noOfButtons,
			final String errorMessage) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = getActivity().getLayoutInflater();

		View v = inflater.inflate(R.layout.alert_dialog, null);
		dialogBuilder.setView(v);
		ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
		Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
		Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
		final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
		LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
		LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);
		ivAbout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (errorMessage != null) {
					tvDescription.setText(errorMessage);
				}
			}
		});

		tvTitle.setText(title);
		tvDescription.setText(Description);
		if (noOfButtons == 1) {
			button2.setVisibility(View.GONE);
			llBtn2.setVisibility(View.GONE);
		}
		button1.setText("Retry");
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				if (isOnline()) {
					if (option == NOTIFICATION) {
						httpTask = new HttpTask(getActivity(), progressBar);
						httpTask.execute();
					} else if (option == APPROVED) {

					} else if (option == REJECTED) {

					}

				} else {
					showInternetAlertBox(getActivity());
				}
			}
		});
		button2.setText("Cancel");
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}

	

	private class GetCoachVenueDetailRequest extends AsyncTask<Void, Void, String> {

		private Activity activity;
		private String requestedId, statusRecived;
		private int position = 0;

		public GetCoachVenueDetailRequest(Activity activity, String requestedId, String statusRecived) {
			onAttach(activity);
			this.requestedId = requestedId;
			this.statusRecived = statusRecived;
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showPDialog(getActivity(), "Processing please wait");
		}

		@Override
		protected String doInBackground(Void... urls) {
			String response = "error", locality = null;
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			String url = "";
			if (statusRecived.equalsIgnoreCase("coach")) {
				if (activity != null) {
					params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
					params.add(new BasicNameValuePair("coachId", requestedId));
					url = AppSettings.getCoachProfile;
				}
			} else if (statusRecived.equalsIgnoreCase("venue")) {
				if (activity != null) {
					params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
					params.add(new BasicNameValuePair("venueId", requestedId));
					url = AppSettings.getVenueProfile;
				}
			}

			HttpReq ob = new HttpReq();
			response = ob.makeConnection(url, HttpReq.GET, params);
			String output = null;
			myCoachList = new ArrayList<FindCoachModel>();
			myVenueList = new ArrayList<FindVenueModel>();
			L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray messageArray = new JSONArray(response);
					JSONObject messageObj = messageArray.getJSONObject(0);
					if (messageObj.optString("status").equals("success")) {
						JSONArray message = messageObj.getJSONArray("message");
						searchCoachData = message.toString();
						searchVenueData = messageArray.toString();
						for (int i = 0; i < message.length(); i++) {
							JSONObject messageData = message.getJSONObject(i);
							if (!messageData.optString("coachId").equals("")) {
								output = "coach";
								FindCoachModel model = new FindCoachModel();
								model.setFavouriteCoach(messageData.optString("favouriteCoach"));
								model.setCoachId(messageData.optString("coachId"));
								model.setAbout(messageData.optString("about"));
								model.setFullName(messageData.optString("fullName"));
								model.setAge(messageData.optString("age"));
								model.setGender(messageData.optString("gender"));
								model.setKm(0.0f);
								model.setImageUrl(messageData.optString("imageUrl"));
								model.setCity(messageData.optString("address"));
								model.setRating(messageData.optString("rating"));
								model.setReview(messageData.optString("review"));
								locality = null;
								JSONArray locArr = messageData.getJSONArray("location");
								for (int k = 0; k < locArr.length(); k++) {
									JSONObject locObj = locArr.getJSONObject(k);
									if (!locObj.optString("locality").equals("")) {
										if (locality == null) {
											locality = locObj.optString("locality") + "  \n\n";
										} else {
											locality += locObj.optString("locality") + " \n\n";
										}
									}
								}
								model.setLocality(locality);
								myCoachList.add(model);
							}
							if (!messageData.optString("venueId").equals("")) {
								output = "venue";
								FindVenueModel model = new FindVenueModel();
								model.setFavouriteVenue(messageData.optString("favouriteVenue"));
								model.setVenueId(messageData.optString("venueId"));
								model.setVenueName(messageData.optString("venueName"));
								model.setImageUrl(messageData.optString("imageUrl"));
								model.setAbout(messageData.optString("about"));
								model.setHighlightOrDetails(messageData.optString("highlightOrDetails"));
								model.setOpenHours(messageData.optString("openHours"));
								model.setMembershipDetails(messageData.optString("membershipDetails"));
								model.setLocality(messageData.optString("locality"));
								model.setKm(null);
								model.setReview(messageData.optString("review"));
								model.setRating(messageData.optString("rating"));
								model.setLatitude(messageData.optString("latitude"));
								model.setLongitude(messageData.optString("longitude"));
								model.setAddress(messageData.optString("address"));
								myVenueList.add(model);
							}
						}
					} else if (messageObj.optString("status").equals("failure")) {
						output = messageObj.optString("message");
					}

				} catch (JSONException e) {
					L.m("Json Error :" + e.toString());
					ErrorMessage = e.toString() + response;
					return null;
					// return e.toString();
				}
			} else {
				L.m("Invalid JSON found : " + response);
				ErrorMessage = response;
				return null;
			}

			return output;
			// return "success";

		}

		@Override
		protected void onPostExecute(String result) {

			if (activity != null) {
				dismissPDialog();
				if (result != null) {
					if (result.equalsIgnoreCase("coach")) {
						MainFindCoachesDetail mainFindCoachesDetail = new MainFindCoachesDetail();
						if (myCoachList != null) {
							MainFindCoachesDetail.SWITCH = 2;
							mainFindCoachesDetail.setData(myCoachList.get(position).getFavouriteCoach(),
									myCoachList.get(position).getAbout(), myCoachList.get(position).getLocality(), "",
									"");

							mainFindCoachesDetail.setPlayerProfile(myCoachList.get(position).getCoachId(),
									myCoachList.get(position).getFullName(), myCoachList.get(position).getImageUrl(),
									myCoachList.get(position).getReview(), myCoachList.get(position).getRating());
							mainFindCoachesDetail.setJSONData(searchCoachData);
							FragmentManager mannager = getFragmentManager();
							FragmentTransaction transaction = mannager.beginTransaction();
							transaction.replace(android.R.id.content, mainFindCoachesDetail, "mainFindCoachesDetail");
							transaction.addToBackStack("mainFindCoachesDetail");
							transaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
							transaction.commit();
						}
					} else if (result.equalsIgnoreCase("venue")) {
						MainFindVenuDetail mainFindVenuDetail = new MainFindVenuDetail();
						if (myVenueList != null) {
							//L.m("Latti : " + myVenueList.get(position).getLatitude());
							MainFindVenuDetail.SWITCH=5;
							mainFindVenuDetail.setJSONData(searchVenueData);
							mainFindVenuDetail.setData(myVenueList.get(position).getFavouriteVenue(),
									myVenueList.get(position).getAbout(), myVenueList.get(position).getHighlightOrDetails(),
									myVenueList.get(position).getOpenHours(), myVenueList.get(position).getMembershipDetails(),
									"", "");

							mainFindVenuDetail.setVenueProfile(myVenueList.get(position).getVenueId(),
									myVenueList.get(position).getVenueName(), myVenueList.get(position).getImageUrl(),
									myVenueList.get(position).getReview(), myVenueList.get(position).getRating(),
									myVenueList.get(position).getLatitude(), myVenueList.get(position).getLongitude());
							mainFindVenuDetail.setVenueAddress(myVenueList.get(position).getAddress());

							FragmentManager mannager = getFragmentManager();
							FragmentTransaction tr2 = mannager.beginTransaction();
							tr2.replace(android.R.id.content, mainFindVenuDetail, "mainFindVenuDetail");
							tr2.addToBackStack("mainFindVenuDetail");
							tr2.commit();
						}
					}else {
						Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
					}
				} else {
					showServerErrorAlertBox1(ErrorMessage, APPROVED);
				}
			}
		}

	}
 
}
