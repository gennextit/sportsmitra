package com.gennext.sports.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gennext.sports.R;
import com.gennext.sports.fragments.venu.MainFindVenuDetail;
import com.gennext.sports.model.FindVenuAdapter;
import com.gennext.sports.model.FindVenueModel;
import com.gennext.sports.util.AppSettings;
import com.gennext.sports.util.Buddy;
import com.gennext.sports.util.DividerItemDecoration;
import com.gennext.sports.util.HttpReq;
import com.gennext.sports.util.L;
import com.gennext.sports.util.internet.BasicNameValuePair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyFavVenue extends CompactFragment {
	Button test;
	EditText et;
	RecyclerView lvMain;
	ProgressBar progressBar;
	FragmentManager mannager;
	ArrayList<FindVenueModel> mySportList;
	FindVenuAdapter adapter;
	String searchVenueJsonData;
	HttpTask httpTask;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (httpTask != null) {
			httpTask.onAttach(activity);
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (httpTask != null) {
			httpTask.onDetach();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.favourate_coach_venu_layout, container, false);
		mannager = getFragmentManager();
		initUi(v);

		httpTask = new HttpTask(getActivity(), progressBar);
		httpTask.execute(AppSettings.getFavouriteVenue);

		return v;
	}

	private void initUi(View v) {
		setActionBarOption(v);

		lvMain = (RecyclerView) v.findViewById(R.id.lv_main);
		LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
		lvMain.setLayoutManager(horizontalManager);
		lvMain.addItemDecoration(new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL_LIST));
		lvMain.setItemAnimator(new DefaultItemAnimator());

		progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);

	}

	public void onItemSelect(FindVenueModel item) {
		MainFindVenuDetail mainFindVenuDetail = new MainFindVenuDetail();
		L.m("Latti : " + item.getLatitude());
		mainFindVenuDetail.setJSONData(searchVenueJsonData);
		mainFindVenuDetail.setData("y", item.getAbout(),
				item.getHighlightOrDetails(), item.getOpenHours(),
				item.getMembershipDetails(), "", "");

		mainFindVenuDetail.setVenueProfile(item.getVenueId(),
				item.getVenueName(), item.getImageUrl(),
				item.getReview(), item.getRating(),
				item.getLatitude(), item.getLongitude());
		mainFindVenuDetail.setVenueAddress(item.getAddress());

		FragmentManager mannager = getFragmentManager();
		FragmentTransaction tr2 = mannager.beginTransaction();
		tr2.replace(android.R.id.content, mainFindVenuDetail, "mainFindVenuDetail");
		tr2.addToBackStack("mainFindVenuDetail");
		tr2.commit();
	}

	public void setActionBarOption(View view) {
		LinearLayout ActionBack, ActionHome;
		ActionBack = (LinearLayout) view.findViewById(R.id.ll_actionbar_back);
		ActionHome = (LinearLayout) view.findViewById(R.id.ll_actionbar_home);
		TextView actionbarTitle=(TextView)view.findViewById(R.id.actionbar_title);
		actionbarTitle.setText("MY FAVOURITE VENUES");
		ActionBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				mannager.popBackStack("myFavVenue", FragmentManager.POP_BACK_STACK_INCLUSIVE);
			}
		});
		ActionHome.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				mannager.popBackStack("mainRSSFeed", 0);
			}
		});
	}

	private class HttpTask extends AsyncTask<String, Void, String> {
		ProgressBar pBar;
		Activity activity;

		public HttpTask(Activity activity, ProgressBar pBar) {
			this.pBar = pBar;
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		private void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error";
			mySportList = new ArrayList<FindVenueModel>();
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
			}
			if (searchVenueJsonData == null) {
				HttpReq ob = new HttpReq();
				response = ob.makeConnection(urls[0], HttpReq.GET, params);
			} else {
				response = searchVenueJsonData;
			}
			String output = "not available", locality = null;
			L.m(response);
			if (response.contains("[")) {
				searchVenueJsonData = response;
				try {
					JSONArray messageArray = new JSONArray(response);
					JSONObject messageObj = messageArray.getJSONObject(0);
					if (messageObj.optString("status").equals("success")) {
						JSONArray message = messageObj.getJSONArray("message");
						for (int j = 0; j < message.length(); j++) {
							JSONObject messageData = message.getJSONObject(j);
							L.m(messageData.toString());
							if (!messageData.optString("venueId").equals("")) {
								output = "success";
								FindVenueModel model = new FindVenueModel();
								model.setVenueId(messageData.optString("venueId"));
								model.setVenueName(messageData.optString("venueName"));
								model.setImageUrl(messageData.optString("imageUrl"));
								model.setAbout(messageData.optString("about"));
								model.setHighlightOrDetails(messageData.optString("highlightOrDetails"));
								model.setOpenHours(messageData.optString("openHours"));
								model.setMembershipDetails(messageData.optString("membershipDetails"));
								model.setLocality(messageData.optString("locality"));
								model.setKm(null);
								model.setReview(messageData.optString("review"));
								model.setRating(messageData.optString("rating"));
								model.setLatitude(messageData.optString("latitude"));
								model.setLongitude(messageData.optString("longitude"));
								model.setAddress(messageData.optString("address"));
								mySportList.add(model);

							}

						}
					} else if (messageObj.optString("status").equals("failure")) {
						output = messageObj.optString("message");
					}
				} catch (JSONException e) {
					L.m("Json Error :" + e.toString());
					ErrorMessage = e.toString() + "\n" + response;
					return null;
				}
			} else {
				L.m("Server Error : " + response);
				ErrorMessage = response;
				return null;
			}

			return output;
			// return "success";

		}

		@Override
		protected void onPostExecute(String result) {
			if (activity != null) {
				pBar.setVisibility(View.GONE);
				if (result != null) {
					if (result.equals("success")) {

						FindVenuAdapter ExpAdapter = new FindVenuAdapter(getActivity(), mySportList, MyFavVenue.this);
						lvMain.setAdapter(ExpAdapter);
					} else {
						mySportList = null;
						setErrorAdapter(lvMain);
					}
				} else {
					showServerErrorAlertBox(ErrorMessage);
				}
			}

		}
	}

	public void showServerErrorAlertBox(String errorDetail) {
		showAlertBox(getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), 2, errorDetail);
	}

	public void showInternetAlertBox() {
		showAlertBox(getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), 2, null);
	}

	public void showAlertBox(String title, String Description, int noOfButtons, final String errorMessage) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = getActivity().getLayoutInflater();

		View v = inflater.inflate(R.layout.alert_dialog, null);
		dialogBuilder.setView(v);
		ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
		Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
		Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
		final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
		LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
		LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);
		ivAbout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (errorMessage != null) {
					tvDescription.setText(errorMessage);
				}
			}
		});

		tvTitle.setText(title);
		tvDescription.setText(Description);
		if (noOfButtons == 1) {
			button2.setVisibility(View.GONE);
			llBtn2.setVisibility(View.GONE);
		}
		button1.setText("Retry");
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				if (isOnline()) {
					httpTask = new HttpTask(getActivity(), progressBar);
					httpTask.execute(AppSettings.findMySbuddy);

				} else {
					showInternetAlertBox(getActivity());
				}
			}
		});
		button2.setText("Cancel");
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}

}
