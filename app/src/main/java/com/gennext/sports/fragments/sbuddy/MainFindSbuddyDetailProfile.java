package com.gennext.sports.fragments.sbuddy;

import com.gennext.sports.R;
import com.gennext.sports.fragments.CompactFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MainFindSbuddyDetailProfile extends CompactFragment{
	
	String about,location,clubNApartment,city;
	TextView tvAbout,tvLocation,tvClubNApartment,tvCity;
	
	public void setData(String about,String location,String clubNApartment,String city){
		this.about=about;
		this.location=location;
		this.clubNApartment=clubNApartment;
		this.city=city;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v =inflater.inflate(R.layout.main_find_sbuddy_detail_profile, container,false);
		
		tvAbout=(TextView)v.findViewById(R.id.tv_main_find_sbuddy_detail_about);
		tvLocation=(TextView)v.findViewById(R.id.tv_main_find_sbuddy_detail_location);
		tvClubNApartment=(TextView)v.findViewById(R.id.tv_main_find_sbuddy_detail_clubApartment);
		tvCity=(TextView)v.findViewById(R.id.tv_main_find_sbuddy_detail_city);
	
		if(about!=null){
			tvAbout.setText(about);
		}
		if(location!=null){
			tvLocation.setText(location);
		}
		if(clubNApartment!=null){
			tvClubNApartment.setText(clubNApartment);
		}
		if(city!=null){
			tvCity.setText(city);
		}
		
		return v;
	}
	
	 
}
