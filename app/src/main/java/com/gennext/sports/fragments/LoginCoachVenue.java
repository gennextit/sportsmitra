package com.gennext.sports.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.sports.R;
import com.gennext.sports.util.AppSettings;
import com.gennext.sports.util.AppTokens;
import com.gennext.sports.util.Coach;
import com.gennext.sports.util.HttpReq;
import com.gennext.sports.util.L;
import com.gennext.sports.util.Validation;
import com.gennext.sports.util.Venue;
import com.gennext.sports.util.WakeLocker;
import com.gennext.sports.util.internet.BasicNameValuePair;
import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.gennext.sports.util.AppSettings.USER_GUIDE_TC;
import static com.gennext.sports.util.AppSettings.coachLogin;
import static com.gennext.sports.util.AppSettings.venueLogin;

public class LoginCoachVenue extends CompactFragment implements View.OnClickListener {

	private EditText etEmail, etPass;
	private Button btnSignIn;
	ImageView tab1, tab2;
	TextView tvTab1, tvTab2, tvForgotPassword;
	private ProgressBar progressBar;
	private OnLoginCoachVenueListener comm;
	Boolean isSoftKeyboardDisplayed = false;
	HttpTask httpTask;
	String JsonData;
	static final int TAB_COACH = 1, TAB_VENUE = 2;
	int SWITCH;
	private String gcmRegId;
	private Activity act;
	FragmentManager mannager;
	String tempMobile,tempUserId;
	ForgotPasswordRequest forgotPasswordRequest;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		this.act = activity;
		if (httpTask != null) {
			httpTask.onAttach(activity);
		}
		if (forgotPasswordRequest != null) {
			forgotPasswordRequest.onAttach(activity);
		}

	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (httpTask != null) {
			httpTask.onDetach();
		}
		if (forgotPasswordRequest != null) {
			forgotPasswordRequest.onDetach();
		}
	}

	public void setCommunicator(OnLoginCoachVenueListener onLoginCoachVenueListener) {
		this.comm = onLoginCoachVenueListener;
	}

	public interface OnLoginCoachVenueListener {
		public void onLoginCoachVenueListener(String CoachJsonData, int CoachVenueStatus);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.login_coach_venue, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		initUi(view);
		mannager = getFragmentManager();
		setActionBarOption(view);

		getActivity().registerReceiver(mHandleMessageReceiver, new IntentFilter(AppTokens.DISPLAY_MESSAGE_ACTION));
		// Getting status
		int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());

		// Showing status
		if (status == ConnectionResult.SUCCESS) {
			gcmRegId = getGCMRegId();
		} else {
			Toast.makeText(getActivity(),
					"This app won't run without Google Play services, which are missing from your phone.",
					Toast.LENGTH_SHORT).show();
			// SavePref(AppTokens.GCM_ID, "test");
		}
	}

	private void initUi(View view) {
		etEmail = (EditText) view.findViewById(R.id.et_login_coach_venue_email);
		etPass = (EditText) view.findViewById(R.id.et_login_coach_venue_password);
		tab1 = (ImageView) view.findViewById(R.id.iv_login_coach_venue_tab1);
		tab2 = (ImageView) view.findViewById(R.id.iv_login_coach_venue_tab2);
		tvTab1 = (TextView) view.findViewById(R.id.tv_login_coach_venue_tab1);
		tvTab2 = (TextView) view.findViewById(R.id.tv_login_coach_venue_tab2);
		tvForgotPassword = (TextView) view.findViewById(R.id.tv_login_coach_venue_forgotpassword);
		btnSignIn = (Button) view.findViewById(R.id.btn_login_coach_venue_signin);
		progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);

		setTypsFace(etEmail);
		setTypsFace(etPass);
		setTypsFace(btnSignIn);
		LinearLayout tearmsAndUse = (LinearLayout) view.findViewById(R.id.tearmsOfUse);
		tearmsAndUse.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				AppWebView appWebView = new AppWebView();
				appWebView.setUrl(getResources().getString(R.string.t_and_c), USER_GUIDE_TC, AppWebView.URL);
				mannager = getFragmentManager();
				FragmentTransaction transaction = mannager.beginTransaction();
				transaction.add(android.R.id.content, appWebView, "appWebView");
				transaction.addToBackStack("appWebView");
				transaction.commit();
			}
		});
		tab1.setOnClickListener(this);
		tab2.setOnClickListener(this);
		btnSignIn.setOnClickListener(this);
		tvForgotPassword.setOnClickListener(this);
		etEmail.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub
				isSoftKeyboardDisplayed = true;
				return false;
			}
		});
		switchTab(TAB_COACH);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_login_coach_venue_tab1:// TAB 1 Click
			switchTab(TAB_COACH);
			break;
		case R.id.iv_login_coach_venue_tab2:// TAB 2 Click
			switchTab(TAB_VENUE);
			break;
		case R.id.btn_login_coach_venue_signin:// btnSignIn Click
			if (checkValidation()) {
				if (isSoftKeyboardDisplayed) {
					hideKeybord();
				}
				httpTask = new HttpTask(getActivity(), btnSignIn, progressBar,etEmail.getText().toString(),etPass.getText().toString());
				if (SWITCH == TAB_COACH) {
					httpTask.execute(coachLogin);
				} else {
					httpTask.execute(venueLogin);
				}
			}
			break;
		case R.id.tv_login_coach_venue_forgotpassword:// Forgot password Click
			showForgotPassAlertBox(etEmail.getText().toString());
			break;

		}
	}

	public void setActionBarOption(View view) {
		LinearLayout ActionBack, ActionHome;
		ActionBack = (LinearLayout) view.findViewById(R.id.ll_actionbar_back);
		// TextView
		// actionbarTitle=(TextView)view.findViewById(R.id.actionbar_title);
		// actionbarTitle.setText(setActionBarTitle("SIGN IN"));
		ActionBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				mannager.popBackStack("loginCoachVenue", FragmentManager.POP_BACK_STACK_INCLUSIVE);
			}
		});

	}

	public String getGCMRegId() {
		String regId;
		// Make sure the device has the proper dependencies.
		GCMRegistrar.checkDevice(act);

		// Make sure the manifest was properly set - comment out this line
		// while developing the app, then uncomment it when it's ready.
		GCMRegistrar.checkManifest(act);

		// Get GCM registration id
		regId = GCMRegistrar.getRegistrationId(act);
		Log.e("@-regId", regId);
		// Check if regid already presents
		if (regId.equals("")) {
			// Registration is not present, register now with GCM
			GCMRegistrar.register(act, AppTokens.SENDER_ID);
			regId = GCMRegistrar.getRegistrationId(act);
			return regId;
		} else {
			gcmRegId = regId;
			SavePref(AppTokens.GCM_ID, regId);
		}
		return regId;
	}

	private boolean checkValidation() {
		boolean ret = true;

		if (!Validation.isEmpty(etEmail, true)) {
			return false;
		} else if (!Validation.isEmpty(etPass, true)) {
			return false;
		}
		return ret;
	}

	private void switchTab(int key) {

		switch (key) {
		case 1:
			SWITCH = TAB_COACH;
			tab1.setColorFilter(getResources().getColor(R.color.sbuddy_orange));
			tab1.setBackgroundResource(R.drawable.rounded_login_tab_orange);
			tvTab1.setTextColor(getResources().getColor(R.color.sbuddy_orange));
			tab2.setColorFilter(getResources().getColor(R.color.mySport_bg_grey));
			tab2.setBackgroundResource(R.drawable.rounded_login_tab_grey);
			tvTab2.setTextColor(getResources().getColor(R.color.mySport_bg_grey));
			break;
		case 2:
			SWITCH = TAB_VENUE;
			tab1.setColorFilter(getResources().getColor(R.color.mySport_bg_grey));
			tab1.setBackgroundResource(R.drawable.rounded_login_tab_grey);
			tvTab1.setTextColor(getResources().getColor(R.color.mySport_bg_grey));
			tab2.setColorFilter(getResources().getColor(R.color.sbuddy_orange));
			tab2.setBackgroundResource(R.drawable.rounded_login_tab_orange);
			tvTab2.setTextColor(getResources().getColor(R.color.sbuddy_orange));
			break;

		}
	}

	private class HttpTask extends AsyncTask<String, Void, String> {
		Button btn;
		ProgressBar pBar;
		Activity activity;
		String email,password;
		public void onAttach(Activity activity) {
			// TODO Auto-generated method stub
			this.activity = activity;

		}

		public void onDetach() {
			// TODO Auto-generated method stub
			this.activity = null;

		}

		public HttpTask(Activity activity, Button btn, ProgressBar pBar,String email,String password) {
			this.activity = activity;
			this.btn = btn;
			this.pBar = pBar;
			this.email=email;
			this.password=password;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pBar.setVisibility(View.VISIBLE);
			btn.setVisibility(View.GONE);
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error";

			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			params.add(new BasicNameValuePair("loginId", email));
			params.add(new BasicNameValuePair("password", password));
			if (activity != null) {
				if (!LoadPref(AppTokens.GCM_ID).equals("")) {
					L.m("A :: " + gcmRegId);
					params.add(new BasicNameValuePair("gcmId", LoadPref(AppTokens.GCM_ID)));
				} else {
					L.m("B :: " + gcmRegId);
					params.add(new BasicNameValuePair("gcmId", "test_parameter_from_mobile"));
				}
			}

			L.m(urls[0]);
			HttpReq ob = new HttpReq();
			response = ob.makeConnection(urls[0], HttpReq.POST, params);
			String MSG = null;
			L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray json = new JSONArray(response);
					for (int i = 0; i < json.length(); i++) {
						JSONObject obj = json.getJSONObject(i);
						if (!obj.optString("status").equals("")
								&& obj.optString("status").equalsIgnoreCase("success")) {
							JSONArray coachDetail = obj.getJSONArray("message");
							JSONObject cDetail = coachDetail.getJSONObject(0);
							if (activity != null) {
								if (SWITCH == TAB_COACH) {
									SavePref(Coach.CoachId, cDetail.optString("coachId"));
									SavePref(Coach.Name, cDetail.optString("fullName"));
									SavePref(Coach.Image, cDetail.optString("imageUrl"));
									SavePref(Coach.noOfCoins, cDetail.optString("noOfCoins"));

								} else {
									SavePref(Venue.VenueId, cDetail.optString("venueId"));
									SavePref(Venue.Name, cDetail.optString("venueName"));
									SavePref(Venue.Image, cDetail.optString("imageUrl"));
									SavePref(Venue.noOfCoins, cDetail.optString("noOfCoins"));

								}
							}
							JsonData = coachDetail.toString();
							MSG = "success";
						} else if (!obj.optString("status").equals("")
								&& obj.optString("status").equalsIgnoreCase("failure")) {
							MSG = obj.optString("message");
						}
					}

				} catch (JSONException e) {
					L.m("Json Errszwor :" + e.toString());
					ErrorMessage = e.toString() + response;
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				ErrorMessage = response;
				return null;
			}

			return MSG;

		}

		@Override
		protected void onPostExecute(String result) {

			pBar.setVisibility(View.GONE);
			btn.setVisibility(View.VISIBLE);
			if (activity != null) {
				if (result != null) {
					L.m(result);
					if (result.equalsIgnoreCase("success")) {
						if (SWITCH == TAB_COACH) {
							comm.onLoginCoachVenueListener(JsonData, TAB_COACH);
						} else {
							comm.onLoginCoachVenueListener(JsonData, TAB_VENUE);
						}

					} else {
						Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
					}
				} else {
					Button retry = showBaseServerErrorAlertBox(ErrorMessage);
					retry.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// Close dialog
							dialog.dismiss();
							if (isOnline()) {

								httpTask = new HttpTask(getActivity(), btnSignIn, progressBar,etEmail.getText().toString(),etPass.getText().toString());
								//httpTask.execute(SET_PLAYER_REGISTRATION_BY_MOBILE);
								if (SWITCH == TAB_COACH) {
									httpTask.execute(coachLogin);
								} else {
									httpTask.execute(venueLogin);
								}
							} else {
								showInternetAlertBox(getActivity());
							}
						}
					});
				}
			}
		}
	}

	public void showForgotPassAlertBox(final String userId) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = getActivity().getLayoutInflater();

		View v = inflater.inflate(R.layout.custom_dialog_forgot_password, null);
		dialogBuilder.setView(v);
		Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
		Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
		final EditText etUserId = (EditText) v.findViewById(R.id.et_forgot_password);
		final EditText etMobile = (EditText) v.findViewById(R.id.et_forgot_mobile);
		etUserId.setText(userId);

		button1.setText("Submit");
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				tempUserId=etUserId.getText().toString();
				tempMobile=etMobile.getText().toString();
				forgotPasswordRequest = new ForgotPasswordRequest(getActivity(), tempUserId,
						tempMobile);
				switch(SWITCH){
					case TAB_COACH:
						forgotPasswordRequest.execute(AppSettings.forgetCoachPassword);
						break;
					case TAB_VENUE:
						forgotPasswordRequest.execute(AppSettings.forgetVenuePassword);
						break;

				}
			}
		});

		button2.setText("Cancel");
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();
	}

	private class ForgotPasswordRequest extends AsyncTask<String, Void, String> {

		private Activity activity;
		private String userId;
		private String mobile;

		public ForgotPasswordRequest(Activity activity, String userId, String mobile) {
			onAttach(activity);
			this.userId = userId;
			this.mobile = mobile;
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showPDialog(getActivity(), "Processing, please wait.");
		}

		@Override
		protected String doInBackground(String... urls) {
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				// sportId,choosePlan, chooseDate, chooseSlot, playerId, venueId
				params.add(new BasicNameValuePair("userId", userId));
				params.add(new BasicNameValuePair("mobileNumber", mobile));

			}

			HttpReq ob = new HttpReq();

			return ob.makeConnection(urls[0], HttpReq.POST, params, HttpReq.EXECUTE_TASK);

		}

		@Override
		protected void onPostExecute(String result) {

			if (activity != null) {
				dismissPDialog();
				if (result != null) {
					L.m(result);
					if (result.equalsIgnoreCase("success")) {
						showReviewAlertSuccess(getActivity(), "forgotPassword", -1);
					} else {
						Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
					}
				} else {
					Button retry=showBaseServerErrorAlertBox(ErrorMessage);
					retry.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View view) {
							if(dialog!=null)dialog.dismiss();
							forgotPasswordRequest = new ForgotPasswordRequest(getActivity(), tempUserId,
									tempMobile);
							switch(SWITCH) {
								case TAB_COACH:
									forgotPasswordRequest.execute(AppSettings.forgetCoachPassword);
									break;
								case TAB_VENUE:
									forgotPasswordRequest.execute(AppSettings.forgetVenuePassword);
									break;
							}
						}
					});

				}
			}
		}
	}

	/**
	 * Receiving push messages
	 */
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String newMessage = intent.getExtras().getString(AppTokens.EXTRA_MESSAGE);
			// Waking up mobile if it is sleeping
			WakeLocker.acquire(getActivity().getApplicationContext());

			/**
			 * Take appropriate action on this message depending upon your app
			 * requirement For now i am just displaying it on the screen
			 */

			// Showing received message
			Toast.makeText(getActivity().getApplicationContext(), "New Message: " + newMessage, Toast.LENGTH_LONG)
					.show();

			// Releasing wake lock
			WakeLocker.release();
		}
	};

	@Override
	public void onDestroy() {
		try {
			getActivity().unregisterReceiver(mHandleMessageReceiver);
			GCMRegistrar.onDestroy(getActivity());
		} catch (Exception e) {
			L.m("UnRegister Receiver Error"+ "> " + e.getMessage());
		}
		super.onDestroy();
	}

}
