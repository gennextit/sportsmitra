package com.gennext.sports.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gennext.sports.GCMIntentService;
import com.gennext.sports.MainActivity;
import com.gennext.sports.R;
import com.gennext.sports.image_cache.ImageLoader;
import com.gennext.sports.model.MyEventModel;
import com.gennext.sports.model.MyEventSlider;
import com.gennext.sports.model.RSSFeedAdapter;
import com.gennext.sports.model.RSSFeedAdvanceAdapter;
import com.gennext.sports.model.RSSFeedModel;
import com.gennext.sports.util.AppSettings;
import com.gennext.sports.util.AppTokens;
import com.gennext.sports.util.AutoScrollViewPager;
import com.gennext.sports.util.internet.BasicNameValuePair;
import com.gennext.sports.util.Buddy;
import com.gennext.sports.util.HttpReq;
import com.gennext.sports.util.L;
import com.gennext.sports.util.WakeLocker;
import com.google.android.gcm.GCMRegistrar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainRSSFeed extends CompactFragment {

	ProgressBar progressBar;
	public ArrayList<RSSFeedModel> rssList;
	public ArrayList<String> errorList;
	RssFeedTask rssFeedTask;
	String TAG = "MainRSSFeeds ";
	ImageView ivMenu;
	LinearLayout llNavDrawer, llMessage, llAlert;
	ImageLoader imageLoader;
	FragmentManager mannager;
	TextView tvMessage, tvNotification;
	AutoScrollViewPager viewPager;
	SliderTask sliderTask;
	RSSFeedAdapter adapter;
	public static boolean oneTimeLoadSports;
	private RecyclerView lmMain;
	private RSSFeedAdvanceAdapter adapter2;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (rssFeedTask != null) {
			rssFeedTask.onAttach(activity);
		}
		if (sliderTask != null) {
			sliderTask.onAttach(activity);
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (rssFeedTask != null) {
			rssFeedTask.onDetach(); 
		}
		if (sliderTask != null) {
			sliderTask.onDetach(); 
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		// L.m(TAG + "onCreateView");
		View v = inflater.inflate(R.layout.main_rss_feed_2, container, false);
		imageLoader = new ImageLoader(getActivity());
		mannager = getFragmentManager();
//		gridView = (GridView) v.findViewById(R.id.gv_main_rss_feed);
		ivMenu = (ImageView) v.findViewById(R.id.actionbar_back);
		llNavDrawer = (LinearLayout) v.findViewById(R.id.ll_nav_drawer);
		llAlert = (LinearLayout) v.findViewById(R.id.ll_actionbar_alert);
		llMessage = (LinearLayout) v.findViewById(R.id.ll_actionbar_message);
		tvMessage = (TextView) v.findViewById(R.id.actionbar_message_counter);
		tvNotification = (TextView) v.findViewById(R.id.actionbar_notification_counter);
//		TextView actionbarTitle=(TextView)v.findViewById(R.id.actionbar_title);
//		actionbarTitle.setText(setActionBarTitle("HOME"));
		
		getActivity().registerReceiver(mHandleMessageReceiver, new IntentFilter(AppTokens.DISPLAY_MESSAGE_ACTION));

		viewPager = (AutoScrollViewPager) v.findViewById(R.id.myviewpager);

//		 L.m("image size NOW - " + String.valueOf(viewPager.getWidth()) 
//         + " : " + String.valueOf(viewPager.getHeight()));
		// viewPager.setCycle(false);
		// the more properties whose you can set
		// // set whether stop auto scroll when touching, default is true
		// viewPager.setStopScrollWhenTouch(false);
		// // set whether automatic cycle when auto scroll reaching the last or
		// first item
		// // default is true
		// viewPager.setCycle(false);
		// /** set auto scroll direction, default is AutoScrollViewPager#RIGHT
		// **/
		// viewPager.setDirection(AutoScrollViewPager.LEFT);
		// // set how to process when sliding at the last or first item
		// // default is AutoScrollViewPager#SLIDE_BORDER_NONE
		// viewPager.setBorderProcessWhenSlide(AutoScrollViewPager.SLIDE_BORDER_CYCLE);
		// viewPager.setScrollDurationFactor(3);
		// viewPager.setBorderAnimation(true);

		// viewPager.setCurrentItem(Integer.MAX_VALUE / 2 - Integer.MAX_VALUE /
		// 2 % ListUtils.getSize(imageIdList));

		// viewPager.setCurrentItem(1);
		SetDrawer(llNavDrawer, v, imageLoader);
		setNotiMsgCounter();

		lmMain = (RecyclerView) v.findViewById(R.id.grid);
//		lmMain.setHasFixedSize(true);
		//set layout manager and adapter for "GridView"
		GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
		lmMain.setLayoutManager(layoutManager);



		llAlert.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				FragmentTransaction transaction;
				MainNotification mainNotification = new MainNotification();
				mainNotification.setComm(getActivity());
				transaction = mannager.beginTransaction();
				transaction.replace(android.R.id.content, mainNotification, "mainNotification");
				transaction.addToBackStack("mainNotification");
				transaction.commit();

			}
		});

		llMessage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				FragmentTransaction transaction;
				MainMessage mainMessage = new MainMessage();
				mainMessage.setComm(getActivity());
				transaction = mannager.beginTransaction();
				transaction.replace(android.R.id.content, mainMessage, "mainMessage");
				transaction.addToBackStack("mainMessage");
				transaction.commit();

			}
		});

		progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
		// stopTask();
		// startTask();
//		rssList = new ArrayList<RSSFeedModel>();
//		adapter = new RSSFeedAdapter(getActivity(), R.layout.custom_rss_feed_slot,
//				 rssList);
//		gridView.setAdapter(adapter);
		if (isOnline()) {
			sliderTask = new SliderTask(getActivity());
			sliderTask.execute(AppSettings.getSportListAndPlayerSelectedSport);

		} else {
			showInternetAlertBox(getActivity());
		}
		return v;
	}
	public void onSelectedItem(RSSFeedModel item) {
		MainRSSFeedDetail mainRSSFeedDetail = new MainRSSFeedDetail();
		if (rssList != null) {
			mainRSSFeedDetail.setData(item.getBlog(), item.getPubDate(),
					item.getTitel(), item.getDescription(),
					item.getCreator(), item.getImgSrc());

			FragmentManager mannager = getFragmentManager();
			FragmentTransaction transaction = mannager.beginTransaction();
			transaction.replace(android.R.id.content, mainRSSFeedDetail, "mainRSSFeedDetail");
			transaction.addToBackStack("mainRSSFeedDetail");
			transaction.setTransitionStyle(android.app.FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			transaction.commit();
		}
	}

	/*@Override
	public void onResume() {
		super.onResume();
		L.m("The onResume() event");
		GetNotificationServices.shouldContinue = true;

		// Register for the particular broadcast based on ACTION string
		IntentFilter filter = new IntentFilter(GetNotificationServices.ACTION);
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(testReceiver, filter);

	}

	@Override
	public void onPause() {
		super.onPause();
		L.m("The onPause() event");
		GetNotificationServices.shouldContinue = false;

		// Unregister the listener when the application is paused
		LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(testReceiver);
		// or `unregisterReceiver(testReceiver)` for a normal broadcast
	}

	// Define the callback for what to do when data is received
	private BroadcastReceiver testReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			int resultCode = intent.getIntExtra(GetNotificationServices.NotiResultCode, Activity.RESULT_CANCELED);
			if (resultCode == Activity.RESULT_OK) {
				int NotiCounter = intent.getIntExtra(GetNotificationServices.NotiCounter, 0);
				setNotificationCounter(AppTokens.newNotificationPush.size()+AppTokens.newNotificationPull.size()+AppTokens.newChat.size());

			}
		}
	};
	*/

	private void setNotiMsgCounter() {

		int message = GCMIntentService.messageCounter;
		int noti = AppTokens.newNotificationPull.size()+ AppTokens.newNotificationPush.size()+AppTokens.newChat.size();
		
		setNotificationCounter(noti);
		setMessageCounter(message);
		
	}
	
	public void setNotificationCounter(int noti) {
		if (noti <= 0) {
			tvNotification.setVisibility(View.INVISIBLE);
		} else {
			tvNotification.setText(String.valueOf(noti));
			tvNotification.setVisibility(View.VISIBLE);
		}
	}
	
	public void setMessageCounter(int message) {
		if (message <= 0) {
			tvMessage.setVisibility(View.INVISIBLE);
		} else {
			tvMessage.setText(String.valueOf(message));
			tvMessage.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onActivityCreated(Bundle bundle) {
		// TODO Auto-generated method stub
		super.onActivityCreated(bundle);
		// setRetainInstance(true);
	}

	private class RssFeedTask extends AsyncTask<String, Void, ArrayList<RSSFeedModel>> {

		Activity activity;

		public RssFeedTask(Activity activity) {
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		private void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressBar.setVisibility(View.VISIBLE);
			// comm.onRSSFeedListener(true);
		}

		@Override
		protected ArrayList<RSSFeedModel> doInBackground(String... params) {
			// TODO Auto-generated method stub
			rssList = new ArrayList<RSSFeedModel>();
			ArrayList<RSSFeedModel> tempList=new ArrayList<>();
//			String response;
//			HttpReq ob=new HttpReq();
//			response=ob.makeConnection(AppSettings.RSS_Feed_ZEE, HttpReq.GET);

//			try {
//				URL url = new URL(params[0]);
//				HttpURLConnection con = (HttpURLConnection) url.openConnection();
//				con.setRequestMethod("GET");
//				con.setConnectTimeout(18000);
//				con.setReadTimeout(20000);
//				InputStream inputStream = con.getInputStream();
//				tempList = HttpReq.processXML(inputStream);
//				rssList.addAll(tempList);
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				ErrorMessage = e.toString();
//				L.m(e.toString() + "");
//				return null;
//			}
//			try {
//				URL url = new URL(AppSettings.RSS_Feed_ZEE);
//				HttpURLConnection con = (HttpURLConnection) url.openConnection();
//				con.setRequestMethod("GET");
//				con.setConnectTimeout(18000);
//				con.setReadTimeout(20000);
//				InputStream inputStream = con.getInputStream();
//				tempList = HttpReq.processXMLparseZee(inputStream);
//				rssList.addAll(tempList);
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				ErrorMessage=e.toString();
//				L.m(e.toString());
//				return null;
//			}
			HttpReq req=new HttpReq();
			String res=req.makeConnection(AppSettings.RSS_Feed_SBUDDY1,HttpReq.GET);
			tempList = HttpReq.processXMLparseSbuddy(res);
			rssList=HttpReq.processXMLparseSbuddy(res);
//			return rssList;
			return rssList;
		}

		@Override
		protected void onPostExecute(ArrayList<RSSFeedModel> result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (activity != null) {
				progressBar.setVisibility(View.GONE);
				if (result != null) {
//					RSSFeedAdapter ExpAdapter = new RSSFeedAdapter(getActivity(), R.layout.custom_rss_feed_slot,
//							result);
//					gridView.setAdapter(ExpAdapter);
					adapter2 = new RSSFeedAdvanceAdapter(getActivity(), result,MainRSSFeed.this);
					lmMain.setAdapter(adapter2);
					//adapter.notifyDataSetChanged();
				} else {
					showServerErrorAlertBox(ErrorMessage);
				}

			}
		}
	}

	public void showServerErrorAlertBox(String errorDetail) {
		showAlertBox(getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), 2, errorDetail);
	}

	public void showInternetAlertBox() {
		showAlertBox(getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), 2, null);
	}

	public void showAlertBox(String title, String Description, int noOfButtons, final String errorMessage) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = getActivity().getLayoutInflater();

		View v = inflater.inflate(R.layout.alert_dialog, null);
		dialogBuilder.setView(v);
		ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
		Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
		Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
		final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
		LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
		LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);
		ivAbout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (errorMessage != null) {
					tvDescription.setText(errorMessage);
				}
			}
		});

		tvTitle.setText(title);
		tvDescription.setText(Description);
		if (noOfButtons == 1) {
			button2.setVisibility(View.GONE);
			llBtn2.setVisibility(View.GONE);
		}
		button1.setText("Retry");
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				if (isOnline()) {
					sliderTask = new SliderTask(getActivity());
					sliderTask.execute(AppSettings.getSportListAndPlayerSelectedSport);
				} else {
					showInternetAlertBox(getActivity());
				}
			}
		});
		button2.setText("Cancel");
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}

	// private ArrayList<MyEventModel> LoadSports() {
	//
	// ArrayList<MyEventModel> sportArrayList = new ArrayList<MyEventModel>();
	// String response = Utility.LoadPref(getActivity(), AppTokens.SportList);
	// // L.m(response);
	// if (response.contains("[")) {
	// try {
	// JSONArray sportList = new JSONArray(response);
	//
	// for (int i = 0; i < sportList.length(); i++) {
	//
	// JSONObject obj = sportList.getJSONObject(i);
	// if (!obj.optString("status").equals("") &&
	// obj.optString("status").equalsIgnoreCase("success")) {
	// JSONArray message = obj.getJSONArray("message");
	// for (int j = 0; j < message.length(); j++) {
	// JSONObject messageData = message.getJSONObject(j);
	// MyEventModel model = new MyEventModel();
	// model.setEventName(messageData.optString("sportName"));
	// model.setSportImage(messageData.optString("baseImage"));
	// sportArrayList.add(model);
	// }
	//
	// }
	// }
	// } catch (JSONException e) {
	// L.m(e.toString());
	// return null;
	// }
	// } else {
	// L.m("Invalid JSON found : " + response);
	// return null;
	// }
	//
	// return sportArrayList;
	// }

	private class SliderTask extends AsyncTask<String, Void, ArrayList<MyEventModel>> {

		Activity activity;

		public SliderTask(Activity activity) {
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		private void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressBar.setVisibility(View.VISIBLE);
			// comm.onRSSFeedListener(true);
		}

		@Override
		protected ArrayList<MyEventModel> doInBackground(String... params) {
			List<BasicNameValuePair> nvps = new ArrayList<BasicNameValuePair>();
			nvps.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));

			HttpReq ob = new HttpReq();
			String response = ob.makeConnection(params[0], HttpReq.GET, nvps);

			String response2 = ob.makeConnection(AppSettings.getAllEvents, HttpReq.GET, nvps);

			ArrayList<MyEventModel> sportArrayList = new ArrayList<MyEventModel>();
			ArrayList<MyEventModel> eventImageList = new ArrayList<MyEventModel>();
			// String response = Utility.LoadPref(getActivity(),
			// AppTokens.SportList);

			if (response.contains("[")) {
				try {
					JSONArray sportList = new JSONArray(response);
					JSONObject sportListObj = sportList.getJSONObject(0);
					JSONArray sportListArr = sportListObj.getJSONArray("sportList");

					JSONObject obj = sportListArr.getJSONObject(0);
					if (!obj.optString("status").equals("") && obj.optString("status").equalsIgnoreCase("success")) {
						if(activity!=null){
							SavePref(AppTokens.SportList, sportListArr.toString());
						} 

					}

				} catch (JSONException e) {
					L.m(e.toString());
					ErrorMessage = e.toString() + response;
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				ErrorMessage = response;
				return null;
			}

			if (response2.contains("[")) {
				try {
					JSONArray sportList = new JSONArray(response2);
					JSONObject sportListObj = sportList.getJSONObject(0);
					if (!sportListObj.optString("status").equals("")
							&& sportListObj.optString("status").equalsIgnoreCase("success")) {
						// SavePref(AppTokens.SportList,
						// sportListArr.toString());
						JSONObject message = sportListObj.getJSONObject("message");

						JSONArray adDetails = message.getJSONArray("adDetails");
						for (int j = 0; j < adDetails.length(); j++) {
							JSONObject messageData = adDetails.getJSONObject(j);
							MyEventModel model = new MyEventModel();
							model.setSliderStatus(model.ADVERTISMENT);
							model.setEventName(messageData.optString("advertisementTitle"));
							model.setEventImages(messageData.optString("imageUrl"));
							sportArrayList.add(model);
						}
						JSONArray eventDetails = message.getJSONArray("eventDetails");
						for (int j = 0; j < eventDetails.length(); j++) {
							JSONObject obj = eventDetails.getJSONObject(j);
							MyEventModel model = new MyEventModel();
							model.setSliderStatus(model.EVENT);
							model.setEventType(obj.optString("eventType"));
							model.setEventID(obj.optString("eventID"));
							model.setEventName(obj.optString("eventName"));
							model.setSport(obj.optString("sport"));
							model.setAgeGroup(obj.optString("ageGroup"));
							model.setAddress(obj.optString("address"));
							model.setLocation(obj.optString("location"));
							model.setStartDate(obj.optString("startDate"));
							model.setEndDate(obj.optString("endDate"));
							model.setStartTime(obj.optString("startTime"));
							model.setEndTime(obj.optString("endTime"));
							model.setMoreDetails(obj.optString("moreDetails"));

							model.setpFullName(obj.optString("pFullName"));
							model.setpMobile(obj.optString("pMobile"));
							model.setpEmail(obj.optString("pEmail"));
							model.setsFullName(obj.optString("sFullName"));
							model.setsMobile(obj.optString("sMobile"));
							model.setsEmail(obj.optString("sEmail"));
							model.setWebUrl(obj.optString("webUrl"));
							model.setFacebookUrl(obj.optString("facebookUrl"));
							model.setRegistrationFee(obj.optString("registrationFee"));
							model.setLastDateOfRegistration(obj.optString("lastDateOfRegistration"));
							model.setRegistrationLink(obj.optString("registrationLink"));
							model.setOwnerId(obj.optString("ownerId"));
							JSONArray eventImage = obj.getJSONArray("eventImages");
							eventImageList = new ArrayList<MyEventModel>();
							for (int l = 0; l < eventImage.length(); l++) {
								JSONObject eImageObj = eventImage.getJSONObject(l);
								MyEventModel img = new MyEventModel();
								img.setEventImages(eImageObj.optString("image"));
								if (l == 0)
									model.setEventImages(eImageObj.optString("image"));
								eventImageList.add(img);
							}
							if (activity != null) {
								;
								model.setSportImage(Buddy.getSportImage(getActivity(), obj.optString("sport")));
							}

							model.setImgList(eventImageList);
							sportArrayList.add(model);

						}

					}

				} catch (JSONException e) {
					L.m(e.toString());
					ErrorMessage = e.toString() + response2;
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response2);
				ErrorMessage = response2;
				return null;
			}

			return sportArrayList;
		}

		@Override
		protected void onPostExecute(ArrayList<MyEventModel> result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (activity != null) {
				if (result != null) {
					MyEventSlider myPagerAdapter = new MyEventSlider(getActivity(),mannager, result);
					viewPager.setAdapter(myPagerAdapter);
					viewPager.setInterval(5000);
					viewPager.startAutoScroll();

					rssFeedTask = new RssFeedTask(getActivity());
					rssFeedTask.execute(AppSettings.RSS_Feed);
				} else {
					progressBar.setVisibility(View.GONE);
					showServerErrorAlertBox(ErrorMessage);
				}

			}
		}
	}

	/**
	 * Receiving push messages
	 */
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			int result = intent.getExtras().getInt(AppTokens.EXTRA_RESULT);
			if (result == AppTokens.MESSAGE || result == AppTokens.NOTIFICATION || result == AppTokens.CHAT) {
				String newMessage = intent.getExtras().getString(AppTokens.EXTRA_MESSAGE);
				// Waking up mobile if it is sleeping
				WakeLocker.acquire(getActivity().getApplicationContext());

				/**
				 * Take appropriate action on this message depending upon your
				 * app requirement For now i am just displaying it on the screen
				 */
				// Showing received message
				// Toast.makeText(getApplicationContext(), "New Message: " +
				// newMessage, Toast.LENGTH_LONG).show();
				((MainActivity)getActivity()).getConnectRequest();
				 setNotiMsgCounter();
				// Releasing wake lock
			}

			WakeLocker.release();
		}
	};

	@Override
	public void onDestroy() {

		try {
			getActivity().unregisterReceiver(mHandleMessageReceiver);
			GCMRegistrar.onDestroy(getActivity());
			GCMIntentService.notificationStatus = true;
		} catch (Exception e) {
			L.m("UnRegister Receiver Error"+ "> " + e.toString());
		}
		super.onDestroy();
	}

}
