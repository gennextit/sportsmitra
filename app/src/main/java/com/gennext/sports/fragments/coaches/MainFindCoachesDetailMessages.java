package com.gennext.sports.fragments.coaches;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.sports.R;
import com.gennext.sports.fragments.CompactFragment;
import com.gennext.sports.model.FindCoachMessageAdapter;
import com.gennext.sports.model.FindCoachModel;
import com.gennext.sports.util.AppSettings;
import com.gennext.sports.util.Buddy;
import com.gennext.sports.util.HttpReq;
import com.gennext.sports.util.L;

import com.gennext.sports.util.internet.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainFindCoachesDetailMessages extends CompactFragment {

	ListView lvMain;
	String jsonData, coachId, sportId;
	ArrayList<FindCoachModel> myMsgList;
	LoadMessageList loadMessageList;
	Button btnSendReview;
	ProgressBar progressBar;
	SendMessage sendMessage;
	FindCoachMessageAdapter ExpAdapter ;
	Boolean isSoftKeyboardDisplayed = false;
	String tempReview="";

	public void setData(String JsonData, String coachId, String sportId) {
		this.jsonData = JsonData;
		this.coachId = coachId;
		this.sportId = sportId;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (loadMessageList != null) {
			loadMessageList.onAttach(activity);
		}
		if (sendMessage != null) {
			sendMessage.onAttach(activity);
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (loadMessageList != null) {
			loadMessageList.onDetach();
		}
		if (sendMessage != null) {
			sendMessage.onDetach();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.main_find_coaches_detail_messages, container, false);

		lvMain = (ListView) v.findViewById(R.id.lv_main_find_coaches_detail_messages);
		btnSendReview = (Button) v.findViewById(R.id.btn_main_find_coaches_detail_messages);
		progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);

		setTypsFace(btnSendReview);
		
		btnSendReview.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showMessageAlert(getActivity());
			}
		});
		 loadMessageList=new LoadMessageList(getActivity());
		 loadMessageList.execute(AppSettings.receivePlayerCoachMessages);
		return v;
	}

	private class LoadMessageList extends AsyncTask<String, Void, String> {

		private Activity activity;

		public LoadMessageList(Activity activity) {
			this.activity = activity;
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		 @Override
		 protected void onPreExecute() {
		 // TODO Auto-generated method stub
			 super.onPreExecute(); 
			 progressBar.setVisibility(View.VISIBLE);
		 }
		@Override
		protected String doInBackground(String... urls) {
			String response = "error";

			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				params.add(new BasicNameValuePair("coachId", coachId));
				params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
			}

			HttpReq ob = new HttpReq();
			response = ob.makeConnection(urls[0], 1, params);
			
			myMsgList = new ArrayList<>();
			L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray message = new JSONArray(response);
					JSONObject coachData = message.getJSONObject(0);
					if(coachData.optString("status").equalsIgnoreCase("success")){
						JSONArray messageArr = coachData.getJSONArray("message");
						JSONObject messageObj = messageArr.getJSONObject(0);
						String CoachName=messageObj.optString("coachName");
						
						JSONArray mainArr = messageObj.getJSONArray("message");
						for (int k = 0; k < mainArr.length(); k++) {
							JSONObject sPData = mainArr.getJSONObject(k);
							if(sPData.optString("status").equalsIgnoreCase("S")){
								response="success";
								FindCoachModel model = new FindCoachModel();
								model.setFullName("You");
								model.setMessage(sPData.optString("message"));
								model.setDate(sPData.optString("dateOfMsg"));
								model.setTime(sPData.optString("timeOfMsg"));
								model.setStatus(sPData.optString("status"));
								
								myMsgList.add(model);
								
							}else if(sPData.optString("status").equalsIgnoreCase("R")){
								FindCoachModel model = new FindCoachModel();
								response="success";
								model.setFullName(CoachName);
								model.setMessage(sPData.optString("message"));
								model.setDate(sPData.optString("dateOfMsg"));
								model.setTime(sPData.optString("timeOfMsg"));
								model.setStatus(sPData.optString("status"));
								
								myMsgList.add(model);
							}
						}
					}else if(coachData.optString("status").equalsIgnoreCase("failure")){
						response="failure";
						ErrorMessage =coachData.optString("message");
					}
					
					
					
				} catch (JSONException e) {
					L.m(e.toString());
					ErrorMessage=e.toString()+response;
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				ErrorMessage=response;
				return null;
			}

			return response;

		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			if (activity != null) {

				 progressBar.setVisibility(View.GONE);
				if (result != null) {
					if(result.equalsIgnoreCase("success")){
						ExpAdapter = new FindCoachMessageAdapter(activity,
								R.layout.custom_slot_find_coach_message_left, myMsgList);
						lvMain.setAdapter(ExpAdapter);
					}else if(result.equalsIgnoreCase("failure")) {
						Toast.makeText(getActivity(), ErrorMessage, Toast.LENGTH_SHORT).show();
					}else{

						Toast.makeText(getActivity(), "No message available", Toast.LENGTH_SHORT).show();
					}
					
				} else {
					showServerErrorAlertBox(ErrorMessage);
				}
			}
		}
	}

	private class SendMessage extends AsyncTask<String, Void, String> {

		private Activity activity;
		String message;

		public SendMessage(Activity activity, String message) {
			this.message = message;
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showPDialog(getActivity(), "Processing please wait");
			
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error";
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				params.add(new BasicNameValuePair("coachId", coachId));
				params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
				params.add(new BasicNameValuePair("message", message)); 
				
			}

			HttpReq ob = new HttpReq();
			response = ob.makeConnection(urls[0], 3, params);
			String output = null;
			L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray json = new JSONArray(response);
					for (int i = 0; i < json.length(); i++) {
						JSONObject obj = json.getJSONObject(i);
						if (obj.optString("status").equals("success")) {
							output = "success";

						} else if (obj.optString("status").equals("failure")) {
							output = obj.optString("message");
						}
					}

				} catch (JSONException e) {
					L.m("Json Error :" + e.toString());
					ErrorMessage=e.toString();
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				ErrorMessage=response;
				return null;
			}

			return output;

		}

		@Override
		protected void onPostExecute(String result) {

			if (activity != null) {
				dismissPDialog();
				if (result != null) {
					L.m(result);
					if (result.equalsIgnoreCase("success")) {
						Toast.makeText(getActivity(), "Message Sent successfull", Toast.LENGTH_SHORT).show();
						addNewReviewInList(message,getDate(),viewTime(),"s");
						
					} else {
						addNewReviewInList(message,getDate(),viewTime(),"s");
						Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
					}
				} else {
					Button retry=showBaseServerErrorAlertBox(ErrorMessage);
					retry.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View view) {
							dialog.dismiss();
							sendMessage = new SendMessage(getActivity(), tempReview);
							sendMessage.execute(AppSettings.sendP2CMessage);
						}
					});
				}
			}
		}
	}

	public void showMessageAlert(Activity Act) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Act);

		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = Act.getLayoutInflater();

		View v = inflater.inflate(R.layout.custorm_find_coach_sendmessage, null);
		dialogBuilder.setView(v);
		Button sendButton = (Button) v.findViewById(R.id.btn_custorm_find_coach_sendmessage_send);
		final EditText etReview = (EditText) v.findViewById(R.id.et_custorm_find_coach_sendmessage);
		etReview.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub
				isSoftKeyboardDisplayed=true;
				return false;
			}
		});
		sendButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				hideKeyboard(getActivity());
				tempReview=etReview.getText().toString();
				sendMessage = new SendMessage(getActivity(), tempReview);
				sendMessage.execute(AppSettings.sendP2CMessage);
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}
	
	
	
	
	public void addNewReviewInList(String message,String dateOfMsg,String timeOfMsg,String status) { 
		FindCoachModel model = new FindCoachModel();
		model.setFullName("You");
		model.setMessage(message);
		model.setDate(dateOfMsg);
		model.setTime(timeOfMsg);
		model.setStatus(status);
		model.setClosedDay(viewTime()); 
		myMsgList.add(model);
		if(ExpAdapter!=null){
			ExpAdapter.notifyDataSetChanged();
             
        }
	}
	
	public void showServerErrorAlertBox(String errorDetail) {
		showAlertBox(getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), 2, errorDetail);
	}

	public void showInternetAlertBox() {
		showAlertBox(getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), 2, null);
	}

	public void showAlertBox(String title, String Description, int noOfButtons, final String errorMessage) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = getActivity().getLayoutInflater();

		View v = inflater.inflate(R.layout.alert_dialog, null);
		dialogBuilder.setView(v);
		ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
		Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
		Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
		final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
		LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
		LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);
		ivAbout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (errorMessage != null) {
					tvDescription.setText(errorMessage);
				}
			}
		});

		tvTitle.setText(title);
		tvDescription.setText(Description);
		if (noOfButtons == 1) {
			button2.setVisibility(View.GONE);
			llBtn2.setVisibility(View.GONE);
		}
		button1.setText("Retry");
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				if (isOnline()) {
					 loadMessageList=new LoadMessageList(getActivity());
					 loadMessageList.execute(AppSettings.receivePlayerCoachMessages);
					
				} else {
					showInternetAlertBox(getActivity());
				}
			}
		});
		button2.setText("Cancel");
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}
}
