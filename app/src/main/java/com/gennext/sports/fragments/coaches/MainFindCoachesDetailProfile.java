package com.gennext.sports.fragments.coaches;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gennext.sports.R;
import com.gennext.sports.fragments.CompactFragment;
import com.gennext.sports.model.FindCoachModel;
import com.gennext.sports.util.L;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainFindCoachesDetailProfile extends CompactFragment {

	String about, location, jsonData, coachId,sportName;
	TextView tvAbout, tvCoachingLocation, tvFees, tvClosedDay;
	// GridView gvMain;
	ArrayList<FindCoachModel> cList;
	ShowProfileTask showProfileTask;
	ImageView ivCat,ivCat1, ivCat2, ivCat3, ivCat4; 
	TextView tvCat1, tvCat2, tvCat3, tvCat4;
	FilterFeeRes filterFeeRes;
	
	public void setData(String coachId,String sportName, String about, String location, String jsonData) {
		this.coachId = coachId;
		this.sportName=sportName;
		this.about = about;
		this.location = location;
		this.jsonData = jsonData;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (showProfileTask != null) {
			showProfileTask.onAttach(activity);
		}
		if(filterFeeRes!=null){
			filterFeeRes.onAttach(activity);
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (showProfileTask != null) {
			showProfileTask.onDetach();
		}
		if(filterFeeRes!=null){
			filterFeeRes.onDetach();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.main_find_coaches_detail_profile, container, false);

		tvAbout = (TextView) v.findViewById(R.id.tv_main_find_coaches_detail_profile_about);
		tvCoachingLocation = (TextView) v.findViewById(R.id.tv_main_find_coaches_detail_profile_location);
		tvFees = (TextView) v.findViewById(R.id.tv_main_find_coaches_detail_profile_fees);
		tvClosedDay = (TextView) v.findViewById(R.id.tv_main_find_coaches_detail_profile_closedDay);
		ivCat = (ImageView) v.findViewById(R.id.iv_main_find_coaches_detail_profile_sportCat);
		ivCat1 = (ImageView) v.findViewById(R.id.iv_main_find_coaches_detail_profile_sportCat1);
		ivCat2 = (ImageView) v.findViewById(R.id.iv_main_find_coaches_detail_profile_sportCat2);
		ivCat3 = (ImageView) v.findViewById(R.id.iv_main_find_coaches_detail_profile_sportCat3);
		ivCat4 = (ImageView) v.findViewById(R.id.iv_main_find_coaches_detail_profile_sportCat4);
		tvCat1 = (TextView) v.findViewById(R.id.tv_main_find_coaches_detail_profile_sportName1);
		tvCat2 = (TextView) v.findViewById(R.id.tv_main_find_coaches_detail_profile_sportName2);
		tvCat3 = (TextView) v.findViewById(R.id.tv_main_find_coaches_detail_profile_sportName3);
		tvCat4 = (TextView) v.findViewById(R.id.tv_main_find_coaches_detail_profile_sportName4);
		
		ivCat1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				switchTab(1);
				filterFeeRes=new FilterFeeRes(getActivity());
				filterFeeRes.execute(tvCat1.getText().toString());
			}
		});
		ivCat2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				switchTab(2);
				filterFeeRes=new FilterFeeRes(getActivity());
				filterFeeRes.execute(tvCat2.getText().toString());
				 
			}
		});
		ivCat3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				switchTab(3);
				filterFeeRes=new FilterFeeRes(getActivity());
				filterFeeRes.execute(tvCat3.getText().toString());
				
			}
		});
		ivCat4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				switchTab(4);
				filterFeeRes=new FilterFeeRes(getActivity());
				filterFeeRes.execute(tvCat4.getText().toString());
				
			}
		});

		if (about != null) {
			tvAbout.setText(about);
		}
		if (location != null) {
			tvCoachingLocation.setText(location);
		}

		showProfileTask = new ShowProfileTask(getActivity());
		showProfileTask.execute(jsonData);

		//switchTab(1);
		
		return v;
	}


	
	private class FilterFeeRes extends AsyncTask<String, Void, String[]>{

		Activity activity;

		public FilterFeeRes(Activity activity) {
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		private void onDetach() {
			this.activity = null;
		}
		
		@Override
		protected String[] doInBackground(String... params) {
			String[] res={"","","","",""};
			if(cList!=null){
				
				for(FindCoachModel ob: cList){
					if(ob.getSportName().equalsIgnoreCase(params[0])){
						res[0]=ob.getSportName();
						res[1]=ob.getLogo();
						res[2]=ob.getFee();
						res[3]=ob.getClosedDay();
						res[4]=ob.getSportId();
						
					}
				}
			}
			return res;
		}
		@SuppressLint("NewApi")
		@Override
		protected void onPostExecute(String[] res) {
			// TODO Auto-generated method stub
			super.onPostExecute(res);
			Glide.with(getActivity())
					.load(res[1])
					.placeholder(R.drawable.profile)
					.error(R.drawable.profile)
					.into(ivCat);
			ivCat.setColorFilter(getResources().getColor(R.color.mySport_green));
			
			tvFees.setText(res[2]);
			tvClosedDay.setText(res[3]);
			//sportId=res[4];
			sportName=res[0];
			MainFindCoachesDetail.sportId=res[4];
			MainFindCoachesDetail.sportName=res[0];
		}
	}
	
	
	private class ShowProfileTask extends AsyncTask<String, Void, ArrayList<FindCoachModel>> {

		Activity activity;

		public ShowProfileTask(Activity activity) {
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		private void onDetach() {
			this.activity = null;
		}

		
		@Override
		protected ArrayList<FindCoachModel> doInBackground(String... params) {
			// TODO Auto-generated method stub
			String response;
			cList = new ArrayList<FindCoachModel>();
			response = params[0];
			if (response.contains("[")) {
				try {
					JSONArray main = new JSONArray(response);
					for (int k = 0; k < main.length(); k++) {
						JSONObject sub = main.getJSONObject(k);
						L.m(sub.optString("coachId") + " : " + (coachId));
						if (sub.optString("coachId").equalsIgnoreCase(coachId)) {
							JSONArray sportPlay = sub.getJSONArray("sportPlay");
							L.m(sportPlay.toString());
							for (int i = 0; i < sportPlay.length(); i++) {
								JSONObject sPData = sportPlay.getJSONObject(i);
								if (!sPData.optString("sportName").equals("")) {
									FindCoachModel model = new FindCoachModel();
									model.setSportId(sPData.optString("sportId"));
									model.setSportName(sPData.optString("sportName"));
									model.setLogo(sPData.optString("logo"));
									model.setFee(sPData.optString("Fee"));
									model.setClosedDay(sPData.optString("closedDay"));
									cList.add(model);
								}
							}
						}
					}
				} catch (JSONException e) {
					L.m(e.toString());
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				return null;
			}

			return cList;
		}

		@Override
		protected void onPostExecute(ArrayList<FindCoachModel> result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			L.m("sportName ::"+sportName);
			int logoOne=0, logoTwo=0,logoThree=0,logoFour=0;
			if (activity != null) {
				if (result != null) {
					for (int i = 0; i < result.size(); i++) {
						if (sportName.equals("")&&logoOne==0||result.get(i).getSportName().equalsIgnoreCase(sportName.trim())) {
							setSport(1,i,result);
							logoOne=1;
						} else if (logoTwo == 0) {
							setSport(2,i,result);
							logoTwo = 1;
						} else if (logoThree == 0) {
							setSport(3,i,result);
							logoThree = 1;
						}else if (logoFour == 0) {
							setSport(4,i,result);
							logoFour = 1;
						}
					}
				}

			}
		}

		private void setSport(int pos,int i,ArrayList<FindCoachModel> result) {
			switch (pos) {
			case 1:
				if (result.get(i).getLogo() != null) {
					ivCat1.setVisibility(View.VISIBLE); 
					tvCat1.setVisibility(View.VISIBLE);
					Glide.with(getActivity())
							.load(result.get(i).getLogo())
							.placeholder(R.drawable.profile)
							.error(R.drawable.profile)
							.into(ivCat1);
					tvCat1.setText(result.get(i).getSportName());
					switchTab(1);
					filterFeeRes=new FilterFeeRes(getActivity());
					filterFeeRes.execute(tvCat1.getText().toString());
					
				}
				break;
			case 2:
				if (result.get(i).getLogo() != null) {
					ivCat2.setVisibility(View.VISIBLE); 
					tvCat2.setVisibility(View.VISIBLE);
					Glide.with(getActivity())
							.load(result.get(i).getLogo())
							.placeholder(R.drawable.profile)
							.error(R.drawable.profile)
							.into(ivCat2);
					tvCat2.setText(result.get(i).getSportName());
				}
				break;
			case 3:
				if (result.get(i).getLogo() != null) {
					ivCat3.setVisibility(View.VISIBLE); 
					tvCat3.setVisibility(View.VISIBLE);
					Glide.with(getActivity())
							.load(result.get(i).getLogo())
							.placeholder(R.drawable.profile)
							.error(R.drawable.profile)
							.into(ivCat3);
					tvCat3.setText(result.get(i).getSportName());
				}
				break;
			case 4:
				if (result.get(i).getLogo() != null) {
					ivCat4.setVisibility(View.VISIBLE); 
					tvCat4.setVisibility(View.VISIBLE);
					Glide.with(getActivity())
							.load(result.get(i).getLogo())
							.placeholder(R.drawable.profile)
							.error(R.drawable.profile)
							.into(ivCat4);
					tvCat4.setText(result.get(i).getSportName());
				}
				break;

			}
		}
	}

	
	private void switchTab(int key) {

		switch (key) {
		case 0:
			ivCat1.setColorFilter(getResources().getColor(R.color.mySport_grey));
			ivCat2.setColorFilter(getResources().getColor(R.color.mySport_grey));
			ivCat3.setColorFilter(getResources().getColor(R.color.mySport_grey));
			ivCat4.setColorFilter(getResources().getColor(R.color.mySport_grey));
			ivCat1.setBackgroundResource(R.drawable.rounded_grey);
			ivCat2.setBackgroundResource(R.drawable.rounded_grey);
			ivCat3.setBackgroundResource(R.drawable.rounded_grey);
			ivCat4.setBackgroundResource(R.drawable.rounded_grey);
			break;
		case 1:
			ivCat1.setColorFilter(getResources().getColor(R.color.mySport_green));
			ivCat2.setColorFilter(getResources().getColor(R.color.mySport_grey));
			ivCat3.setColorFilter(getResources().getColor(R.color.mySport_grey));
			ivCat4.setColorFilter(getResources().getColor(R.color.mySport_grey));
			//ivCatBorder1.setImageResource(R.drawable.circle_green);
			ivCat1.setBackgroundResource(R.drawable.rounded_green);
			ivCat2.setBackgroundResource(R.drawable.rounded_grey);
			ivCat3.setBackgroundResource(R.drawable.rounded_grey);
			ivCat4.setBackgroundResource(R.drawable.rounded_grey);
			break;
		case 2:
			ivCat1.setColorFilter(getResources().getColor(R.color.mySport_grey));
			ivCat2.setColorFilter(getResources().getColor(R.color.mySport_green));
			ivCat3.setColorFilter(getResources().getColor(R.color.mySport_grey));
			ivCat4.setColorFilter(getResources().getColor(R.color.mySport_grey));
			ivCat1.setBackgroundResource(R.drawable.rounded_grey);
			ivCat2.setBackgroundResource(R.drawable.rounded_green);
			ivCat3.setBackgroundResource(R.drawable.rounded_grey);
			ivCat4.setBackgroundResource(R.drawable.rounded_grey);
			break;
		case 3:
			ivCat1.setColorFilter(getResources().getColor(R.color.mySport_grey));
			ivCat2.setColorFilter(getResources().getColor(R.color.mySport_grey));
			ivCat3.setColorFilter(getResources().getColor(R.color.mySport_green));
			ivCat4.setColorFilter(getResources().getColor(R.color.mySport_grey));
			ivCat1.setBackgroundResource(R.drawable.rounded_grey);
			ivCat2.setBackgroundResource(R.drawable.rounded_grey);
			ivCat3.setBackgroundResource(R.drawable.rounded_green);
			ivCat4.setBackgroundResource(R.drawable.rounded_grey);
			break;
		case 4:
			ivCat1.setColorFilter(getResources().getColor(R.color.mySport_grey));
			ivCat2.setColorFilter(getResources().getColor(R.color.mySport_grey));
			ivCat3.setColorFilter(getResources().getColor(R.color.mySport_grey));
			ivCat4.setColorFilter(getResources().getColor(R.color.mySport_green));
			ivCat1.setBackgroundResource(R.drawable.rounded_grey);
			ivCat2.setBackgroundResource(R.drawable.rounded_grey);
			ivCat3.setBackgroundResource(R.drawable.rounded_grey);
			ivCat4.setBackgroundResource(R.drawable.rounded_green);
			break;
		}
	}

}
