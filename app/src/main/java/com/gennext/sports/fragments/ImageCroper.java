package com.gennext.sports.fragments;

import android.app.Activity;
import android.support.v4.app.FragmentManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.gennext.sports.R;
import com.gennext.sports.fragments.event.MainCreateEvent;
import com.gennext.sports.fragments.profile.ProfileMyProfile;
import com.gennext.sports.util.Buddy;

/*import com.edmodo.cropper.CropImageView;*/

/**
 * Created by Abhijit on 28-Jul-16.
 */
public class ImageCroper extends CompactFragment{

    // Private Constants ///////////////////////////////////////////////////////////////////////////
    Uri cropImageUri;
    private static final int GUIDELINES_ON_TOUCH = 1;
    FragmentManager mannager;
    ProgressBar progressBar;
    Button cropButton;
    Bitmap imgBmp;
    Uri imgUri;
    public static final int PROFILE=1,EVENT=2;
    public static int SWITCH;
    public static final String ProfileStatus="ProfileStatus sMitra";
    /*CropImageView cropImageView;*/
    // Activity Methods ////////////////////////////////////////////////////////////////////////////


    public void setImgBmp(Bitmap imgBmp) {
        this.imgBmp = imgBmp;
    }

    public void setImgUri(Uri imgUri) {
        this.imgUri = imgUri;
    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = inflater.inflate(R.layout.image_croper, container, false);
        mannager = getFragmentManager();
        setActionBarOption(v);
        // Initialize Views.
      /*  cropImageView = (CropImageView) v.findViewById(R.id.CropImageView);
        cropButton = (Button) v.findViewById(R.id.btn_crop);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
*/

       /* initImage();



        // Initialize the Crop button.
        cropButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cropingProgress();
                Bitmap croppedImage;
                try {
                    croppedImage = cropImageView.getCroppedImage();

                   *//* throw new IllegalArgumentException("Threw an IllegalArgumentException");*//*
                } catch(IllegalArgumentException e) {
                    croppedImage=null;
                    initImage();
                    System.out.println("Caught an IllegalArgumentException..." + e.getMessage());
                }
                if(croppedImage!=null){
                    cropImageUri = Uri.fromFile(CameraUtility.getOutputCropedMediaFile());
                    CameraUtility.saveImageExternal(getActivity(), croppedImage, cropImageUri);
                    startActivityToSetImage(croppedImage,cropImageUri);
                    SavePref(ProfileStatus,"set");
                }else{
                    cropingShowProgress();
                    Toast.makeText(getActivity(),"Please set frame first",Toast.LENGTH_SHORT).show();
                }

            }
        });*/

        return v;
    }

   /* private void initImage() {
        if(imgBmp!=null){
            cropImageView.setImageBitmap(imgBmp);
        }else if(imgUri!=null){
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imgUri);
                cropImageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                L.m(e.toString());
            }
        }

        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int dpWidth = displayMetrics.widthPixels/2;

        switch (SWITCH){
            case PROFILE:
                cropImageView.setAspectRatio(dpWidth, dpWidth);
                cropImageView.setFixedAspectRatio(true);
                cropImageView.setGuidelines(1);
                break;
            case EVENT:
                cropImageView.setAspectRatio(2, 1);
                cropImageView.setFixedAspectRatio(true);
                cropImageView.setGuidelines(1);
                break;

        }


    }*/

    private void cropingProgress(){
        cropButton.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void cropingShowProgress(){
        cropButton.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }


    public void setActionBarOption(View view) {
        LinearLayout ActionBack;
        ActionBack = (LinearLayout) view.findViewById(R.id.ll_actionbar_back);
        ActionBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                mannager.popBackStack();
            }
        });

    }

    private void startActivityToSetImage(Bitmap bmp,Uri cropImageUri) {

        switch (SWITCH){
            case PROFILE:
                Buddy.SaveBuddyProfileImage(getActivity(), bmp);

                ProfileMyProfile profileMyFrofile = (ProfileMyProfile) mannager.findFragmentByTag("profileMyFrofile");
                if (profileMyFrofile != null) {
                    profileMyFrofile.setCropedImageAndUri(bmp,cropImageUri);
                }
                break;
            case EVENT:
                MainCreateEvent mainCreateEvent = (MainCreateEvent) mannager.findFragmentByTag("mainCreateEvent");
                if (mainCreateEvent != null) {
                    mainCreateEvent.setCropedImageAndUri(bmp,cropImageUri);
                }
                break;

        }



       mannager.popBackStack();

    }




}
