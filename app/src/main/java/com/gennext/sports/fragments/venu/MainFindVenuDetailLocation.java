package com.gennext.sports.fragments.venu;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.sports.R;
import com.gennext.sports.fragments.CompactFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainFindVenuDetailLocation extends CompactFragment implements OnMapReadyCallback{
	
	String venuDetail="Cannnot Place",venueAddrs; 
	//String lat ,lng;
	String lat;
	String lng;
	// Google Map
	TextView tvAddrs;
	private GoogleMap map;


	public void setData(String venuName,String venueAddrs,String lat,String lng){
		this.venuDetail=venuName;
		this.lat=lat;
		this.lng=lng;
		this.venueAddrs=venueAddrs;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v =inflater.inflate(R.layout.main_find_venu_detail_location, container,false);
		//L.m(venueAddrs);
		tvAddrs = (TextView) v.findViewById(R.id.tv_main_find_venu_detail_location_address);
		if(venueAddrs!=null){
			if(!venueAddrs.equals("")){
				tvAddrs.setText(venueAddrs);
			}else{
				tvAddrs.setText("Not available");
			}
		}
		SupportMapFragment mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
		mMapFragment.getMapAsync(this);


		return v;
	}
	


	@Override
	public void onMapReady(GoogleMap googleMap) {
		this.map = googleMap;
		// DO WHATEVER YOU WANT WITH GOOGLEMAP
		map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
//		map.setMyLocationEnabled(true);
		map.setIndoorEnabled(true);
		map.setBuildingsEnabled(true);
		map.getUiSettings().setZoomControlsEnabled(true);

		loadLocation();
	}

	private void loadLocation() {
		if(lat!=null&&!lat.equals("")&&lng!=null&&!lng.equals("")){
			// Getting status
			int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());

			// Showing status
			if (status == ConnectionResult.SUCCESS) {
				showLocationOnMap();
			} else {
				Toast.makeText(getActivity(),
						"This app won't run without Google Map services, which are missing from your phone.",
						Toast.LENGTH_SHORT).show();
			}
		}else{
			Toast.makeText(getActivity(), "Location not Available", Toast.LENGTH_SHORT).show();
		}

	}

	private void showLocationOnMap() {



		double latitude= Double.parseDouble(lat);
		double longitude=Double.parseDouble(lng);
		// create marker
		MarkerOptions marker = new MarkerOptions().position(
				new LatLng(latitude, longitude)).title(venuDetail);

//	    // Changing marker icon
//	    marker.icon(BitmapDescriptorFactory
//	            .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));

		// adding marker
		map.addMarker(marker);
		CameraPosition cameraPosition = new CameraPosition.Builder()
				.target(new LatLng(latitude, longitude)).zoom(14).build();
		map.animateCamera(CameraUpdateFactory
				.newCameraPosition(cameraPosition));


	}

}
