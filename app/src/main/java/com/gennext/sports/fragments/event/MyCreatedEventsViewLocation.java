package com.gennext.sports.fragments.event;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.sports.R;
import com.gennext.sports.fragments.CompactFragment;
import com.gennext.sports.util.Geocoding;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MyCreatedEventsViewLocation extends CompactFragment implements OnMapReadyCallback {

    String EventAddress, EventLocation, Direction;
    TextView tvEventAddress, tvEventLocation, tvDirection;
    LinearLayout llDirection, llAddress;
    // Google Map
    private GoogleMap map;
    TextView tvAddrs;
    LoadAddress loadAddress;

    public void setData(String EventAddress, String EventLocation, String Direction) {
        this.EventAddress = EventAddress;
        this.EventLocation = EventLocation;
        this.Direction = Direction;
    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (loadAddress != null) {
            loadAddress.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (loadAddress != null) {
            loadAddress.onDetach();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = inflater.inflate(R.layout.my_created_events_view_location, container, false);

        tvEventAddress = (TextView) v.findViewById(R.id.tv_my_created_events_view_location_eventAddress);
        tvEventLocation = (TextView) v.findViewById(R.id.tv_my_created_events_view_location_eventLocation);
        tvDirection = (TextView) v.findViewById(R.id.tv_my_created_events_view_location_direction);
        llDirection = (LinearLayout) v.findViewById(R.id.ll_my_created_events_view_location_direction);
        llAddress = (LinearLayout) v.findViewById(R.id.ll_my_created_events_view_location_eventAddress);

        if (EventAddress != null) {
            if (EventAddress.equals("")) {
                llAddress.setVisibility(View.GONE);
            } else {
                tvEventAddress.setText(EventAddress);
            }
        }
        if (EventLocation != null) {
            tvEventLocation.setText(EventLocation);
        }

        SupportMapFragment mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);

        return v;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        // DO WHATEVER YOU WANT WITH GOOGLEMAP
        map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
//		map.setMyLocationEnabled(true);
        map.setIndoorEnabled(true);
        map.setBuildingsEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);

        loadLocation();
    }

    private void loadLocation() {
        if (EventLocation != null && !EventLocation.equals("")) {
            // Getting status
            int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());

            // Showing status
            if (status == ConnectionResult.SUCCESS) {
                loadAddress = new LoadAddress(getActivity());
                loadAddress.execute();
            } else {
                Toast.makeText(getActivity(),
                        "This app won't run without Google Map services, which are missing from your phone.",
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getActivity(), "Location not Available", Toast.LENGTH_SHORT).show();
        }
    }

    private class LoadAddress extends AsyncTask<Void, Void, LatLng> {

        Activity activity;

        public LoadAddress(Activity activity) {
            this.activity = activity;
        }

        public void onAttach(Activity activity) {
            this.activity = activity;
        }

        public void onDetach() {
            this.activity = null;
        }

        @Override
        protected LatLng doInBackground(Void... arg0) {

            return Geocoding.reverseGeocoding(getActivity(), EventLocation);
        }

        @Override
        protected void onPostExecute(LatLng latLang) {
            // TODO Auto-generated method stub
            super.onPostExecute(latLang);
            if (activity != null) {
                if (latLang != null) {
                    showLocationOnMap(latLang);
                } else {
                    Toast.makeText(getActivity(), "No location found", Toast.LENGTH_SHORT).show();
                }
            }
        }

    }

    private void showLocationOnMap(LatLng latLang) {

        // create marker
        MarkerOptions marker = new MarkerOptions().position(latLang).title(EventLocation);

        // // Changing marker icon
        // marker.icon(BitmapDescriptorFactory
        // .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));

        // adding marker
        map.addMarker(marker);
        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLang).zoom(14).build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        // Perform any camera updates here

    }
}
