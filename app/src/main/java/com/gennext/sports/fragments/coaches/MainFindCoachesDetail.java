package com.gennext.sports.fragments.coaches;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.sports.MainActivity;
import com.gennext.sports.R;
import com.gennext.sports.fragments.CompactFragment;
import com.gennext.sports.util.AppSettings;
import com.gennext.sports.util.AppTokens;
import com.gennext.sports.util.Buddy;
import com.gennext.sports.util.HttpReq;
import com.gennext.sports.util.L;
import com.gennext.sports.util.internet.BasicNameValuePair;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MainFindCoachesDetail extends CompactFragment {
	ImageView ivProfile;
	TextView tvName, tvReviews;
	RatingBar rbRating;
	LinearLayout tabProfile, tabMessages, tabReviews, llMessage, llReview;
	String favouriteCoach, about, coachingLocation;
	public static String sportId, sportName;
	String JsonData, coachId, coachName, coachImage, coachReviews, coachRating;
	SendReview sendReview;
	SendMessage sendMessage;
	TextView ActionBarHeading;
	ImageView ivShare, ivFav;
	ProgressBar pbFav;
	SendFavRequest sendFavRequest;
	public static int SWITCH = 1;
	String tempReview="",tempMessage="";
	float tempRating=0.0f;

	public void setJSONData(String JSONData) {
		this.JsonData = JSONData;
	}

	public void setData(String favouriteCoach, String about, String locality, String sportId, String sportName) {
		this.favouriteCoach = favouriteCoach;
		this.about = about;
		this.coachingLocation = locality;
		this.sportId = sportId;
		this.sportName = sportName;
	}

	public void setPlayerProfile(String coachId, String coachName, String coachImage, String coachReviews,
			String coachRating) {
		this.coachId = coachId;
		this.coachName = coachName;
		this.coachImage = coachImage;
		this.coachReviews = coachReviews;
		this.coachRating = coachRating;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (sendReview != null) {
			sendReview.onAttach(activity);
		}
		if (sendMessage != null) {
			sendMessage.onAttach(activity);
		}
		if (sendFavRequest != null) {
			sendFavRequest.onAttach(activity);
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (sendReview != null) {
			sendReview.onDetach();
		}
		if (sendMessage != null) {
			sendMessage.onDetach();
		}
		if (sendFavRequest != null) {
			sendFavRequest.onDetach();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.main_find_coaches_detail, container, false);
		setActionBarOption(v);
		ivProfile = (ImageView) v.findViewById(R.id.iv_main_find_coaches_detail_profile);
		ivShare = (ImageView) v.findViewById(R.id.iv_coach_share);
		tvName = (TextView) v.findViewById(R.id.tv_main_find_coaches_detail_pName);
		tvReviews = (TextView) v.findViewById(R.id.tv_main_find_coaches_detail_reviews);
		rbRating = (RatingBar) v.findViewById(R.id.rb_main_find_coaches_detail);
		tabProfile = (LinearLayout) v.findViewById(R.id.ll_main_find_coaches_detail_profileTab);
		tabMessages = (LinearLayout) v.findViewById(R.id.ll_main_find_coaches_detail_messagesTab);
		tabReviews = (LinearLayout) v.findViewById(R.id.ll_main_find_coaches_detail_reviewsTab);
		llMessage = (LinearLayout) v.findViewById(R.id.ll_main_find_coaches_detail_message);
		llReview = (LinearLayout) v.findViewById(R.id.ll_main_find_coaches_detail_review);
		ivFav = (ImageView) v.findViewById(R.id.iv_main_find_coaches_detail_fav);
		pbFav = (ProgressBar) v.findViewById(R.id.pb_main_find_coaches_detail_fav);

		if (favouriteCoach.equalsIgnoreCase("y")) {
			ivFav.setImageResource(R.drawable.fav_48);
		}

		ivFav.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				sendFavRequest = new SendFavRequest(getActivity());
				sendFavRequest.execute(AppSettings.setFavouriteCoach);

			}
		});

		ivShare.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				try {
					Intent i = new Intent(Intent.ACTION_SEND);
					i.setType("text/plain");
					i.putExtra(Intent.EXTRA_TITLE, coachName);
					String sAux = getResources().getString(R.string.share_coach_profile_start);
					String eAux = getResources().getString(R.string.share_coach_profile_end);
					sAux = sAux + " " + sportName + " coach " + coachName + " at GURGAON" + eAux;
					i.putExtra(Intent.EXTRA_TEXT, sAux);
					startActivity(Intent.createChooser(i, "choose one"));
				} catch (Exception e) { // e.toString();
				}
			}
		});
		llMessage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				showMessageAlert(getActivity());
			}
		});
		llReview.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				showReviewAlert(getActivity());
			}
		});

		if (coachName != null) {
			tvName.setText(coachName);
		}

		setRatingAndReview(coachRating, coachReviews);

		if (coachImage != null) {

			Picasso.with(getContext())
					.load(coachImage)
					.resize(AppTokens.IMAGE_RESIZE, AppTokens.IMAGE_RESIZE).centerCrop()
					.placeholder(R.drawable.profile)
					.error(R.drawable.profile)
					.into(ivProfile);
		}

		tabProfile.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				switchTab(1);
				setScreenDynamically(1);
			}
		});
		tabMessages.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (JsonData != null) {
					switchTab(2);
					setScreenDynamically(2);
				}
			}
		});
		tabReviews.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				switchTab(3);
				setScreenDynamically(3);
			}
		});
		switchTab(SWITCH);
		setScreenDynamically(SWITCH);

		return v;
	}

	public void setRatingAndReview(String coachRating2, String coachReviews2) {
		if (coachReviews2 != null) {
			tvReviews.setText(coachReviews2);
		}
		if (coachRating2 != null) {
			if (!coachRating2.equals("false")) {
				rbRating.setRating(Float.parseFloat(coachRating2));
			}
		} else {
			rbRating.setRating(0);
		}
	}

	public void setActionBarOption(View view) {
		LinearLayout ActionBack, ActionHome;
		ActionBack = (LinearLayout) view.findViewById(R.id.ll_actionbar_back);
		ActionHome = (LinearLayout) view.findViewById(R.id.ll_actionbar_home);
		ActionBarHeading = (TextView) view.findViewById(R.id.actionbar_title);
		ActionBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				getFragmentManager().popBackStack("mainFindCoachesDetail", FragmentManager.POP_BACK_STACK_INCLUSIVE);
			}
		});
		ActionHome.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				((MainActivity)getActivity()).SwitchTab();
				getFragmentManager().popBackStack("mainFindCoaches", FragmentManager.POP_BACK_STACK_INCLUSIVE);
			}
		});
	}

	private void switchTab(int key) {

		switch (key) {
		case 0:
			tabProfile.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabMessages.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabReviews.setBackgroundColor(getResources().getColor(R.color.main_grey));

			break;
		case 1:
			tabProfile.setBackgroundColor(getResources().getColor(R.color.profile_ul_profile));
			tabMessages.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabReviews.setBackgroundColor(getResources().getColor(R.color.main_grey));

			break;
		case 2:
			tabProfile.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabMessages.setBackgroundColor(getResources().getColor(R.color.profile_ul_profile));
			tabReviews.setBackgroundColor(getResources().getColor(R.color.main_grey));
			break;
		case 3:
			tabProfile.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabMessages.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabReviews.setBackgroundColor(getResources().getColor(R.color.profile_ul_profile));
			break;
		}
	}

	private void setScreenDynamically(int position) {
		setScreenDynamically(position, JsonData, coachId, sportId);
	}


	private void setScreenDynamically(int position, String JsonData, String coachId, String sportId) {
		switch (position) {
		case 1:

			ActionBarHeading.setText("PROFILE");

			MainFindCoachesDetailProfile mainFindCoachesDetailProfile = new MainFindCoachesDetailProfile();
			mainFindCoachesDetailProfile.setData(coachId, sportName, about, coachingLocation, JsonData);
//			transaction = mannager.beginTransaction();
//			transaction.replace(R.id.group_coaches, mainFindCoachesDetailProfile, "mainFindCoachesDetailProfile");
//			transaction.commit();
			replaceFragmentWithoutBackstack(mainFindCoachesDetailProfile,R.id.group_coaches,"mainFindCoachesDetailProfile");
			break;
		case 2:
			ActionBarHeading.setText("MESSAGES");
			MainFindCoachesDetailMessages mainFindCoachesDetailMessages = new MainFindCoachesDetailMessages();
			mainFindCoachesDetailMessages.setData(JsonData, coachId, sportId);
			// mainFindCoachesDetailMessages.setCoachId(coachId);
//			transaction = mannager.beginTransaction();
//			transaction.replace(R.id.group_coaches, mainFindCoachesDetailMessages, "mainFindCoachesDetailMessages");
//			transaction.commit();
			replaceFragmentWithoutBackstack(mainFindCoachesDetailMessages,R.id.group_coaches,"mainFindCoachesDetailMessages");

			break;
		case 3:
			ActionBarHeading.setText("REVIEWS");
			MainFindCoachesDetailReviews mainFindCoachesDetailReviews = new MainFindCoachesDetailReviews();
			mainFindCoachesDetailReviews.setCommunicator(getActivity());
			mainFindCoachesDetailReviews.setData(JsonData, coachId, sportId);
//			transaction = mannager.beginTransaction();
//			transaction.replace(R.id.group_coaches, mainFindCoachesDetailReviews, "mainFindCoachesDetailReviews");
//			transaction.commit();
			replaceFragmentWithoutBackstack(mainFindCoachesDetailReviews,R.id.group_coaches,"mainFindCoachesDetailReviews");

			break;
		}
	}

	private class SendReview extends AsyncTask<String, Void, String> {

		private Activity activity;
		String rating, review;

		public SendReview(Activity activity, String rating, String review) {
			this.rating = rating;
			this.review = review;
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showPDialog(getActivity(), "Processing please wait");
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error";
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				// L.m("Player Id : "+LoadPref(AppTokens.PlayerId));
				params.add(new BasicNameValuePair("coachId", coachId));
				params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
				params.add(new BasicNameValuePair("sportId", sportId));
				params.add(new BasicNameValuePair("rating", rating));
				params.add(new BasicNameValuePair("review", review));
			}

			HttpReq ob = new HttpReq();
			return response = ob.makeConnection(urls[0], HttpReq.POST, params, HttpReq.EXECUTE_TASK);
		}

		@Override
		protected void onPostExecute(String result) {

			if (activity != null) {
				dismissPDialog();
				if (result != null) {
					L.m(result);
					if (result.equalsIgnoreCase("success")) {
						showReviewAlertSuccess(getActivity(), "reviewCoach");
						switchTab(3);
						setScreenDynamically(3);
					} else {
						Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
					}
				} else {

					Button retry=showBaseServerErrorAlertBox(ErrorMessage);
					retry.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View view) {
							if(dialog!=null)dialog.dismiss();
							sendReview = new SendReview(getActivity(), String.valueOf(tempRating),
									tempReview);
							sendReview.execute(AppSettings.setCoachRating);
						}
					});
				}
			}
		}
	}

	public void showReviewAlert(Activity Act) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Act);

		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = Act.getLayoutInflater();

		View v = inflater.inflate(R.layout.custorm_find_coach_sendreview, null);
		dialogBuilder.setView(v);
		Button sendButton = (Button) v.findViewById(R.id.btn_custorm_find_coach_sendreview_send);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_custorm_find_coach_sendreview_title);
		final EditText etReview = (EditText) v.findViewById(R.id.et_custorm_find_coach_sendreview_review);
		final RatingBar rbRating = (RatingBar) v.findViewById(R.id.rb_custorm_find_coach_sendreview_rating);

		tvTitle.setText("Rate Us");
		sendButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				hideKeyboard(getActivity());
				tempReview =etReview.getText().toString();
				tempRating=rbRating.getRating();
				sendReview = new SendReview(getActivity(), String.valueOf(tempRating),
						tempReview);
				sendReview.execute(AppSettings.setCoachRating);
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}

	private class SendMessage extends AsyncTask<String, Void, String> {

		private Activity activity;
		String message;

		public SendMessage(Activity activity, String message) {
			this.message = message;
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showPDialog(getActivity(), "Processing please wait");
		}

		@Override
		protected String doInBackground(String... urls) {

			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				params.add(new BasicNameValuePair("coachId", coachId));
				params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
				params.add(new BasicNameValuePair("message", message));
				params.add(new BasicNameValuePair("status", "s"));

			}

			HttpReq ob = new HttpReq();
			return ob.makeConnection(urls[0], HttpReq.POST, params, HttpReq.EXECUTE_TASK);

		}

		@Override
		protected void onPostExecute(String result) {

			if (activity != null) {
				dismissPDialog();
				if (result != null) {
					L.m(result);
					if (result.equalsIgnoreCase("success")) {
						Toast.makeText(getActivity(), "Message Sent successfull", Toast.LENGTH_SHORT).show();

					} else {
						Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
					}
				} else {
					Button retry =showBaseServerErrorAlertBox(ErrorMessage);
					retry.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View view) {
							dialog.dismiss();
							sendMessage = new SendMessage(getActivity(), tempMessage);
							sendMessage.execute(AppSettings.sendP2CMessage);
						}
					});
				}
			}
		}
	}

	public void showMessageAlert(Activity Act) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Act);

		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = Act.getLayoutInflater();

		View v = inflater.inflate(R.layout.custorm_find_coach_sendmessage, null);
		dialogBuilder.setView(v);
		Button sendButton = (Button) v.findViewById(R.id.btn_custorm_find_coach_sendmessage_send);
		final EditText etMessage = (EditText) v.findViewById(R.id.et_custorm_find_coach_sendmessage);

		sendButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				hideKeyboard(getActivity());
				tempMessage=etMessage.getText().toString();
				sendMessage = new SendMessage(getActivity(), tempMessage);
				sendMessage.execute(AppSettings.sendP2CMessage);
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}

	private class SendFavRequest extends AsyncTask<String, Void, String> {

		private Activity activity;

		public SendFavRequest(Activity activity) {
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			ivFav.setVisibility(View.GONE);
			pbFav.setVisibility(View.VISIBLE);
		}

		
		
		@Override
		protected String doInBackground(String... urls) {
			String response = "error";
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
				params.add(new BasicNameValuePair("coachId", coachId));

			}
			HttpReq ob = new HttpReq();
			return response = ob.makeConnection(urls[0], 3, params, HttpReq.EXECUTE_TASK);
		}

		@Override
		protected void onPostExecute(String result) {

			if (activity != null) {
				ivFav.setVisibility(View.VISIBLE);
				pbFav.setVisibility(View.GONE);
				if (result != null) {
					L.m(result);
					if (result.equalsIgnoreCase("success")) {
						if (favouriteCoach.equalsIgnoreCase("y")) {
							favouriteCoach = "n";
							ivFav.setImageDrawable(getResources().getDrawable(R.drawable.fav_red_48));
						} else {
							favouriteCoach = "y";
							ivFav.setImageDrawable(getResources().getDrawable(R.drawable.fav_48));
						}
					} else {
						Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
					}
				} else {
					Button retry=showBaseServerErrorAlertBox(ErrorMessage);
					retry.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View view) {
							if(dialog!=null)dialog.dismiss();
							sendFavRequest = new SendFavRequest(getActivity());
							sendFavRequest.execute(AppSettings.setFavouriteCoach);
						}
					});
				}
			}
		}
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		SWITCH = 1;
	}

}
