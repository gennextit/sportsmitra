package com.gennext.sports.fragments.event;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.sports.R;
import com.gennext.sports.fragments.CompactFragment;
import com.gennext.sports.model.EventImageModel;
import com.gennext.sports.util.AppSettings;
import com.gennext.sports.util.Buddy;
import com.gennext.sports.util.DateTimeUtility;
import com.gennext.sports.util.HttpReq;
import com.gennext.sports.util.L;
import com.gennext.sports.util.StringBody;
import com.gennext.sports.util.Validation;
import com.gennext.sports.util.internet.FileBody;
import com.gennext.sports.util.internet.MultipartEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

public class MainEventContactDetails extends CompactFragment implements View.OnTouchListener {

    EditText etPFullName, etPCountryCode, etPMobile, etPEmail, etWebsite, etFbUrl, etRegFee, etRegLink;
    EditText etSFullName, etSCountryCode, etSMobile, etSEmail;
    LinearLayout llRegisterEvent, llInviteSbuddy, llTab1, llTab2, llTab3, llLastDayReg;
    TextView tvTab1, tvTab2, tvTab3, tvLastDayReg;
    String EventName, SportId, age;
    ArrayList<EventImageModel> list;
    String location, address, startDate, endDate, startTime, endTime, addMoreDetail;
    UploadEventDetail uploadEventDetail;
    private static String sltEventType;
    static String lastDay = "";
    int currentDate, startDateTimeStamp;
    public static int endDateTimeStamp;
    Boolean isSoftKeyboardDisplayed = false;
    int sday, smonth, syear, shours, smin;

    public void setCreateEvent(String EventName, String sportId, String age, ArrayList<EventImageModel> list) {
        this.EventName = EventName;
        this.SportId = sportId;
        this.age = age;
        this.list = list;
    }

    public void setEventDetails(String location, String address, String startDate, String endDate, String startTime,
                                String endTime, String addMoreDetail) {
        this.location = location;
        this.address = address;
        this.startDate = startDate;
        this.endDate = endDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.addMoreDetail = addMoreDetail;
    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (uploadEventDetail != null) {
            uploadEventDetail.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (uploadEventDetail != null) {
            uploadEventDetail.onDetach();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = inflater.inflate(R.layout.main_event_contact, container, false);
        isSoftKeyboardDisplayed = false;
        etPFullName = (EditText) v.findViewById(R.id.et_main_event_contact_detail_pfullname);
        etPCountryCode = (EditText) v.findViewById(R.id.et_main_event_contact_detail_pcountryCode);
        etPMobile = (EditText) v.findViewById(R.id.et_main_event_contact_detail_pmobile);
        etPEmail = (EditText) v.findViewById(R.id.et_main_event_contact_detail_pemail);
        etSFullName = (EditText) v.findViewById(R.id.et_main_event_contact_detail_sfullname);
        etSCountryCode = (EditText) v.findViewById(R.id.et_main_event_contact_detail_scountryCode);
        etSMobile = (EditText) v.findViewById(R.id.et_main_event_contact_detail_smobile);
        etSEmail = (EditText) v.findViewById(R.id.et_main_event_contact_detail_semail);
        etWebsite = (EditText) v.findViewById(R.id.et_main_event_contact_detail_websiteUrl);
        etFbUrl = (EditText) v.findViewById(R.id.et_main_event_contact_detail_fbUrl);
        etRegFee = (EditText) v.findViewById(R.id.et_main_event_contact_detail_registrationFee);
        etRegLink = (EditText) v.findViewById(R.id.et_main_event_contact_detail_regLink);
        llRegisterEvent = (LinearLayout) v.findViewById(R.id.ll_main_event_contact_detail_registerEvent);
        llInviteSbuddy = (LinearLayout) v.findViewById(R.id.ll_main_event_contact_detail_inviteSbuddy);
        llTab1 = (LinearLayout) v.findViewById(R.id.ll_main_event_contact_detail_tab1);
        llTab2 = (LinearLayout) v.findViewById(R.id.ll_main_event_contact_detail_tab2);
        llTab3 = (LinearLayout) v.findViewById(R.id.ll_main_event_contact_detail_tab3);
        tvTab1 = (TextView) v.findViewById(R.id.tv_main_event_contact_detail_tab1);
        tvTab2 = (TextView) v.findViewById(R.id.tv_main_event_contact_detail_tab2);
        tvTab3 = (TextView) v.findViewById(R.id.tv_main_event_contact_detail_tab3);
        llLastDayReg = (LinearLayout) v.findViewById(R.id.ll_main_event_contact_detail_lastDateOfReg);
        tvLastDayReg = (TextView) v.findViewById(R.id.tv_main_event_contact_detail_lastDateOfReg);

        setTypsFace(etPFullName, etPCountryCode, etPMobile, etPEmail, etSFullName);
        setTypsFace(etSMobile, etSEmail, etWebsite, etFbUrl, etRegFee);
        setTypsFace(etRegLink);

        etPFullName.setOnTouchListener(this);
        etRegLink.setOnTouchListener(this);

        llLastDayReg.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new SelectDateFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });
        llTab1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                switchTab(OPEN_TO_ALL);
            }
        });
        llTab2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                switchTab(INVITE_ONLY);
            }
        });
        llTab3.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                switchTab(PAID_ENTRY);
            }
        });
        llRegisterEvent.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (isSoftKeyboardDisplayed) {
                    hideKeybord();
                }
                if (checkValidation()) {
                    uploadEventDetail = new UploadEventDetail(getActivity());
                    uploadEventDetail.execute(AppSettings.setEventRegistrationByMobile);
                }
            }
        });
        llInviteSbuddy.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (isSoftKeyboardDisplayed) {
                    hideKeybord();
                }
                if (checkValidation()) {
                    uploadEventDetail = new UploadEventDetail(getActivity());
                    uploadEventDetail.execute(AppSettings.setEventRegistrationByMobile);
                }
            }
        });
        switchTab(OPEN_TO_ALL);
        setDefaultCode();
        getDateTime();
        return v;
    }


    public void getDateTime() {
        String date;
        final Calendar cal = Calendar.getInstance();
        syear = cal.get(Calendar.YEAR);
        smonth = cal.get(Calendar.MONTH);
        sday = cal.get(Calendar.DAY_OF_MONTH);

        shours = cal.get(Calendar.HOUR_OF_DAY);
        smin = cal.get(Calendar.MINUTE);

        date = DateTimeUtility.convertDate(sday, smonth, syear);
        currentDate = Integer.parseInt(DateTimeUtility.convertDateStamp(syear, smonth, sday));
        startDateTimeStamp = Integer.parseInt(DateTimeUtility.convertDateStamp(syear, smonth, sday));
        tvLastDayReg.setText(date);


    }


    private void switchTab(int key) {

        switch (key) {

            case 1:
                llTab1.setBackgroundResource(R.drawable.button_green);
                llTab2.setBackgroundResource(R.drawable.button_grey);
                llTab3.setBackgroundResource(R.drawable.button_grey);
                tvTab1.setTextColor(getResources().getColor(R.color.white));
                tvTab2.setTextColor(getResources().getColor(R.color.text));
                tvTab3.setTextColor(getResources().getColor(R.color.text));
                llInviteSbuddy.setVisibility(View.GONE);
                llRegisterEvent.setVisibility(View.VISIBLE);
                sltEventType = "openToAll";
                break;
            case 2:
                llTab1.setBackgroundResource(R.drawable.button_grey);
                llTab2.setBackgroundResource(R.drawable.button_green);
                llTab3.setBackgroundResource(R.drawable.button_grey);
                tvTab1.setTextColor(getResources().getColor(R.color.text));
                tvTab2.setTextColor(getResources().getColor(R.color.white));
                tvTab3.setTextColor(getResources().getColor(R.color.text));
                llInviteSbuddy.setVisibility(View.VISIBLE);
                llRegisterEvent.setVisibility(View.GONE);
                sltEventType = "inviteOnly";
                break;
            case 3:
                llTab1.setBackgroundResource(R.drawable.button_grey);
                llTab2.setBackgroundResource(R.drawable.button_grey);
                llTab3.setBackgroundResource(R.drawable.button_green);
                tvTab1.setTextColor(getResources().getColor(R.color.text));
                tvTab2.setTextColor(getResources().getColor(R.color.text));
                tvTab3.setTextColor(getResources().getColor(R.color.white));
                llInviteSbuddy.setVisibility(View.GONE);
                llRegisterEvent.setVisibility(View.VISIBLE);
                sltEventType = "paidEntry";
                break;

        }
    }

    private boolean checkValidation() {
        boolean ret = true;

        if (!Validation.isEmpty(etPFullName, true)) {
            return false;
        }
        if (!Validation.isEmpty(etPCountryCode, true)) {
            return false;
        }
        if (!Validation.is10DigitMobile(etPMobile)) {
            return false;
        }
        if (!Validation.isEmail(etPEmail, true)) {
            return false;
        }
//		if (startDateTimeStamp < currentDate) {
//			Toast.makeText(getActivity(), "Please select a future date", Toast.LENGTH_SHORT)
//					.show();
//			return false;
//
//		}
//		L.m(startDateTimeStamp +" >= "+endDateTimeStamp);
        if (startDateTimeStamp > endDateTimeStamp) {
            Toast.makeText(getActivity(), "Event Registration date must be less than event end date", Toast.LENGTH_SHORT)
                    .show();
            return false;

        }

        // if (!Validation.isEmpty(etWebsite, true)) {
        // return false;
        // }
        // if (!Validation.isEmpty(etFbUrl, true)) {
        // return false;
        // }
        // if (!Validation.isEmpty(etRegFee, true)) {
        // return false;
        // }
        // if (lastDay == null) {
        // Toast.makeText(getActivity(), "Please select last date of
        // registration", Toast.LENGTH_SHORT).show();
        // return false;
        // }
        // if (!Validation.isEmpty(etRegLink, true)) {
        // return false;
        // }
        return ret;
    }

    private class UploadEventDetail extends AsyncTask<String, Void, String> {
        Activity activity;

        public UploadEventDetail(Activity activity) {
            onAttach(activity);
        }

        public void onAttach(Activity activity) {
            this.activity = activity;
        }

        private void onDetach() {
            this.activity = null;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            showPDialog(getActivity(), "Processing, please wait...");

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = "error";
            File sourceFile = null;
            String output = null;
            // Adding file data to http body
            MultipartEntity entity = new MultipartEntity();
            entity.addPart("eventName", new StringBody(EventName));
            entity.addPart("sportId", new StringBody(SportId));
            entity.addPart("ageGroup", new StringBody(age));
            entity.addPart("location", new StringBody(location));
            entity.addPart("address", new StringBody(address));
            entity.addPart("startDate", new StringBody(startDate));
            entity.addPart("endDate", new StringBody(endDate));
            entity.addPart("startTime", new StringBody(startTime));
            entity.addPart("endTime", new StringBody(endTime));
            entity.addPart("moreDetails", new StringBody(addMoreDetail));
            entity.addPart("pFullName", new StringBody(etPFullName.getText().toString()));
            entity.addPart("pMobile",
                    new StringBody(etPCountryCode.getText().toString() + etPMobile.getText().toString()));
            entity.addPart("pEmail", new StringBody(etPEmail.getText().toString()));
            entity.addPart("sFullName", new StringBody(etSFullName.getText().toString()));
            if (etSMobile.getText().toString().length() > 0) {
                entity.addPart("sMobile",
                        new StringBody(etSCountryCode.getText().toString() + etSMobile.getText().toString()));
            } else {
                entity.addPart("sMobile", new StringBody(""));
            }
            entity.addPart("sEmail", new StringBody(etSEmail.getText().toString()));

            String website = etWebsite.getText().toString();
            if (!website.contains("http://") && !website.contains("https://") && website.length() != 0) {
                website = "http://" + website;
            }
            entity.addPart("webUrl", new StringBody(website));

            String facebookUrl = etFbUrl.getText().toString();
            if (!facebookUrl.contains("http://") && !facebookUrl.contains("https://") && facebookUrl.length() != 0) {
                facebookUrl = "http://" + facebookUrl;
            }
            entity.addPart("facebookUrl", new StringBody(facebookUrl));
            entity.addPart("eventType", new StringBody(sltEventType));
            entity.addPart("lastDateOfRegistration", new StringBody(lastDay));
            entity.addPart("registrationFee", new StringBody(etRegFee.getText().toString()));

            String registrationLink = etRegLink.getText().toString();
            if (!registrationLink.contains("http://") && !registrationLink.contains("https://") && registrationLink.length() != 0) {
                registrationLink = "http://" + registrationLink;
            }
            entity.addPart("registrationLink", new StringBody(registrationLink));
            entity.addPart("playerId", new StringBody(LoadPref(Buddy.PlayerId)));
            if (list != null) {
                for (EventImageModel model : list) {
                    sourceFile = new File(model.getImagePath().getPath());
                    entity.addPart("eventImage[]", new FileBody(sourceFile));
                }
            } else {
                L.m("list is empty");
            }


            HttpReq ob = new HttpReq();
            response = ob.makeConnection(urls[0], HttpReq.POST, entity);
            L.m(response);
            if (response.contains("[")) {
                String finalResult = response.substring(response.indexOf('['), response.indexOf(']') + 1);

                try {
                    JSONArray data = new JSONArray(finalResult);
                    JSONObject key = data.getJSONObject(0);
                    if (!key.optString("status").equals("") && key.optString("status").equals("success")) {
                        output = key.optString("status");
                    } else if (key.optString("status").equals("failure")) {
                        output = key.optString("message");
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    L.m(e.toString());
                    ErrorMessage = e.toString() + response;
                    return null;
                }

            } else {
                L.m(response);
                ErrorMessage = response;
                return null;
            }
            return output;
            // return "success";

        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            if (activity != null) {
                dismissPDialog();

                if (result != null) {
                    if (result.equalsIgnoreCase("success")) {
                        // Toast.makeText(getActivity(), result,
                        // Toast.LENGTH_LONG).show();
                        if (sltEventType.equalsIgnoreCase("inviteOnly")) {
                            showReviewAlertSuccess(getActivity(), "createEventInviteOnly", 1);
                        } else {
                            showReviewAlertSuccess(getActivity(), "createEvent", 1);
                        }
                    } else {
                        Toast.makeText(getActivity(), result, Toast.LENGTH_LONG).show();
                    }

                } else {
                    showServerErrorAlertBox(ErrorMessage);
                }

            }

        }
    }

    public void showServerErrorAlertBox(String errorDetail) {
        showAlertBox(getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), 2, errorDetail);
    }

    public void showInternetAlertBox() {
        showAlertBox(getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), 2, null);
    }

    public void showAlertBox(String title, String Description, int noOfButtons, final String errorMessage) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.alert_dialog, null);
        dialogBuilder.setView(v);
        ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
        LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
        LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);
        ivAbout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (errorMessage != null) {
                    tvDescription.setText(errorMessage);
                }
            }
        });

        tvTitle.setText(title);
        tvDescription.setText(Description);
        if (noOfButtons == 1) {
            button2.setVisibility(View.GONE);
            llBtn2.setVisibility(View.GONE);
        }
        button1.setText("Retry");
        button1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if (isOnline()) {
                    uploadEventDetail = new UploadEventDetail(getActivity());
                    uploadEventDetail.execute(AppSettings.setEventRegistrationByMobile);
                } else {
                    showInternetAlertBox(getActivity());
                }
            }
        });
        button2.setText("Cancel");
        button2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

    }

    @SuppressLint("ValidFragment")
    public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            DatePickerDialog dpd = new DatePickerDialog(getActivity(), this, syear, smonth, sday);
            //DatePicker dp = dpd.getDatePicker();
            // Set the DatePicker minimum date selection to current date
            dpd.getDatePicker().setMinDate(calendar.getTimeInMillis());// get the current day
            // dp.setMinDate(System.currentTimeMillis() - 1000);// Alternate way
            // to get the current day

            // //Add 6 days with current date
            // calendar.add(Calendar.DAY_OF_MONTH,6);
            //
            // //Set the maximum date to select from DatePickerDialog
            // dp.setMaxDate(calendar.getTimeInMillis());

            return dpd;
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            lastDay = DateTimeUtility.convertDate(day, month, year);
            tvLastDayReg.setText(DateTimeUtility.convertDate(day, month, year));
            startDateTimeStamp = Integer.parseInt(DateTimeUtility.convertDateStamp(year, month, day));
            syear = year;
            smonth = month;
            sday = day;
        }

    }

    private void setDefaultCode() {
        String CountryID = "";
        String CountryZipCode = null;

        TelephonyManager manager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        // getNetworkCountryIso
        CountryID = manager.getSimCountryIso().toUpperCase();
        String[] rl = this.getResources().getStringArray(R.array.CountryCodes);
        for (int i = 0; i < rl.length; i++) {
            String[] g = rl[i].split(",");
            if (g[1].trim().equals(CountryID.trim())) {
                CountryZipCode = g[0];
                break;
            }
        }
        if (CountryZipCode != null) {
            etPCountryCode.setText(CountryZipCode);
            etSCountryCode.setText(CountryZipCode);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        // TODO Auto-generated method stub
        isSoftKeyboardDisplayed = true;
        return false;
    }
}
