package com.gennext.sports.fragments.venu;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.sports.R;
import com.gennext.sports.fragments.CompactFragment;
import com.gennext.sports.image_cache.ImageLoader;
import com.gennext.sports.model.FindVenueModel;
import com.gennext.sports.util.AppSettings;
import com.gennext.sports.util.Buddy;
import com.gennext.sports.util.HttpReq;
import com.gennext.sports.util.L;
import com.gennext.sports.util.internet.BasicNameValuePair;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainFindVenuDetailAbout extends CompactFragment {

	private String venueId, sportId, sportName, about, highlightDetails, openHours, memberShip, JsonData;
	TextView tvAbout, tvHighlights, tvOpenHours, tvMembership, tvCoachingFee,tabMemberText,tabNonMemberText;

	ArrayList<FindVenueModel> cList;
	RssFeedTask rssFeedTask;
	ImageView ivCat;// , ivCat1, ivCat2, ivCat3, ivCat4;
	// TextView tvCat1, tvCat2, tvCat3, tvCat4;
	ImageLoader imageLoader;
	// FilterFeeRes filterFeeRes;
	LinearLayout tabMember, tabNonMember, llJoin, llImInterested, llvenueSportcontainer;
	int selectFee, selectCat, MEM_FEE = 1, NON_MEM_FEE = 2;
	IamInterestedRequest iamInterestedRequest;
	ArrayList<String> sortList;
	int sportPosition;

	public void setData(String venueId, String sportId, String sportName, String about, String highlightDetails,
			String openHours, String memberShip, String JsonData) {
		this.venueId = venueId;
		this.sportId = sportId;
		this.sportName = sportName;
		this.about = about;
		this.highlightDetails = highlightDetails;
		this.openHours = openHours;
		this.memberShip = memberShip;
		this.JsonData = JsonData;
	}

	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (rssFeedTask != null) {
			rssFeedTask.onAttach(activity);
		}
		// if (filterFeeRes != null) {
		// filterFeeRes.onAttach(activity);
		// }
		if (iamInterestedRequest != null) {
			iamInterestedRequest.onAttach(activity);
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (rssFeedTask != null) {
			rssFeedTask.onDetach();
		}
		// if (filterFeeRes != null) {
		// filterFeeRes.onDetach();
		// }
		if (iamInterestedRequest != null) {
			iamInterestedRequest.onDetach();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.main_find_venu_detail_about, container, false);
		imageLoader = new ImageLoader(getActivity());

		tvAbout = (TextView) v.findViewById(R.id.tv_main_find_venue_detail_about_about);
		tvHighlights = (TextView) v.findViewById(R.id.tv_main_find_venue_detail_about_highlights);
		tvOpenHours = (TextView) v.findViewById(R.id.tv_main_find_venue_detail_about_openhours);
		tvMembership = (TextView) v.findViewById(R.id.tv_main_find_venue_detail_about_membership);
		tvCoachingFee = (TextView) v.findViewById(R.id.tv_main_find_venue_detail_about_coachingFee);
		tabMember = (LinearLayout) v.findViewById(R.id.ll_main_find_sbuddy_detail_about_memberTab);
		tabMemberText = (TextView) v.findViewById(R.id.tv_main_find_sbuddy_detail_about_memberTab);
		tabNonMember = (LinearLayout) v.findViewById(R.id.ll_main_find_sbuddy_detail_about_nonMemberTab);
		tabNonMemberText = (TextView) v.findViewById(R.id.tv_main_find_sbuddy_detail_about_nonMemberTab);
		llJoin = (LinearLayout) v.findViewById(R.id.ll_main_find_venue_detail_about_join);
		llImInterested = (LinearLayout) v.findViewById(R.id.ll_main_find_venue_detail_about_iamInterested);
		llvenueSportcontainer = (LinearLayout) v.findViewById(R.id.venue_sport_container);

		ivCat = (ImageView) v.findViewById(R.id.iv_main_find_venue_detail_about_sportCat);
 
		
		llJoin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				MainFindVenuJoin mainFindVenuJoin = new MainFindVenuJoin();
				mainFindVenuJoin.setData(venueId, cList);
//				FragmentManager mannager = getActivity().getFragmentManager();
//				FragmentTransaction tr2 = mannager.beginTransaction();
//				tr2.replace(android.R.id.content, mainFindVenuJoin, "mainFindVenuJoin");
//				tr2.addToBackStack("mainFindVenuJoin");
//				tr2.commit();
				replaceFragment(mainFindVenuJoin,"mainFindVenuJoin");
			}
		});
		llImInterested.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				iamInterestedRequest = new IamInterestedRequest(getActivity());
				iamInterestedRequest.execute(AppSettings.setIamIntrested);
			}
		});

		if (about != null) {
			tvAbout.setText(about);
		}
		if (highlightDetails != null) {
			tvHighlights.setText(highlightDetails);
		}
		if (openHours != null) {
			tvOpenHours.setText(openHours);
		}
		if (memberShip != null) {
			tvMembership.setText(memberShip);
		}

		rssFeedTask = new RssFeedTask(getActivity());
		rssFeedTask.execute(JsonData);

		tabMember.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				switchMemberTab(MEM_FEE);
				setSelectedSportOnUi(sportPosition);
			}

		});
		tabNonMember.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				switchMemberTab(NON_MEM_FEE);
				setSelectedSportOnUi(sportPosition);
			}
		});

		switchMemberTab(MEM_FEE);
		// switchTab(1);
		return v;
	}

	private void switchMemberTab(int key) {

		switch (key) {
		case 0:
			tabMember.setBackgroundResource(R.drawable.tab_venue_deselect);
			tabNonMember.setBackgroundResource(R.drawable.tab_venue_deselect);
			break;
		case 1:
			tabMember.setBackgroundResource(R.drawable.tab_venue_select);
			tabMemberText.setTextColor(getResources().getColor(R.color.white));
			tabNonMember.setBackgroundResource(R.drawable.tab_venue_deselect);
			tabNonMemberText.setTextColor(getResources().getColor(R.color.text));
			selectFee = MEM_FEE;
			break;
		case 2:
			tabMember.setBackgroundResource(R.drawable.tab_venue_deselect);
			tabMemberText.setTextColor(getResources().getColor(R.color.text));
			tabNonMember.setBackgroundResource(R.drawable.tab_venue_select);
			tabNonMemberText.setTextColor(getResources().getColor(R.color.white));
			selectFee = NON_MEM_FEE;
			break;

		}
	}

	private class RssFeedTask extends AsyncTask<String, Void, ArrayList<FindVenueModel>> {

		Activity activity;

		public RssFeedTask(Activity activity) {
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		private void onDetach() {
			this.activity = null;
		}

		@Override
		protected ArrayList<FindVenueModel> doInBackground(String... params) {
			// TODO Auto-generated method stub
			String response;
			cList = new ArrayList<FindVenueModel>();
			response = params[0];
			// L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray messageArray = new JSONArray(response);
					JSONObject messageObj = messageArray.getJSONObject(0);
					JSONArray message = messageObj.getJSONArray("message");
					for (int k = 0; k < message.length(); k++) {
						JSONObject sub = message.getJSONObject(k);
						L.m(sub.optString("venueId") + " : " + (venueId));
						if (sub.optString("venueId").equalsIgnoreCase(venueId)) {
							JSONArray sportPlay = sub.getJSONArray("sportPlay");
							L.m(sportPlay.toString());
							for (int i = 0; i < sportPlay.length(); i++) {
								JSONObject sPData = sportPlay.getJSONObject(i);
								if (!sPData.optString("sportName").equals("")) {
									FindVenueModel model = new FindVenueModel();
									model.setSportId(sPData.optString("sportId"));
									model.setSportName(sPData.optString("sportName"));
									model.setLogo(sPData.optString("logo"));
									model.setmemFee(sPData.optString("memFee"));
									model.setPnpFee(sPData.optString("pnpFee"));
									if (sPData.optString("sportName").trim().equalsIgnoreCase(sportName.trim())) {
										sportPosition = i;
										model.setSltStatus(1);
									} else {
										model.setSltStatus(0);
									}

									cList.add(model);
								}
							}
						}
					}
				} catch (JSONException e) {
					L.m(e.toString());
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				return null;
			}

			return cList;
		}

		@Override
		protected void onPostExecute(ArrayList<FindVenueModel> result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (activity != null) {
				if (result != null) {
					allocateSport(result);
					setSelectedSportOnUi(sportPosition);

				}else{
					cList=null;
				}
			}else{
				cList=null;
			}
		}
	}

	private void allocateSport(ArrayList<FindVenueModel> result) {
		for (int i = 0; i < result.size(); i++) {
			addImageDynamacally(i, result.get(i).getSportName(), result.get(i).getLogo(), result.get(i).getSltStatus());
		}
	}

	private void addImageDynamacally(final int position, String sportName, String sportImage, int status) {
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.MATCH_PARENT);
		params.setMargins(0, 0, 20, 0);
		LinearLayout layout = new LinearLayout(getActivity());
		layout.setOrientation(LinearLayout.VERTICAL);
		layout.setGravity(Gravity.CENTER);
		layout.setLayoutParams(params);

		LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		
		LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
				getResources().getDimensionPixelSize(R.dimen.slt_sport_image_size),
				getResources().getDimensionPixelSize(R.dimen.slt_sport_image_size));
		
		ImageView imageView = new ImageView(getActivity());
		imageView.setLayoutParams(params2);
		imageView.setTag(position);
		Picasso.with(getActivity())
				.load(sportImage)
				.placeholder(R.drawable.sports_bg_squire)
				.error(R.drawable.sports_bg_squire)
				.into(imageView);
//		imageLoader.DisplayImage(sportImage, imageView, "blank");

		if (status == 0) {
			imageView.setBackgroundResource(R.drawable.rounded_grey);
			imageView.setColorFilter(getResources().getColor(R.color.mySport_grey));
		} else {
			imageView.setBackgroundResource(R.drawable.rounded_green);
			imageView.setColorFilter(getResources().getColor(R.color.mySport_green));
		}

		layout.addView(imageView);

		TextView textView = new TextView(getActivity());
		textView.setLayoutParams(params1);
		textView.setText(sportName);
		textView.setGravity(Gravity.CENTER);
		setTypsFace(textView);
		layout.addView(textView);

		imageView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				int but_id = Integer.parseInt(v.getTag().toString().trim());
				int spPosition = sportPosition;
				if (but_id == position) {
					// Toast.makeText(getActivity(), String.valueOf(but_id),
					// Toast.LENGTH_SHORT).show();
					// sltLogic(but_id);

					View view = llvenueSportcontainer.getRootView();
					ImageView iv2 = (ImageView) view.findViewWithTag(spPosition);
					iv2.setBackgroundResource(R.drawable.rounded_grey);
					iv2.setColorFilter(getResources().getColor(R.color.mySport_grey));

					ImageView iv = (ImageView) view.findViewWithTag(but_id);
					iv.setBackgroundResource(R.drawable.rounded_green);
					iv.setColorFilter(getResources().getColor(R.color.mySport_green));

					sportPosition = but_id;
					setSelectedSportOnUi(but_id);
				}
			}

		});

		llvenueSportcontainer.addView(layout);

	}

	private void setSelectedSportOnUi(int position) {
		String coachingFees;
		if(cList!=null){
			ivCat.setColorFilter(getResources().getColor(R.color.mySport_green));
			imageLoader.DisplayImage(cList.get(position).getLogo(), ivCat, "blank");

			if (selectFee == MEM_FEE) {
				coachingFees = cList.get(position).getmemFee();
			} else if (selectFee == NON_MEM_FEE) {
				coachingFees = cList.get(position).getPnpFee();
			} else {
				coachingFees = "";
			}
			if (!coachingFees.equals("")) {
				tvCoachingFee.setText(coachingFees);
			} else {
				tvCoachingFee.setText("");
			}
			sportId = cList.get(position).getSportId();
			sportName = cList.get(position).getSportName();
			MainFindVenuDetail.sportId = sportId;
			MainFindVenuDetail.sportName = sportName;
		}
	}

	private class IamInterestedRequest extends AsyncTask<String, Void, String> {

		private Activity activity;

		public IamInterestedRequest(Activity activity) {
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showPDialog(getActivity(), "Processing, please wait...");
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error";
			L.m("PlayerId " + LoadPref(Buddy.PlayerId));
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				// sportId,choosePlan, chooseDate, chooseSlot, playerId, venueId
				params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
				params.add(new BasicNameValuePair("sportId", sportId));
				params.add(new BasicNameValuePair("venueId", venueId));

			}

			HttpReq ob = new HttpReq();
			response = ob.makeConnection(urls[0], HttpReq.POST, params);
			String output = null;
			L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray json = new JSONArray(response);
					for (int i = 0; i < json.length(); i++) {
						JSONObject obj = json.getJSONObject(i);
						if (obj.optString("status").equals("success")) {
							output = "success";

						} else if (obj.optString("status").equals("failure")) {
							output = obj.optString("message");
						}
					}

				} catch (JSONException e) {
					L.m("Json Error :" + e.toString());
					ErrorMessage = e.toString() + response;
					return null;
					// return e.toString();
				}
			} else {
				L.m("Invalid JSON found : " + response);
				ErrorMessage = response;
				// return response;
				return null;
			}

			return output;
			// return "success";

		}

		@Override
		protected void onPostExecute(String result) {

			if (activity != null) {
				dismissPDialog();
				if (result != null) {
					L.m(result);
					if (result.equalsIgnoreCase("success")) {
						showReviewAlertSuccess(getActivity(), "iaminterested");
					} else {
						Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
					}
				} else {
					showServerErrorAlertBox(ErrorMessage);
				}
			}
		}
	}

	public void showServerErrorAlertBox(String errorDetail) {
		showAlertBox(getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), 2, errorDetail);
	}

	public void showInternetAlertBox() {
		showAlertBox(getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), 2, null);
	}

	public void showAlertBox(String title, String Description, int noOfButtons, final String errorMessage) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = getActivity().getLayoutInflater();

		View v = inflater.inflate(R.layout.alert_dialog, null);
		dialogBuilder.setView(v);
		ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
		Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
		Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
		final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
		LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
		LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);
		ivAbout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (errorMessage != null) {
					tvDescription.setText(errorMessage);
				}
			}
		});

		tvTitle.setText(title);
		tvDescription.setText(Description);
		if (noOfButtons == 1) {
			button2.setVisibility(View.GONE);
			llBtn2.setVisibility(View.GONE);
		}
		button1.setText("Retry");
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				if (isOnline()) {
					iamInterestedRequest = new IamInterestedRequest(getActivity());
					iamInterestedRequest.execute(AppSettings.setIamIntrested);
				} else {
					showInternetAlertBox(getActivity());
				}
			}
		});
		button2.setText("Cancel");
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}

}
