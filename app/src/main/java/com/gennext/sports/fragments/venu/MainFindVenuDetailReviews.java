package com.gennext.sports.fragments.venu;

import java.util.ArrayList;
import java.util.List;

import com.gennext.sports.util.internet.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gennext.sports.MainActivity;
import com.gennext.sports.R;
import com.gennext.sports.fragments.CompactFragment;
import com.gennext.sports.model.FindCoachModel;
import com.gennext.sports.model.FindCoachReviewsAdapter;
import com.gennext.sports.util.AppSettings;
import com.gennext.sports.util.Buddy;
import com.gennext.sports.util.HttpReq;
import com.gennext.sports.util.L;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainFindVenuDetailReviews extends CompactFragment {

	ListView lvMain;
	private String jsonData, venueId, sportId;
	ArrayList<FindCoachModel> myReviewList;
	LoadReviewList loadReviewList;
	Button btnSendReview;
	SendReview sendReview;
//	private AlertDialog dialog = null;
	ProgressBar progressBar;
	FindCoachReviewsAdapter ExpAdapter ;
	OnVenueReviewCount comm;
	String ratingCount,reviewCount;
	String tempReview="";
	float tempRating=0.0f;

	public void setData(String JsonData, String venueId, String sportId) {
		this.jsonData = JsonData;
		this.venueId = venueId;
		this.sportId = sportId;
	}

	public void setCommunicator(Activity onVenueReviewCount) {
		this.comm=(MainActivity)onVenueReviewCount;
	}

	public interface OnVenueReviewCount{
		public void onVenueReviewCount(String review,String rating);
	}
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (loadReviewList != null) {
			loadReviewList.onAttach(activity);
		}
		if (sendReview != null) {
			sendReview.onAttach(activity);
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (loadReviewList != null) {
			loadReviewList.onDetach();
		}
		if (sendReview != null) {
			sendReview.onDetach();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.main_find_venu_detail_reviews, container, false);

		lvMain = (ListView) v.findViewById(R.id.lv_main_find_venue_detail_reviews);
		btnSendReview = (Button) v.findViewById(R.id.btn_main_find_venue_detail_reviews);
		progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
		setTypsFace(btnSendReview);
		btnSendReview.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// sendReview = new SendReview(getActivity(), "5", "review");
				// sendReview.execute(AppSettings.setCoachReview);
				showReviewAlert(getActivity());
			}
		});
		// L.m(jsonData);
		loadReviewList = new LoadReviewList(getActivity());
		loadReviewList.execute(AppSettings.getVenueRating);
		return v;
	}

	private class LoadReviewList extends AsyncTask<String, Void, String> {

		private Activity activity;

		public LoadReviewList(Activity activity) {
			this.activity = activity;
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressBar.setVisibility(View.VISIBLE);
			viewTime();
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error";
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				// L.m("Player Id : "+LoadPref(AppTokens.PlayerId));
				params.add(new BasicNameValuePair("venueId", venueId));
				params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
				params.add(new BasicNameValuePair("sportId", sportId)); 
			}

			HttpReq ob = new HttpReq();
			response = ob.makeConnection(urls[0], 1, params);
			String output = null;
			myReviewList = new ArrayList<FindCoachModel>();
			L.m(response);
			if (response.contains("[")) {
				try {

					JSONArray review = new JSONArray(response);
					JSONObject coachData = review.getJSONObject(0);
					if(coachData.optString("status").equalsIgnoreCase("success")){
						JSONArray message = coachData.getJSONArray("message");
						JSONObject messageObj = message.getJSONObject(0);
						ratingCount=messageObj.optString("rating");
						reviewCount=messageObj.optString("reviewCount");
						
						JSONArray main = messageObj.getJSONArray("message");
						for (int k = 0; k < main.length(); k++) {
							JSONObject sPData = main.getJSONObject(k);
							if(!sPData.optString("fullname").equals("")){
								response="success";
								FindCoachModel model = new FindCoachModel();
								model.setFullName(sPData.optString("fullname"));
								model.setReview(sPData.optString("review"));
								model.setRating(sPData.optString("rating"));
								myReviewList.add(model);
							}
							
						}
					}else if(coachData.optString("status").equalsIgnoreCase("failure")){
						response="failure";
						ErrorMessage=coachData.optString("message");
					}
					
				} catch (JSONException e) {
					L.m(e.toString());
					ErrorMessage=e.toString()+response;
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				ErrorMessage=response;
				return null;
			}

			return response;
			// return "success";

		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			if (activity != null) {
				progressBar.setVisibility(View.GONE);
				if (result != null) {
					if(result.equals("success")){
						ExpAdapter = new FindCoachReviewsAdapter(activity,
								R.layout.custom_slot_find_coach_reviews, myReviewList);
						lvMain.setAdapter(ExpAdapter);

						comm.onVenueReviewCount(reviewCount, ratingCount);
					}else{
						Toast.makeText(getActivity(), ErrorMessage, Toast.LENGTH_SHORT).show();
					}
					
				} else {
					showServerErrorAlertBox(ErrorMessage);
				}
			}
		}
	}

	public void getColorList() {
		
		
	}
	
	
	private class SendReview extends AsyncTask<String, Void, String> {

		private Activity activity;
		String rating, review;

		public SendReview(Activity activity, String rating, String review) {
			this.rating = rating;
			this.review = review;
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showPDialog(getActivity(), "Processing please wait");
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error";
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				// L.m("Player Id : "+LoadPref(AppTokens.PlayerId));
				params.add(new BasicNameValuePair("venueId", venueId));
				params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
				params.add(new BasicNameValuePair("sportId", sportId));
				params.add(new BasicNameValuePair("rating", rating));
				params.add(new BasicNameValuePair("review", review));
			}

			HttpReq ob = new HttpReq();
			response = ob.makeConnection(urls[0], 3, params);
			String output = null;
			L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray json = new JSONArray(response);
					for (int i = 0; i < json.length(); i++) {
						JSONObject obj = json.getJSONObject(i);
						if (obj.optString("status").equals("success")) {
							output = "success";

						} else if (obj.optString("status").equals("failure")) {
							output = obj.optString("message");
						}
					}

				} catch (JSONException e) {
					L.m("Json Error :" + e.toString());
					return null;
					// return e.toString();
				}
			} else {
				L.m("Invalid JSON found : " + response);
				// return response;
				return null;
			}

			return output;
			// return "success";

		}

		@Override
		protected void onPostExecute(String result) {

			if (activity != null) {
				dismissPDialog();
				if (result != null) {
					L.m(result);
					if (result.equalsIgnoreCase("success")) {
						showReviewAlertSuccess(getActivity(),"reviewVenue"); 
						loadReviewList = new LoadReviewList(getActivity());
						loadReviewList.execute(AppSettings.getVenueRating);
						
					} else {
						Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
					}
				} else {
//					Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.internet_error_msg),
//							Toast.LENGTH_SHORT).show();
					Button retry=showBaseServerErrorAlertBox(ErrorMessage);
					retry.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View view) {
							if(dialog!=null)dialog.dismiss();
							sendReview = new SendReview(getActivity(), String.valueOf(tempRating),
									tempReview);
							sendReview.execute(AppSettings.setVenueRating);
						}
					});
				}
			}
		}
	}

	public void showReviewAlert(Activity Act) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Act);

		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = Act.getLayoutInflater();

		View v = inflater.inflate(R.layout.custorm_find_coach_sendreview, null);
		dialogBuilder.setView(v);
		Button sendButton = (Button) v.findViewById(R.id.btn_custorm_find_coach_sendreview_send);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_custorm_find_coach_sendreview_title);
		final EditText etReview = (EditText) v.findViewById(R.id.et_custorm_find_coach_sendreview_review);
		final RatingBar rbRating = (RatingBar) v.findViewById(R.id.rb_custorm_find_coach_sendreview_rating);

		tvTitle.setText("Rate Us");
		sendButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				hideKeyboard(getActivity());
				tempReview =etReview.getText().toString();
				tempRating=rbRating.getRating();
				sendReview = new SendReview(getActivity(), String.valueOf(tempRating),
						tempReview);
				sendReview.execute(AppSettings.setVenueRating);
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}
	
	public void showServerErrorAlertBox(String errorDetail) {
		showAlertBox(getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), 2, errorDetail);
	}

	public void showInternetAlertBox() {
		showAlertBox(getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), 2, null);
	}

	public void showAlertBox(String title, String Description, int noOfButtons, final String errorMessage) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = getActivity().getLayoutInflater();

		View v = inflater.inflate(R.layout.alert_dialog, null);
		dialogBuilder.setView(v);
		ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
		Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
		Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
		final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
		LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
		LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);
		ivAbout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (errorMessage != null) {
					tvDescription.setText(errorMessage);
				}
			}
		});

		tvTitle.setText(title);
		tvDescription.setText(Description);
		if (noOfButtons == 1) {
			button2.setVisibility(View.GONE);
			llBtn2.setVisibility(View.GONE);
		}
		button1.setText("Retry");
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				if (isOnline()) {
					loadReviewList = new LoadReviewList(getActivity());
					loadReviewList.execute(AppSettings.getVenueRating);
					
				} else {
					showInternetAlertBox(getActivity());
				}
			}
		});
		button2.setText("Cancel");
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}
}
