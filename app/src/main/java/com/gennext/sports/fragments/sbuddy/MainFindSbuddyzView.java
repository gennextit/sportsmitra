package com.gennext.sports.fragments.sbuddy;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gennext.sports.MainActivity;
import com.gennext.sports.R;
import com.gennext.sports.fragments.CompactFragment;
import com.gennext.sports.image_cache.ImageLoader;
import com.gennext.sports.model.FindBuddyAdapter;
import com.gennext.sports.model.FindBuddyModel;
import com.gennext.sports.util.Buddy;
import com.gennext.sports.util.DividerItemDecoration;
import com.gennext.sports.util.L;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

public class MainFindSbuddyzView extends CompactFragment {
    LinearLayout tabLeft, tabRight;
    ImageView ivSportBaseImage;
    ArrayList<FindBuddyModel> mySportList;
    private String selectSportId, selectSportName;
    private ProgressBar progressBar;
    private static String setSearchBuddyData;
    private String sportBaseImage;
    public ImageLoader imageLoader;

    LeftTabLoadData leftTabLoadData;
    RightTabLoadData rightTabLoadData;
    TextView tvSportCount;
    int SportCounter = 0;
    FragmentManager mannager;
    String PlayerId;
    public static int TAB_RADIUS = 1, TAB_LOCALITY = 2, SWITCH = 1;
    private RecyclerView lvMain;

    public void setSelectSport(String sltSport, String selectSportName) {
        this.selectSportId = sltSport;
        this.selectSportName = selectSportName;
    }

    public void setSearchBuddyData(String setSearchBuddyData) {
        this.setSearchBuddyData = setSearchBuddyData;
    }

    public void setSportBaseImage(String sportBaseImage) {
        this.sportBaseImage = sportBaseImage;
    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (leftTabLoadData != null) {
            leftTabLoadData.onAttach(getActivity());
        }
        if (rightTabLoadData != null) {
            rightTabLoadData.onAttach(getActivity());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.main_search_sbuddyz_view_2, container, false);
        mannager = getFragmentManager();
        setActionBarOption(view);
        PlayerId = LoadPref(Buddy.PlayerId);
        tabLeft = (LinearLayout) view.findViewById(R.id.ll_find_tabLeft);
        tabRight = (LinearLayout) view.findViewById(R.id.ll_find_tabRight);
        ivSportBaseImage = (ImageView) view.findViewById(R.id.iv_search_sbuddyz_view_baseSport);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
        tvSportCount = (TextView) view.findViewById(R.id.tv_search_sbuddyz_view_SportCount);


        lvMain = (RecyclerView) view.findViewById(R.id.lv_main);
        LinearLayoutManager horizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lvMain.setLayoutManager(horizontalManager);
        lvMain.addItemDecoration(new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL_LIST));
        lvMain.setItemAnimator(new DefaultItemAnimator());


        tabLeft.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                switchTab(TAB_RADIUS);
            }
        });
        tabRight.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                switchTab(TAB_LOCALITY);
            }
        });
        switchTab(SWITCH);

        imageLoader = new ImageLoader(getActivity());
        if (sportBaseImage != null) {
            imageLoader.DisplayImage(sportBaseImage, ivSportBaseImage, "base_medium", false);
        }

        return view;
    }

    public void onItemSelect(FindBuddyModel item) {
        MainFindSbuddyDetailMain mainFindSbuddyDetailMain = new MainFindSbuddyDetailMain();
        // mainSearchSbuddyz.setCommunicator(AppIntroActivity.this);
				/*MainFindSbuddyDetail.openFromMain=true;
				MainFindSbuddyDetail.openFromChat=false;*/
        if (mySportList != null) {
            mainFindSbuddyDetailMain.setData("search", item.getsBudddy(), item.getAbout(),
                    item.getLocation(), item.getClubOrApartment(),
                    item.getCity());
            mainFindSbuddyDetailMain.setPlayerProfile(item.getPlayerId(),
                    item.getFullName(), item.getImageUrl(),
                    item.getGender(), item.getAge(), "online");
            mainFindSbuddyDetailMain.setJSONData(setSearchBuddyData);
            FragmentManager mannager = getFragmentManager();
            FragmentTransaction transaction = mannager.beginTransaction();
            transaction.replace(android.R.id.content, mainFindSbuddyDetailMain, "mainFindSbuddyDetail");
            transaction.addToBackStack("mainFindSbuddyDetail");
            transaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            transaction.commit();
        }

    }

    public void setActionBarOption(View view) {
        LinearLayout ActionBack, ActionHome;
        ActionBack = (LinearLayout) view.findViewById(R.id.ll_actionbar_back);
        ActionHome = (LinearLayout) view.findViewById(R.id.ll_actionbar_home);
		TextView actionbarTitle=(TextView)view.findViewById(R.id.actionbar_title);
		actionbarTitle.setText("SMITRA");
        ActionBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                mannager.popBackStack("mainFindSbuddyzView", FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        });
        ActionHome.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                ((MainActivity) getActivity()).SwitchTab();
                mannager.popBackStack("mainFindSbuddyz", FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        });
    }

    @SuppressLint("NewApi")
    private void switchTab(int key) {

        switch (key) {
            case 1:
                tabLeft.setBackground(getResources().getDrawable(R.drawable.tab_left_green));
                tabRight.setBackground(getResources().getDrawable(R.drawable.tab_right_grey));
                leftTabLoadData = new LeftTabLoadData(getActivity());
                leftTabLoadData.execute();
                break;

            case 2:
                tabLeft.setBackground(getResources().getDrawable(R.drawable.tab_left_grey));
                tabRight.setBackground(getResources().getDrawable(R.drawable.tab_right_green));
                rightTabLoadData = new RightTabLoadData(getActivity());
                rightTabLoadData.execute();
                break;
        }
    }


    private class LeftTabLoadData extends AsyncTask<Void, Void, Boolean> {
        Activity activity;

        public LeftTabLoadData(Activity activity) {
            onAttach(activity);
        }

        public void onAttach(Activity activity) {
            this.activity = activity;
        }

        private void onDetach() {
            this.activity = null;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            // progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(Void... urls) {
            // String response = LoadPref(AppTokens.SearchBuddyData);
            String response = setSearchBuddyData;
            if (response == null) {
                return null;
            }
            SportCounter = 0;
            Boolean output = false;
            int sLogoCount = 0, logoOne = 0, logoTwo = 0, logoThree = 0;
            mySportList = new ArrayList<FindBuddyModel>();
            if (response.contains("[")) {
                try {
                    JSONArray messageArray = new JSONArray(response);
                    JSONObject messageObj = messageArray.getJSONObject(0);
                    JSONArray message = messageObj.getJSONArray("message");
                    for (int j = 0; j < message.length(); j++) {
                        JSONObject messageData = message.getJSONObject(j);
                        if (messageData.optString("radius").equalsIgnoreCase("y")
                                && !PlayerId.equalsIgnoreCase(messageData.optString("playerId"))) {
                            SportCounter++;
                            output = true;
                            sLogoCount = 0;
                            logoOne = 0;
                            logoTwo = 0;
                            logoThree = 0;
                            FindBuddyModel model = new FindBuddyModel();
                            model.setsBudddy(messageData.optString("sBudddy"));
                            model.setPlayerId(messageData.optString("playerId"));
                            model.setFullName(messageData.optString("fullName"));
                            model.setAge(messageData.optString("age"));
                            model.setGender(messageData.optString("gender"));
                            model.setKm(Float.parseFloat(messageData.optString("km")));
                            model.setImageUrl(messageData.optString("imageUrl"));
                            model.setLocation(messageData.optString("location"));
                            model.setAbout(messageData.optString("about"));
                            model.setCity(messageData.optString("city"));
                            model.setClubOrApartment(messageData.optString("clubOrApartment"));

                            JSONArray sportPlay = messageData.getJSONArray("sportPlay");
                            for (int k = 0; k < sportPlay.length(); k++) {
                                sLogoCount++;
                                JSONObject sPdata = sportPlay.getJSONObject(k);
                                if (sPdata.optString("sportName").trim().equalsIgnoreCase(selectSportName.trim())) {
                                    model.setsLogo1(sPdata.optString("logo"));
                                    logoOne = 1;
                                } else if (logoTwo == 0) {
                                    model.setsLogo2(sPdata.optString("logo"));
                                    logoTwo = 1;
                                } else if (logoThree == 0) {
                                    model.setsLogo3(sPdata.optString("logo"));
                                    logoThree = 1;
                                }

                            }
                            model.setsLogoCount(String.valueOf(sLogoCount));
                            mySportList.add(model);

                        }

                    }

                } catch (JSONException e) {
                    L.m(e.toString());
                    return null;
                }
            } else {
                L.m("Invalid JSON found : " + response);
                return null;
            }
            if (output) {
                // Sort by km.
                Collections.sort(mySportList, FindBuddyModel.COMPARE_BY_KILOMETER);
            }
            return output;
            // return "success";

        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(Boolean result) {

            // progressBar.setVisibility(View.GONE);
            if (activity != null && result != null) {
                if (result) {
//					FindBuddyAdapter ExpAdapter = new FindBuddyAdapter(getActivity(), R.layout.custom_slot_find_sbuddyz,
//							mySportList);
//					lvMain.setAdapter(ExpAdapter);
                    FindBuddyAdapter adapter = new FindBuddyAdapter(getActivity(), mySportList, MainFindSbuddyzView.this);
                    lvMain.setAdapter(adapter);

                    tvSportCount.setVisibility(View.VISIBLE);
                    tvSportCount.setText(String.valueOf(SportCounter) + " Players");
                } else {
                    setErrorAdapter(lvMain);
                    mySportList = null;
                    tvSportCount.setVisibility(View.GONE);
                }
            }
        }
    }

    private class RightTabLoadData extends AsyncTask<Void, Void, Boolean> {

        Activity activity;

        public RightTabLoadData(Activity activity) {
            onAttach(activity);
        }

        public void onAttach(Activity activity) {
            this.activity = activity;
        }

        private void onDetach() {
            this.activity = null;
        }

        @Override
        protected Boolean doInBackground(Void... urls) {
            // String response = LoadPref(AppTokens.SearchBuddyData);
            String response = setSearchBuddyData;
            if (response == null) {
                return null;
            }
            SportCounter = 0;
            Boolean output = false;
            int sLogoCount = 0, logoOne = 0, logoTwo = 0, logoThree = 0;
            mySportList = new ArrayList<FindBuddyModel>();
            if (response.contains("[")) {
                try {
                    JSONArray messageArray = new JSONArray(response);
                    JSONObject messageObj = messageArray.getJSONObject(0);
                    JSONArray message = messageObj.getJSONArray("message");
                    for (int j = 0; j < message.length(); j++) {
                        JSONObject messageData = message.getJSONObject(j);
                        if (messageData.optString("clubOrApartmentAvailable").equalsIgnoreCase("Y")
                                && !PlayerId.equalsIgnoreCase(messageData.optString("playerId"))) {
                            output = true;
                            SportCounter++;
                            sLogoCount = 0;
                            logoOne = 0;
                            logoTwo = 0;
                            logoThree = 0;
                            FindBuddyModel model = new FindBuddyModel();
                            model.setsBudddy(messageData.optString("sBudddy"));
                            model.setPlayerId(messageData.optString("playerId"));
                            model.setFullName(messageData.optString("fullName"));
                            model.setAge(messageData.optString("age"));
                            model.setGender(messageData.optString("gender"));
                            model.setKm(0.0f);
                            model.setImageUrl(messageData.optString("imageUrl"));
                            model.setClubOrApartmentAvailable(messageData.optString("clubOrApartmentAvailable"));
                            model.setClubOrApartment(messageData.optString("clubOrApartment"));
                            model.setAbout(messageData.optString("about"));
                            model.setCity(messageData.optString("city"));
                            model.setLocation(messageData.optString("location"));

                            JSONArray sportPlay = messageData.getJSONArray("sportPlay");
                            for (int k = 0; k < sportPlay.length(); k++) {
                                sLogoCount++;
                                JSONObject sPdata = sportPlay.getJSONObject(k);
                                if (sPdata.optString("sportName").trim().equalsIgnoreCase(selectSportName.trim())) {
                                    model.setsLogo1(sPdata.optString("logo"));
                                    logoOne = 1;
                                } else if (logoTwo == 0) {
                                    model.setsLogo2(sPdata.optString("logo"));
                                    logoTwo = 1;
                                } else if (logoThree == 0) {
                                    model.setsLogo3(sPdata.optString("logo"));
                                    logoThree = 1;
                                }

                            }
                            model.setsLogoCount(String.valueOf(sLogoCount));
                            mySportList.add(model);

                        }

                    }

                } catch (JSONException e) {
                    L.m(e.toString());
                    return null;
                }
            } else {
                L.m("Invalid JSON found : " + response);
                return null;
            }

            return output;
            // return "success";

        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(Boolean result) {

            // progressBar.setVisibility(View.GONE);
            if (activity != null && result != null) {
                if (result) {
//					FindBuddyAdapter ExpAdapter = new FindBuddyAdapter(getActivity(), R.layout.custom_slot_find_sbuddyz,
//							mySportList);
//					lvMain.setAdapter(ExpAdapter);
                    FindBuddyAdapter adapter = new FindBuddyAdapter(getActivity(), mySportList, MainFindSbuddyzView.this);
                    lvMain.setAdapter(adapter);

                    tvSportCount.setVisibility(View.VISIBLE);
                    tvSportCount.setText(String.valueOf(SportCounter) + " Players");
                } else {
                    setErrorAdapter(lvMain);

                    mySportList = null;
                    tvSportCount.setVisibility(View.GONE);
                }
            }
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (leftTabLoadData != null) {
            leftTabLoadData.onDetach();
        }
        if (rightTabLoadData != null) {
            rightTabLoadData.onDetach();
        }
    }
}