package com.gennext.sports.fragments.event;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gennext.sports.GCMIntentService;
import com.gennext.sports.R;
import com.gennext.sports.fragments.CompactFragment;
import com.gennext.sports.model.MyEventAdapter;
import com.gennext.sports.model.MyEventModel;
import com.gennext.sports.util.AppSettings;
import com.gennext.sports.util.AppTokens;
import com.gennext.sports.util.Buddy;
import com.gennext.sports.util.Coach;
import com.gennext.sports.util.HttpReq;
import com.gennext.sports.util.L;
import com.gennext.sports.util.Utility;
import com.gennext.sports.util.Venue;
import com.gennext.sports.util.internet.BasicNameValuePair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyCreatedEvents extends CompactFragment {
	Button test;
	EditText et;
	ListView lvMain;
	ProgressBar progressBar;
	FragmentManager mannager;
	ArrayList<MyEventModel> myEventList;
	ArrayList<MyEventModel> eventImageList;
	MyEventAdapter adapter;
	HttpTask httpTask;
	String tempJson;
	public static int SWITCH, SBUDDY = 1, COACH = 2, VENUE = 3;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (httpTask != null) {
			httpTask.onAttach(activity);
		}

	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (httpTask != null) {
			httpTask.onDetach();
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.my_created_events, container, false);
		initUi(v);

		LoadTask(SWITCH);

		return v;
	}

	private void LoadTask(int s) {
		httpTask = new HttpTask(getActivity(), progressBar);
		switch (s) {
		case 1:
			httpTask.execute(AppSettings.getMyEvents);
			break;
		case 2:
			httpTask.execute(AppSettings.getCoachEvents);
			break;
		case 3:
			httpTask.execute(AppSettings.getVenueEvents);
			break;

		}
		
	}

	private void initUi(View v) {
		setActionBarOption(v);

		lvMain = (ListView) v.findViewById(R.id.lv_my_created_event_list);
		progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);

		lvMain.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int pos, long arg3) {
				MyCreatedEventsView myCreatedEventsView = new MyCreatedEventsView();
				if (myEventList != null) {
					myCreatedEventsView.setEventData(myEventList.get(pos).getEventName(),
							myEventList.get(pos).getEventImages(), myEventList.get(pos).getSportImage(),
							myEventList.get(pos).getImgList());
					myCreatedEventsView.setAboutData(myEventList.get(pos).getEventType(),
							myEventList.get(pos).getStartDate(), myEventList.get(pos).getEndDate(),
							myEventList.get(pos).getStartTime(), myEventList.get(pos).getEndTime(),
							myEventList.get(pos).getMoreDetails(), myEventList.get(pos).getAgeGroup(),
							myEventList.get(pos).getRegistrationFee(),
							myEventList.get(pos).getLastDateOfRegistration());
					myCreatedEventsView.setLocationData(myEventList.get(pos).getAddress(),
							myEventList.get(pos).getLocation(), "N/A");
					// myCreatedEventsView.setContactData(myEventList.get(pos).getWebUrl(),
					// myEventList.get(pos).getpMobile(),
					// myEventList.get(pos).getpEmail());
					myCreatedEventsView.setContactData(myEventList.get(pos).getWebUrl(),
							myEventList.get(pos).getFacebookUrl(), myEventList.get(pos).getRegistrationFee(),
							myEventList.get(pos).getRegistrationLink(), myEventList.get(pos).getpFullName(),
							myEventList.get(pos).getpMobile(), myEventList.get(pos).getpEmail(),
							myEventList.get(pos).getsFullName(), myEventList.get(pos).getsMobile(),
							myEventList.get(pos).getsEmail());
//					FragmentManager mannager = getActivity().getFragmentManager();
//					FragmentTransaction transaction = mannager.beginTransaction();
//					transaction.replace(android.R.id.content,);
//					transaction.addToBackStack("myCreatedEventsView");
//					transaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
//					transaction.commit();
					replaceFragment( myCreatedEventsView, "myCreatedEventsView");
				}

			}
		});

	}

	public void setActionBarOption(View view) {
		LinearLayout ActionBack, ActionHome;
		ActionBack = (LinearLayout) view.findViewById(R.id.ll_actionbar_back);
		ActionHome = (LinearLayout) view.findViewById(R.id.ll_actionbar_home);
		// TextView
		// actionbarTitle=(TextView)view.findViewById(R.id.actionbar_title);
		// actionbarTitle.setText(setActionBarTitle("EVENTS"));
		ActionBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				mannager.popBackStack("myCreatedEvents", FragmentManager.POP_BACK_STACK_INCLUSIVE);

			}
		});
		ActionHome.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				switch (SWITCH) {
				case 1:
					mannager.popBackStack("mainRSSFeed", 0);
					break;
				case 2:
					mannager.popBackStack("myCreatedEvents", FragmentManager.POP_BACK_STACK_INCLUSIVE);
					break;
				case 3:
					mannager.popBackStack("myCreatedEvents", FragmentManager.POP_BACK_STACK_INCLUSIVE);
					break;

				}
			}
		});
	}

	private class HttpTask extends AsyncTask<String, Void, String> {
		ProgressBar pBar;
		Activity activity;

		public HttpTask(Activity activity, ProgressBar pBar) {
			this.pBar = pBar;
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		private void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error";
			HttpReq ob = new HttpReq();
			myEventList = new ArrayList<MyEventModel>();
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				switch (SWITCH) {
				case 1:
					params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
					break;
				case 2:
					params.add(new BasicNameValuePair("coachId", LoadPref(Coach.CoachId)));
					if( Utility.LoadPref(activity, AppTokens.SportList).equals("")){
						SavePref(AppTokens.SportList, ob.makeConnection(AppSettings.getSportList, HttpReq.GET, params)); 
					}
					break;
				case 3:
					params.add(new BasicNameValuePair("venueId", LoadPref(Venue.VenueId)));
					if( Utility.LoadPref(activity, AppTokens.SportList).equals("")){
						SavePref(AppTokens.SportList, ob.makeConnection(AppSettings.getSportList, HttpReq.GET, params)); 
					}
					break;

				}
				
				
			}
			if (tempJson != null) {
				response = tempJson;
			} else {
				response = ob.makeConnection(urls[0], HttpReq.GET, params);
				tempJson = response;
			}
			String output = "not available";
			// L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray json = new JSONArray(response);
					JSONObject jobj = json.getJSONObject(0);
					if (jobj.optString("status").equalsIgnoreCase("success")) {
						JSONArray objArr = jobj.getJSONArray("message");
						for (int i = 0; i < objArr.length(); i++) {

							JSONObject obj = objArr.getJSONObject(i);
							if (!obj.optString("eventID").equals("")) {
								output = "success";
								MyEventModel model = new MyEventModel();
								model.setEventType(obj.optString("eventType"));
								model.setEventID(obj.optString("eventID"));
								model.setEventName(obj.optString("eventName"));
								model.setSport(obj.optString("sport"));
								model.setAgeGroup(obj.optString("ageGroup"));
								model.setAddress(obj.optString("address"));
								model.setLocation(obj.optString("location"));
								model.setStartDate(obj.optString("startDate"));
								model.setEndDate(obj.optString("endDate"));
								model.setStartTime(obj.optString("startTime"));
								model.setEndTime(obj.optString("endTime"));
								model.setMoreDetails(obj.optString("moreDetails"));

								model.setpFullName(obj.optString("pFullName"));
								model.setpMobile(obj.optString("pMobile"));
								model.setpEmail(obj.optString("pEmail"));
								model.setsFullName(obj.optString("sFullName"));
								model.setsMobile(obj.optString("sMobile"));
								model.setsEmail(obj.optString("sEmail"));
								model.setWebUrl(obj.optString("webUrl"));
								model.setFacebookUrl(obj.optString("facebookUrl"));
								model.setRegistrationFee(obj.optString("registrationFee"));
								model.setLastDateOfRegistration(obj.optString("lastDateOfRegistration"));
								model.setRegistrationLink(obj.optString("registrationLink"));
								model.setOwnerId(obj.optString("ownerId"));
								JSONArray eventImage = obj.getJSONArray("eventImages");
								eventImageList = new ArrayList<MyEventModel>();
								for (int k = 0; k < eventImage.length(); k++) {
									JSONObject eImageObj = eventImage.getJSONObject(k);
									MyEventModel img = new MyEventModel();
									img.setEventImages(eImageObj.optString("image"));
									if (k == 0)
										model.setEventImages(eImageObj.optString("image"));
									eventImageList.add(img);
								}
								if (activity != null) {
									
									model.setSportImage(Buddy.getSportImage(getActivity(), obj.optString("sport")));
								}

								model.setImgList(eventImageList);
								myEventList.add(model);
							}
						}
					} else if (jobj.optString("status").equalsIgnoreCase("failure")) {
						output = "failure";
						ErrorMessage = jobj.optString("message");
					}

				} catch (JSONException e) {
					L.m("Json Error :" + e.toString());
					ErrorMessage = e.toString() + "\n" + response;
					return null;
				}
			} else {
				L.m("Server Error : " + response);
				ErrorMessage = response;
				return null;
			}

			return output;
			// return "success";

		}

		@Override
		protected void onPostExecute(String result) {
			if (activity != null) {
				pBar.setVisibility(View.GONE);
				if (result != null) {

					switch (result) {
					case "success":
						adapter = new MyEventAdapter(getActivity(), R.layout.custom_notification_slot, myEventList);
						lvMain.setAdapter(adapter);

						break;
					case "failure":
						ArrayList<String> errorList = new ArrayList<String>();
						errorList.add("No record available");
						ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
								R.layout.custom_error_slot, R.id.tv_error_text, errorList);
						lvMain.setAdapter(adapter);
						myEventList = null;
						break;
					case "not available":

						break;
					}
				} else {
					showServerErrorAlertBox(ErrorMessage);
				}
			}

		}
	}

	public void showServerErrorAlertBox(String errorDetail) {
		showAlertBox(getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), 2, errorDetail);
	}

	public void showInternetAlertBox() {
		showAlertBox(getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), 2, null);
	}

	public void showAlertBox(String title, String Description, int noOfButtons, final String errorMessage) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = getActivity().getLayoutInflater();

		View v = inflater.inflate(R.layout.alert_dialog, null);
		dialogBuilder.setView(v);
		ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
		Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
		Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
		final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
		LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
		LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);
		ivAbout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (errorMessage != null) {
					tvDescription.setText(errorMessage);
				}
			}
		});

		tvTitle.setText(title);
		tvDescription.setText(Description);
		if (noOfButtons == 1) {
			button2.setVisibility(View.GONE);
			llBtn2.setVisibility(View.GONE);
		}
		button1.setText("Retry");
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				if (isOnline()) {
					httpTask = new HttpTask(getActivity(), progressBar);
					httpTask.execute(AppSettings.getConnectRequest);

				} else {
					showInternetAlertBox(getActivity());
				}
			}
		});
		button2.setText("Cancel");
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		GCMIntentService.notificationCounter = 0;
	}
}
