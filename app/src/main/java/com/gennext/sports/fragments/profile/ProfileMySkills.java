package com.gennext.sports.fragments.profile;

import android.app.Activity;
import android.app.AlertDialog;
import android.support.v4.app.FragmentManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.sports.R;
import com.gennext.sports.fragments.CompactFragment;
import com.gennext.sports.image_cache.ImageLoader;
import com.gennext.sports.model.MySkillsAdapter;
import com.gennext.sports.model.MySportToJSON;
import com.gennext.sports.model.MySportsModel;
import com.gennext.sports.util.AppSettings;
import com.gennext.sports.util.AppTokens;
import com.gennext.sports.util.Buddy;
import com.gennext.sports.util.HttpReq;
import com.gennext.sports.util.L;
import com.gennext.sports.util.Utility;
import com.gennext.sports.util.internet.BasicNameValuePair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ProfileMySkills extends CompactFragment implements View.OnClickListener {

	private OnInitMainListener comm;
	private TextView tvName, tvCity;
	private ImageView ivProfile;
	private ListView lvSkillList;
	private Button btnSave;
	TextView tvEditrofile;

	public ImageLoader imageLoader;
	private ArrayList<MySportsModel> mySkillList;
	private ProgressBar progressBar, progressBar2;
	LoadSportAndSkillList loadSportAndSkillList;
	HttpTask httpTask;
	public static boolean editStatus;
	LinearLayout llEditrofile;
	public int TASK_RETRY = 1, TASK_GOBACK = 2;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		imageLoader = new ImageLoader(getActivity());

	}

	public void setCommunicator(OnInitMainListener onInitMainListener) {
		this.comm = onInitMainListener;
	}

	public interface OnInitMainListener {
		public void onInitMainListener(int screenPosition, ArrayList<MySportsModel> skillList);
	}
	// public void setData(boolean editStatus) {
	// this.editStatus=editStatus;
	// }

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (loadSportAndSkillList != null) {
			loadSportAndSkillList.onAttach(activity);
		}
		if (httpTask != null) {
			httpTask.onAttach(activity);
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (loadSportAndSkillList != null) {
			loadSportAndSkillList.onDetach();
		}
		if (httpTask != null) {
			httpTask.onDetach();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.profile_my_skills, container, false);
		initUi(view);
		return view;
	}

	private void initUi(View view) {
		tvName = (TextView) view.findViewById(R.id.tv_myskills_name);
		tvCity = (TextView) view.findViewById(R.id.tv_myskills_city);
		ivProfile = (ImageView) view.findViewById(R.id.iv_myskills_profilepic);
		lvSkillList = (ListView) view.findViewById(R.id.lv_myskills_sportlist);
		btnSave = (Button) view.findViewById(R.id.btn_myskills);
		progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
		progressBar2 = (ProgressBar) view.findViewById(R.id.progressBar2);
		btnSave.setOnClickListener(this);
		llEditrofile = (LinearLayout) view.findViewById(R.id.ll_edit_profile);
		tvEditrofile = (TextView) view.findViewById(R.id.tv_edit_profile);
		llEditrofile.setOnClickListener(this);

		setProfileData();
		// TextView
		// actionbarTitle=(TextView)view.findViewById(R.id.actionbar_title);
		// actionbarTitle.setText(setActionBarTitle("MY SKILLS"));
		setTypsFace(btnSave);

		if (AppTokens.EDIT_PROFILE_FROM.equals("main")) {
			ChangeButtonListener();
		} else {
			llEditrofile.setVisibility(View.GONE);
		}
		loadSportAndSkillList = new LoadSportAndSkillList(getActivity());
		loadSportAndSkillList.execute(AppSettings.getSportListAndPlayerSelectedSport);
	}

	private void setProfileData() {
		tvName.setText(LoadPref(Buddy.Name));
		tvCity.setText(LoadPref(Buddy.City));
		Buddy.setBuddyProfileImage(getActivity(), ivProfile);
		// imageLoader.DisplayImage(LoadPref(getActivity(), Buddy.Image),
		// ivProfile,"profile");
	}

	@Override
	public void onClick(View v) {
		// L.m(CreateJson());

		switch (v.getId()) {
		case R.id.btn_myskills:
			if (editStatus) {
				httpTask = new HttpTask(getActivity(), btnSave, progressBar2);
				httpTask.execute(AppSettings.setPlayerSport);
			} else {
				comm.onInitMainListener(4, mySkillList);
			}
			break;
		case R.id.ll_edit_profile:
			if (editStatus) {
				editStatus = false;
			} else {
				editStatus = true;
			}
			ChangeButtonListener();
			break;

		}
	}

	private void ChangeButtonListener() {
		if (editStatus) {
			btnSave.setText("Save & Proceed");
			tvEditrofile.setText("View");

		} else {
			btnSave.setText("Skip");
			tvEditrofile.setText("Edit");

		}
	}

	private class LoadSportAndSkillList extends AsyncTask<String, Void, ArrayList<MySportsModel>> {
		Activity act;

		public LoadSportAndSkillList(Activity activity) {
			this.act = activity;
		}

		public void onAttach(Activity activity) {
			this.act = activity;
		}

		public void onDetach() {
			this.act = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected ArrayList<MySportsModel> doInBackground(String... urls) {

			MySportsModel ob = new MySportsModel();
			mySkillList = new ArrayList<MySportsModel>();
			for (MySportsModel ref : ob.getSkillList()) {
				if (ref.getSelect() == 1) {
					MySportsModel data = new MySportsModel();
					data.setSportId(ref.getSportId());
					data.setSportName(ref.getSportName());
					data.setSelect(ref.getSelect());
					data.setLogo(ref.getLogo());
					data.setSkillLevel(ref.getSkillLevel());
					mySkillList.add(data);
				}
			}

			ob.setSkillList(mySkillList);
			return mySkillList;
			// return "success";

		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(ArrayList<MySportsModel> result) {
			if (act != null) {
				progressBar.setVisibility(View.GONE);
				if (result != null) {

					MySkillsAdapter ExpAdapter = new MySkillsAdapter(getActivity(), R.layout.custom_slot_skills,
							result);
					lvSkillList.setAdapter(ExpAdapter);
				}
			}
		}
	}

	private String CreateJson() {
		String StoreJSON = null;
		StoreJSON = MySportToJSON.toJSonArray();

		return StoreJSON;

	}

	private class HttpTask extends AsyncTask<String, Void, String> {
		Button btn;
		ProgressBar pBar;
		Activity activity;

		public void onAttach(Activity activity) {
			// TODO Auto-generated method stub
			this.activity = activity;

		}

		public void onDetach() {
			// TODO Auto-generated method stub
			this.activity = null;

		}

		public HttpTask(Activity activity, Button btn, ProgressBar pBar) {
			this.activity = activity;
			this.btn = btn;
			this.pBar = pBar;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pBar.setVisibility(View.VISIBLE);
			btn.setVisibility(View.GONE);
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error", response1 = "error";
			String sportAndSkillJson = null;
			if (activity != null) {
				sportAndSkillJson = CreateJson();
				L.m(sportAndSkillJson);
			}
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
			params.add(new BasicNameValuePair("sportAndSkillJson", sportAndSkillJson));

			HttpReq ob = new HttpReq();
			response = ob.makeConnection(urls[0], HttpReq.POST, params);

			response1 = ob.makeConnection(AppSettings.getSportListAndPlayerSelectedSport, HttpReq.GET, params);

			if (response1.contains("[")) {
				try {
					JSONArray main = new JSONArray(response1);
					JSONObject sub = main.getJSONObject(0);

					JSONArray sportList = sub.getJSONArray("sportList");
					if (activity != null) {
						Utility.SavePref(getActivity(), AppTokens.selectSportAndSportList, response1);
						Utility.SavePref(getActivity(), AppTokens.SportList, sportList.toString());

					}
				} catch (JSONException e) {
					L.m("Json Errszwor :" + e.toString());
					ErrorMessage = e.toString() + response1;
					return null;
				}
			}

			String MSG = null;
			L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray json = new JSONArray(response);
					for (int i = 0; i < json.length(); i++) {
						JSONObject obj = json.getJSONObject(i);
						if (!obj.optString("status").equals("")
								&& obj.optString("status").equalsIgnoreCase("success")) {
							MSG = "success";
						} else if (!obj.optString("status").equals("")
								&& obj.optString("status").equalsIgnoreCase("failure")) {
							MSG = obj.optString("message");
						}
					}

				} catch (JSONException e) {
					L.m("Json Errszwor :" + e.toString());
					ErrorMessage = e.toString() + response;
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				ErrorMessage = response;
				return null;
			}

			return MSG;
			// return "success";

		}

		@Override
		protected void onPostExecute(String result) {

			pBar.setVisibility(View.GONE);
			btn.setVisibility(View.VISIBLE);
			if (activity != null) {
				if (result != null) {
					L.m(result);
					if (result.equalsIgnoreCase("success")) {
						comm.onInitMainListener(4, mySkillList);
					} else {
						if (result.contains("Check Json Format")) {
							showAlertBox("Alert!!!", "Please select at least one sport.", 2, ErrorMessage, TASK_GOBACK);
							// Toast.makeText(getActivity(), "Please select
							// atleast one sport.", Toast.LENGTH_SHORT)
							// .show();
						} else {
							Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
						}
					}
				} else {
					showServerErrorAlertBox(ErrorMessage);
				}
			}
		}
	}

	public void showServerErrorAlertBox(String errorDetail) {
		showAlertBox(getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), 2, errorDetail,
				TASK_RETRY);
	}

	public void showInternetAlertBox() {
		showAlertBox(getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), 2, null, TASK_RETRY);
	}

	public void showAlertBox(String title, String Description, int noOfButtons, final String errorMessage,
			final int buttonTask) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = getActivity().getLayoutInflater();

		View v = inflater.inflate(R.layout.alert_dialog, null);
		dialogBuilder.setView(v);
		ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
		Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
		Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
		final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
		LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
		LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);
		ivAbout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (errorMessage != null) {
					tvDescription.setText(errorMessage);
				}
			}
		});

		tvTitle.setText(title);
		tvDescription.setText(Description);
		if (noOfButtons == 1) {
			button2.setVisibility(View.GONE);
			llBtn2.setVisibility(View.GONE);
		}
		switch (buttonTask) {
		case 1:
			button1.setText("Retry");
			break;
		case 2:
			button1.setText("OK");
			break;

		}
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				switch (buttonTask) {
				case 1:
					if (isOnline()) {
						httpTask = new HttpTask(getActivity(), btnSave, progressBar2);
						httpTask.execute(AppSettings.setPlayerSport);

					} else {
						showInternetAlertBox(getActivity());
					}
					break;
				case 2:
					FragmentManager mannager = getFragmentManager();
					mannager.popBackStack();
					break;

				}

			}
		});
		button2.setText("Cancel");
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}

}
