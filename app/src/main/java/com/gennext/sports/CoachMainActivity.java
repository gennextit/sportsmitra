
package com.gennext.sports;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.sports.coach.CoachDashboard;
import com.gennext.sports.coach.CoachDashboard.OnCoachDashboardListener;
import com.gennext.sports.coach.CoachInbox;
import com.gennext.sports.coach.CoachInbox.OnCoachInboxListener;
import com.gennext.sports.util.Coach;
import com.gennext.sports.util.L;

public class CoachMainActivity extends BaseActivity implements OnCoachDashboardListener,OnCoachInboxListener{
	LinearLayout llMenu, llShadow, llDashbord, llInbox,llNavDrawer;
	LinearLayout llDashbordLine, llInboxLine;
	ImageView ivDashbord, ivInbox;
	TextView tvDashbord, tvInbox,ActionBarTitle,tvCoinCounter;
	FragmentManager mannager;
	int TAB_DASHBOARD=1,TAB_INDEX=2,SWITCH;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_coach);
		hideActionBar();
		// setActionBarOption();
		mannager = getSupportFragmentManager();
		
		initUi();
		SetDrawer(this,llNavDrawer,COACH);
		
		tvCoinCounter.setText(LoadPref(Coach.noOfCoins));
	}

	private void initUi() {
		llMenu = (LinearLayout) findViewById(R.id.ll_main_menu_bottom);
		llShadow = (LinearLayout) findViewById(R.id.ll_main_shadow);
		llDashbord = (LinearLayout) findViewById(R.id.ll_main_coach_dashbord);
		llDashbordLine = (LinearLayout) findViewById(R.id.ll_main_coach_dashbord_line);
		llInbox = (LinearLayout) findViewById(R.id.ll_main_coach_inbox);
		llInboxLine = (LinearLayout) findViewById(R.id.ll_main_coach_inbox_line);
		llNavDrawer = (LinearLayout) findViewById(R.id.ll_nav_drawer);
		ActionBarTitle = (TextView) findViewById(R.id.actionbar_title);

		ivDashbord = (ImageView) findViewById(R.id.iv_main_coach_dashbord);
		ivInbox = (ImageView) findViewById(R.id.iv_main_coach_inbox);
		tvDashbord = (TextView) findViewById(R.id.tv_main_coach_dashbord);
		tvCoinCounter = (TextView) findViewById(R.id.actionbar_coin_counter);
		tvInbox = (TextView) findViewById(R.id.tv_main_coach_inbox);

		llDashbord.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				switchTab(TAB_DASHBOARD);
				setScreenDynamically(TAB_DASHBOARD);
			}
		});
//		llInbox.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View arg0) {
//
//				//mannager.popBackStack("coachDashboard", FragmentManager.POP_BACK_STACK_INCLUSIVE);
//				switchTab(TAB_INDEX);
//				setScreenDynamically(TAB_INDEX);
//			}
//		});
		switchTab(TAB_DASHBOARD);
		setScreenDynamically(TAB_DASHBOARD);

	}

	private void switchTab(int key) {

		switch (key) {
		case 1:
			ActionBarTitle.setText("DASHBOARD");
			llDashbordLine.setVisibility(View.VISIBLE);
			llInboxLine.setVisibility(View.INVISIBLE);
			ivDashbord.setColorFilter(Color.WHITE);
			tvDashbord.setTextColor(Color.WHITE);
			ivInbox.setColorFilter(getResources().getColor(R.color.c_tab_icon));
			tvInbox.setTextColor(getResources().getColor(R.color.c_tab_icon));
			break;
		case 2:
			ActionBarTitle.setText("INBOX");
			llDashbordLine.setVisibility(View.INVISIBLE);
			llInboxLine.setVisibility(View.VISIBLE);
			ivDashbord.setColorFilter(getResources().getColor(R.color.c_tab_icon));
			tvDashbord.setTextColor(getResources().getColor(R.color.c_tab_icon));
			ivInbox.setColorFilter(Color.WHITE);
			tvInbox.setTextColor(Color.WHITE);
			break;

		}

	}

	private void setScreenDynamically(int position) {
		setScreenDynamically(position, null, null, null);
	}

	private void setScreenDynamically(int position, String sltPlayerId, String sltPlayerImage, String jsonData) {
		FragmentTransaction transaction;
		switch (position) {
		case 1:
			CoachDashboard coachDashboard = new CoachDashboard();
			coachDashboard.setCommunicator(CoachMainActivity.this);
			transaction = mannager.beginTransaction();
			transaction.add(R.id.group_main_coach, coachDashboard, "coachDashboard");
			//transaction.addToBackStack("coachDashboard");
			transaction.commit();

			break;
		case 2:
			CoachInbox coachInbox = new CoachInbox();
			coachInbox.setConnecter(CoachMainActivity.this);
			coachInbox.setData(jsonData, sltPlayerId, sltPlayerImage);
			transaction = mannager.beginTransaction();
			transaction.add(R.id.group_main_coach, coachInbox, "coachInbox");
			//transaction.addToBackStack("coachInbox");
			transaction.commit();

			break;
		
		}
	}

	@Override
	public void onBackPressed() {
		if (mannager.getBackStackEntryCount() > 1) {
			int count = mannager.getBackStackEntryCount();
			mannager.popBackStack();
			if (count == 2) {
				// ActionBarHeading.setText("HOME");
				llMenu.setVisibility(View.VISIBLE);
				llShadow.setVisibility(View.VISIBLE);
				switchTab(TAB_DASHBOARD);
			}
		} else {
			L.m("MainActivity" + "nothing on backstack, calling super");
			super.onBackPressed();
		}
	}

	


	@Override
	public void onCoachDashboardListener(String JsonData, String playerId, String playerImage) {
		switchTab(TAB_INDEX);
		setScreenDynamically(TAB_INDEX,playerId,playerImage,JsonData);
	}

	@Override
	public void onCoachInboxListener(String availableCoins) {
		tvCoinCounter.setText(LoadPref(Coach.noOfCoins));
	}

	@Override
	public void onUpdateCoin(String availableCoin) {
		tvCoinCounter.setText(LoadPref(Coach.noOfCoins));
	}

}
