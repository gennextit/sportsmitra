package com.gennext.sports.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

public class CameraUtility {

	private static int THUMBNAIL_SIZE=5;
	
	public static void saveImageExternal(Activity activity,Bitmap bitmap,Uri cropImgUri) {
		//Bundle m_extras = extras;
		 // get the cropped bitmap
//		 Bitmap m_thePic = m_extras.getParcelable("data");
		 Bitmap m_thePic =bitmap;
		 OutputStream m_fOut = null;
		File outputFile = new File(cropImgUri.getPath());
		try
		{ 
		    m_fOut = new FileOutputStream(outputFile);      
		    Bitmap m_bitmap = m_thePic.copy(Bitmap.Config.ARGB_8888, true);
		    m_bitmap.compress(Bitmap.CompressFormat.PNG, 100, m_fOut);
		    m_fOut.flush();
		    m_fOut.close();
		    MediaStore.Images.Media.insertImage(activity.getContentResolver(),
		    		outputFile.getAbsolutePath(), outputFile.getName(), outputFile.getName());
		}
		catch (Exception p_e)
		{
		}
	}
	
	
	public static File getOutputCropedMediaFile() {

		// External sdcard location
		File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				AppTokens.PROFILE_IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(AppTokens.PROFILE_IMAGE_DIRECTORY_NAME,
						"Oops! Failed create " + AppTokens.PROFILE_IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
		File mediaFile;

		mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");

		return mediaFile;
	}
	
	public static Uri StringToUri(String imagepath) {
		//File mediaFile = new File(imagepath);
		return Uri.parse(imagepath);
	}

	public static File bitmapToFileConvert(Bitmap bmp){
		File file = new File(bmp + ".png");
		return file;
	}

	public static Bitmap getBitmapFromUri(Activity activity,Uri uri) throws FileNotFoundException, IOException {
		Bitmap bitmap;
		try
		{
			bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver() , uri);
		}
		catch (OutOfMemoryError e){
			return null;
		}
		catch (Exception e)
		{
			return null;
		}

		return bitmap;
	}

	/*public static Bitmap getBitmapFromUri(Activity activity,Uri uri) throws FileNotFoundException, IOException {
		InputStream input = activity.getContentResolver().openInputStream(uri);

		BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
		onlyBoundsOptions.inJustDecodeBounds = true;
		onlyBoundsOptions.inDither=true;//optional
		onlyBoundsOptions.inPreferredConfig=Bitmap.Config.ARGB_8888;//optional
		BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
		input.close();
		if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1))
			return null;

		int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

		double ratio = (originalSize > THUMBNAIL_SIZE) ? (originalSize / THUMBNAIL_SIZE) : 1.0;

		BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
		bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
		bitmapOptions.inDither=true;//optional
		bitmapOptions.inPreferredConfig=Bitmap.Config.ARGB_8888;//optional
		input = activity.getContentResolver().openInputStream(uri);
		Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
		input.close();
		return bitmap;
	}

	private static int getPowerOfTwoForSampleRatio(double ratio){
		int k = Integer.highestOneBit((int)Math.floor(ratio));
		if(k==0) return 1;
		else return k;
	}*/







}
