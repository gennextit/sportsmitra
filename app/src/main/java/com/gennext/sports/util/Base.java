package com.gennext.sports.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gennext.sports.R;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class Base extends AppCompatActivity {
	boolean conn = false;
	Builder alertDialog;
	ProgressDialog progressDialog;
	public AlertDialog dialog = null;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	// public void setFont(EditText et){
	// Typeface font = Typeface.createFromAsset(this.getAssets(),
	// "fonts/CircularStd-Book.otf");
	// et.setTypeface(font);
	// et.setTextColor(getResources().getColor(R.color.text_color));
	// }
	public void setFont(Button btn, String text) {
		Typeface font = Typeface.createFromAsset(this.getAssets(), "fonts/CircularStd-Book.otf");
		btn.setTypeface(font);
		btn.setText(toTitleCase(text));
		btn.setTextColor(Color.parseColor("#FFFFFF"));
	}
	// public void setFont(TextView tv){
	// Typeface font = Typeface.createFromAsset(this.getAssets(),
	// "fonts/CircularStd-Book.otf");
	// tv.setTypeface(font);
	// tv.setTextColor(getResources().getColor(R.color.text_color));
	// }

	public void hideKeybord(Activity act) {
		InputMethodManager inputManager = (InputMethodManager) getSystemService(act.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

	}

//	public boolean isConnected() {
//
//		if (isOnline() == false) {
//			showAlertInternet(getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), false);
//			return false;
//		} else {
//			return true;
//		}
//	}

//	public void showToast(String txt, Boolean status) {
//		// Inflate the Layout
//		LayoutInflater lInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
//		// LayoutInflater inflater = context.getLayoutInflater();
//
//		// View layout = lInflater.inflate(R.layout.custom_toast,(ViewGroup)
//		// findViewById(R.id.custom_toast_layout_id));
//		View layout = lInflater.inflate(R.layout.custom_toast, null);
//		TextView a = (TextView) layout.findViewById(R.id.tv_toast_msg);
//		ImageView b = (ImageView) layout.findViewById(R.id.imageView1);
//		// layout.setBackgroundResource((status) ? R.drawable.toast_bg :
//		// R.drawable.toast_bg_red);
//		b.setImageResource((status) ? R.drawable.success : R.drawable.fail);
//		a.setText(txt);
//		// a.setTextColor((status) ? getResources().getColor(R.color.icon_green)
//		// : getResources().getColor(R.color.icon_red));
//		// Create Custom Toast
//		Toast toast = new Toast(Base.this);
//		toast.setGravity(Gravity.CENTER, 0, 0);
//		toast.setDuration(Toast.LENGTH_LONG);
//		toast.setView(layout);
//		toast.show();
//	}

//	public void AlertInternet(Activity Act) {
//
//		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Act);
//
//		// ...Irrelevant code for customizing the buttons and title
//		LayoutInflater inflater = Act.getLayoutInflater();
//
//		View v = inflater.inflate(R.layout.custom_dialog, null);
//		dialogBuilder.setView(v);
//		Button button1 = (Button) v.findViewById(R.id.button1);
//		Button button2 = (Button) v.findViewById(R.id.button2);
//		Button button3 = (Button) v.findViewById(R.id.button3);
//		TextView tvTitle = (TextView) v.findViewById(R.id.tv_title);
//		TextView tvBody = (TextView) v.findViewById(R.id.tv_body);
//
//		tvTitle.setText(getSt(R.string.internet_error_tag));
//		tvBody.setText(getSt(R.string.internet_error_msg));
//		// if decline button is clicked, close the custom dialog
//		//button1.setVisibility(View.INVISIBLE);
//		button1.setText("Setting");
//		button1.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				// Close dialog
//				dialog.dismiss();
//				EnableMobileIntent();
//			}
//		});
//		button2.setVisibility(View.INVISIBLE);
//		button2.setText("Retry");
//		button2.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				// Close dialog
//				dialog.dismiss();
//				EnableMobileIntent();
//			}
//		});
//		//button3.setVisibility(View.INVISIBLE);
//		button3.setText("Exit");
//		button3.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				// Close dialog
//				dialog.dismiss();
//				finish();
//			}
//		});
//		dialog = dialogBuilder.create();
//		dialog.show();
//	}

	public void showInternetAlertBox(Activity activity) {
		showDefaultAlertBox(activity, getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), 2);
	}

	public void showDefaultAlertBox(Activity activity, String title, String Description) {
		showDefaultAlertBox(activity, title, Description, 1);
	}

	public void showDefaultAlertBox(final Activity activity, String title, String Description, int noOfButtons) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);

		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = activity.getLayoutInflater();

		View v = inflater.inflate(R.layout.alert_dialog, null);
		dialogBuilder.setView(v);
		Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
		Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
		TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
		LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
		LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);

		tvTitle.setText(title);
		tvDescription.setText(Description);
		if (noOfButtons == 1) {
			button1.setVisibility(View.GONE);
			llBtn1.setVisibility(View.GONE);
			button2.setText("Done");
		}
		button1.setText("Setting");
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				EnableMobileIntent();
			}
		});
		button2.setText("Exit");
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				activity.finish();
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}

	 
	
	public void ExitAlertBox(Activity Act) {

		alertDialog = new AlertDialog.Builder(Act);

		// Setting Dialog Title
		alertDialog.setTitle("Alert");

		// Setting Dialog Message
		alertDialog.setMessage("Are you sure to exit.");

		// On pressing Settings button
		alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});
		// on pressing cancel button
		alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});

		// Showing Alert Message
		alertDialog.show();
	}

	// ProgressDialog progressDialog; I have declared earlier.
	public void showPDialog(String msg) {
		progressDialog = new ProgressDialog(Base.this);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		// progressDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.dialog_animation));
		progressDialog.setMessage(msg);
		progressDialog.setIndeterminate(false);
		progressDialog.setCancelable(false);
		progressDialog.show();
	}

	public void dismissPDialog() {
		if (progressDialog != null)
			progressDialog.dismiss();
	}

	public static String toTitleCase(String givenString) {
		String[] arr = givenString.split(" ");
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < arr.length; i++) {
			sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1)).append(" ");
		}
		return sb.toString().trim();
	}

	public boolean isConnectionAvailable() {

		if (isOnline() == false) {
			return false;
		} else {
			return true;
		}
	}

	public String LoadPref(String key) {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Base.this);
		String data = sharedPreferences.getString(key, "");
		return data;
	}

	public void SavePref(String key, String value) {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Base.this);

		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(key, value);
		editor.commit();

	}

	public String convert(String res) {
		String UrlEncoding = null;
		try {
			UrlEncoding = URLEncoder.encode(res, "UTF-8");

		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		return UrlEncoding;
	}

	public boolean isOnline() {
		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Base.this.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					haveConnectedMobile = true;
		}
		if (haveConnectedWifi == true || haveConnectedMobile == true) {
			L.m("Log-Wifi"+ String.valueOf(haveConnectedWifi));
			L.m("Log-Mobile"+String.valueOf(haveConnectedMobile));
			conn = true;
		} else {
			Log.e("Log-Wifi", String.valueOf(haveConnectedWifi));
			Log.e("Log-Mobile", String.valueOf(haveConnectedMobile));
			conn = false;
		}

		return conn;
	}

//	public void showAlertInternet(String title, String message, Boolean status) {
//		alertDialog = new AlertDialog.Builder(Base.this);
//
//		// Setting Dialog Title
//		alertDialog.setTitle(title);
//
//		// Setting Dialog Message
//		alertDialog.setMessage(message);
//
//		if (status != null)
//			// Setting alert dialog icon
//			alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
//
//		// On pressing Settings button
//		alertDialog.setPositiveButton("SETTING", new DialogInterface.OnClickListener() {
//			public void onClick(DialogInterface dialog, int which) {
//				EnableMobileIntent();
//			}
//		});
//		// Showing Alert Message
//		alertDialog.show();
//	}

	public String getSt(int id) {

		return getResources().getString(id);
	}

	public void EnableMobileIntent() {
		Intent intent = new Intent();
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setAction(android.provider.Settings.ACTION_SETTINGS);
		startActivity(intent);

	}

	public String viewTime() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("h:mm a");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		return df.format(c.getTime());

	}

	public String getDate() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String formattedDate = df.format(c.getTime());
		return formattedDate;
	}

	public String getTime12() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("h:mm a");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		return df.format(c.getTime());

	}

	public String getTime24() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df1 = new SimpleDateFormat("H:mm:ss");
		df1.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		return df1.format(c.getTime());
	}

	public String getTime() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("H:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		return df.format(c.getTime());
	}

	// give format like dd-MMM-yyyy,dd/MMM/yyyy
	public String viewDate(String format) {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat(format);
		String formattedDate = df.format(c.getTime());
		return formattedDate;
	}

	public String vDate() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(c.getTime());
		return formattedDate;
	}

	// Enter Format eg.dd-MMM-yyyy,dd/MM/yyyy
	public String viewFormatDate(String format) {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat(format);
		String formattedDate = df.format(c.getTime());
		return formattedDate;

	}

	// missing 0 issue resolve
	// enter Date eg. dd-MM-yyyy
	// enter dateFormat eg. dd-MM-yyyy
	public String currectFormateDate(String Date, String dateFormat) {
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		Date cDate = null;
		try {
			cDate = sdf.parse(Date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String formattedDate = sdf.format(cDate);
		return formattedDate;
	}

	public void showToast(String txt) {
		// Inflate the Layout
		Toast toast = Toast.makeText(Base.this, txt, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
		toast.show();
	}

	// // enter fix Date eg. dd-MM-yyyy
	// // enter dateFormat eg. dd-MM-yyyy,yyyy-MM-dd,
	// public String convertFormateDate(String Date, String dateFormat) {
	// // String s = "12:18:00";
	// String finalDate = Date;
	// String Day = Date.substring(0, 2);
	// String middle = Date.substring(2, 3);
	// String Month = Date.substring(3, 5);
	// String Year = Date.substring(6, 10);
	//
	// switch (dateFormat) {
	// case "dd-MM-yyyy":
	// finalDate = Day + middle + Month + middle + Year;
	// break;
	// case "yyyy-MM-dd":
	// finalDate = Year + middle + Month + middle + Day;
	// break;
	// case "MM-dd-yyyy":
	// finalDate = Month + middle + Day + middle + Year;
	// break;
	// default:
	// finalDate = "Date Format Incorrest";
	// }
	// return finalDate;
	// }
}