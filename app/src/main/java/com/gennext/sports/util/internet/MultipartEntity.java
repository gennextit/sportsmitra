package com.gennext.sports.util.internet;

import com.gennext.sports.util.StringBody;
import com.gennext.sports.util.internet.FileBody;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Admin on 2/6/2018.
 */

public class MultipartEntity {
    MultipartBody.Builder builder;


    public MultipartEntity() {
        builder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM);
    }

    public void addPart(String key, StringBody value) {
        builder.addFormDataPart(key, value.getText() == null ? "" : value.getText());
    }
    public void addPart(String key, FileBody value) {
        builder.addFormDataPart(key, value.getFile().getName(),
                RequestBody.create(MediaType.parse("text/plain"), value.getFile()));
    }

    public MultipartBody.Builder getBuilder() {
        return builder;
    }

    public RequestBody RequestBody() {
        return builder.build();
    }
}

