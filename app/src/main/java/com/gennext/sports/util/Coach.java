package com.gennext.sports.util;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.gennext.sports.R;
import com.bumptech.glide.Glide;

import java.io.ByteArrayOutputStream;

public class Coach {
	
	public static final String COMMON = "coach";

	public static final String CoachId = "CoachId" + COMMON;
	public static final String Name = "Name" + COMMON;
	public static final String countryCode = "countryCode" + COMMON;
	public static final String Mobile = "Mobile" + COMMON;
	public static final String CompleteMobile = "CompleteMobile" + COMMON;
	public static final String Email = "Email" + COMMON;
	public static final String Image = "Image" + COMMON;
	public static final String ImageBitmapData = "ImageBitmapData" + COMMON;

	public static final String City = "City" + COMMON;
	public static final String AGE = "age" + COMMON;
	public static final String GENDER = "gender" + COMMON;
	public static final String LOCATION = "location" + COMMON;
	public static final String CLUB_OR_APPARTMENT = "cluborappartment" + COMMON;
	public static final String ABOUT = "about" + COMMON;

	public static final String fbId = "fbId" + COMMON;
	public static final String gmainId = "gmainId" + COMMON;
	public static final String CoachJsonData = "CoachJsonData" + COMMON;
	public static final String noOfCoins = "noOfCoins" + COMMON;
	public static final String coachIntro = "coachIntro" + COMMON;

	public static void setPlayerDetail(Activity act, EditText etName, ImageView ivImg,
			EditText etAge, Spinner spGender,AutoCompleteTextView acCode,EditText etMobile,EditText etEmail, AutoCompleteTextView acCity, AutoCompleteTextView acLocation,
			AutoCompleteTextView acClub, EditText etAbout, String googlePlaceId, String gender) {

		String pName = Utility.LoadPref(act, Coach.Name);
		String pImage = Utility.LoadPref(act, Coach.Image);
		String pAge = Utility.LoadPref(act, Coach.AGE);
		String pCity = Utility.LoadPref(act, Coach.City);
		String pLocation = Utility.LoadPref(act, Coach.LOCATION);
		String pClub = Utility.LoadPref(act, Coach.CLUB_OR_APPARTMENT);
		String pAbout = Utility.LoadPref(act, Coach.ABOUT);
		String pCountryCode = Utility.LoadPref(act, Coach.countryCode);
		String pMobile = Utility.LoadPref(act, Coach.Mobile);
		String pEmail = Utility.LoadPref(act, Coach.gmainId);

		if (!pName.equals("")) {
			etName.setText(pName);
			etName.setSelection(etName.getText().length());
		}
		
		setCoachProfileImage(act, ivImg);
		
		if (!pAge.equals("")) {
			etAge.setText(pAge);
			etAge.setSelection(etAge.getText().length());
		}
		if (!pCity.equals("")) {
			acCity.setText(pCity);
			acCity.setSelection(acCity.getText().length());
		}
		if (!pLocation.equals("")) {
			acLocation.setText(pLocation);
			acLocation.setSelection(acLocation.getText().length());
		}
		if (!pClub.equals("")) {
			acClub.setText(pClub);
			acClub.setSelection(acClub.getText().length());
		}
		if (!pAbout.equals("")) {
			etAbout.setText(pAbout);
			etAbout.setSelection(etAbout.getText().length());
		}
		if (!pMobile.equals("")) {
			etMobile.setText(pMobile);
			etMobile.setSelection(etMobile.getText().length());
		}
		if (!pEmail.equals("")) {
			etEmail.setText(pEmail);
			etEmail.setSelection(etEmail.getText().length());
		}
		if (!pCountryCode.equals("")) {
			acCode.setText(pCountryCode);
			acCode.setSelection(acCode.getText().length());
		}

	}

	public static void SaveCoachProfileImage(Activity act, Bitmap bmp) {
		if (bmp != null) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			bmp.compress(Bitmap.CompressFormat.PNG, 100, baos); // bm is the
																// bitmap object
			byte[] b = baos.toByteArray();
			String encoded = Base64.encodeToString(b, Base64.DEFAULT);
			// adding member to session
			Utility.SavePref(act, Coach.ImageBitmapData, encoded);
		} else {
			L.m("No Coach image Bitmap found to store image");
		}
	}
	public static void setCoachProfileImage(Activity act, ImageView ivProfile) {
		
		   if(!Utility.LoadPref(act,Coach.ImageBitmapData).equals("")){
				String tempImage=Utility.LoadPref(act,Coach.ImageBitmapData);
				byte[] imageAsBytes = Base64.decode(tempImage.getBytes(), Base64.DEFAULT);
	      		Bitmap bmp = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
	      		 
	            ivProfile.setImageBitmap(bmp);
		  }else{
			  String pImage = Utility.LoadPref(act, Coach.Image);
			  if (!pImage.equals("")) {
				  Glide.with(act)
						  .load(pImage)
						  .placeholder(R.drawable.profile)
						  .error(R.drawable.profile)
						  .into(ivProfile);
			  }
				
		  }
	}

}
