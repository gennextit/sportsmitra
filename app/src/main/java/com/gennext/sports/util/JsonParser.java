package com.gennext.sports.util;


import com.gennext.sports.model.Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by Abhijit on 08-Dec-16.
 */
public class JsonParser {
    public static String ERRORMESSAGE = "Not Available";
    public static final String ERROR_CODE = "ERROR-101";
    public static final String ERROR_MSG = "ERROR-101 No msg available";
    public static final String SERVER = ApiCallError.SERVER_ERROR;
    public static final String INTERNET = ApiCallError.INTERNET_ERROR;
    public static final String IO_EXCEPTION = ApiCall.IO_EXCEPTION;
    public static final String JSON_ERROR = ApiCallError.JSON_ERROR;

    //default constant variable
    public static final String STATUS = "status";
    public static final String MESSAGE = "message";


    public static Model defaultSuccessResponse(String response) {
        Model model = new Model();
        model.setOutput(Const.SUCCESS);
        model.setOutputMsg("Attendance Marked successful");
        return model;
    }

    public static Model defaultParser(String response) {
        Model jsonModel = new Model();
        jsonModel.setOutput(ERROR_CODE);
        jsonModel.setOutputMsg(ERROR_MSG);
        if (response.contains(IO_EXCEPTION)) {
            jsonModel.setOutput(INTERNET);
            jsonModel.setOutputMsg(response);
            return jsonModel;
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString(STATUS).equals(Const.SUCCESS)) {
                        jsonModel.setOutput(Const.SUCCESS);
                        jsonModel.setOutputMsg(mainObject.getString(MESSAGE));
                    } else if (mainObject.getString(STATUS).equals(Const.FAILURE)) {
                        jsonModel.setOutput(Const.FAILURE);
                        jsonModel.setOutputMsg(mainObject.getString(MESSAGE));
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    jsonModel.setOutput(SERVER);
                    jsonModel.setOutputMsg(e.toString() + "\n" + response);
                }
            } else {
                ERRORMESSAGE = response;
                jsonModel.setOutput(SERVER);
                jsonModel.setOutputMsg(response);
            }
        }
        return jsonModel;
    }

    private static String[] defaultParserModel(String response) {
        String output = "", outputMsg = "";
        if (response.contains(IO_EXCEPTION)) {
            output = INTERNET;
            outputMsg = response;
            return new String[]{output, outputMsg};
        } else {
            if (response.contains("[")) {
                try {
                    JSONArray mainArray = new JSONArray(response);
                    JSONObject mainObject = mainArray.getJSONObject(0);
                    if (mainObject.getString(STATUS).equals(Const.SUCCESS)) {
                        output = Const.SUCCESS;
                        outputMsg = mainObject.getString(MESSAGE);
                    } else if (mainObject.getString(STATUS).equals(Const.FAILURE)) {
                        output = Const.FAILURE;
                        outputMsg = mainObject.getString(MESSAGE);
                    }
                } catch (JSONException e) {
                    ERRORMESSAGE = e.toString() + "\n" + response;
                    output = SERVER;
                    outputMsg = e.toString() + "\n" + response;
                }
            } else {
                ERRORMESSAGE = response;
                output = SERVER;
                outputMsg = response;
            }
        }
        return new String[]{output, outputMsg};
    }

    public static Model manageDefault(String response) {
        Model model = new Model();
        String[] result = defaultParserModel(response);
        model.setOutput(result[0]);
        model.setOutputMsg(result[1]);
        if(!model.getOutput().equals(Const.SUCCESS)){
            return model;
        }
        //write your logic here for success response
//        model.setList(new ArrayList<Model>());
//        try {
//            JSONArray mainArray=new JSONArray(model.getOutputMsg());
//            for (int i=0;i<mainArray.length();i++){
//                JSONObject mainObj=mainArray.getJSONObject(i);
//                CompanyModel item = new CompanyModel();
//                item.setId(mainObj.optString("id"));
//                item.setName(mainObj.optString("companyName"));
//                item.setEmail(mainObj.optString("email"));
//                model.getList().add(item);
//            }
//
//        } catch (JSONException e) {
//            model.setOutput(JSON_ERROR);
//            model.setOutputMsg(e.toString() + "\n" + response);
//        }
        return model;
    }

}