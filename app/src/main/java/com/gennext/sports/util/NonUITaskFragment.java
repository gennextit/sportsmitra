package com.gennext.sports.util;

import java.util.List;

import com.gennext.sports.util.internet.BasicNameValuePair;

import com.gennext.sports.util.AsyncRequest.OnTaskComplete;
import com.gennext.sports.util.internet.BasicNameValuePair;

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

// call method 
/*
 *   NonUITaskFragment task=new NonUITaskFragment();
 *   getSupportFragmentManager().beginTransaction().add(task,"taskFragment").commit();
 *   					or find fragment
 *   task=(NonUITaskFragment)getSupportFragmentManager().findFragmentByTag("taskFragment");
 *   
 */


public class NonUITaskFragment extends Fragment{
	
	public final static int GET = 1;
	public final static int PUT = 2;
	public final static int POST = 3;
	private List<BasicNameValuePair> params;
	private int method;
	OnTaskComplete caller;
	ProgressBar pBar;
	Button btn;
	Activity activity;
	MyTask myTask;
	
	public NonUITaskFragment() {
		// TODO Auto-generated constructor stub
	}
//	public NonUITaskFragment(Activity a, int method) {
//		this.activity = a;
//		this.method = method;
//		this.caller=(OnTaskComplete) a;
//	}
//
//	public NonUITaskFragment(Activity a,ProgressBar pBar,Button btn, int method, List<BasicNameValuePair> params) {
//		this.activity = a;
//		this.method = method;
//		this.params = params;
//		this.pBar= pBar;
//		this.btn=btn;
//		this.caller=(OnTaskComplete)a;
//	}
  

	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		this.activity=activity;
		if(myTask!=null){
			myTask.onAttach(activity);
		}
	}
	
	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		this.activity=null;
		if(myTask!=null){
			myTask.onDetach();
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		setRetainInstance(true);
	}

	
	
	private class MyTask extends AsyncTask<String, Void, String> {

		private Activity activity;

		public MyTask(Activity activity) {
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			
			//((MainActivity)activity).showProgressBar();
			
			if(pBar!=null && btn!=null){
				pBar.setVisibility(View.VISIBLE);
				btn.setVisibility(View.GONE);
			}
			if(pBar!=null){
				pBar.setVisibility(View.VISIBLE);
			}
		}

		@Override
		protected String doInBackground(String... urls) {
			HttpReq ob = new HttpReq();

			return ob.makeConnection(urls[0], HttpReq.POST, params, HttpReq.EXECUTE_TASK);

		}

		@Override
		protected void onPostExecute(String result) {
			//((MainActivity)activity).hideProgressBar();
			//((MainActivity)activity).updateDetailInUi();
			
			if(activity!=null){
				if(pBar!=null && btn!=null){
					pBar.setVisibility(View.GONE);
					btn.setVisibility(View.VISIBLE);
				}
				if(pBar!=null){
					pBar.setVisibility(View.GONE);
				}
				caller.asyncResponseServer(result);
				
			}
		}
	}
}
