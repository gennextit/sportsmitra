package com.gennext.sports.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.json.JSONObject;

/**
 * Created by Abhijit on 13-Dec-16.
 */

public class AppUser {
    public static final String COMMON = "bankg";
    private static final String APP_USER = "app_user" + COMMON;
    private static final String APP_PASS = "app_pass" + COMMON;
    private static final String APP_NAME = "app_name" + COMMON;
    private static final String APP_IMAGE = "app_image" + COMMON;
    private static final String APP_LANGUAGE = COMMON + "app_language";


    public static String LoadPref(Context context, String key) {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            String data = sharedPreferences.getString(key, "");
            return data;
        }
        return "";
    }

    public static String getBuddyName(Context activity) {
        return LoadPref(activity, Buddy.Name);
    }

    public static void setBuddyName(Context activity, String value) {
        Utility.SavePref(activity, Buddy.Name, value);
    }


    public static String getBuddyImage(Context activity) {
        return LoadPref(activity, Buddy.Image);

    }

    public static void setBuddyImage(Context activity, String value) {
        Utility.SavePref(activity, Buddy.Image, value);
    }

    public static String getBuddyEmail(Context activity) {
        return LoadPref(activity, Buddy.gmainId);

    }

    public static void setBuddyEmail(Context activity, String value) {
        Utility.SavePref(activity, Buddy.gmainId, value);
    }


}