package com.gennext.sports.util;

import com.gennext.sports.model.KeyValuePair;
import com.gennext.sports.util.internet.BasicNameValuePair;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Abhijit on 13-Dec-16.
 */

public class RequestBuilder {
    public static RequestBody Default(String consultantId) {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("consultantId", consultantId)
                .build();
        return formBody;
    }


    public static RequestBody getParams(List<BasicNameValuePair> params) {
        FormBody.Builder builder = new FormBody.Builder();
        for (BasicNameValuePair model: params){
            builder.add(model.getKey(), model.getValue());
        }
        RequestBody formBody = builder.build();
        return formBody;
    }

    public static List<KeyValuePair> DefaultUser(String userId) {
        List<KeyValuePair> params = new ArrayList<>();
        params.add(new KeyValuePair("userId",userId));
        return params;
    }

    public static RequestBody getRequestBody(List<KeyValuePair> params) {
        FormBody.Builder builder = new FormBody.Builder();
        for (KeyValuePair model: params){
            builder.add(model.getKey(), model.getValue());
        }
//        RequestBody formBody = builder.build();
        return builder.build();
    }

    public static RequestBody uploadMultipleImages(String userId, List<File> file) {
        MultipartBody.Builder builder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM);
        if (file != null) {
            for (File model : file) {
                builder.addFormDataPart("image[]", model.getName(),
                        RequestBody.create(MediaType.parse("text/plain"), model));
            }
        }
        builder.addFormDataPart("userId", userId == null ? "" : userId);
        return builder.build();
    }

    public static RequestBody uploadSingleImages(String userId, File file) {
        MultipartBody.Builder builder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM);
        if (file != null) {
            builder.addFormDataPart("image", file.getName(),
                    RequestBody.create(MediaType.parse("text/plain"), file));
        }
        builder.addFormDataPart("userId", userId == null ? "" : userId);
        return builder.build();
    }

    public static RequestBody FeedbackDetail(String consultantId, String feedback) {
        return new FormBody.Builder()
                .addEncoded("consultantId", consultantId)
                .addEncoded("feedback", feedback)
                .build();
    }

    public static RequestBody ErrorReport(String report) {
        return new FormBody.Builder()
                .addEncoded("reportLog", report)
                .build();
    }



}
