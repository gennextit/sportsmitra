package com.gennext.sports.util;

import com.gennext.sports.fragments.CompactFragment;
import com.gennext.sports.model.RSSFeedModel;
import com.gennext.sports.util.internet.BasicNameValuePair;
import com.gennext.sports.util.internet.MultipartEntity;
import com.gennext.sports.util.internet.URLEncodedUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

//import org.apache.http.client.HttpClient;
//import org.apache.http.client.entity.UrlEncodedFormEntity;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.client.methods.HttpPut;
//import org.apache.http.client.utils.URLEncodedUtils;

public class HttpReq {
	static String response = null;
	public final static int GET = 1;
	public final static int POST = 3;
	public final static int EXECUTE_TASK = 4;
	// We don't use namespaces
    private static String ns = null;

	public HttpReq() {

	}

	/**
	 * Making service call
	 * 
	 * @url - url to make request
	 * @method - http request method
	 */
	public String makeConnection(String url, int method) {
		return this.makeConnection(url, method, null, null, false, 0);
	}

	public String makeConnection(String url, int method, List<BasicNameValuePair> params) {
		return this.makeConnection(url, method, params, null, false, 0);
	}

	public String makeConnection(String url, int method, List<BasicNameValuePair> params, int executeTask) {
		return this.makeConnection(url, method, params, null, false, EXECUTE_TASK);
	}

	public String makeConnection(String url, int method, MultipartEntity params) {
		return this.makeConnection(url, method, null, params, true, 0);
	}

	/**
	 * Making service call
	 * 
	 * @url - url to make request
	 * @method - http request method
	 * @params - http request params
	 */
	public String makeConnection(String url, int method, List<BasicNameValuePair> params, MultipartEntity mParams,
			Boolean type, int executeTask) {
		String result = "";
		//return null;
		if (method == POST) {
			// adding post params
			if (params != null && type == false) {
				result= ApiCall.POST(url,RequestBuilder.getParams(params));
			} else {
				result= ApiCall.POST(url,mParams.RequestBody());
			}
		}
//		else if (method == GET) {
		else{
			if (params != null) {
				String paramString = URLEncodedUtils.format(params,"utf-8");
				url += "?" + paramString;
			}
			result= ApiCall.GET(url);
		}
		if (executeTask != EXECUTE_TASK) {
			return result;
		} else {
			return getSimpleJsonTask(result);
		}
	}

	private String getSimpleJsonTask(String response) {
		String output = null;
		if (response.contains("[")) {
			try {
				JSONArray json = new JSONArray(response);
				for (int i = 0; i < json.length(); i++) {
					JSONObject obj = json.getJSONObject(i);
					if (obj.optString("status").equals("success")) {
						output = "success";

					} else if (obj.optString("status").equals("failure")) {
						output = obj.optString("message");
					}
				}

			} catch (JSONException e) {
				L.m("Json Error :" + e.toString());
				CompactFragment.ErrorMessage = e.toString() + response;
				return null;
				// return e.toString();
			}
		} else {
			L.m("Invalid JSON found : " + response);
			CompactFragment.ErrorMessage = response;
			return null;
		}

		return output;
	}

	/***********************************************/

	/************** Get JSON from Assets folder ***********/
	/***********************************************/

	// Method that will parse the JSON file and will return a JSONObject
	public String parseAssetsData(InputStream data) {
		String JSONString = null;
		try {

			// open the inputStream to the file
			InputStream inputStream = data;

			int sizeOfJSONFile = inputStream.available();

			// array that will store all the data
			byte[] bytes = new byte[sizeOfJSONFile];

			// reading data into the array from the file
			inputStream.read(bytes);

			// close the input stream
			inputStream.close();

			JSONString = new String(bytes, "UTF-8");

		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}
		return JSONString;
	}

//	public static ArrayList<RSSFeedModel> processXML(InputStream inputStream) throws Exception {
//		RSSFeedModel ob = null;
//		StringBuilder sbDesc;
//		ArrayList<RSSFeedModel> rssList = new ArrayList<RSSFeedModel>();
//
//		DocumentBuilderFactory docBulderFactory = DocumentBuilderFactory.newInstance();
//		docBulderFactory.setNamespaceAware(true);
//		DocumentBuilder docBulder = docBulderFactory.newDocumentBuilder();
//		Document xmlDoc = docBulder.parse(inputStream);
//		Element rootElement = xmlDoc.getDocumentElement();
//		// L.m("" + rootElement.getTagName());
//		NodeList itemList = rootElement.getElementsByTagName("item");
//		NodeList itemChildren = null;
//		Node currentItem = null;
//		Node currentChild = null;
//		NodeList imglist = null;
//		Node imgChild = null;
//
//		org.jsoup.nodes.Document jdom;
//		org.jsoup.select.Elements tag;
//		if (itemList.getLength() >= 3)
//			for (int i = 0; i <= 3; i++) {
//				ob = new RSSFeedModel();
//				sbDesc = new StringBuilder();
//				currentItem = itemList.item(i);
//				itemChildren = currentItem.getChildNodes();
//				for (int j = 0; j < itemChildren.getLength(); j++) {
//					currentChild = itemChildren.item(j);
//					if (currentChild.getNodeName().equalsIgnoreCase("title")) {
//						ob.setTitel(currentChild.getTextContent());
//						// L.m("Title : " + currentChild.getTextContent());
//					}
//
//					if (currentChild.getNodeName().equalsIgnoreCase("pubDate")) {
//						String pubDate=currentChild.getTextContent();
//						if(pubDate.length()>25){
//							pubDate=pubDate.substring(5,16);
//						}
//						ob.setPubDate(pubDate);
//						// L.m("pubDate : " + currentChild.getTextContent());
//					}
//					if (currentChild.getNodeName().equalsIgnoreCase("dc:creator")) {
//						ob.setCreator(currentChild.getTextContent());
//						// L.m("dc:creator : " + currentChild.getTextContent());
//					}
//					if (currentChild.getNodeName().equalsIgnoreCase("description")) {
//						// ob.setDescription(currentChild.getTextContent());
//						// L.m("description : " +
//						// currentChild.getTextContent());
//					}
//
//					if (currentChild.getNodeName().equalsIgnoreCase("content:encoded")) {
//						imglist = currentChild.getChildNodes();
//						for (int k = 0; k < imglist.getLength(); k++) {
//							imgChild = imglist.item(k);
//							if (imgChild.getNodeName().equalsIgnoreCase("#cdata-section")) {
//								String html = imgChild.getTextContent();
//								jdom = Jsoup.parse(html);
//								jdom.select("p.wp-caption-text").remove();
//								tag = jdom.select("p");
//
//								for (org.jsoup.nodes.Element element : tag) {
//									// L.m(element.text());
//									sbDesc.append(element.text());
//									sbDesc.append("\n\n");
//
//								}
//
//								ob.setDescription(sbDesc);
//								if (html.contains("<img"))
//									;
//								{
//									String img = html.substring(html.indexOf("<img "));
//									// String cleanUp = img.substring(0,
//									// img.indexOf(">")+1);
//									img = img.substring(img.indexOf("src=") + 5);
//
//									int indexOf = img.indexOf("\"");
//									if (indexOf == -1) {
//										indexOf = img.indexOf("\"");
//									}
//									img = img.substring(0, indexOf);
//									ob.setImgSrc(img);
//									L.m(img);
//								}
//
//							}
//						}
//					}
//				}
//				if (ob != null) {
//					rssList.add(ob);
//				}
//			}
//		return rssList;
//	}

//	public static ArrayList<RSSFeedModel> processXMLparseZee(InputStream inputStream) throws Exception {
//		RSSFeedModel ob = null;
//		String imageUrl = null;
//		StringBuilder sbDesc;
//		ArrayList<RSSFeedModel> rssList = new ArrayList<RSSFeedModel>();
//
//		DocumentBuilderFactory docBulderFactory = DocumentBuilderFactory.newInstance();
//		DocumentBuilder docBulder = docBulderFactory.newDocumentBuilder();
//		Document xmlDoc = docBulder.parse(inputStream);
//		Element rootElement = xmlDoc.getDocumentElement();
//		// L.m("" + rootElement.getTagName());
//		NodeList itemImage = rootElement.getElementsByTagName("image");
//		NodeList itemList = rootElement.getElementsByTagName("item");
//		NodeList itemChildren = null;
//		Node currentItem = null;
//		Node currentChild = null;
//
//		currentItem = itemImage.item(0);
//		itemChildren = currentItem.getChildNodes();
//		for (int j = 0; j < itemChildren.getLength(); j++) {
//			currentChild = itemChildren.item(j);
//			if (currentChild.getNodeName().equalsIgnoreCase("url")) {
//				imageUrl = currentChild.getTextContent();
//				L.m("Image Url : " + imageUrl);
//			}
//		}
//
//		if (itemList.getLength() >= 3)
//			for (int i = 0; i <= 3; i++) {
//				ob = new RSSFeedModel();
//				sbDesc = new StringBuilder();
//				currentItem = itemList.item(i);
//				itemChildren = currentItem.getChildNodes();
//				for (int j = 0; j < itemChildren.getLength(); j++) {
//					currentChild = itemChildren.item(j);
//					if (currentChild.getNodeName().equalsIgnoreCase("title")) {
//						ob.setTitel(Html.fromHtml(currentChild.getTextContent()).toString());
//						ob.setImgSrc(imageUrl);
//						ob.setCreator("");
//						L.m("Title : " + currentChild.getTextContent());
//					}
//					if (currentChild.getNodeName().equalsIgnoreCase("pubDate")) {
//						String pubDate=currentChild.getTextContent();
//						String[] modifyDate=pubDate.split(",");
//						ob.setPubDate(modifyDate[1].trim()+","+modifyDate[2]);
//						L.m("pubDate : " + currentChild.getTextContent());
//					}
//
//					if (currentChild.getNodeName().equalsIgnoreCase("link")) {
//						ob.setLink(currentChild.getTextContent());
//						L.m("link : " + currentChild.getTextContent());
//					}
//					if (currentChild.getNodeName().equalsIgnoreCase("description")) {
//						sbDesc.append(currentChild.getTextContent());
//						ob.setDescription(sbDesc);
//						L.m("description : " + currentChild.getTextContent());
//					}
//
//				}
//				if (ob != null) {
//					rssList.add(ob);
//				}
//			}
//		return rssList;
//	}

	public static ArrayList<RSSFeedModel> processXMLparseSbuddy(String response){
		StringBuilder sbDesc,sbTitle;
		RSSFeedModel ob = null;
		ArrayList<RSSFeedModel> rssList = new ArrayList<RSSFeedModel>();

		try {
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(false);
			XmlPullParser xpp = factory.newPullParser();
			InputStream is = new ByteArrayInputStream(response.getBytes());
			xpp.setInput(is,null);
			// xpp.setInput(getInputStream(url), "UTF-8");

			boolean insideItem = false;

			// Returns the type of current event: START_TAG, END_TAG, etc..
			int eventType = xpp.getEventType();
			while (eventType != XmlPullParser.END_DOCUMENT) {
				if (eventType == XmlPullParser.START_TAG) {

					if (xpp.getName().equalsIgnoreCase("item")) {
						insideItem = true;
					} else if (xpp.getName().equalsIgnoreCase("title")) {
						if (insideItem) {
							//Log.i("Title is", xpp.nextText());

							ob=new RSSFeedModel();
							sbTitle=new StringBuilder();
							sbTitle.append(xpp.nextText());
							ob.setTitel(sbTitle);
							ob.setCreator("");
						}

					} else if (xpp.getName().equalsIgnoreCase("link")) {
						if (insideItem) {
							//Log.i("Link is", xpp.nextText());
							ob.setLink(xpp.nextText());
						}
					}
					else if (xpp.getName().equalsIgnoreCase("content:encoded")) {
						if (insideItem) {
							//Log.i("description is.", xpp.nextText());
							sbDesc=new StringBuilder();
							//sbDesc.append(xpp.nextText());
							//ob.setDescription(sbDesc);
							xpp.require(XmlPullParser.START_TAG, ns, "content:encoded");
							// Descriptions end in " [...]" (but with unicode ellipses) and then a 1x1px image so we
							// remove those
							String content = null;// = readText(xpp).replaceAll("<img.*/>$", "");
							if (xpp.next() == XmlPullParser.TEXT) {
								content = xpp.getText();
								xpp.nextTag();
							}
							// Unicode characters are escaped in the XML, so we unescape those
							//return StringEscapeUtils.unescapeXml(result);
							content.replaceAll("<img.*/>$", "");
							sbDesc.append(content);
							ob.setDescription(sbDesc);
							xpp.require(XmlPullParser.END_TAG, ns, "content:encoded");

						}
					}
					else if (xpp.getName().equalsIgnoreCase("pubDate")) {
						if (insideItem) {
							//Log.i("Publish Date is.", xpp.nextText());
							ob.setPubDate(xpp.nextText().substring(5,16));
						}
					}
					else if (xpp.getName().equalsIgnoreCase("media:content")) {
						if (insideItem) {

							//Log.i("Media Content url is.",xpp.getAttributeValue(null, "url"));
//							ob.setImgSrc(xpp.getAttributeValue(null, "url"));
//							rssList.add(ob);
						}
					}
					else if (xpp.getName().equalsIgnoreCase("media:thumbnail")) {
						if (insideItem) {
							//Log.i("Media thumbnail title.",xpp.nextText());
							ob.setImgSrc(xpp.getAttributeValue(null, "url"));
						}

					}

				} else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase("item")) {
                    if(ob!=null)
                        rssList.add(ob);
					insideItem = false;
				}
				eventType = xpp.next(); /// move to next element
			}
		} catch (XmlPullParserException e) {
			L.m(e.toString());
			CompactFragment.ErrorMessage=e.toString() +response;
			return null;
		} catch (IOException e) {
			L.m(e.toString());
			CompactFragment.ErrorMessage=e.toString()+response;
			return null;
		}



		return rssList;
	}

//	public static arraylist<rssfeedmodel> processxmlparsesbuddy(inputstream inputstream) throws exception {
//		rssfeedmodel ob = null;
//		string imageurl = null;
//		stringbuilder sbdesc;
//		arraylist<rssfeedmodel> rsslist = new arraylist<rssfeedmodel>();
//
//		documentbuilderfactory docbulderfactory = documentbuilderfactory.newinstance();
//		docbulderfactory.setnamespaceaware(true);
//		documentbuilder docbulder = docbulderfactory.newdocumentbuilder();
//		document xmldoc = docbulder.parse(inputstream);
//		element rootelement = xmldoc.getdocumentelement();
//		// l.m("" + rootelement.gettagname());
//		// nodelist itemimage = rootelement.getelementsbytagname("image");
//		nodelist itemlist = rootelement.getelementsbytagname("item");
//		nodelist itemchildren = null;
//		node currentitem = null;
//		node currentchild = null;
////		org.jsoup.nodes.document jdom;
////		org.jsoup.select.elements tag;
//
//		// currentitem = itemimage.item(0);
//		// itemchildren = currentitem.getchildnodes();
//		// for (int j = 0; j < itemchildren.getlength(); j++) {
//		// currentchild = itemchildren.item(j);
//		// if (currentchild.getnodename().equalsignorecase("url")) {
//		// imageurl=currentchild.gettextcontent();
//		// l.m("image url : "+ imageurl);
//		// }
//		// }
//
//		if (itemlist.getlength() >= 3)
//			for (int i = 0; i <= 3; i++) {
//				ob = new rssfeedmodel();
//				sbdesc = new stringbuilder();
//				currentitem = itemlist.item(i);
//				itemchildren = currentitem.getchildnodes();
//				for (int j = 0; j < itemchildren.getlength(); j++) {
//					currentchild = itemchildren.item(j);
//					if (currentchild.getnodename().equalsignorecase("title")) {
//						ob.settitel(currentchild.gettextcontent());
//						ob.setimgsrc(imageurl);
//						l.m("title : " + currentchild.gettextcontent());
//					}
//					if (currentchild.getnodename().equalsignorecase("pubdate")) {
//						ob.setpubdate(currentchild.gettextcontent());
//						l.m("pubdate : " + currentchild.gettextcontent());
//					}
//					if (currentchild.getnodename().equalsignorecase("link")) {
//						ob.setlink(currentchild.gettextcontent());
//						l.m("link : " + currentchild.gettextcontent());
//					}
//					if (currentchild.getnodename().equalsignorecase("description")) {
//						sbdesc.append(currentchild.gettextcontent());
//						ob.setdescription(sbdesc);
//						l.m("description : " + currentchild.gettextcontent());
//					}
//					if (currentchild.getnodename().equalsignorecase("media:content")) {
//						l.m("media:content : " + "content tag");
//						l.m("media:content : " + rootelement.getelementsbytagname("media:content"));
//						currentchild = itemchildren.item(0);
////						if (currentchild.getnodename().equalsignorecase("url")) {
////							imageurl = currentchild.gettextcontent();
////							l.m("image url : " + imageurl);
////						}
//						// sbdesc.append(currentchild.gettextcontent());
////						jdom = jsoup.parse(currentchild.gettextcontent());
////						tag = jdom.select("url");
//						// ob.setimgsrc(jdom.attr("media:content"));
//
//					}
//
//				}
//				if (ob != null) {
//					rsslist.add(ob);
//				}
//			}
//		return rsslist;
//	}

//
//	public ArrayList<RSSFeedModel> processXMLparseSbuddy2(String res) throws Exception {
//		// String res = null;
//
////		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
////		factory.setNamespaceAware(false);
////		XmlPullParser xpp = factory.newPullParser();
//		InputStream is = new ByteArrayInputStream(res.getBytes());
////		xpp.setInput(is, null);
//		return parse(is);
//	}
//
//	public static class Entry {
//	    public final String title;
//	    public final String link;
//	    public final String summary;
//
//	    private Entry(String title, String summary, String link) {
//	        this.title = title;
//	        this.summary = summary;
//	        this.link = link;
//	    }
//	}
//
//	public ArrayList<RSSFeedModel> parse(InputStream in) throws XmlPullParserException, IOException {
//        try {
//            XmlPullParser parser = Xml.newPullParser();
//            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
//            parser.setInput(in, null);
//            ns = parser.getNamespace();
//            parser.nextTag();
//            return readFeed(parser);
//        } finally {
//            in.close();
//        }
//    }
//	private ArrayList<RSSFeedModel> readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
//	    ArrayList<RSSFeedModel> entries = new ArrayList<RSSFeedModel>();
//
//	    parser.require(XmlPullParser.START_TAG, ns, "rss");
//
//	     while (parser.next() != XmlPullParser.END_TAG) {
//	        if (parser.getEventType() != XmlPullParser.START_TAG) {
//	            continue;
//	        }
//	        parser.next();
//	        String name = parser.getName();
//	        L.m("name "+ name);
//	        // Starts by looking for the entry tag
//	        if (name.equals("item")) {
//	            entries.add(readEntry(parser));
//	        } else {
//	            skip(parser);
//	        }
//	    }
//	    return entries;
//	}
//
//	// Parses the contents of an entry. If it encounters a title, summary, or link tag, hands them off
//	// to their respective "read" methods for processing. Otherwise, skips the tag.
//	private RSSFeedModel readEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
//	    parser.require(XmlPullParser.START_TAG, ns, "item");
//	    String title = null;
//	    String link = null;
//	    String descriptionString = null;
//	    String pubDate = null;
//	    String imgSrc = null;
//	    while (parser.next() != XmlPullParser.END_TAG) {
//	        if (parser.getEventType() != XmlPullParser.START_TAG) {
//	            continue;
//	        }
//	        String name = parser.getName();
//	        if (name.equals("title")) {
//	            title = readTitle(parser);
//	            L.m("title : "+title);
//	        } else if (name.equals("description")) {
//	        	descriptionString = readDescription(parser);
//	        	L.m("descriptionString : "+descriptionString);
//	        } else if (name.equals("link")) {
//	            link = readLink(parser);
//	            L.m("link : "+link);
//	        } else {
//	            skip(parser);
//	        }
//	    }
////	    return new RSSFeedModel(title, link, pubDate, descriptionString, imgSrc);
//	    return null;
//	}
//
//	// Processes title tags in the feed.
//	private String readTitle(XmlPullParser parser) throws IOException, XmlPullParserException {
//	    parser.require(XmlPullParser.START_TAG, ns, "title");
//	    String title = readText(parser);
//	    if (parser.getName().equalsIgnoreCase("title")) {
//                Log.i("Title is",parser.nextText());
//	    }
//	    parser.require(XmlPullParser.END_TAG, ns, "title");
//	    return title;
//	}
//
//	// Processes link tags in the feed.
//	private String readLink(XmlPullParser parser) throws IOException, XmlPullParserException {
//	    String link = "";
//	    parser.require(XmlPullParser.START_TAG, ns, "link");
//	    String tag = parser.getName();
//	    String relType = parser.getAttributeValue(null, "rel");
//	    if (tag.equals("link")) {
//	        if (relType.equals("alternate")){
//	            link = parser.getAttributeValue(null, "href");
//	            parser.nextTag();
//	        }
//	    }
//	    parser.require(XmlPullParser.END_TAG, ns, "link");
//	    return link;
//	}
//
//	// Processes summary tags in the feed.
//	private String readDescription(XmlPullParser parser) throws IOException, XmlPullParserException {
//	    parser.require(XmlPullParser.START_TAG, ns, "description");
//	    String summary = readText(parser);
//	    parser.require(XmlPullParser.END_TAG, ns, "description");
//	    return summary;
//	}
//
//	// For the tags title and summary, extracts their text values.
//	private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
//	    String result = "";
//	    if (parser.next() == XmlPullParser.TEXT) {
//	        result = parser.getText();
//	        parser.nextTag();
//	    }
//	    return result;
//	}
//
//	private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
//	    if (parser.getEventType() != XmlPullParser.START_TAG) {
//	        throw new IllegalStateException();
//	    }
//	    int depth = 1;
//	    while (depth != 0) {
//	        switch (parser.next()) {
//	        case XmlPullParser.END_TAG:
//	            depth--;
//	            break;
//	        case XmlPullParser.START_TAG:
//	            depth++;
//	            break;
//	        }
//	    }
//	 }
}