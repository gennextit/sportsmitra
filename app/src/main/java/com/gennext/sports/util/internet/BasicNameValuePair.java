package com.gennext.sports.util.internet;

/**
 * Created by Admin on 2/6/2018.
 */

public class BasicNameValuePair {
    private final String key;
    private final String value;

    public BasicNameValuePair(String key, String value) {
        this.key=key;
        this.value=value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
