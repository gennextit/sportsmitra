package com.gennext.sports.util;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DBManager {
	// Database Tokens
	SQLiteDatabase db;
	Activity act;
	//ArrayList<TokenModel> tableList;

	public DBManager(Activity activity) {
		this.act = activity;
		CreateDataBase();
	}

	public void CreateDataBase() {
		db = act.openOrCreateDatabase("sbuddyDB", Context.MODE_PRIVATE, null);
		db.execSQL(
				"CREATE TABLE IF NOT EXISTS dashboard(playerId VARCHAR,counter INT);");
		db.execSQL(
				"CREATE TABLE IF NOT EXISTS venuedashboard(playerId VARCHAR,counter INT);");
		
	}
	
	/***************** Coach Database ****************************/

	public void InsertCoachCounter(String playerId, int counter) {
		// Inserting record
		
		db.execSQL("INSERT INTO dashboard ('playerId','counter') VALUES('" + playerId + "','" + counter + "');");
		Cursor c = db.rawQuery("SELECT * FROM dashboard WHERE playerId='" + playerId + "'",null);
		// Checking if no records found
		if (c.getCount() != 0) {
			L.m("Player id : "+playerId+" inserted into database"); 
		}
	}

	public Cursor ViewCoachCounter(String playerId) {
		// Inserting record
		// Retrieving all records
		Cursor c = db.rawQuery("SELECT * FROM dashboard WHERE playerId='" + playerId + "'",null);
		// Checking if no records found
		if (c.getCount() == 0) {
			L.m("Error No records found");
			return null;
		}
		return c;
	}
	
	public int matchCoachIdAndReturnCounter(String playerId,final int currentCounter) {
		String cCounter=String.valueOf(currentCounter);
		Cursor c = db.rawQuery("SELECT * FROM dashboard WHERE playerId='" + playerId + "'",null);
		// Checking if records found
		if (c.getCount() != 0) {
			while (c.moveToNext()) {
//				if(c.getString(0).equalsIgnoreCase(playerId)){
//					
//				}
				if(c.getInt(0)<currentCounter){
					UpdateCoachCounter(playerId, cCounter);
					return (currentCounter-c.getInt(0));
				}else{
					UpdateCoachCounter(playerId, cCounter);
					return 0;
				}
			} 
		}else{
			InsertCoachCounter(playerId, currentCounter);
			return currentCounter;
		}
		return 0;
	}

	public void UpdateCoachCounter(String playerId, String counter) {
		// Searching token number
		Cursor c = db.rawQuery("SELECT * FROM dashboard WHERE playerId='" + playerId + "'", null);
		if (c.moveToFirst()) {
			// Modifying record if found
			db.execSQL("UPDATE dashboard SET counter='" + counter + "'WHERE playerId='" + playerId + "'");
			L.m("Player id : "+playerId+" Updated into database"); 
		} else {
			L.m("Error Invalid Email Id");
		}

	}

	
	
	
	/***************** Venue Database ****************************/

	public void InsertVenueCounter(String playerId, int counter) {
		// Inserting record
		
		db.execSQL("INSERT INTO venuedashboard ('playerId','counter') VALUES('" + playerId + "','" + counter + "');");
		Cursor c = db.rawQuery("SELECT * FROM venuedashboard WHERE playerId='" + playerId + "'",null);
		// Checking if no records found
		if (c.getCount() != 0) {
			L.m("Player id : "+playerId+" inserted into database"); 
		}
	}

	public Cursor ViewVenueCounter(String playerId) {
		// Inserting record
		// Retrieving all records
		Cursor c = db.rawQuery("SELECT * FROM venuedashboard WHERE playerId='" + playerId + "'",null);
		// Checking if no records found
		if (c.getCount() == 0) {
			L.m("Error No records found");
			return null;
		}
		return c;
	}
	
	public int matchVenueIdAndReturnCounter(String playerId,final int currentCounter) {
		String cCounter=String.valueOf(currentCounter);
		Cursor c = db.rawQuery("SELECT * FROM venuedashboard WHERE playerId='" + playerId + "'",null);
		// Checking if records found
		if (c.getCount() != 0) {
			while (c.moveToNext()) {
//				if(c.getString(0).equalsIgnoreCase(playerId)){
//					
//				}
				if(c.getInt(0)<currentCounter){
					UpdateVenueCounter(playerId, cCounter);
					return (currentCounter-c.getInt(0));
				}else{
					UpdateVenueCounter(playerId, cCounter);
					return 0;
				}
			} 
		}else{
			InsertVenueCounter(playerId, currentCounter);
			return currentCounter;
		}
		return 0;
	}

	public void UpdateVenueCounter(String playerId, String counter) {
		// Searching token number
		Cursor c = db.rawQuery("SELECT * FROM venuedashboard WHERE playerId='" + playerId + "'", null);
		if (c.moveToFirst()) {
			// Modifying record if found
			db.execSQL("UPDATE venuedashboard SET counter='" + counter + "'WHERE playerId='" + playerId + "'");
			L.m("Player id : "+playerId+" Updated into database"); 
		} else {
			L.m("Error Invalid player Id");
		}

	}

	
//	public void DeleteData(String ticketId) {
//		// Searching token number
//		Cursor c = db.rawQuery("SELECT * FROM tickets WHERE ticketId='" + ticketId + "'", null);
//		if (c.moveToFirst()) {
//			// Deleting record if found
//			db.execSQL("DELETE FROM token WHERE ticketId='" + ticketId + "'");
//			L.m("Success " + "Record Deleted");
//		} else {
//			L.m("Error " + "Invalid Rollno");
//		}
//	}

	
	
	public void CloseDB() {
		db.close();
	}
}
