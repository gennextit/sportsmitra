package com.gennext.sports.util;

import com.gennext.sports.fragments.CompactFragment;
import com.gennext.sports.util.internet.BasicNameValuePair;
import com.gennext.sports.util.internet.MultipartEntity;
import com.gennext.sports.util.internet.URLEncodedUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

//import org.apache.http.client.HttpClient;
//import org.apache.http.client.entity.UrlEncodedFormEntity;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.client.methods.HttpPut;
//import org.apache.http.client.utils.URLEncodedUtils;

public class HttpReqService {
    static String response = null;
    public final static int GET = 1;
    public final static int PUT = 2;
    public final static int POST = 3;
    public final static int EXECUTE_TASK = 4;
    // We don't use namespaces
    private static String ns = null;


    /**
     * Making service call
     *
     * @url - url to make request
     * @method - http request method
     */
    public String makeConnection(String url, int method) {
        return this.makeConnection(url, method, null, null, false, 0);
    }

    public String makeConnection(String url, int method, List<BasicNameValuePair> params) {
        return this.makeConnection(url, method, params, null, false, 0);
    }

    public String makeConnection(String url, int method, List<BasicNameValuePair> params, int executeTask) {
        return this.makeConnection(url, method, params, null, false, EXECUTE_TASK);
    }

    public String makeConnection(String url, int method, MultipartEntity params) {
        return this.makeConnection(url, method, null, params, true, 0);
    }

    /**
     * Making service call
     *
     * @url - url to make request
     * @method - http request method
     * @params - http request params
     */
    public String makeConnection(String url, int method, List<BasicNameValuePair> params, MultipartEntity mParams,
                                 Boolean type, int executeTask) {
        String result = "";
        //return null;
        if (method == POST) {
            // adding post params
            if (params != null && type == false) {
                result= ApiCall.POST(url,RequestBuilder.getParams(params));
            } else {
                result= ApiCall.POST(url,mParams.RequestBody());
            }
        }
//		else if (method == GET) {
        else{
            if (params != null) {
                String paramString = URLEncodedUtils.format(params,"utf-8");
                url += "?" + paramString;
            }
            result= ApiCall.GET(url);
        }
        if (executeTask != EXECUTE_TASK) {
            return result;
        } else {
            return getSimpleJsonTask(result);
        }
    }

    private String getSimpleJsonTask(String response) {
        String output = null;
        if (response.contains("[")) {
            try {
                JSONArray json = new JSONArray(response);
                for (int i = 0; i < json.length(); i++) {
                    JSONObject obj = json.getJSONObject(i);
                    if (obj.optString("status").equals("success")) {
                        output = "success";

                    } else if (obj.optString("status").equals("failure")) {
                        output = obj.optString("message");
                    }
                }

            } catch (JSONException e) {
                L.m("Json Error :" + e.toString());
                CompactFragment.ErrorMessage = e.toString() + response;
                return null;
                // return e.toString();
            }
        } else {
            L.m("Invalid JSON found : " + response);
            CompactFragment.ErrorMessage = response;
            return null;
        }

        return output;
    }

    /***********************************************/

    /************** Get JSON from Assets folder ***********/
    /***********************************************/

    // Method that will parse the JSON file and will return a JSONObject
    public String parseAssetsData(InputStream data) {
        String JSONString = null;
        try {

            // open the inputStream to the file
            InputStream inputStream = data;

            int sizeOfJSONFile = inputStream.available();

            // array that will store all the data
            byte[] bytes = new byte[sizeOfJSONFile];

            // reading data into the array from the file
            inputStream.read(bytes);

            // close the input stream
            inputStream.close();

            JSONString = new String(bytes, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return JSONString;
    }

//    public static ArrayList<RSSFeedModel> processXML(InputStream inputStream) throws Exception {
//        RSSFeedModel ob = null;
//        StringBuilder sbDesc;
//        ArrayList<RSSFeedModel> rssList = new ArrayList<RSSFeedModel>();
//
//        DocumentBuilderFactory docBulderFactory = DocumentBuilderFactory.newInstance();
//        DocumentBuilder docBulder = docBulderFactory.newDocumentBuilder();
//        Document xmlDoc = docBulder.parse(inputStream);
//        Element rootElement = xmlDoc.getDocumentElement();
//        // L.m("" + rootElement.getTagName());
//        NodeList itemList = rootElement.getElementsByTagName("item");
//        NodeList itemChildren = null;
//        Node currentItem = null;
//        Node currentChild = null;
//        NodeList imglist = null;
//        Node imgChild = null;
//
//        org.jsoup.nodes.Document jdom;
//        org.jsoup.select.Elements tag;
//        if (itemList.getLength() >= 3)
//            for (int i = 0; i <= 3; i++) {
//                ob = new RSSFeedModel();
//                sbDesc = new StringBuilder();
//                currentItem = itemList.item(i);
//                itemChildren = currentItem.getChildNodes();
//                for (int j = 0; j < itemChildren.getLength(); j++) {
//                    currentChild = itemChildren.item(j);
//                    if (currentChild.getNodeName().equalsIgnoreCase("title")) {
//                        ob.setTitel(currentChild.getTextContent());
//                        // L.m("Title : " + currentChild.getTextContent());
//                    }
//                    if (currentChild.getNodeName().equalsIgnoreCase("pubDate")) {
//                        ob.setPubDate(currentChild.getTextContent());
//                        // L.m("pubDate : " + currentChild.getTextContent());
//                    }
//                    if (currentChild.getNodeName().equalsIgnoreCase("dc:creator")) {
//                        ob.setCreator(currentChild.getTextContent());
//                        // L.m("dc:creator : " + currentChild.getTextContent());
//                    }
//                    if (currentChild.getNodeName().equalsIgnoreCase("description")) {
//                        // ob.setDescription(currentChild.getTextContent());
//                        // L.m("description : " +
//                        // currentChild.getTextContent());
//                    }
//
//                    if (currentChild.getNodeName().equalsIgnoreCase("content:encoded")) {
//                        imglist = currentChild.getChildNodes();
//                        for (int k = 0; k < imglist.getLength(); k++) {
//                            imgChild = imglist.item(k);
//                            if (imgChild.getNodeName().equalsIgnoreCase("#cdata-section")) {
//                                String html = imgChild.getTextContent();
//                                jdom = Jsoup.parse(html);
//                                jdom.select("p.wp-caption-text").remove();
//                                tag = jdom.select("p");
//
//                                for (org.jsoup.nodes.Element element : tag) {
//                                    // L.m(element.text());
//                                    sbDesc.append(element.text());
//                                    sbDesc.append("\n\n");
//
//                                }
//
//                                ob.setDescription(sbDesc);
//                                if (html.contains("<img"))
//                                    ;
//                                {
//                                    String img = html.substring(html.indexOf("<img "));
//                                    // String cleanUp = img.substring(0,
//                                    // img.indexOf(">")+1);
//                                    img = img.substring(img.indexOf("src=") + 5);
//
//                                    int indexOf = img.indexOf("\"");
//                                    if (indexOf == -1) {
//                                        indexOf = img.indexOf("\"");
//                                    }
//                                    img = img.substring(0, indexOf);
//                                    ob.setImgSrc(img);
//                                    L.m(img);
//                                }
//
//                            }
//                        }
//                    }
//                }
//                if (ob != null) {
//                    rssList.add(ob);
//                }
//            }
//        return rssList;
//    }
//
//    public static ArrayList<RSSFeedModel> processXMLparseZee(InputStream inputStream) throws Exception {
//        RSSFeedModel ob = null;
//        String imageUrl = null;
//        StringBuilder sbDesc;
//        ArrayList<RSSFeedModel> rssList = new ArrayList<RSSFeedModel>();
//
//        DocumentBuilderFactory docBulderFactory = DocumentBuilderFactory.newInstance();
//        DocumentBuilder docBulder = docBulderFactory.newDocumentBuilder();
//        Document xmlDoc = docBulder.parse(inputStream);
//        Element rootElement = xmlDoc.getDocumentElement();
//        // L.m("" + rootElement.getTagName());
//        NodeList itemImage = rootElement.getElementsByTagName("image");
//        NodeList itemList = rootElement.getElementsByTagName("item");
//        NodeList itemChildren = null;
//        Node currentItem = null;
//        Node currentChild = null;
//
//        currentItem = itemImage.item(0);
//        itemChildren = currentItem.getChildNodes();
//        for (int j = 0; j < itemChildren.getLength(); j++) {
//            currentChild = itemChildren.item(j);
//            if (currentChild.getNodeName().equalsIgnoreCase("url")) {
//                imageUrl = currentChild.getTextContent();
//                L.m("Image Url : " + imageUrl);
//            }
//        }
//
//        if (itemList.getLength() >= 3)
//            for (int i = 0; i <= 3; i++) {
//                ob = new RSSFeedModel();
//                sbDesc = new StringBuilder();
//                currentItem = itemList.item(i);
//                itemChildren = currentItem.getChildNodes();
//                for (int j = 0; j < itemChildren.getLength(); j++) {
//                    currentChild = itemChildren.item(j);
//                    if (currentChild.getNodeName().equalsIgnoreCase("title")) {
//                        ob.setTitel(currentChild.getTextContent());
//                        ob.setImgSrc(imageUrl);
//                        L.m("Title : " + currentChild.getTextContent());
//                    }
//                    if (currentChild.getNodeName().equalsIgnoreCase("pubDate")) {
//                        ob.setPubDate(currentChild.getTextContent());
//                        L.m("pubDate : " + currentChild.getTextContent());
//                    }
//                    if (currentChild.getNodeName().equalsIgnoreCase("link")) {
//                        ob.setLink(currentChild.getTextContent());
//                        L.m("link : " + currentChild.getTextContent());
//                    }
//                    if (currentChild.getNodeName().equalsIgnoreCase("description")) {
//                        sbDesc.append(currentChild.getTextContent());
//                        ob.setDescription(sbDesc);
//                        L.m("description : " + currentChild.getTextContent());
//                    }
//
//                }
//                if (ob != null) {
//                    rssList.add(ob);
//                }
//            }
//        return rssList;
//    }

//	public static ArrayList<RSSFeedModel> processXMLparseSbuddy(InputStream inputStream) throws Exception {
//		RSSFeedModel ob = null;
//		String imageUrl = null;
//		StringBuilder sbDesc;
//		ArrayList<RSSFeedModel> rssList = new ArrayList<RSSFeedModel>();
//
//		DocumentBuilderFactory docBulderFactory = DocumentBuilderFactory.newInstance();
//		DocumentBuilder docBulder = docBulderFactory.newDocumentBuilder();
//		Document xmlDoc = docBulder.parse(inputStream);
//		Element rootElement = xmlDoc.getDocumentElement();
//		// L.m("" + rootElement.getTagName());
//		// NodeList itemImage = rootElement.getElementsByTagName("image");
//		NodeList itemList = rootElement.getElementsByTagName("item");
//		NodeList itemChildren = null;
//		Node currentItem = null;
//		Node currentChild = null;
////		org.jsoup.nodes.Document jdom;
////		org.jsoup.select.Elements tag;
//
//		// currentItem = itemImage.item(0);
//		// itemChildren = currentItem.getChildNodes();
//		// for (int j = 0; j < itemChildren.getLength(); j++) {
//		// currentChild = itemChildren.item(j);
//		// if (currentChild.getNodeName().equalsIgnoreCase("url")) {
//		// imageUrl=currentChild.getTextContent();
//		// L.m("Image Url : "+ imageUrl);
//		// }
//		// }
//
//		if (itemList.getLength() >= 3)
//			for (int i = 0; i <= 3; i++) {
//				ob = new RSSFeedModel();
//				sbDesc = new StringBuilder();
//				currentItem = itemList.item(i);
//				itemChildren = currentItem.getChildNodes();
//				for (int j = 0; j < itemChildren.getLength(); j++) {
//					currentChild = itemChildren.item(j);
//					if (currentChild.getNodeName().equalsIgnoreCase("title")) {
//						ob.setTitel(currentChild.getTextContent());
//						ob.setImgSrc(imageUrl);
//						L.m("Title : " + currentChild.getTextContent());
//					}
//					if (currentChild.getNodeName().equalsIgnoreCase("pubDate")) {
//						ob.setPubDate(currentChild.getTextContent());
//						L.m("pubDate : " + currentChild.getTextContent());
//					}
//					if (currentChild.getNodeName().equalsIgnoreCase("link")) {
//						ob.setLink(currentChild.getTextContent());
//						L.m("link : " + currentChild.getTextContent());
//					}
//					if (currentChild.getNodeName().equalsIgnoreCase("description")) {
//						sbDesc.append(currentChild.getTextContent());
//						ob.setDescription(sbDesc);
//						L.m("description : " + currentChild.getTextContent());
//					}
//					if (currentChild.getNodeName().equalsIgnoreCase("media:content")) {
//						L.m("media:content : " + "content tag");
//						L.m("media:content : " + currentChild.getTextContent());
//						currentChild = itemChildren.item(0);
//						if (currentChild.getNodeName().equalsIgnoreCase("url")) {
//							imageUrl = currentChild.getTextContent();
//							L.m("Image Url : " + imageUrl);
//						}
//						// sbDesc.append(currentChild.getTextContent());
////						jdom = Jsoup.parse(currentChild.getTextContent());
////						tag = jdom.select("url");
//						// ob.setImgSrc(jdom.attr("media:content"));
//
//					}
//
//				}
//				if (ob != null) {
//					rssList.add(ob);
//				}
//			}
//		return rssList;
//	}
//
//	public ArrayList<RSSFeedModel> processXMLparseSbuddy2(String res) throws Exception {
//		// String res = null;
//
////		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
////		factory.setNamespaceAware(false);
////		XmlPullParser xpp = factory.newPullParser();
//		InputStream is = new ByteArrayInputStream(res.getBytes());
////		xpp.setInput(is, null);
//		return parse(is);
//	}
//
////	public static class Entry {
////	    public final String title;
////	    public final String link;
////	    public final String summary;
////
////	    private Entry(String title, String summary, String link) {
////	        this.title = title;
////	        this.summary = summary;
////	        this.link = link;
////	    }
////	}
//
//	public ArrayList<RSSFeedModel> parse(InputStream in) throws XmlPullParserException, IOException {
//        try {
//            XmlPullParser parser = Xml.newPullParser();
//            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
//            parser.setInput(in, null);
//            ns = parser.getNamespace();
//            parser.nextTag();
//            return readFeed(parser);
//        } finally {
//            in.close();
//        }
//    }
//	private ArrayList<RSSFeedModel> readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
//	    ArrayList<RSSFeedModel> entries = new ArrayList<RSSFeedModel>();
//
//	    parser.require(XmlPullParser.START_TAG, ns, "rss");
//
//	     while (parser.next() != XmlPullParser.END_TAG) {
//	        if (parser.getEventType() != XmlPullParser.START_TAG) {
//	            continue;
//	        }
//	        parser.next();
//	        String name = parser.getName();
//	        L.m("name "+ name);
//	        // Starts by looking for the entry tag
//	        if (name.equals("item")) {
//	            entries.add(readEntry(parser));
//	        } else {
//	            skip(parser);
//	        }
//	    }
//	    return entries;
//	}
//
//	// Parses the contents of an entry. If it encounters a title, summary, or link tag, hands them off
//	// to their respective "read" methods for processing. Otherwise, skips the tag.
//	private RSSFeedModel readEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
//	    parser.require(XmlPullParser.START_TAG, ns, "item");
//	    String title = null;
//	    String link = null;
//	    String descriptionString = null;
//	    String pubDate = null;
//	    String imgSrc = null;
//	    while (parser.next() != XmlPullParser.END_TAG) {
//	        if (parser.getEventType() != XmlPullParser.START_TAG) {
//	            continue;
//	        }
//	        String name = parser.getName();
//	        if (name.equals("title")) {
//	            title = readTitle(parser);
//	            L.m("title : "+title);
//	        } else if (name.equals("description")) {
//	        	descriptionString = readDescription(parser);
//	        	L.m("descriptionString : "+descriptionString);
//	        } else if (name.equals("link")) {
//	            link = readLink(parser);
//	            L.m("link : "+link);
//	        } else {
//	            skip(parser);
//	        }
//	    }
////	    return new RSSFeedModel(title, link, pubDate, descriptionString, imgSrc);
//	    return null;
//	}
//
//	// Processes title tags in the feed.
//	private String readTitle(XmlPullParser parser) throws IOException, XmlPullParserException {
//	    parser.require(XmlPullParser.START_TAG, ns, "title");
//	    String title = readText(parser);
//	    if (parser.getName().equalsIgnoreCase("title")) {
//                Log.i("Title is",parser.nextText());
//	    }
//	    parser.require(XmlPullParser.END_TAG, ns, "title");
//	    return title;
//	}
//
//	// Processes link tags in the feed.
//	private String readLink(XmlPullParser parser) throws IOException, XmlPullParserException {
//	    String link = "";
//	    parser.require(XmlPullParser.START_TAG, ns, "link");
//	    String tag = parser.getName();
//	    String relType = parser.getAttributeValue(null, "rel");
//	    if (tag.equals("link")) {
//	        if (relType.equals("alternate")){
//	            link = parser.getAttributeValue(null, "href");
//	            parser.nextTag();
//	        }
//	    }
//	    parser.require(XmlPullParser.END_TAG, ns, "link");
//	    return link;
//	}
//
//	// Processes summary tags in the feed.
//	private String readDescription(XmlPullParser parser) throws IOException, XmlPullParserException {
//	    parser.require(XmlPullParser.START_TAG, ns, "description");
//	    String summary = readText(parser);
//	    parser.require(XmlPullParser.END_TAG, ns, "description");
//	    return summary;
//	}
//
//	// For the tags title and summary, extracts their text values.
//	private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
//	    String result = "";
//	    if (parser.next() == XmlPullParser.TEXT) {
//	        result = parser.getText();
//	        parser.nextTag();
//	    }
//	    return result;
//	}
//
//	private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
//	    if (parser.getEventType() != XmlPullParser.START_TAG) {
//	        throw new IllegalStateException();
//	    }
//	    int depth = 1;
//	    while (depth != 0) {
//	        switch (parser.next()) {
//	        case XmlPullParser.END_TAG:
//	            depth--;
//	            break;
//	        case XmlPullParser.START_TAG:
//	            depth++;
//	            break;
//	        }
//	    }
//	 }
}