package com.gennext.sports.util;

import com.gennext.sports.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DefaultAlertBox {

	public interface OnDefaultAlertBoxListener{
		public void onSuccessButtonClick(Boolean status);
		public void onCancelButtonClick(Boolean status);
	}

	OnDefaultAlertBoxListener comm;
	int myMax;
	Activity activity;
	AlertDialog dialog = null;

	public DefaultAlertBox(Activity activity,OnDefaultAlertBoxListener callback) {
		this.comm = callback; 
		this.activity=activity;
	}

	public void showDefaultAlertBox() {
		showDefaultAlertBox(getSt(R.string.internet_error_tag),getSt(R.string.internet_error_msg),2);
	}
	public void showDefaultAlertBox(String title,String Description,int noOfButtons) {
		
		
		
		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);

		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = activity.getLayoutInflater();

		View v = inflater.inflate(R.layout.alert_dialog, null);
		dialogBuilder.setView(v);
		Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
		Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
		TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
		LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
		LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);
		  
		tvTitle.setText(title);
		tvDescription.setText(Description);
		if(noOfButtons==1){
			button2.setVisibility(View.GONE);
			llBtn2.setVisibility(View.GONE);
		}
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				comm.onSuccessButtonClick(true);
			}
		});
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				comm.onCancelButtonClick(false);
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}
	public String getSt(int id) {

		return activity.getResources().getString(id);
	}
 

}