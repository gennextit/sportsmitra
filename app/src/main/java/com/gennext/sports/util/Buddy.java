package com.gennext.sports.util;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Base64;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.gennext.sports.R;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

public class Buddy {
//
	public static final String COMMON = "sports";

	public static final String PlayerId = "PlayerId" + COMMON;
	public static final String Name = "Name" + COMMON;
	public static final String countryCode = "countryCode" + COMMON;
	public static final String Mobile = "Mobile" + COMMON;
	public static final String CompleteMobile = "CompleteMobile" + COMMON;
	public static final String Email = "Email" + COMMON;
	public static final String Image = "Image" + COMMON;
	public static final String ImageBitmapData = "ImageBitmapData" + COMMON;
	public static final String profileImageUri = "profileImageUri" + COMMON;

	public static final String City = "City" + COMMON;
	public static final String AGE = "age" + COMMON;
	public static final String GENDER = "gender" + COMMON;
	public static final String LOCATION = "location" + COMMON;
	public static final String CLUB_OR_APPARTMENT = "cluborappartment" + COMMON;
	public static final String ABOUT = "about" + COMMON;

	public static final String fbId = "fbId" + COMMON;
	public static final String gmainId = "gmainId" + COMMON;
	public static final String socialId = "socialId" + COMMON;

	public static void setPlayerDetail(Activity act, EditText etName, ImageView ivImg,
			EditText etAge, Spinner spGender, AutoCompleteTextView acCode, EditText etMobile, EditText etEmail,
			AutoCompleteTextView acCity, AutoCompleteTextView acLocation, AutoCompleteTextView acClub, EditText etAbout,
			String googlePlaceId, String gender) {

		String pName = Utility.LoadPref(act, Buddy.Name);
		String pImage = Utility.LoadPref(act, Buddy.Image);
		String pAge = Utility.LoadPref(act, Buddy.AGE);
		String pCity = Utility.LoadPref(act, Buddy.City);
		String pLocation = Utility.LoadPref(act, Buddy.LOCATION);
		String pClub = Utility.LoadPref(act, Buddy.CLUB_OR_APPARTMENT);
		String pAbout = Utility.LoadPref(act, Buddy.ABOUT);
		String pCountryCode = Utility.LoadPref(act, Buddy.countryCode);
		String pMobile = Utility.LoadPref(act, Buddy.Mobile);
		String pEmail = Utility.LoadPref(act, Buddy.gmainId);

		if (!pName.equals("")) {
			etName.setText(pName);
			etName.setSelection(etName.getText().length());
		}

		setBuddyProfileImage(act, ivImg);

		if (!pAge.equals("")) {
			etAge.setText(pAge);
			etAge.setSelection(etAge.getText().length());
		}
		if (!pCity.equals("")) {
			acCity.setText(pCity);
			acCity.setSelection(acCity.getText().length());
		}
		if (!pLocation.equals("")) {
			acLocation.setText(pLocation);
			acLocation.setSelection(acLocation.getText().length());
		}
		if (!pClub.equals("")) {
			acClub.setText(pClub);
			acClub.setSelection(acClub.getText().length());
		}
		if (!pAbout.equals("")) {
			etAbout.setText(pAbout);
			etAbout.setSelection(etAbout.getText().length());
		}
		if (!pMobile.equals("")) {
			etMobile.setText(pMobile);
			etMobile.setSelection(etMobile.getText().length());
		}
		if (!pEmail.equals("")) {
			etEmail.setText(pEmail);
			etEmail.setSelection(etEmail.getText().length());
		}
		if (!pCountryCode.equals("")) {
			acCode.setText(pCountryCode);
			acCode.setSelection(acCode.getText().length());
		}

	}

	public static void SaveBuddyProfileImage(Activity act, Bitmap bmp) {
		if (bmp != null) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			bmp.compress(Bitmap.CompressFormat.PNG, 100, baos); // bm is the
																// bitmap object
			byte[] b = baos.toByteArray();
			String encoded = Base64.encodeToString(b, Base64.DEFAULT);
			// adding member to session
			Utility.SavePref(act, Buddy.ImageBitmapData, encoded);
		} else {
			L.m("No buddy image Bitmap found to store image");
		}
	}

	public static void SaveBuddyProfileImage(Activity act, Uri uri) {
		if(uri!=null){
			Utility.SavePref(act, Buddy.profileImageUri, uri.getPath());
		}

		/*Bitmap bitmap = null;
		try {
			if(uri!=null)
			bitmap=CameraUtility.getBitmapFromUri(act,uri);
		} catch (IOException e) {
			L.m(e.toString());
		}

		if (bitmap != null) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos); // bm is the
			// bitmap object
			byte[] b = baos.toByteArray();
			String encoded = Base64.encodeToString(b, Base64.DEFAULT);
			// adding member to session
			Utility.SavePref(act, Buddy.profileImageUri, uri);
		} else {
			Toast.makeText(act,"Image is to large" ,Toast.LENGTH_SHORT).show();
			L.m("No buddy image Bitmap found to store image");
		}*/
	}

	public static void setBuddyProfileImage(Activity act, ImageView ivProfile) {

		if(!Utility.LoadPref(act, Buddy.profileImageUri).equals("")){

			try{
				ivProfile.setImageURI(CameraUtility.StringToUri(Utility.LoadPref(act, Buddy.profileImageUri)));

			}catch (OutOfMemoryError e){
				L.m(e.toString());
				Toast.makeText(act,"Image is to large, please select small image from gallery.",Toast.LENGTH_LONG).show();
			}

		} else if (!Utility.LoadPref(act, Buddy.ImageBitmapData).equals("")) {
			String tempImage = Utility.LoadPref(act, Buddy.ImageBitmapData);
			byte[] imageAsBytes = Base64.decode(tempImage.getBytes(), Base64.DEFAULT);
			Bitmap bmp = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
			try{
				ivProfile.setImageBitmap(bmp);
			}catch (OutOfMemoryError e){
				L.m(e.toString());
				Toast.makeText(act,"Image is to large, please select small image from gallery.",Toast.LENGTH_LONG).show();
			}

		} else {
			String pImage = Utility.LoadPref(act, Buddy.Image);
			if (!pImage.equals("")) {
				try{
					Glide.with(act)
							.load(pImage)
							.placeholder(R.drawable.profile)
							.error(R.drawable.profile)
							.into(ivProfile);
				}catch (OutOfMemoryError e){
					L.m(e.toString());
					Toast.makeText(act,"Image is to large, please select small image from gallery.",Toast.LENGTH_LONG).show();
				}

			}

		}
	}

	public static String getSportImage(Activity activity, String SportId) {
		String sportImage = null, baseImage = null;
		String response = Utility.LoadPref(activity, AppTokens.SportList);

		if (response.contains("[")) {
			try {
				JSONArray sportList = new JSONArray(response);

				for (int i = 0; i < sportList.length(); i++) {

					JSONObject obj = sportList.getJSONObject(i);
					if (!obj.optString("status").equals("") && obj.optString("status").equalsIgnoreCase("success")) {
						JSONArray message = obj.getJSONArray("message");
						for (int j = 0; j < message.length(); j++) {
							JSONObject messageData = message.getJSONObject(j);
							if (messageData.optString("sportId").equalsIgnoreCase(SportId)) {
								sportImage = messageData.optString("logo");
								baseImage = messageData.optString("baseImage");
							}
						}
					}
				}
			} catch (JSONException e) {
				L.m(e.toString());
				return null;
			}
		} else {
			L.m("Invalid JSON found : " + response);
			return null;
		}

		return sportImage;
	}

}
