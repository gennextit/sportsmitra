package com.gennext.sports.util;

/**
 * Created by Admin on 7/4/2017.
 */

public class Const {

    public static final String COMPLETE = "complete";
    public static final String SUCCESS = "success";
    public static final String FAILURE = "failure";
    public static final String SUBMIT = "Submit";
    public static final String RECORD_INSERTED = "Record Inserted";
    public static final String RECORD_UPDATED = "Record Updated";
    public static final String NO_RECORD_FOUND = "No record found";
    public static final String RECORD_ALREADY_EXISTS = "Record already exists";
    public static final String RECORD_DELETED_SUCCESSFUL = "Record deleted successful";
    public static final String RECORD_NOT_EXISTS = "Record not exists";
}
