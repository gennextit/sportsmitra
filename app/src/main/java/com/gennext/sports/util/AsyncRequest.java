package com.gennext.sports.util;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.gennext.sports.util.internet.BasicNameValuePair;

import java.util.List;

//import org.apache.http.client.HttpClient;
//import org.apache.http.client.entity.UrlEncodedFormEntity;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.client.methods.HttpPut;
//import org.apache.http.client.utils.URLEncodedUtils;
//import org.apache.http.impl.client.DefaultHttpClient;
;

public class AsyncRequest extends AsyncTask<String, Void, String> {

    Activity activity;
    static String response = null;
    public final static int GET = 1;
    public final static int PUT = 2;
    public final static int POST = 3;
    private List<BasicNameValuePair> params;
    private int method;
    OnTaskComplete caller;
    ProgressBar pBar;
    Button btn;

    public AsyncRequest(Activity a, int method) {
        this.activity = a;
        this.method = method;
        this.caller = (OnTaskComplete) a;
    }

    public AsyncRequest(Activity a, ProgressBar pBar, Button btn, int method, List<BasicNameValuePair> params) {
        this.activity = a;
        this.method = method;
        this.params = params;
        this.pBar = pBar;
        this.btn = btn;
        this.caller = (OnTaskComplete) a;
    }

    public void onAttach(Activity activity) {
        this.activity = activity;
    }

    public void onDetach() {
        activity = null;
    }

    // Interface to be implemented by calling activity
    public interface OnTaskComplete {
        public void asyncResponseServer(String result);
    }

    @Override
    protected void onPreExecute() {
        L.m("AsyncRequest_onPreExecute" + "executed");
        if (pBar != null && btn != null) {
            pBar.setVisibility(View.VISIBLE);
            btn.setVisibility(View.GONE);
        }
        if (pBar != null) {
            pBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected String doInBackground(String... urls) {
        L.m("AsyncRequest_doInBackground" + "execute");

        String result = "";
        HttpReq ob = new HttpReq();
        result = ob.makeConnection(urls[0], method, params);

        return result;

    }

    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(String result) {
        Log.e("AsyncRequest_post", "executed");
        if (activity != null) {
            if (pBar != null && btn != null) {
                pBar.setVisibility(View.GONE);
                btn.setVisibility(View.VISIBLE);
            }
            if (pBar != null) {
                pBar.setVisibility(View.GONE);
            }
            caller.asyncResponseServer(result);

        }

    }

}
