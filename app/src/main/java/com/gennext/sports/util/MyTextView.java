package com.gennext.sports.util;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class MyTextView extends TextView {
 
      public MyTextView(Context context, AttributeSet attrs, int defStyle) {
          super(context, attrs, defStyle);
      }
 
     public MyTextView(Context context, AttributeSet attrs) {
          super(context, attrs);
      }
 
     public MyTextView(Context context) {
          super(context);
     }
 
 
     public void setTypeface(Typeface tf, int style) {
           if (style == Typeface.BOLD) {
                super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/segoeuib.ttf")/*, -1*/);
            } else {
               super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/segoeui.ttf")/*, -1*/);
            }
      }
 }