package com.gennext.sports.model;

import java.util.Comparator;

public class FindBuddyModel {
	
	private String sBudddy;
	private String playerId;
	private String locationLatitudeLongitude;
	private String fullName;
	private String age;
	private String gender;
	private String imageUrl;
	private Float km;
	private String sportName;
	private String city;
	private String sLogoCount;
	private String sLogo1;
	private String sLogo2;
	private String sLogo3;
	private String location;
	private String clubOrApartment;
	private String clubOrApartmentAvailable;
	private String tab;
	private int counter;
	
	public int getCounter() {
		return counter;
	}
	public void setCounter(int counter) {
		this.counter = counter;
	}
	public static Comparator<FindBuddyModel> COMPARE_BY_KILOMETER = new Comparator<FindBuddyModel>() {
        public int compare(FindBuddyModel one, FindBuddyModel other) {
            return Float.valueOf(one.getKm()).compareTo(other.getKm());
        }
    };
	
    public String getsBudddy() {
		return sBudddy;
	}
	public void setsBudddy(String sBudddy) {
		this.sBudddy = sBudddy;
	}
	
    public Float getKm() {
		return km;
	}
	public void setKm(Float km) {
		this.km = km;
	}
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getTab() {
		return tab;
	}
	public void setTab(String tab) {
		this.tab = tab;
	}
	public String getClubOrApartmentAvailable() {
		return clubOrApartmentAvailable;
	}
	public void setClubOrApartmentAvailable(String clubOrApartmentAvailable) {
		this.clubOrApartmentAvailable = clubOrApartmentAvailable;
	}
	public String getAbout() {
		return about;
	}
	public void setAbout(String about) {
		this.about = about;
	}
	private String about;
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public String getLocationLatitudeLongitude() {
		return locationLatitudeLongitude;
	}
	public void setLocationLatitudeLongitude(String locationLatitudeLongitude) {
		this.locationLatitudeLongitude = locationLatitudeLongitude;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getClubOrApartment() {
		return clubOrApartment;
	}
	public void setClubOrApartment(String clubOrApartment) {
		this.clubOrApartment = clubOrApartment;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	} 
	public String getSportName() {
		return sportName;
	}
	public void setSportName(String sportName) {
		this.sportName = sportName;
	}
	public String getsLogo1() {
		return sLogo1;
	}
	public void setsLogo1(String sLogo1) {
		this.sLogo1 = sLogo1;
	}
	public String getsLogo2() {
		return sLogo2;
	}
	public void setsLogo2(String sLogo2) {
		this.sLogo2 = sLogo2;
	}
	public String getsLogo3() {
		return sLogo3;
	}
	public void setsLogo3(String sLogo3) {
		this.sLogo3 = sLogo3;
	}
	public String getsLogoCount() {
		return sLogoCount;
	}
	public void setsLogoCount(String sLogoCount) {
		this.sLogoCount = sLogoCount;
	}
	

}
