package com.gennext.sports.model;

import java.util.ArrayList;
import java.util.List;

public class MySportsModel {
	private String sportId;
	private String sportName;
	private String logo;
	private String baseImage;
	private int select;
	private String skillLevel;
	
	private static ArrayList<MySportsModel> skillList;
	private static List<MySportsModel> SportList;

	public String getBaseImage() {
		return baseImage;
	}
	public void setBaseImage(String baseImage) {
		this.baseImage = baseImage;
	}
	
	public int getSelect() {
		return select;
	}
	public void setSelect(int select) {
		this.select = select;
	}
	public String getSportId() {
		return sportId;
	}
	public void setSportId(String sportId) {
		this.sportId = sportId;
	}
	public String getSportName() {
		return sportName;
	}
	public void setSportName(String sportName) {
		this.sportName = sportName;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getSkillLevel() {
		return skillLevel;
	}
	public void setSkillLevel(String skillLevel) {
		this.skillLevel = skillLevel;
	}
	
	public ArrayList<MySportsModel> getSkillList() {
		return skillList;
	}
	public void setSkillList(ArrayList<MySportsModel> skillList) {
		MySportsModel.skillList = skillList;
	}
	public List<MySportsModel> getSportList() {
		return SportList;
	}
	public void setSportList(List<MySportsModel> sportList) {
		MySportsModel.SportList = sportList;
	}
	
}
