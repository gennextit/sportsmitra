package com.gennext.sports.model;

import java.util.ArrayList;

import com.gennext.sports.R;
import com.gennext.sports.fragments.profile.ProfileMySkills;
import com.gennext.sports.image_cache.ImageLoader;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MySkillsAdapter extends ArrayAdapter<MySportsModel> {
	private final ArrayList<MySportsModel> list;
	Boolean ClickListener = true;
	private final Activity context;
	public ImageLoader imageLoader;

	public MySkillsAdapter(Activity context, int textViewResourceId, ArrayList<MySportsModel> list) {
		super(context, textViewResourceId, list);
		this.context = context;
		this.list = list;
		imageLoader = new ImageLoader(context.getApplicationContext());

	}

	public MySkillsAdapter(Activity context, int textViewResourceId, ArrayList<MySportsModel> list,
			Boolean ClickListener) {
		super(context, textViewResourceId, list);
		this.context = context;
		this.list = list;
		this.ClickListener = ClickListener;
		imageLoader = new ImageLoader(context.getApplicationContext());

	}

	class ViewHolder {
		TextView catName;
		ImageView image, sk1, sk2, sk3, sk4;
		LinearLayout skillOne, skillTwo, skillThree, skillFour;

		public ViewHolder(View v) {
			catName = (TextView) v.findViewById(R.id.tv_custom_slot_skills_name);
			image = (ImageView) v.findViewById(R.id.iv_custom_slot_skills_image);
			sk1 = (ImageView) v.findViewById(R.id.iv_custom_slot_skills_1);
			sk2 = (ImageView) v.findViewById(R.id.iv_custom_slot_skills_2);
			sk3 = (ImageView) v.findViewById(R.id.iv_custom_slot_skills_3);
			sk4 = (ImageView) v.findViewById(R.id.iv_custom_slot_skills_4);
			skillOne = (LinearLayout) v.findViewById(R.id.ll_slot_skill_1);
			skillTwo = (LinearLayout) v.findViewById(R.id.ll_slot_skill_2);
			skillThree = (LinearLayout) v.findViewById(R.id.ll_slot_skill_3);
			skillFour = (LinearLayout) v.findViewById(R.id.ll_slot_skill_4);

		}
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder = null;

		if (v == null) {
			// Inflate the layout according to the view type
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			v = inflater.inflate(R.layout.custom_slot_skills, parent, false);
			holder = new ViewHolder(v);
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}

		holder.catName.setText(toTitleCase(list.get(position).getSportName()));
		imageLoader.DisplayImage(list.get(position).getLogo(), holder.image, ImageLoader.DEFAULT_SQUIRE);
		if (list.get(position).getSelect() == 0) {
			// image.setBackground(new
			// ColorDrawable(context.getResources().getColor(R.color.mySport_grey)));
			holder.image.setColorFilter(context.getResources().getColor(R.color.mySport_grey));
		} else {
			holder.image.setColorFilter(context.getResources().getColor(R.color.mySport_green));
		}
		if (list.get(position).getSkillLevel() != null) {
			switch (list.get(position).getSkillLevel()) {

			case "1":
				holder.sk1.setImageResource(R.drawable.skill_button);
				holder.sk2.setImageResource(R.drawable.skill_button_2);
				holder.sk3.setImageResource(R.drawable.skill_button_2);
				holder.sk4.setImageResource(R.drawable.skill_button_2);
				break;
			case "2":
				holder.sk1.setImageResource(R.drawable.skill_button);
				holder.sk2.setImageResource(R.drawable.skill_button);
				holder.sk3.setImageResource(R.drawable.skill_button_2);
				holder.sk4.setImageResource(R.drawable.skill_button_2);

				break;
			case "3":
				holder.sk1.setImageResource(R.drawable.skill_button);
				holder.sk2.setImageResource(R.drawable.skill_button);
				holder.sk3.setImageResource(R.drawable.skill_button);
				holder.sk4.setImageResource(R.drawable.skill_button_2);

				break;
			case "4":
				holder.sk1.setImageResource(R.drawable.skill_button);
				holder.sk2.setImageResource(R.drawable.skill_button);
				holder.sk3.setImageResource(R.drawable.skill_button);
				holder.sk4.setImageResource(R.drawable.skill_button);
				break;
			default:
				holder.sk1.setImageResource(R.drawable.skill_button_2);
				holder.sk2.setImageResource(R.drawable.skill_button_2);
				holder.sk3.setImageResource(R.drawable.skill_button_2);
				holder.sk4.setImageResource(R.drawable.skill_button_2);
				break;
			}
		}

		if (ClickListener) {
			// image.setImageResource(R.drawable.frouts_vegitable);
			holder.skillOne.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View arg0) {

					replaceItemAt(position, list.get(position).getSportId(), list.get(position).getSportName(),
							list.get(position).getLogo(), list.get(position).getSelect(), "1");
				}
			});
			holder.skillTwo.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View arg0) {

					replaceItemAt(position, list.get(position).getSportId(), list.get(position).getSportName(),
							list.get(position).getLogo(), list.get(position).getSelect(), "2");
				}
			});
			holder.skillThree.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View arg0) {

					replaceItemAt(position, list.get(position).getSportId(), list.get(position).getSportName(),
							list.get(position).getLogo(), list.get(position).getSelect(), "3");
				}
			});
			holder.skillFour.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View arg0) {

					replaceItemAt(position, list.get(position).getSportId(), list.get(position).getSportName(),
							list.get(position).getLogo(), list.get(position).getSelect(), "4");
				}
			});
		}

		return v;
	}

	public static String toTitleCase(String givenString) {
		String[] arr = givenString.split(" ");
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < arr.length; i++) {
			sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1)).append(" ");
		}
		return sb.toString().trim();
	}

	public void replaceItemAt(int position, String StoreId, String sportName, String logo, int select,
			String skillLevel) {
		if (ProfileMySkills.editStatus) {
			// Replace the item in the array list
			MySportsModel ob = new MySportsModel();
			ob.setSportId(StoreId);
			ob.setSportName(sportName);
			ob.setLogo(logo);
			ob.setSelect(select);
			ob.setSkillLevel(skillLevel);
			this.list.set(position, ob);
			ob.setSportList(list);

			// Let the custom adapter know it needs to refresh the view
			this.notifyDataSetChanged();
		}

	}

}