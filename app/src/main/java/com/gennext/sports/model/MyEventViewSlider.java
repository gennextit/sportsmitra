package com.gennext.sports.model;

import java.util.ArrayList;

import com.gennext.sports.R;
import com.gennext.sports.image_cache.ImageLoader;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;

public class MyEventViewSlider extends PagerAdapter {
	private final ArrayList<MyEventModel> list;
	
	//int NumberOfPages = 5;
	Context context;
	public ImageLoader imageLoader; 
	
//	int[] res = { R.drawable.bg, R.drawable.rss_main_bg,
//			R.drawable.sbuddy_profile_bg, R.drawable.sport_bg_medium,
//			R.drawable.rss_main_bg };
//	int[] backgroundcolor = { 0xFFFFFF, 0xFFFFFF, 0xFFFFFF, 0xFFFFFF, 0xFFFFFF };
//	String[] text = { "Event one", "Event two", "Event three", "Event four", "Event five" };
//	String[] location = { "Location Noida","Location Delhi","Location Gurgaon","Location Haryana","Location New Delhi",};
	
	public MyEventViewSlider(Context context,ArrayList<MyEventModel> list) {
		this.list = list;
		this.context = context;
		imageLoader = new ImageLoader(context.getApplicationContext());
	    
	}

//	public RSSFeedSliderAdapter(Context context) {
//		this.context = context;
//	}

	
	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {

		   
		
		ImageView imageView = new ImageView(context);
		//imageView.setImageResource(res[position]);
		LayoutParams imageParams = new LayoutParams(LayoutParams.MATCH_PARENT, context.getResources().getDimensionPixelSize(R.dimen.slider_height_1));
		imageView.setLayoutParams(imageParams);
		imageView.setScaleType(ImageView.ScaleType.FIT_XY); 
		imageView.setBackground(context.getResources().getDrawable(R.drawable.sport_background));

		if(list.get(position).getEventImages()!=null){
			imageLoader.DisplayImage(list.get(position).getEventImages(), imageView,"base_medium",false);
		} 
		
		LinearLayout layout = new LinearLayout(context);
		layout.setOrientation(LinearLayout.VERTICAL);
		LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		layout.setBackgroundColor(0xFFFFFF);
		layout.setLayoutParams(layoutParams);
		layout.addView(imageView);
		
//		layout.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				//Toast.makeText(context, "Page " + page + " clicked", Toast.LENGTH_LONG).show();
//			}
//		});

		container.addView(layout);
		return layout;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((LinearLayout) object);
	}

}
