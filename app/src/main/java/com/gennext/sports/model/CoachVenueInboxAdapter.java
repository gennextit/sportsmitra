package com.gennext.sports.model;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gennext.sports.R;
import com.gennext.sports.util.Coach;
import com.gennext.sports.util.Venue;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class CoachVenueInboxAdapter extends ArrayAdapter<CoachDashboardModel> {
	private final ArrayList<CoachDashboardModel> list;
	TextView tvMsg, tvDate, tvTime;
	ImageView ivProfile;
	ProgressDialog progressDialog;
	public static int COACH=1,VENUE=2;
	private Activity context;
	private final int switchData;
	public CoachVenueInboxAdapter(Activity context,int Switch, int textViewResourceId, ArrayList<CoachDashboardModel> list) {
		super(context, textViewResourceId, list);
		this.context = context;
		this.list = list;
		this.switchData=Switch;

	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public int getItemViewType(int position) {
		if (list.get(position).getStatus().equalsIgnoreCase("r")) {
			return 0;
		} else if (list.get(position).getStatus().equalsIgnoreCase("s")) {
			return 1;
		}
		return 2;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		int type = getItemViewType(position);

		if (v == null) {
			// Inflate the layout according to the view type
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			if (type == 0) {
				// Inflate the layout with image
				v = inflater.inflate(R.layout.chat_custom_slot_left, parent, false);
			} else if (type == 1) {
				v = inflater.inflate(R.layout.chat_custom_slot_right, parent, false);
			}
		}
		if (type == 0) { 
			tvMsg = (TextView) v.findViewById(R.id.tv_chat_custom_slot_text);
			tvDate = (TextView) v.findViewById(R.id.tv_chat_custom_slot_date);
			tvTime = (TextView) v.findViewById(R.id.tv_chat_custom_slot_time);
			ivProfile = (ImageView) v.findViewById(R.id.iv_chat_custom_slot_profile);

//			if (list.get(position).getName() != null) {
//				tvName.setText(list.get(position).getName());
//			}
			if (list.get(position).getMessage() != null) {
				tvMsg.setText(list.get(position).getMessage());
			}
			if (list.get(position).getDateOfMsg() != null) {
				tvDate.setText(convertFormateDate(list.get(position).getDateOfMsg(), 2, "dd-MM-yyyy"));
			}
			if (list.get(position).getTimeOfMsg() != null) {
				tvTime.setText(convertTime(list.get(position).getTimeOfMsg()));
			}
			if (list.get(position).getImageUrl() != null) {
				Glide.with(context)
						.load(list.get(position).getImageUrl())
						.placeholder(R.drawable.profile)
						.error(R.drawable.profile)
						.into( ivProfile);
			}
			
		} else if (type == 1) { 
			tvMsg = (TextView) v.findViewById(R.id.tv_chat_custom_slot_text);
			tvDate = (TextView) v.findViewById(R.id.tv_chat_custom_slot_date);
			tvTime = (TextView) v.findViewById(R.id.tv_chat_custom_slot_time);
			ivProfile = (ImageView) v.findViewById(R.id.iv_chat_custom_slot_profile);

//			if (list.get(position).getName() != null) {
//				tvName.setText(list.get(position).getName());
//			}
			if (list.get(position).getMessage() != null) {
				tvMsg.setText(list.get(position).getMessage());
			}
			if (list.get(position).getDateOfMsg() != null) {
				tvDate.setText(convertFormateDate(list.get(position).getDateOfMsg(), 2, "dd-MM-yyyy"));
			}
			if (list.get(position).getTimeOfMsg() != null) {
				tvTime.setText(convertTime(list.get(position).getTimeOfMsg()));
			}
			if(switchData==COACH){
				Coach.setCoachProfileImage(context, ivProfile);
			}else{
				Venue.setVenueProfileImage(context, ivProfile);
			}
				
		}

		return v;
	}
	
	

	public void replaceItemAt(int position) { 
		 
		this.list.remove(position); 
		// Let the custom adapter know it needs to refresh the view
		this.notifyDataSetChanged();
	}
	
	
//	public class DashboardTask extends AsyncTask<Void, Void, String> {
//
//		private Activity activity; 
//		String playerId,messageId;
//		int position;
//		public DashboardTask(Activity activity,String playerId,String messageId,int position) {
//			onAttach(activity); 
//			this.playerId=playerId;
//			this.position=position;
//			this.messageId=messageId;
//		}
//
//		public void onAttach(Activity activity) {
//			this.activity = activity;
//		}
//
//		public void onDetach() {
//			this.activity = null;
//		}
//
//		@Override
//		protected void onPreExecute() {
//			// TODO Auto-generated method stub
//			super.onPreExecute();
//			
//			//((MainActivity)activity).showProgressBar();
//			showPDialog(context, "Processing... please wait");
////			if(pBar!=null && btn!=null){
////				pBar.setVisibility(View.VISIBLE);
////				btn.setVisibility(View.GONE);
////			}
////			if(pBar!=null){
////				pBar.setVisibility(View.VISIBLE);
////			}
//		}
//
//		@Override
//		protected String doInBackground(Void... urls) {
//			String url = "",response;
//			HttpReq ob = new HttpReq();
//			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
//			if (activity != null) {
//				if(switchData==COACH){
//					url=AppSettings.deleteCoachPlayerMessage;
//					params.add(new BasicNameValuePair("coachId", Utility.LoadPref(context,Coach.CoachId)));
//					params.add(new BasicNameValuePair("playerId",playerId));
//					params.add(new BasicNameValuePair("messageId",messageId));
//					
//				}else{
//					url=AppSettings.deleteVenuePlayerMessage;
//					params.add(new BasicNameValuePair("venueId", Utility.LoadPref(context,Venue.VenueId)));
//					params.add(new BasicNameValuePair("playerId",playerId));
//					params.add(new BasicNameValuePair("messageId",messageId));
//					
//				}
//			}
//			return response=ob.makeConnection(url, HttpReq.POST, params, HttpReq.EXECUTE_TASK);
//
//		}
//
//		@Override
//		protected void onPostExecute(String result) {
//			//((MainActivity)activity).hideProgressBar();
//			//((MainActivity)activity).updateDetailInUi();
//			
//			if(activity!=null){
////				if(pBar!=null && btn!=null){
////					pBar.setVisibility(View.GONE);
////					btn.setVisibility(View.VISIBLE);
////				}
////				if(pBar!=null){
////					pBar.setVisibility(View.GONE);
////				} 
//				dismissPDialog();
//				
//				if(result!=null){
//					if(result.equals("success")){
//						replaceItemAt(position);
//					}else{
//						Toast.makeText(context, result, Toast.LENGTH_LONG).show();
//						L.m(result);
//					}
//				}else{
//					Toast.makeText(context, CompactFragment.ErrorMessage, Toast.LENGTH_LONG).show();
//					L.m(CompactFragment.ErrorMessage);
//				}
//			}
//		}
//	}
//	
//
//	public void showPDialog(Context context, String msg) {
//		progressDialog = new ProgressDialog(context);
//		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//		progressDialog.setMessage(msg);
//		progressDialog.setIndeterminate(false);
//		progressDialog.setCancelable(false);
//		progressDialog.show();
//	}
//
//	public void dismissPDialog() {
//		if (progressDialog != null)
//			progressDialog.dismiss();
//	}

//	public static String toTitleCase(String givenString) {
//		String[] arr = givenString.split(" ");
//		StringBuffer sb = new StringBuffer();
//
//		for (int i = 0; i < arr.length; i++) {
//			sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1)).append(" ");
//		}
//		return sb.toString().trim();
//	}

	// enter fix Date eg.type 1- dd-MM-yyyy ,type 2- yyyy-MM-dd
	// enter dateFormat eg. dd-MM-yyyy,yyyy-MM-dd,
	public String convertFormateDate(String Date, int type, String dateFormat) {
		String Day, middle, Month, Year;
		String finalDate = Date;
		if (type == 1) {
			Day = Date.substring(0, 2);
			middle = Date.substring(2, 3);
			Month = Date.substring(3, 5);
			Year = Date.substring(6, 10);

		} else {
			Year = Date.substring(0, 4);
			middle = Date.substring(4, 5);
			Month = Date.substring(5, 7);
			Day = Date.substring(8, 10);
		}

		switch (dateFormat) {
		case "dd-MM-yyyy":
			finalDate = Day + middle + Month + middle + Year;
			break;
		case "yyyy-MM-dd":
			finalDate = Year + middle + Month + middle + Day;
			break;
		case "MM-dd-yyyy":
			finalDate = Month + middle + Day + middle + Year;
			break;
		default:
			finalDate = "Date Format Incorrest";
		}
		return finalDate;
	}
	public String convertTime(String time){
		//String s = "12:18:00"; 
		String[] res=time.split(":");
		int hr=Integer.parseInt(res[0]);
		String min=res[1];
		
		if(hr==12){ 
			return (12+":"+min+" "+((hr>=12)?"PM":"AM"));
		}
		
//	  String  finaltime = "" ;
//
//	     SimpleDateFormat f1 = new SimpleDateFormat("kk:mm");
//	     Date d = null;
//	        try {
//	             d = f1.parse(time);
//	             SimpleDateFormat f2 = new SimpleDateFormat("hh:mm aa");
//	             finaltime = f2.format(d).toUpperCase(); // "12:18am"
//
//	    } catch (ParseException e) {
//
//	        // TODO Auto-generated catch block
//	            e.printStackTrace();
//	        }
	    
	    
	    return (hr%12+":"+min+" "+((hr>=12)?"PM":"AM"));
	}
}