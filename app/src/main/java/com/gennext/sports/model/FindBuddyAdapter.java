package com.gennext.sports.model;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gennext.sports.R;
import com.gennext.sports.fragments.sbuddy.MainFindSbuddyzView;
import com.gennext.sports.util.AppTokens;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class FindBuddyAdapter extends RecyclerView.Adapter<FindBuddyAdapter.ReyclerViewHolder> {

    private final ArrayList<FindBuddyModel> items;
    private final LayoutInflater layoutInflater;
    private final MainFindSbuddyzView parentRef;

    private Context context;

    public FindBuddyAdapter(Activity context, ArrayList<FindBuddyModel> items, MainFindSbuddyzView parentRef) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.items = items;
        this.parentRef = parentRef;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.custom_slot_find_sbuddyz, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final FindBuddyModel item = items.get(position);

        if (item.getFullName() != null) {
            // tvName.setText(toTitleCase(item.getFullName()));
            if (item.getAge() != null) {
                if (item.getGender() != null) {
                    String gender = item.getGender().substring(0, 1);
                    holder.tvName.setText(item.getFullName() + " | " + item.getAge() + " | "
                            + toTitleCase(gender));
                } else {
                    holder.tvName.setText(item.getFullName());
                }
            }
        }

        holder.tvDistance.setText(String.valueOf(item.getKm()) + " km");

        if (item.getLocation() != null) {
            holder.tvLocation.setText(item.getLocation());
        }
        int count = Integer.parseInt(item.getsLogoCount());
        holder.tvTotalCat.setVisibility(View.INVISIBLE);
        if (count > 3) {
            holder.tvTotalCat.setText(" + " + String.valueOf(count - 3) + " more");
            holder.tvTotalCat.setVisibility(View.VISIBLE);
        }
        if (item.getImageUrl() != null) {
//            Glide.with(context)
//                    .load(item.getImageUrl())
//                    .override(20, 20)
//                    .placeholder(R.drawable.profile)
//                    .error(R.drawable.profile)
//                    .into(holder.ivProfile);
            Picasso.with(context)
                    .load(item.getImageUrl()).resize(AppTokens.IMAGE_RESIZE, AppTokens.IMAGE_RESIZE).centerCrop()
                    .placeholder(R.drawable.profile)
                    .error(R.drawable.profile)
                    .into(holder.ivProfile);
        }
        if (item.getImageUrl() != null) {
            holder.ivCat1.setColorFilter(context.getResources().getColor(R.color.mySport_green));
            Glide.with(context)
                    .load(item.getsLogo1())
                    .placeholder(R.drawable.sports_bg_squire)
                    .error(R.drawable.sports_bg_squire)
                    .into(holder.ivCat1);
        }
        if (item.getsLogo2() != null) {
            Glide.with(context)
                    .load(item.getsLogo2())
                    .placeholder(R.drawable.sports_bg_squire)
                    .error(R.drawable.sports_bg_squire)
                    .into(holder.ivCat2);
            holder.ivCat2.setColorFilter(context.getResources().getColor(R.color.mySport_grey));
            holder.ivCat2.setVisibility(View.VISIBLE);
        } else {
            holder.ivCat2.setVisibility(View.GONE);
        }
        if (item.getsLogo3() != null) {
            Glide.with(context)
                    .load(item.getsLogo3())
                    .placeholder(R.drawable.sports_bg_squire)
                    .error(R.drawable.sports_bg_squire)
                    .into(holder.ivCat3);
            holder.ivCat3.setColorFilter(context.getResources().getColor(R.color.mySport_grey));
            holder.ivCat3.setVisibility(View.VISIBLE);
        } else {
            holder.ivCat3.setVisibility(View.GONE);
        }
        // if(position>previousPosition){
        //
        // AnimationUtils.animate(holder,true);
        // }else{
        //
        // AnimationUtils.animate(holder,false);
        // }
        // previousPosition=position;

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentRef.onItemSelect(item);
            }
        });
    }

    public static String toTitleCase(String givenString) {
        String[] arr = givenString.split(" ");
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < arr.length; i++) {
            sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1)).append(" ");
        }
        return sb.toString().trim();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName, tvDistance, tvTotalCat, tvLocation;
        private ImageView ivProfile, ivCat1, ivCat2, ivCat3, ivDistance;

        private ReyclerViewHolder(final View v) {
            super(v);
            tvName = (TextView) v.findViewById(R.id.tv_custom_slot_find_sbuddyz_name);
            // tvAge = (TextView) v.findViewById(R.id.
            // tv_custom_slot_find_sbuddyz_age);
            tvDistance = (TextView) v.findViewById(R.id.tv_custom_slot_find_sbuddyz_distance);
            ivDistance = (ImageView) v.findViewById(R.id.iv_custom_slot_find_sbuddyz_distance);
            tvTotalCat = (TextView) v.findViewById(R.id.tv_custom_slot_find_sbuddyz_totalSports);
            tvLocation = (TextView) v.findViewById(R.id.tv_custom_slot_find_sbuddyz_city);
            ivProfile = (ImageView) v.findViewById(R.id.iv_custom_slot_find_sbuddyz_profile);
            ivCat1 = (ImageView) v.findViewById(R.id.iv_custom_slot_find_sbuddyz_sCat1);
            ivCat2 = (ImageView) v.findViewById(R.id.iv_custom_slot_find_sbuddyz_sCat2);
            ivCat3 = (ImageView) v.findViewById(R.id.iv_custom_slot_find_sbuddyz_sCat3);
        }
    }
}

