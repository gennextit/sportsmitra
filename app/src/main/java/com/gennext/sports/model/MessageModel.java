package com.gennext.sports.model;

public class MessageModel {
	
	private String message;
	private String requestId;
	private String requestedName;
	private String statusRecived;
	
	public String getStatusRecived() {
		return statusRecived;
	}
	public void setStatusRecived(String statusRecived) {
		this.statusRecived = statusRecived;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getRequestedName() {
		return requestedName;
	}
	public void setRequestedName(String requestedName) {
		this.requestedName = requestedName;
	}
	
	
	
}
