package com.gennext.sports.model;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.sports.R;
import com.gennext.sports.fragments.event.MyCreatedEvents;
import com.gennext.sports.fragments.event.MyCreatedEventsView;
import com.gennext.sports.image_cache.ImageLoader;
import com.gennext.sports.util.Utility;

import java.util.ArrayList;

public class MyEventSlider extends PagerAdapter {
	private final ArrayList<MyEventModel> list;
	
	//int NumberOfPages = 5;
	Context context;
	public ImageLoader imageLoader; 
	FragmentManager mannager;
	public final int ADVERTISMENT=1;
	public final int EVENT=2;
	
//	int[] res = { R.drawable.bg, R.drawable.rss_main_bg,
//			R.drawable.sbuddy_profile_bg, R.drawable.sport_bg_medium,
//			R.drawable.rss_main_bg };
//	int[] backgroundcolor = { 0xFFFFFF, 0xFFFFFF, 0xFFFFFF, 0xFFFFFF, 0xFFFFFF };
//	String[] text = { "Event one", "Event two", "Event three", "Event four", "Event five" };
//	String[] location = { "Location Noida","Location Delhi","Location Gurgaon","Location Haryana","Location New Delhi",};
	
	public MyEventSlider(Context context,FragmentManager mannager,ArrayList<MyEventModel> list) {
		this.list = list;
		this.context = context;
		this.mannager=mannager;
		imageLoader = new ImageLoader(context.getApplicationContext());
	    
	}

//	public RSSFeedSliderAdapter(Context context) {
//		this.context = context;
//	}

	
	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {

		TextView textView = new TextView(context);
		textView.setTextColor(context.getResources().getColor(R.color.text));
		textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
		Utility.setTypsFace(context, textView);
		textView.setText(list.get(position).getEventName());
		//textView.setText(Utility.setBoldFont(context, R.color.text, Typeface.NORMAL, ));
		textView.setPadding(5, 0, 0, 0);  
		   
		
		ImageView imageView = new ImageView(context);
		//imageView.setImageResource(res[position]);
		LayoutParams imageParams = new LayoutParams(LayoutParams.MATCH_PARENT, context.getResources().getDimensionPixelSize(R.dimen.slider_height));
		imageView.setLayoutParams(imageParams);
		imageView.setScaleType(ImageView.ScaleType.FIT_XY); 
		imageView.setBackgroundResource(R.drawable.sport_background);

		if(list.get(position).getEventImages()!=null){
			imageLoader.DisplayImage(list.get(position).getEventImages(), imageView,"base_medium",false);
		} 
		
		LinearLayout layout = new LinearLayout(context);
		layout.setOrientation(LinearLayout.VERTICAL);
		LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		layout.setBackgroundColor(0xFFFFFF);
		layout.setLayoutParams(layoutParams);
		layout.addView(imageView);
		layout.addView(textView);
		
		final int pos = position;
		layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//Toast.makeText(context, "Page " + pos + " clicked", Toast.LENGTH_LONG).show();
				if (list != null&&list.get(pos).getSliderStatus()==EVENT) {
					MyCreatedEventsView myCreatedEventsView=new MyCreatedEventsView();
					MyCreatedEvents.SWITCH=MyCreatedEvents.SBUDDY;
					myCreatedEventsView.setEventData(list.get(pos).getEventName(), list.get(pos).getEventImages(),list.get(pos).getSportImage(),list.get(pos).getImgList());
					myCreatedEventsView.setAboutData(list.get(pos).getEventType(),list.get(pos).getStartDate(),list.get(pos).getEndDate(), list.get(pos).getStartTime(), list.get(pos).getEndTime(), list.get(pos).getMoreDetails(), list.get(pos).getAgeGroup(), list.get(pos).getRegistrationFee(), list.get(pos).getLastDateOfRegistration());
					myCreatedEventsView.setLocationData(list.get(pos).getAddress(),list.get(pos).getLocation(), "N/A");
					//myCreatedEventsView.setContactData(list.get(pos).getWebUrl(), list.get(pos).getpMobile(), list.get(pos).getpEmail());
					myCreatedEventsView.setContactData(list.get(pos).getWebUrl(), list.get(pos).getFacebookUrl(), list.get(pos).getRegistrationFee(), list.get(pos).getRegistrationLink(), list.get(pos).getpFullName(), list.get(pos).getpMobile(), list.get(pos).getpEmail(), list.get(pos).getsFullName(), list.get(pos).getsMobile(), list.get(pos).getsEmail());
					FragmentTransaction transaction = mannager.beginTransaction();
					transaction.replace(android.R.id.content, myCreatedEventsView, "myCreatedEventsView");
					transaction.addToBackStack("myCreatedEventsView");
					transaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
					transaction.commit();
				}
			}
		});

		container.addView(layout);
		return layout;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((LinearLayout) object);
	}

}
