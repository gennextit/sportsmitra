package com.gennext.sports.model;

import java.util.ArrayList;
import java.util.Random;

import com.gennext.sports.R;
import com.gennext.sports.image_cache.ImageLoader;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

public class FindCoachReviewsAdapter extends ArrayAdapter<FindCoachModel> {
	private final ArrayList<FindCoachModel> list;

	private Context context;
	public ImageLoader imageLoader;

	public FindCoachReviewsAdapter(Context context, int textViewResourceId, ArrayList<FindCoachModel> list) {
		super(context, textViewResourceId, list);
		this.context = context;
		this.list = list;
		imageLoader = new ImageLoader(context.getApplicationContext());

	}

	class ViewHolder {
		TextView tvNameTag, tvName, tvReviews;
		RatingBar rbRating;

		public ViewHolder(View v) {
			tvName = (TextView) v.findViewById(R.id.tv_custom_slot_find_coach_reviews_name);
			tvNameTag = (TextView) v.findViewById(R.id.tv_custom_slot_find_coach_reviews_NameTag);
			tvReviews = (TextView) v.findViewById(R.id.tv_custom_slot_find_coach_reviews_reviewsDetail);
			rbRating = (RatingBar) v.findViewById(R.id.rb_custom_slot_find_coach_reviews);

		}
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder=null;
		
		if (v == null) {
			// Inflate the layout according to the view type
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			v = inflater.inflate(R.layout.custom_slot_find_coach_reviews, parent, false);
			holder=new ViewHolder(v);
			v.setTag(holder);
		}else{
			holder=(ViewHolder) v.getTag();
		}
		
		if (list.get(position).getFullName() != null) {
			holder.tvName.setText(toTitleCase(list.get(position).getFullName()));
			holder.tvNameTag.setText(toTitleCase(list.get(position).getFullName().substring(0, 1)));
			setColor(holder.tvNameTag);
		}
		if (list.get(position).getReview() != null) {
			holder.tvReviews.setText(list.get(position).getReview());
		}
		if (list.get(position).getRating() != null) {
			holder.rbRating.setRating(Float.parseFloat(list.get(position).getRating()));
		} else {
			holder.rbRating.setRating(0);
		}

		return v;
	}

	public void setColor(TextView tv) {
		Random rnd = new Random();
		int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
		tv.setBackgroundColor(color);

	}

	public static String toTitleCase(String givenString) {
		String[] arr = givenString.split(" ");
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < arr.length; i++) {
			sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1)).append(" ");
		}
		return sb.toString().trim();
	}

}