package com.gennext.sports.model;

public class RSSFeedSbuddyModel {
	
	private String titel; 
	private String creator; 
	private String imgSrc;
	private String blog;
	private StringBuilder description;
	private String descriptionString;
	private String link;
	private String pubDate;
	
	public RSSFeedSbuddyModel(String titel,String link,String pubDate,String descriptionString,String imgSrc) {
		this.titel=titel;
		this.link=link;
		this.pubDate=pubDate;
		this.descriptionString=descriptionString;
		this.imgSrc=imgSrc;
	}
	
	public String getDescriptionString() {
		return descriptionString;
	}
	public void setDescriptionString(String descriptionString) {
		this.descriptionString = descriptionString;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getPubDate() {
		return pubDate;
	}
	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}
	public StringBuilder getDescription() {
		return description;
	}
	public void setDescription(StringBuilder sbDesc) {
		this.description = sbDesc;
	}
	public String getBlog() {
		return blog;
	}
	public void setBlog(String blog) {
		this.blog = blog;
	}
	public String getTitel() {
		return titel;
	}
	public void setTitel(String titel) {
		this.titel = titel;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getImgSrc() {
		return imgSrc;
	}
	public void setImgSrc(String imgSrc) {
		this.imgSrc = imgSrc;
	} 
	
	

}
