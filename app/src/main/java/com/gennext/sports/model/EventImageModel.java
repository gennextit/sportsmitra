package com.gennext.sports.model;

import android.graphics.Bitmap;
import android.net.Uri;

public class EventImageModel {
	
	private Bitmap imageBitmap;
	private Uri imagePath;
	
	
	public Bitmap getImageBitmap() {
		return imageBitmap;
	}
	public void setImageBitmap(Bitmap imageBitmap) {
		this.imageBitmap = imageBitmap;
	}
	public Uri getImagePath() {
		return imagePath;
	}
	public void setImagePath(Uri imagePath) {
		this.imagePath = imagePath;
	} 

}
