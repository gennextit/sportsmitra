package com.gennext.sports.model;
 

import java.util.ArrayList;

import com.gennext.sports.R;
import com.gennext.sports.image_cache.ImageLoader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

public class VenuGalleryAdapter extends ArrayAdapter<FindVenueModel> {
	private final ArrayList<FindVenueModel> list; 
	
	private Context context;
	public ImageLoader imageLoader; 
	
	public VenuGalleryAdapter(Context context, int textViewResourceId, ArrayList<FindVenueModel> list) {
		super(context, textViewResourceId, list);
		this.context = context;
		this.list = list;
		imageLoader = new ImageLoader(context.getApplicationContext());
	       
	}

class ViewHolder{
	ImageView ivImage;
	
		public ViewHolder(View v) {
			ivImage=(ImageView)v.findViewById(R.id.iv_custom_find_venu_gallery);
		}
	}
	
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder=null;
		
		if (v == null) {
			// Inflate the layout according to the view type
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			v = inflater.inflate(R.layout.custom_find_venu_gallery, parent, false);
			holder=new ViewHolder(v);
			v.setTag(holder);
		}else{
			holder=(ViewHolder) v.getTag();
		} 
		 
		
//		if(list.get(position).getFullName()!=null){
//			tvName.setText(toTitleCase(list.get(position).getFullName())); 
//		}
		
		if(list.get(position).getImageUrl()!=null){
			imageLoader.DisplayImage(list.get(position).getImageUrl(), holder.ivImage,"base_medium",false);
		}
		
		
		return v;
	}

	public static String toTitleCase(String givenString) {
	    String[] arr = givenString.split(" ");
	    StringBuffer sb = new StringBuffer();

	    for (int i = 0; i < arr.length; i++) {
	        sb.append(Character.toUpperCase(arr[i].charAt(0)))
	            .append(arr[i].substring(1)).append(" ");
	    }          
	    return sb.toString().trim();
	}
	
	
}