package com.gennext.sports.model;

/**
 * Created by Admin on 2/9/2018.
 */
 

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gennext.sports.R;
import com.gennext.sports.fragments.MainRSSFeed;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class RSSFeedAdvanceAdapter extends RecyclerView.Adapter<RSSFeedAdvanceAdapter.ReyclerViewHolder> {

    private LayoutInflater layoutInflater;
    private MainRSSFeed parentRef;
    private Context context;
    private ArrayList<RSSFeedModel> items;

    public RSSFeedAdvanceAdapter(Activity context, ArrayList<RSSFeedModel> items, MainRSSFeed parentRef) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef = parentRef;
        this.items = items;
    }

    @Override
    public ReyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = layoutInflater.inflate(R.layout.custom_rss_feed_slot, parent, false);

        return new ReyclerViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final ReyclerViewHolder holder, final int position) {
        final RSSFeedModel item = items.get(position);

        if(item.getCreator()!=null){
            holder.tvCreatedBy.setText(item.getCreator());
        }
        if(item.getTitel()!=null){
            StringBuilder res=item.getTitel();
//			res=res.substring(0,25);
//			res=res+"...";
            holder.tvTitle.setText(res);
        }
        if(item.getBlog()!=null){
            holder.tvBlog.setText(item.getBlog());
        }else{
            holder.tvBlog.setText("BLOG");
        }
        if(item.getPubDate()!=null){
            String pubDate=item.getPubDate();

            holder.tvPubDate.setText(pubDate);
        }

        if(item.getImgSrc()!=null){
            Glide.with(context)
                    .load(item.getImgSrc())
                    .placeholder(R.drawable.sport_bg_small)
                    .error(R.drawable.sport_bg_small)
                    .into(holder.ivProfile);
//			imageLoader.DisplayImage(item.getImgSrc(), holder.ivProfile,"base_small");
        }else{
            holder.ivProfile.setImageResource(R.drawable.sport_bg_medium);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (parentRef != null) {
                    parentRef.onSelectedItem(item);
                }

            }
        });
    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    class ReyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView tvBlog,tvTitle,tvCreatedBy,tvPubDate;
        private ImageView ivProfile;

        private ReyclerViewHolder(final View v) {
            super(v);
            tvBlog = (TextView) v.findViewById(R.id. tv_custom_rss_feed_slot_blog);
            tvTitle = (TextView) v.findViewById(R.id. tv_custom_rss_feed_slot_title);
            tvCreatedBy = (TextView) v.findViewById(R.id. tv_custom_rss_feed_slot_createdBy);
            tvPubDate = (TextView) v.findViewById(R.id. tv_custom_rss_feed_slot_blogDandT);
            ivProfile = (ImageView) v.findViewById(R.id. iv_custom_rss_feed_slot_img);

        }
    }
}

