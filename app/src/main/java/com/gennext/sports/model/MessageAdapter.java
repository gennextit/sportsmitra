package com.gennext.sports.model;
 

import java.util.ArrayList;

import com.gennext.sports.R;
import com.gennext.sports.image_cache.ImageLoader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MessageAdapter extends ArrayAdapter<MessageModel> {
	private final ArrayList<MessageModel> list;
	
	private Context context;
	public ImageLoader imageLoader; 
	
	public MessageAdapter(Context context, int textViewResourceId, ArrayList<MessageModel> list) {
		super(context, textViewResourceId, list);
		this.context = context;
		this.list = list;
		imageLoader = new ImageLoader(context.getApplicationContext());
	       
	}

	class ViewHolder{
	TextView tvName,tvMsg; 
	
		public ViewHolder(View v) {
			tvName = (TextView) v.findViewById(R.id.tv_custom_message_name);
			tvMsg = (TextView) v.findViewById(R.id.tv_custom_message_msg);
		}
	}
	
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder=null;
		
		if (v == null) {
			// Inflate the layout according to the view type
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			v = inflater.inflate(R.layout.custom_message_slot, parent, false);
			holder=new ViewHolder(v);
			v.setTag(holder);
		}else{
			holder=(ViewHolder) v.getTag();
		}
		
		if(list.get(position).getRequestedName()!=null){
			holder.tvName.setText(list.get(position).getRequestedName()); 
		}
		if(list.get(position).getMessage()!=null){
			holder.tvMsg.setText(list.get(position).getMessage()); 
		}
		 
		
		return v;
	}

	 
	
	
}