package com.gennext.sports.model;

public class NotificationModel {
	
	private String notification;
	private String requestId;
	private String requestedName;
	private String requestedStatue;
	
	
	public String getRequestedStatue() {
		return requestedStatue;
	}
	public void setRequestedStatue(String requestedStatue) {
		this.requestedStatue = requestedStatue;
	}
	public String getRequestedName() {
		return requestedName;
	}
	public void setRequestedName(String requestedName) {
		this.requestedName = requestedName;
	}
	public String getNotification() {
		return notification;
	}
	public void setNotification(String notification) {
		this.notification = notification;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	

}
