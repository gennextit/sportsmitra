package com.gennext.sports.model;

public class CountryCodeModel {
	
	private String phoneCode;
	private String countryCode;
	private String both;
	
	
	public String getPhoneCode() {
		return phoneCode;
	}
	public void setPhoneCode(String phoneCode) {
		this.phoneCode = phoneCode;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getBoth() {
		return both;
	}
	public void setBoth(String both) {
		this.both = both;
	}
	

}
