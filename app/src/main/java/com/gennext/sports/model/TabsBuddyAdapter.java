package com.gennext.sports.model;
 
 
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class TabsBuddyAdapter extends FragmentPagerAdapter {
	String about, location, clubNApartment, city;
	String JsonData, playerId;
	
	public TabsBuddyAdapter(FragmentManager fm) {
		super(fm);
	}

	public void setData(String about, String location, String clubNApartment, String city) {
		this.about = about;
		this.location = location;
		this.clubNApartment = clubNApartment;
		this.city = city;
	}

	public void setJSONData(String JSONData,String playerId) {
		this.JsonData = JSONData;
		this.playerId=playerId;
	}

	
	@Override
	public Fragment getItem(int index) {

//		switch (index) {
//		case 0:
//			MainFindSbuddyDetailProfile mainFindSbuddyDetailProfile = new MainFindSbuddyDetailProfile();
//			mainFindSbuddyDetailProfile.setData(about, location, clubNApartment, city);
//			return mainFindSbuddyDetailProfile;
//		case 1:
//			MainFindSbuddyDetailSports mainFindSbuddyDetailSports = new MainFindSbuddyDetailSports();
//			mainFindSbuddyDetailSports.setData(JsonData);
//			mainFindSbuddyDetailSports.setPlayerId(playerId);
//			return mainFindSbuddyDetailSports;
//		case 2:
//			MainFindSbuddyDetailPosts mainFindSbuddyDetailPosts = new MainFindSbuddyDetailPosts();
//			return mainFindSbuddyDetailPosts;
//		}

		return null;
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 3;
	}

}

