package com.gennext.sports.model;

import java.util.Comparator;

public class CoachDashboardModel {

	private String playerId;
	private String fullName;
	private String gender;
	private String age;
	private String imageUrl;
	private String mobileNumber;
	private String emailId;
	private String lastAccess;
	private String lastAccessDateTime;

	public String getLastAccessDateTime() {
		return lastAccessDateTime;
	}

	public void setLastAccessDateTime(String lastAccessDateTime) {
		this.lastAccessDateTime = lastAccessDateTime;
	}

	private String message;
	private String messageId;
	private String dateOfMsg;
	private String timeOfMsg;
	private String status;
	private int counter;

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public static Comparator<CoachDashboardModel> COMPARE_BY_COUNTER = new Comparator<CoachDashboardModel>() {
		public int compare(CoachDashboardModel one, CoachDashboardModel other) {
			return Integer.valueOf(other.getCounter()).compareTo(one.getCounter());
		}
	};
	public static Comparator<CoachDashboardModel> COMPARE_BY_Date = new Comparator<CoachDashboardModel>() {
		public int compare(CoachDashboardModel first, CoachDashboardModel second)  {
	        return second.getLastAccess().compareTo(first.getLastAccess());
	    }
	};

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

	public String getLastAccess() {
		return lastAccess;
	}

	public void setLastAccess(String lastAccess) {
		this.lastAccess = lastAccess;
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDateOfMsg() {
		return dateOfMsg;
	}

	public void setDateOfMsg(String dateOfMsg) {
		this.dateOfMsg = dateOfMsg;
	}

	public String getTimeOfMsg() {
		return timeOfMsg;
	}

	public void setTimeOfMsg(String timeOfMsg) {
		this.timeOfMsg = timeOfMsg;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
