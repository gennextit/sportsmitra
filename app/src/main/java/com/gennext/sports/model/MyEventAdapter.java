package com.gennext.sports.model;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gennext.sports.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class MyEventAdapter extends ArrayAdapter<MyEventModel> {
	private final ArrayList<MyEventModel> list;
	
	private Context context;
	
	public MyEventAdapter(Context context, int textViewResourceId, ArrayList<MyEventModel> list) {
		super(context, textViewResourceId, list);
		this.context = context;
		this.list = list;
	       
	}

class ViewHolder{
	TextView tvEventName,tvLocation,tvDate; 
	ImageView ivEventImage,ivSportImage;

		public ViewHolder(View v) {
			tvEventName = (TextView) v.findViewById(R.id.tv_custom_slot_my_events_name);
			tvLocation = (TextView) v.findViewById(R.id.tv_custom_slot_my_events_location);
			tvDate = (TextView) v.findViewById(R.id.tv_custom_slot_my_events_created_date);
			ivEventImage = (ImageView) v.findViewById(R.id.iv_custom_slot_my_events_base);
			ivSportImage = (ImageView) v.findViewById(R.id.iv_custom_slot_my_events_sltSport);
		}
	}
	
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder=null;
		
		if (v == null) {
			// Inflate the layout according to the view type
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			v = inflater.inflate(R.layout.custom_slot_my_events, parent, false);
			holder=new ViewHolder(v);
			v.setTag(holder);
		}else{
			holder=(ViewHolder) v.getTag();
		}
		
		
		
		if(list.get(position).getSportImage()!=null){
			Glide.with(context)
					.load(list.get(position).getSportImage())
					.placeholder(R.drawable.profile)
					.error(R.drawable.profile)
					.into( holder.ivSportImage);
		} 
		if(list.get(position).getEventImages()!=null){
			Glide.with(context)
					.load(list.get(position).getEventImages())
					.placeholder(R.drawable.sport_bg_medium)
					.error(R.drawable.sport_bg_medium)
					.into( holder.ivEventImage);
		} 
		
		if(list.get(position).getEventName()!=null){
			holder.tvEventName.setText(list.get(position).getEventName()); 
		} 
		
		if(list.get(position).getLocation()!=null){
			holder.tvLocation.setText(list.get(position).getLocation()); 
		} 
		if(list.get(position).getStartDate()!=null){
			holder.tvDate.setText(list.get(position).getStartDate()+", "+list.get(position).getStartTime()+", Onwards"); 
		} 
		 
		
		
		return v;
	}

	 
	
	
}