package com.gennext.sports.model;

import java.util.ArrayList;

import com.gennext.sports.R;
import com.gennext.sports.fragments.profile.ProfileMySports;
import com.bumptech.glide.Glide;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MySportsAdapter extends ArrayAdapter<MySportsModel> {
	private final ArrayList<MySportsModel> list;

	private final Activity context;

	public MySportsAdapter(Activity context, int textViewResourceId, ArrayList<MySportsModel> list) {
		super(context, textViewResourceId, list);
		this.context = context;
		this.list = list;

	}

	class ViewHolder {
		TextView catName;
		ImageView image;
		LinearLayout lvMain;

		public ViewHolder(View v) {
			catName = (TextView) v.findViewById(R.id.tv_custom_slot_sports_name);
			image = (ImageView) v.findViewById(R.id.iv_custom_slot_sports_image);
			lvMain = (LinearLayout) v.findViewById(R.id.ll_custom_slot_sports);

		}
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder=null;
		
		if (v == null) {
			// Inflate the layout according to the view type
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.custom_slot_sports, parent, false);
			holder=new ViewHolder(v);
			v.setTag(holder);
		}else{
			holder=(ViewHolder) v.getTag();
		}

		holder.catName.setText(toTitleCase(list.get(position).getSportName()));
		Glide.with(context)
				.load(list.get(position).getLogo())
				.placeholder(R.drawable.sports_bg_squire)
				.error(R.drawable.sports_bg_squire)
				.into( holder.image);
		if (list.get(position).getSelect() == 0) {
			// image.setBackground(new
			// ColorDrawable(context.getResources().getColor(R.color.mySport_grey)));
			holder.image.setColorFilter(context.getResources().getColor(R.color.mySport_grey));
		} else {
			holder.image.setColorFilter(context.getResources().getColor(R.color.mySport_green));
		}
		// image.setImageResource(R.drawable.frouts_vegitable);
		holder.lvMain.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(ProfileMySports.editStatus){
					if (list.get(position).getSelect() == 0) {
						replaceItemAt(position, list.get(position).getSportId(), list.get(position).getSportName(),
								list.get(position).getLogo(), 1);
					} else {
						replaceItemAt(position, list.get(position).getSportId(), list.get(position).getSportName(),
								list.get(position).getLogo(), 0);
					}
				}
			}
		});
		return v;
	}

	public static String toTitleCase(String givenString) {
		String[] arr = givenString.split(" ");
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < arr.length; i++) {
			sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1)).append(" ");
		}
		return sb.toString().trim();
	}

	public void replaceItemAt(int position, String StoreId, String sportName, String logo, int select) {
		// Replace the item in the array list
		MySportsModel ob = new MySportsModel();
		ob.setSportId(StoreId);
		ob.setSportName(sportName);
		ob.setLogo(logo);
		ob.setSelect(select);
		ob.setSkillLevel("0");
		this.list.set(position, ob);
		ob.setSkillList(list);
		// Let the custom adapter know it needs to refresh the view
		this.notifyDataSetChanged();
	}

}