package com.gennext.sports.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MySportToJSON {


	
	public static String toJSonArray() {
	      try {
	    	// In this case we need a json array to hold the java list
	          JSONArray jsonArr = new JSONArray();
			  //JSONObject jsonOBJ = new JSONObject();
	          MySportsModel sport=new MySportsModel();
	          if(sport.getSkillList()!=null){
	        	  for (MySportsModel model : sport.getSkillList() ) {
		              JSONObject pnObj = new JSONObject();
					  pnObj.put("sportId", model.getSportId());
					  pnObj.put("skillLevel", model.getSkillLevel());
					   
					  jsonArr.put(pnObj);
					  //jsonOBJ.put(pnObj);
		          }
	          }else{
	        	  return null;
	          }
			  return jsonArr.toString();

	
	    }
	    catch(JSONException ex) {
	        ex.printStackTrace();
	    }
	
	    return null;
	
	}
	
//	public class JsonUtil {
//
//		public static String toJSon(Person person) {
//		      try {
//		        // Here we convert Java Object to JSON 
//		        JSONObject jsonObj = new JSONObject();
//		        jsonObj.put("name", person.getName()); // Set the first name/pair 
//		        jsonObj.put("surname", person.getSurname());
//
//		        JSONObject jsonAdd = new JSONObject(); // we need another object to store the address
//		        jsonAdd.put("address", person.getAddress().getAddress());
//		        jsonAdd.put("city", person.getAddress().getCity());
//		        jsonAdd.put("state", person.getAddress().getState());
//
//		        // We add the object to the main object
//		        jsonObj.put("address", jsonAdd);
//
//		        // and finally we add the phone number
//		        // In this case we need a json array to hold the java list
//		        JSONArray jsonArr = new JSONArray();
//
//		        for (PhoneNumber pn : person.getPhoneList() ) {
//		            JSONObject pnObj = new JSONObject();
//		            pnObj.put("num", pn.getNumber());
//		            pnObj.put("type", pn.getType());
//		            jsonArr.put(pnObj);
//		        }
//
//		        jsonObj.put("phoneNumber", jsonArr);
//
//		        return jsonObj.toString();
//
//		    }
//		    catch(JSONException ex) {
//		        ex.printStackTrace();
//		    }
//
//		    return null;
//
//		}
//	public static String toJSon(JSONModel person) {
//    try {
//      // Here we convert Java Object to JSON 
//      JSONObject jsonObj = new JSONObject();
//      jsonObj.put("name", person.getName()); // Set the first name/pair 
//      jsonObj.put("surname", person.getSurName());
//      jsonObj.put("address", person.getAddress());
//      
//      JSONArray jsonArr = new JSONArray();
//      jsonArr.put(jsonObj);
//
//      
//      return jsonArr.toString();
//
//  }
//  catch(JSONException ex) {
//      ex.printStackTrace();
//  }
//
//  return null;
//
//}
}