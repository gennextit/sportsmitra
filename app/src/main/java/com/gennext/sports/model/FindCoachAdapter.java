package com.gennext.sports.model;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gennext.sports.R;
import com.gennext.sports.fragments.MyFavCoaches;
import com.gennext.sports.fragments.coaches.MainFindCoachesView;
import com.gennext.sports.util.AppTokens;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class FindCoachAdapter extends RecyclerView.Adapter<FindCoachAdapter.MyViewHolder>{

    private MainFindCoachesView parentRef;
    private MyFavCoaches parentRef2;
    private LayoutInflater inflater;
    private Context context;
    private final ArrayList<FindCoachModel> list;


    public FindCoachAdapter(Context context, ArrayList<FindCoachModel> list, MyFavCoaches parentRef2) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef2 = parentRef2;
        this.list = list;
    }
    public FindCoachAdapter(Context context, ArrayList<FindCoachModel> list, MainFindCoachesView parentRef) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.parentRef = parentRef;
        this.list = list;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.custom_slot_find_coaches, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final FindCoachModel item = list.get(position);
        if(item.getFullName()!=null){
            if(item.getAge()!=null){
                if(item.getGender()!=null){
                    String gender=item.getGender().substring(0,1);
                    holder.tvName.setText(item.getFullName()+" | "+item.getAge()+" | "+toTitleCase(gender));
                }else{
                    holder.tvName.setText(item.getFullName());
                }
            }
        }

        if(item.getKm()!=null){
            holder.tvDistance.setText(String.valueOf(item.getKm())+" km");
        }else{
            holder.tvDistance.setVisibility(View.INVISIBLE);
            holder.ivDistance.setVisibility(View.INVISIBLE);
        }
        if(item.getCity()!=null){
            holder.tvCity.setText(item.getCity());
        }
        if(item.getRating()!=null){
            if(!item.getRating().equals("false")){
                holder.ratingBar.setRating(Float.parseFloat(item.getRating()));
            }
        }else{
            holder.ratingBar.setRating(1);
        }
        if(item.getImageUrl()!=null){

            Picasso.with(context)
                    .load(item.getImageUrl()).resize(AppTokens.IMAGE_RESIZE, AppTokens.IMAGE_RESIZE).centerCrop()
                    .placeholder(R.drawable.profile)
                    .error(R.drawable.profile)
                    .into(holder.ivProfile);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(parentRef!=null) {
                    parentRef.onItemSelect(item);
                }if(parentRef2!=null) {
                    parentRef2.onItemSelect(item);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvName,tvDistance,tvCity;
        ImageView ivProfile,ivDistance;
        RatingBar ratingBar;

        public MyViewHolder(View v) {
            super(v);
            tvName = (TextView) v.findViewById(R.id. tv_custom_slot_find_coaches_name);
            //tvAge = (TextView) v.findViewById(R.id. tv_custom_slot_find_coaches_age);
            tvDistance = (TextView) v.findViewById(R.id. tv_custom_slot_find_coaches_distance);
            ivDistance= (ImageView) v.findViewById(R.id. iv_custom_slot_find_coaches_distance);
            tvCity = (TextView) v.findViewById(R.id. tv_custom_slot_find_coaches_city);
            ivProfile = (ImageView) v.findViewById(R.id. iv_custom_slot_find_coaches_profile);
            ratingBar= (RatingBar) v.findViewById(R.id. ratingBar1);

        }
    }

    public static String toTitleCase(String givenString) {
        String[] arr = givenString.split(" ");
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < arr.length; i++) {
            sb.append(Character.toUpperCase(arr[i].charAt(0)))
                    .append(arr[i].substring(1)).append(" ");
        }
        return sb.toString().trim();
    }

}