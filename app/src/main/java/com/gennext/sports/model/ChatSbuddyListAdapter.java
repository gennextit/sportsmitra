package com.gennext.sports.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.gennext.sports.R;
import com.bumptech.glide.Glide;
import com.gennext.sports.util.AppTokens;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ChatSbuddyListAdapter extends ArrayAdapter<FindBuddyModel> {
	private final ArrayList<FindBuddyModel> list;
	// LinearLayout lvMain;
	// ImageView online;

	private Context context;

	public ChatSbuddyListAdapter(Context context, int textViewResourceId, ArrayList<FindBuddyModel> list) {
		super(context, textViewResourceId, list);
		this.context = context;
		this.list = list;

	}

	class ViewHolder{
		TextView tvName, tvDistance, tvCity,tvMsgCounter;
		ImageView ivProfile, ivDistance;
		RatingBar ratingBar;
		
		public ViewHolder(View v) {
			tvName = (TextView) v.findViewById(R.id.tv_chat_custom_slot_buddylist_name); 
			tvMsgCounter = (TextView) v.findViewById(R.id.chat_message_counter); 
			tvCity = (TextView) v.findViewById(R.id.tv_chat_custom_slot_buddylist_city);
			ivProfile = (ImageView) v.findViewById(R.id.iv_chat_custom_slot_buddylist_profile); 

		}
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView; 
		ViewHolder holder=null;
		if (v == null) {
			// Inflate the layout according to the view type
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			v = inflater.inflate(R.layout.chat_custom_slot_buddylist, parent, false);
			holder=new ViewHolder(v);
			v.setTag(holder);
		}else{
			holder=(ViewHolder) v.getTag();
		}
		
		if (list.get(position).getFullName() != null&& list.get(position).getAge()!=null && list.get(position).getGender()!=null) {
			holder.tvName.setText(toTitleCase(list.get(position).getFullName())+" | "+list.get(position).getAge()+" | "+list.get(position).getGender());
		}
		if (list.get(position).getCounter() != 0) {
			holder.tvMsgCounter.setText(String.valueOf(list.get(position).getCounter()));
		}else{
			holder.tvMsgCounter.setVisibility(View.GONE);
		}
		  
		if (list.get(position).getLocation() != null) {
			holder.tvCity.setText(list.get(position).getLocation());
		}
		if (list.get(position).getImageUrl() != null) {
			Picasso.with(context)
					.load(list.get(position).getImageUrl())
					.resize(AppTokens.IMAGE_RESIZE,AppTokens.IMAGE_RESIZE)
					.placeholder(R.drawable.profile)
					.error(R.drawable.profile)
					.into( holder.ivProfile);
		}

		return v;
	}

	public static String toTitleCase(String givenString) {
		String[] arr = givenString.split(" ");
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < arr.length; i++) {
			sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1)).append(" ");
		}
		return sb.toString().trim();
	}

}